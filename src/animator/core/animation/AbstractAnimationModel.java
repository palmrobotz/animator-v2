
package animator.core.animation;

import animator.core.animation.event.AnimationModelChangedEvent;
import animator.core.shape.AnimatorShapeBase;
import java.util.Collections;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.ReadOnlyLongWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author nuntipat
 */
public abstract class AbstractAnimationModel/* implements Comparable<AnimationModel>*/
{
    protected AnimatorShapeBase shape;    // Change to our own shape implementation
    protected LayerModel layer;
    //private long startTime = 0;
    //private long endTime = 0;
    protected final ReadOnlyLongWrapper startTime = new ReadOnlyLongWrapper();
    protected final ReadOnlyLongWrapper endTime = new ReadOnlyLongWrapper();
    protected final ReadOnlyLongWrapper duration = new ReadOnlyLongWrapper();   // We might leak duration by cast the ReadOnlyProperty to Simple..
    
    // List of KeyTime in this animation
    private final ObservableList<KeyTime> keyTimeList = FXCollections.observableArrayList();
    // Cache the instance of the unmodifiable list so we can return the same instance
    private final ObservableList<KeyTime> unmodifiableKeyTimeList = FXCollections.unmodifiableObservableList(keyTimeList);
    
    /**
     * Construct a new animation region with the given start time and end time.
     * 
     * The first {@link KeyTime} will be create automatically at the start time.
     * 
     * @param startTime start time of this animation in millisecond
     * @param endTime end time of this animation in millisecond
     */
    AbstractAnimationModel(LayerModel layer, AnimatorShapeBase shape, long startTime, long endTime)
    {
        if (shape == null)
            throw new NullPointerException("Shape is null.");

        if (startTime < 0 || endTime <= startTime)
            throw new IllegalArgumentException("Invalid time range.");

        this.layer = layer;
        this.shape = shape;
        shape.setAnimationModel(this);
        this.startTime.set(startTime);
        this.endTime.bind(this.startTime.add(this.duration));
        this.duration.set(endTime - startTime);
        

        keyTimeList.add(KeyTime.valueOf(this, startTime, shape));
    }

    /**
     * Returns an immutable list of KeyValue instances 
     * @return 
     */
    public final ObservableList<KeyTime> getKeyTimeUnmodifiable()
    {
        return unmodifiableKeyTimeList;
    }
    
    /**
     * Add new KeyTime to this animation instance.
     * 
     * The keyTime time should be between this animation startTime and endTime.
     * 
     * If the keyTime at this time is already exist, the keyTime will be merge by
     * given higher priority to the new keyTime
     * 
     * @param keyTime the keyTime to add to this animation
     * @return true if keyTime is successfully added
     * 
     * @throws NullPointerException if the keyTime is null
     * @throws IllegalArgumentException if the keyTime time is invalid
     */
    public final boolean addKeyTime(KeyTime keyTime)
    {
        if (keyTime == null)
            throw new NullPointerException("KeyTime is null.");

        if (keyTime.getTime() < startTime.get() || keyTime.getTime() > endTime.get())
            throw new IllegalArgumentException("KeyTime time is beyong the animation time.");

        // Check for duplication and find the index to insert new keyframe
        int p = Collections.binarySearch(keyTimeList, keyTime);
        // We found the keyTime with the same time
        if (p >= 0)
        {
            KeyTime oldKt = keyTimeList.get(p);
            // For each property in the new keytime, replace the value of new keytime
            // with the old one
            for (ShapeProperty ap : keyTime.getPropertyChange())
            {
                oldKt.addProperty(keyTime.getProperty(ap));
            }
            return true;
        }
        // No KeyTime exist thus we can add
        else
        {
            keyTimeList.add(-p-1, keyTime);
            getLayerModel().getTimelineModel().fireAnimationModelChangedEvent(
                    new AnimationModelChangedEvent(this, AnimationModelChangedEvent.Changed.ADD_KEYTIME, keyTime));
            return true;
        }
    }

    public final KeyTime removeKeyTime(long time)
    {
        int p = Collections.binarySearch(keyTimeList, KeyTime.getInstance(this, time));
        if (p >= 0)
        {
            KeyTime kt = keyTimeList.remove(p);
            return kt;
        }
        else
        {
            return null;
        }
    }

    public final KeyTime removeKeyTime(KeyTime keyTime)
    {
        return keyTimeList.remove(keyTime) ? keyTime : null;
    }

    /**
     * Use this one instead of set start time
     * @param fromTime
     * @param toTime
     * @return 
     */
    public final boolean moveKeyTime(long fromTime, long toTime)
    {
        int p = Collections.binarySearch(keyTimeList, KeyTime.getInstance(this, toTime));
        if (p >= 0)
        {
            return false;
        }
        // We know that no keytime exist at toTime ms
        else
        {
            p = Collections.binarySearch(keyTimeList, KeyTime.getInstance(this, fromTime));
            if (p >= 0)
            {
                // Remove the keytime
                KeyTime kt = keyTimeList.remove(p);
                kt.setTime(toTime);
                // Add the keytime back to the list
                return addKeyTime(kt);
            }
            else
            {
                return false; // No keytime exist at from time
            }
        }
    }

    public final boolean isKeyTimeExist(long time)
    {
        int p = Collections.binarySearch(keyTimeList, KeyTime.getInstance(this, time));
        return p >= 0;
    }

    public final long getStartTime()
    {
        return startTime.get();
    }
    
    public final ReadOnlyLongProperty startTimeProperty()
    {
        return startTime.getReadOnlyProperty();
    }
    
    public final long getEndTime()
    {
        return endTime.get();
    }
    
    public final ReadOnlyLongProperty endTimeProperty()
    {
        return endTime.getReadOnlyProperty();
    }
    
    public final long getDuration()
    {
        return duration.get();
    }
    
    /** Implement by calling {@code setEndTime(getStartTime() + duration) } */
    public final boolean setDuration(long duration)
    {
        return setEndTime(startTime.get() + duration);
    }
    
    public final ReadOnlyLongProperty durationProperty()
    {
        return duration.getReadOnlyProperty();
    }
    
    public abstract boolean moveToTime(long time);

    /**
     * Now get absolute time
     * @param time 
     */
    public abstract boolean setEndTime(long time);

    public final LayerModel getLayerModel()
    {
        return layer;
    }

    public final AnimatorShapeBase getShape()
    {
        return shape;
    }
    
//    public boolean changeLayer(LayerModel newLayer)
//    {
//        throw new UnsupportedOperationException();
////        // Check if the new layer free at this time
////        if (newLayer.isAnimationExist(startTime.get(), endTime.get()))
////        {
////            System.err.println("New layer not free");
////            return false;
////        }
////        else
////        {
////            // Remove this animation from current layer
////            layer.removeAnimation(this);
////
////            // Add this animation to the new layer
////            newLayer.addAnimation(this);
////            
////            return true;
////        }
//    }

    /**
     * 
     * @param millis
     * @return 
     */
    public final boolean isContainTime(long millis)
    {
        return (millis >= startTime.get()) && (millis < endTime.get()); 
    }

    /**
     * 
     * @param millis
     * @return 
     */
    public final int isInRange(long millis)
    {
        if (millis < startTime.get())
            return -1;
        else if (millis >= endTime.get())
            return 1;
        else
            return 0;
    }

    /*@Override
    public int hashCode()
    {
        int result = 17;
        result = 31 * result + shape.hashCode();
        result = 31 * result + (int) (startTime ^ (startTime >>> 32));
        result = 31 * result + (int) (endTime ^ (endTime >>> 32));
        result = 31 * result + keyTimeList.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!(obj instanceof AnimationModel))
            return false;

        AnimationModel animation = (AnimationModel) obj;
        return shape.equals(animation.shape) && (startTime == animation.startTime) && (endTime == animation.endTime )
                && keyTimeList.equals(animation.keyTimeList);
    }*/

//    /**
//     * Note: this class has a natural ordering that is inconsistent with equals.
//     * @param o
//     * @return 
//     */
//    @Override
//    public int compareTo(AnimationModel o)
//    {
//        if (startTime < o.startTime)
//            return -1;
//        else if (startTime == o.startTime)
//            return 0;
//        else
//            return 1;
//    }
    
    //        /**
//         * Comparator use for compare the element by the start time
//         * Note: this comparator has a natural ordering that is inconsistent with equals.
//         */
//        public static final Comparator<AnimationData> STARTTIME_COMPARATOR = new Comparator<AnimationData>()
//        {
//            @Override
//            public int compare(AnimationData o1, AnimationData o2)
//            {
//                if (o1.startTime < o2.startTime)
//                    return -1;
//                else if (o1.startTime > o2.startTime)
//                    return 1;
//                else
//                    return 0;
//            }
//        };
//        
//        /**
//         * Comparator use for compare the element by the end time
//         * Note: this comparator has a natural ordering that is inconsistent with equals.
//         */
//        public static final Comparator<AnimationData> ENDTIME_COMPARATOR = new Comparator<AnimationData>()
//        {
//            @Override
//            public int compare(AnimationData o1, AnimationData o2)
//            {
//                if (o1.endTime < o2.endTime)
//                    return -1;
//                else if (o1.endTime > o2.endTime)
//                    return 1;
//                else
//                    return 0;
//            }
//        };
}
