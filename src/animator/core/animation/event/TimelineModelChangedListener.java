
package animator.core.animation.event;

/**
 *
 * @author nuntipat
 */
public interface TimelineModelChangedListener
{
    public void onTimelineModelChanged(TimelineModelChangedEvent e);
}
