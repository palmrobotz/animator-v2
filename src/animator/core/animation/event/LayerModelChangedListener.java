
package animator.core.animation.event;

/**
 *
 * @author nuntipat
 */
public interface LayerModelChangedListener
{
    public void onLayerModelChanged(LayerModelChangedEvent e);
}
