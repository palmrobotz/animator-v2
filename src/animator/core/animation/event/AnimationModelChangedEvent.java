
package animator.core.animation.event;

import animator.core.animation.AbstractAnimationModel;
import animator.core.animation.KeyTime;
import java.util.EventObject;

/**
 *
 * @author nuntipat
 */
public final class AnimationModelChangedEvent extends EventObject
{
    public static enum Changed { STARTTIME, ENDTIME, ADD_KEYTIME, REMOVE_KEYTIME, MOVE_KEYTIME };
    
    private final Changed change;
    private final KeyTime keyTime;
    
    public AnimationModelChangedEvent(AbstractAnimationModel source, Changed change)
    {
        super(source);
        this.change = change;
        this.keyTime = null;
    }
    
    public AnimationModelChangedEvent(AbstractAnimationModel source, Changed change, KeyTime keyTime)
    {
        super(source);
        this.change = change;
        this.keyTime = keyTime;
    }

    @Override
    public AbstractAnimationModel getSource()
    {
        return (AbstractAnimationModel) super.getSource();
    }
    
    public Changed getChange()
    {
        return change;
    }

    public KeyTime getKeyTime()
    {
        return keyTime;
    }

}
