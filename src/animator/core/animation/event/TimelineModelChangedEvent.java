
package animator.core.animation.event;

import animator.core.animation.LayerModel;
import animator.core.animation.TimelineModel;
import java.util.Arrays;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;

/**
 *
 * @author nuntipat
 */
public class TimelineModelChangedEvent extends EventObject
{
    public static enum Changed { ADD_LAYER, REMOVE_LAYER, MOVE_LAYER };
    
    private final Changed change;
    private final List<LayerModel> layerModel;
    
    public TimelineModelChangedEvent(TimelineModel source, Changed change, LayerModel... layerModel)
    {
        super(source);
        this.change = change;
        this.layerModel = Collections.unmodifiableList(Arrays.asList(layerModel));
    }

    @Override
    public TimelineModel getSource()
    {
        return (TimelineModel) super.getSource();
    }

    public Changed getChange()
    {
        return change;
    }

    public List<LayerModel> getModel()
    {
        return layerModel;
    }
    
}
