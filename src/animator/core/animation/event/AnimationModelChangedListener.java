
package animator.core.animation.event;

/**
 *
 * @author nuntipat
 */
public interface AnimationModelChangedListener
{
    public void onAnimationModelChanged(AnimationModelChangedEvent e);
}
