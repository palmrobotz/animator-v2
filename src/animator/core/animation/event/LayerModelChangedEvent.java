
package animator.core.animation.event;

import animator.core.animation.AnimationModel;
import animator.core.animation.LayerModel;
import java.util.EventObject;

/**
 *
 * @author nuntipat
 */
public class LayerModelChangedEvent extends EventObject
{
    public static enum Changed { ADD_ANIMATION, REMOVE_ANIMATION, MOVE_ANIMATION, UPDATE_ANIMATION };
    
    private final AnimationModel animationModel;
    private final Changed change;
    
    public LayerModelChangedEvent(LayerModel source, AnimationModel animationModel, Changed change)
    {
        super(source);
        this.animationModel = animationModel;
        this.change = change;
    }

    @Override
    public LayerModel getSource()
    {
        return (LayerModel) super.getSource();
    }

    public Changed getChange()
    {
        return change;
    }

    public AnimationModel getAnimationModel()
    {
        return animationModel;
    }
    
}
