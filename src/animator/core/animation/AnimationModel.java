
package animator.core.animation;

import animator.core.animation.datastructor.ObservableNavigableMap;
import animator.core.animation.event.LayerModelChangedEvent;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.GroupShape;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Nuntipat narkthong
 */
public class AnimationModel extends AbstractAnimationModel
{
    private final GroupShape gs;
    
    private final ObservableList<SubAnimationModel> childModel = FXCollections.observableArrayList();
    private final ObservableList<SubAnimationModel> unmodifiableChildModel = 
            FXCollections.unmodifiableObservableList(childModel);
    
    AnimationModel(LayerModel layer, long startTime, long endTime)
    {
        super(layer, new GroupShape(), startTime, endTime);
        
        gs = (GroupShape) getShape();
    }
    
    
    AnimationModel(LayerModel layer, GroupShape gs, long startTime, long endTime)
    {
        super(layer, gs, startTime, endTime);
        
        this.gs = gs;
    }
            
    public ObservableList<SubAnimationModel> getChildModelModifiable()
    {
        return childModel;
    }
    
    public SubAnimationModel addChildShape(AnimatorShapeBase asb, long startTime)
    {
        if (!isContainTime(startTime))
            throw new AssertionError("Invalid start time");
        SubAnimationModel sa = new SubAnimationModel(this, asb, startTime, getEndTime());
        gs.addShape(asb);
        childModel.add(sa);
        
        // Notify the layer that the block has been updated
        // So the EPW will be force redraw. This is from the fix that we need to
        // jump to the same time to make the change visible immediatly
        getLayerModel().getTimelineModel().fireLayerModelChangedEvent(
                new LayerModelChangedEvent(getLayerModel(), this, 
                    LayerModelChangedEvent.Changed.UPDATE_ANIMATION));
        
        return sa;
    }
    
    public ObservableList<SubAnimationModel> getChildModelUnmodifiable()
    {
        return unmodifiableChildModel;
    }
    
    @Override
    public boolean moveToTime(long time)
    {
        // We need a mutable instance, so we can modified our key
        ObservableNavigableMap<Long, AnimationModel> layerData = layer.getData();
        
        // Get the previous and next AnimationModel
        Map.Entry<Long, AnimationModel> prevEntry = layerData.lowerEntry(time);
        Map.Entry<Long, AnimationModel> nextEntry = layerData.higherEntry(time);
        
        // Check whether the new time overlap previous or next AnimationModel
        if (prevEntry != null && prevEntry.getValue() != this && time < prevEntry.getValue().getEndTime())
            return false;
        if (nextEntry != null && nextEntry.getValue() != this && time + getDuration() >= nextEntry.getKey())
            return false;
        
        AnimationModel tmp = layerData.remove(getStartTime());
        if (tmp != this)
            throw new IllegalStateException("This AnimationModel is not in the layer NavigableMap");
        
        updateStartTime(time);
        
        layerData.put(time, tmp);
        
        layer.updateDuration();
        return true;
    }
    
    private void updateStartTime(long time)
    {
        // We will use this to shift the SubAnimationModel start time
        long delta = time - getStartTime();
        
        startTime.set(time);
        
        // Update every child model by shift its start time by time ms
        for (SubAnimationModel sam : childModel)
        {
            if (!sam.moveToTime(delta))
                throw new IllegalStateException("Child model not changed with parent (moveToTime())");
        }
    }
    
    @Override
    public boolean setEndTime(long time)
    {
        ObservableNavigableMap<Long, AnimationModel> layerData = layer.getDataUnmodifiable();
        // Find the endtime of the next animation model in the layer
        Map.Entry<Long, AnimationModel> nextEntry = layerData.ceilingEntry(startTime.get()+1);
        if (nextEntry == null)
        {
            if (time > startTime.get())
            {
                duration.set(time - startTime.get());
                updateChildEndTime(time);
                layer.updateDuration();
                return true;
            }
            else
            {
                System.err.println("New end time is invalid");
                return false;
            }
        }
        else
        {
            if (time <= startTime.get())
            {
                System.err.println("New end time is invalid");
                return false;
            }
            
            if (time <= nextEntry.getValue().getStartTime())
            {
                duration.set(time - startTime.get());
                updateChildEndTime(time);
                layer.updateDuration();
                return true;
            }
            else
            {
                System.err.println("New end time overlap the next animation model in this layer");
                return false;
            }
        }
    }
    
    // Only update child if the new endtime is less than the old endtime
    // Since parent might be longer than the child but child can't longer than the parents
    private void updateChildEndTime(long time)
    {
        // Update every child model
        for (SubAnimationModel sam : childModel)
        {
            if (time < sam.getEndTime())
            {
                if (!sam.setEndTime(time))
                    throw new RuntimeException("Child model not changed with parent (setEndTime())");
            }
        }
    }
    
    public boolean changeToLayerModel(LayerModel lm, long startTime)
    {
        // Check if the destination is free
        if (lm.isAnimationExist(startTime, startTime + getDuration()))
        {
            System.err.println("changeToLayerModel() : Animation already exist at destination");
            return false;
        }
        
        // Remove from old layer
        layer.removeAnimation(this);
        
        // Add to new layer at new start time
        updateStartTime(startTime);
        layer = lm;
        layer.addAnimation(this);
        
        layer.updateDuration();
        return true;
    }
}
