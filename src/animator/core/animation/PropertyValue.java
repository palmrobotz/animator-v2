
package animator.core.animation;

import javafx.animation.Interpolator;
import javafx.beans.value.WritableValue;

/**
 *
 * @author Nuntipat narkthong
 */
public class PropertyValue<T>
{
    private ShapeProperty property;
    private WritableValue<T> valueProperty;
    private T value;
    private Interpolator interpolator;

    PropertyValue(ShapeProperty property, WritableValue<T> valueProperty, T value, Interpolator interpolator)
    {
        this.property = property;
        this.valueProperty = valueProperty;
        this.value = value;
        this.interpolator = interpolator;
    }
    
    public ShapeProperty getProperty()
    {
        return property;
    }
    
    public WritableValue<T> getWritableValue()
    {
        return valueProperty;
    }

    public T getValue()
    {
        return value;
    }
    
    public Interpolator getInterpolator()
    {
        return interpolator;
    }
    
    public void interpolate(PropertyValue<T> ap, double percent)
    {
        // @TODO Check wheather we can interpolate
        if (property != ap.getProperty())
            throw new AssertionError("Animate PropertyValue of the different type");
        
        @SuppressWarnings("unchecked")
        T result = (T) ap.interpolator.interpolate(value, ap.getValue(), percent);        
        valueProperty.setValue(result);
    }
    
}
