
package animator.core.animation;

import animator.core.shape.AnimatorShapeBase;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.Set;
import javafx.animation.Interpolator;

/**
 * Class use to encapsulate the change at this time in the animation.
 * @author Nuntipat narkthong
 */
public final class KeyTime implements Comparable<KeyTime>
{
    private AbstractAnimationModel model;
    // Time in ms of this keyTime (relative to model start time)
    private long time;
    // EnumMap use to store the property value
    private Map<ShapeProperty, PropertyValue<?>> propertyChange = new EnumMap<>(ShapeProperty.class);
    private Map<ShapeProperty, PropertyValue<?>> unmodifiablePropertyChange = Collections.unmodifiableMap(propertyChange);
    
    /**
     * Create new KeyTime instance at the given time.
     * @param time time in millisecond
     */
    public KeyTime(AbstractAnimationModel model, long time)
    {
        this.model = model;
        this.time = time - model.getStartTime();
    }
    
    /**
     * Constructor for use on in this class to create compareDummy
     * @param time 
     */
//    private KeyTime(long time)
//    {
//        this.model = null;
//        this.time = time;
//    }

    /**
     * Create the KeyTime instance for the state of the given Shape.
     * @param s the shape to create a KeyTime for
     * @return the new KeyTime instance
     */
    public static KeyTime valueOf(AbstractAnimationModel model, long time, AnimatorShapeBase s)
    {
        KeyTime kt = new KeyTime(model, time);
        for (ShapeProperty ap : ShapeProperty.getAnimableProperty())
        {
            if (s.getSupportProperty().contains(ap))
                kt.addProperty(ap.createPropertyValue(s, Interpolator.LINEAR));
        }
        return kt;
    }
    
    //private static final KeyTime compareDummy = new KeyTime(0);
    /**
     * The instance return by this method use its time member variable as ABSOLUTE time
     * thus it is not conform to this class rule
     * @param time
     * @return 
     */
    static KeyTime getInstance(AbstractAnimationModel model, long time)
    {
        KeyTime kt = new KeyTime(model, time);
        kt.setTime(time);
        return kt;
    }
    
    /**
     * Get the time of this keytime (absolute time from the beginning of the timeline).
     * @return time of this keytime
     */
    public long getTime()
    {
        return time + model.getStartTime();
    }
    
    public long getRelativeTime()
    {
        return time;
    }
    
    /**
     * Set the time of this keytime (absolute time from the beginning of the timeline)
     * @param time 
     */
    public void setTime(long time)
    {
        this.time = time - model.getStartTime();
    }
    
    /**
     * Add new change to this keyTime.
     * The old change of the same type ({@link AnimateProperty}) will be replaced.
     * @param property the value of the change
     */
    public void addProperty(PropertyValue<?> property)
    {
        propertyChange.put(property.getProperty(), property);
    }
    
    // @TODO Fix raw type here
    /**
     * 
     * @param property
     * @return 
     */
    public PropertyValue getProperty(ShapeProperty property)
    {
        return propertyChange.get(property);
    }
    
    public void removeProperty(ShapeProperty property)
    {
        propertyChange.remove(property);
    }
    
    public Set<ShapeProperty> getPropertyChange()
    {
        return propertyChange.keySet();
    }

    public boolean isPropertyChange(ShapeProperty property)
    {
        return propertyChange.containsKey(property);
    }
    
    @Override
    public int hashCode()
    {
        int result = 17;
        result = 31 * result + (int) (time ^ (time >>> 32));
        result = 31 * result + propertyChange.hashCode();
        return result;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
            return true;
        if (!(obj instanceof KeyTime))
            return false;
        
        KeyTime kt = (KeyTime) obj;
        return (time == kt.time) && propertyChange.equals(kt.propertyChange);
    }
    
    /**
     * Note: this class has a natural ordering that is inconsistent with equals.
     * @param kt
     * @return 
     */
    @Override
    public int compareTo(KeyTime kt)
    {
        if (time < kt.time)
            return -1;
        else if (time == kt.time)
            return 0;
        else
            return 1;
    }

    public Map<ShapeProperty, PropertyValue<?>> getMapData()
    {
        return unmodifiablePropertyChange;
    }
}
