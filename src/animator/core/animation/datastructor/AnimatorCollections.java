
package animator.core.animation.datastructor;

import java.util.NavigableMap;
import java.util.NavigableSet;

/**
 *
 * @author nuntipat
 */
public class AnimatorCollections
{
    
    public static <K, V> ObservableNavigableMap<K, V> observableNavigableMap(NavigableMap<K, V> map)
    {
        return new ObservableNavigableMapImplement<>(map);
    }
    
    public static <K, V> NavigableMap<K, V> unmodifiableNavigableMap(NavigableMap<K, V> map)
    {
        return new UnmodifiableNavigableMap<>(map);
    }
            
    public static <E> NavigableSet<E> unmodifiableNavigableSet(NavigableSet<E> set)
    {
        return new UnmodifiableNavigableSet<>(set);
    }
    
    public static <K, V> ObservableNavigableMap<K, V> unmodifiableObservableNavigableMap(ObservableNavigableMap<K, V> map)
    {
        return new UnmodifiableObservableNavigableMap<>(map);
    }
    
    
}
