
package animator.core.animation.datastructor;

import java.util.NavigableMap;
import javafx.collections.ObservableMap;

/**
 *
 * @author nuntipat
 */
public interface ObservableNavigableMap<K, V> extends ObservableMap<K, V>, NavigableMap<K, V>
{
}
