
package animator.core.animation.datastructor;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;

/**
 *
 * @author nuntipat
 */
public class UnmodifiableObservableNavigableMap<K, V> extends UnmodifiableNavigableMap<K, V> implements ObservableNavigableMap<K, V>
{
    private ObservableMap<K, V> observableMap;
    
    public UnmodifiableObservableNavigableMap(ObservableNavigableMap<K, V> map)
    {
        super(map);
        
        if (map == null)
            throw new NullPointerException("Wrap null NavigableMap!!!");
        
        observableMap = map;
    }

    @Override
    public void addListener(MapChangeListener<? super K, ? super V> ml)
    {
        observableMap.addListener(ml);
    }

    @Override
    public void removeListener(MapChangeListener<? super K, ? super V> ml)
    {
        observableMap.removeListener(ml);
    }

    @Override
    public int size()
    {
        return observableMap.size();
    }

    @Override
    public boolean isEmpty()
    {
        return observableMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return observableMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return observableMap.containsValue(value);
    }

    @Override
    public V get(Object key)
    {
        return observableMap.get(key);
    }

    @Override
    public V put(K key, V value)
    {
        throw new UnsupportedOperationException("This map is unmodifiable.");
    }

    @Override
    public V remove(Object key)
    {
        throw new UnsupportedOperationException("This map is unmodifiable.");
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m)
    {
        throw new UnsupportedOperationException("This map is unmodifiable.");
    }

    @Override
    public void clear()
    {
        throw new UnsupportedOperationException("This map is unmodifiable.");
    }

    // Try this
    @Override
    public Set<K> keySet()
    {
        return Collections.unmodifiableSet(observableMap.keySet());
    }

    @Override
    public void addListener(InvalidationListener il)
    {
        observableMap.addListener(il);
    }

    @Override
    public void removeListener(InvalidationListener il)
    {
        observableMap.removeListener(il);
    }

    // Try this
    @Override
    public Collection<V> values()
    {
        return Collections.unmodifiableCollection(observableMap.values());
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet()
    {
        return Collections.unmodifiableSet(observableMap.entrySet());
    }

}
