
package animator.core.animation.datastructor;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;

/**
 *
 * @author nuntipat
 */ 
class UnmodifiableNavigableSet<E> implements NavigableSet<E>
{
    private NavigableSet<E> set;
    
    public UnmodifiableNavigableSet(NavigableSet<E> set)
    {
        if (set == null)
            throw new NullPointerException("Wrap a null NavigableSet!!!");
        
        this.set = set;
    }
    
    @Override
    public E lower(E e)
    {
        return set.lower(e);
    }

    @Override
    public E floor(E e)
    {
        return set.floor(e);
    }

    @Override
    public E ceiling(E e)
    {
        return set.ceiling(e);
    }

    @Override
    public E higher(E e)
    {
        return set.higher(e);
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public E pollFirst()
    {
        throw new UnsupportedOperationException("This collection is unmodifiedable."); 
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public E pollLast()
    {
        throw new UnsupportedOperationException("This collection is unmodifiedable."); 
    }

    @Override
    public Iterator<E> iterator()
    {
        return new ReadOnlyIterator<>(set.iterator());
    }

    @Override
    public NavigableSet<E> descendingSet()
    {
        return new UnmodifiableNavigableSet<>(set.descendingSet());
    }

    @Override
    public Iterator<E> descendingIterator()
    {
        return new ReadOnlyIterator<>(set.descendingIterator());
    }

    @Override
    public NavigableSet<E> subSet(E fromElement, boolean fromInclusive, E toElement, boolean toInclusive)
    {
        return new UnmodifiableNavigableSet<>(set.subSet(fromElement, fromInclusive, toElement, toInclusive));
    }

    @Override
    public NavigableSet<E> headSet(E toElement, boolean inclusive)
    {
        return new UnmodifiableNavigableSet<>(set.headSet(toElement, inclusive));
    }

    @Override
    public NavigableSet<E> tailSet(E fromElement, boolean inclusive)
    {
        return new UnmodifiableNavigableSet<>(set.tailSet(fromElement, inclusive));
    }

    @Override
    public SortedSet<E> subSet(E fromElement, E toElement)
    {
        return Collections.unmodifiableSortedSet(set.subSet(fromElement, toElement));
    }

    @Override
    public SortedSet<E> headSet(E toElement)
    {
        return Collections.unmodifiableSortedSet(set.headSet(toElement));
    }

    @Override
    public SortedSet<E> tailSet(E fromElement)
    {
        return Collections.unmodifiableSortedSet(set.tailSet(fromElement));
    }

    @Override
    public Comparator<? super E> comparator()
    {
        return set.comparator();
    }

    @Override
    public E first()
    {
        return set.first();
    }

    @Override
    public E last()
    {
        return set.last();
    }

    @Override
    public int size()
    {
        return set.size();
    }

    @Override
    public boolean isEmpty()
    {
        return set.isEmpty();
    }

    @Override
    public boolean contains(Object o)
    {
        return set.contains(o);
    }

    @Override
    public Object[] toArray()
    {
        return set.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a)
    {
        return set.toArray(a);
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public boolean add(E e)
    {
        throw new UnsupportedOperationException("This collection is unmodifiedable."); 
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public boolean remove(Object o)
    {
        throw new UnsupportedOperationException("This collection is unmodifiedable."); 
    }

    @Override
    public boolean containsAll(Collection<?> c)
    {
        return set.containsAll(c);
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public boolean addAll(Collection<? extends E> c)
    {
        throw new UnsupportedOperationException("This collection is unmodifiedable."); 
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public boolean retainAll(Collection<?> c)
    {
        throw new UnsupportedOperationException("This collection is unmodifiedable."); 
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public boolean removeAll(Collection<?> c)
    {
        throw new UnsupportedOperationException("This collection is unmodifiedable."); 
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public void clear()
    {
        throw new UnsupportedOperationException("This collection is unmodifiedable."); 
    }
}
