
package animator.core.animation.datastructor;

import java.util.Map;

/**
 *
 * @author nuntipat
 */
class UnmodifiableEntry<K, V> implements Map.Entry<K, V>
{
    private Map.Entry<K, V> entry;

    private UnmodifiableEntry(Map.Entry<K, V> entry)
    {
        this.entry = entry;
    }

    // Use static factory so we can return null thus be able to use with function
    // that may return null Entry
    public static <K, V> UnmodifiableEntry<K, V> of(Map.Entry<K, V> entry)
    {
        if (entry == null)
            return null;
        else
            return new UnmodifiableEntry<>(entry);
    }

    @Override
    public K getKey()
    {
        return entry.getKey();
    }

    @Override
    public V getValue()
    {
        return entry.getValue();
    }

    @Override
    public V setValue(V value)
    {
        throw new UnsupportedOperationException("This entry is unmodifiable.");
    }
}
