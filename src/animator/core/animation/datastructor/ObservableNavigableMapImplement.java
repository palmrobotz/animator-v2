
package animator.core.animation.datastructor;

import java.util.Collection;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;

/**
 *
 * @author nuntipat
 */
class ObservableNavigableMapImplement<K, V> extends UnmodifiableNavigableMap<K, V> implements ObservableNavigableMap<K, V>
{
    private ObservableMap<K, V> observableMap;
    
    public ObservableNavigableMapImplement(NavigableMap<K, V> map)
    {
        super(map);
        
        if (map == null)
            throw new NullPointerException("Wrap null NavigableMap!!!");
        
        observableMap = FXCollections.observableMap(map);
    }

    @Override
    public void addListener(MapChangeListener<? super K, ? super V> ml)
    {
        observableMap.addListener(ml);
    }

    @Override
    public void removeListener(MapChangeListener<? super K, ? super V> ml)
    {
        observableMap.removeListener(ml);
    }

    @Override
    public int size()
    {
        return observableMap.size();
    }

    @Override
    public boolean isEmpty()
    {
        return observableMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return observableMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return observableMap.containsValue(value);
    }

    @Override
    public V get(Object key)
    {
        return observableMap.get(key);
    }

    @Override
    public V put(K key, V value)
    {
        return observableMap.put(key, value);
    }

    @Override
    public V remove(Object key)
    {
        return observableMap.remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m)
    {
        observableMap.putAll(m);
    }

    @Override
    public void clear()
    {
        observableMap.clear();
    }

    // Try this
    @Override
    public Set<K> keySet()
    {
        return observableMap.keySet();
    }

    @Override
    public void addListener(InvalidationListener il)
    {
        observableMap.addListener(il);
    }

    @Override
    public void removeListener(InvalidationListener il)
    {
        observableMap.removeListener(il);
    }

    // Try this
    @Override
    public Collection<V> values()
    {
        return observableMap.values();
    }

    // try this
    @Override
    public Set<Map.Entry<K, V>> entrySet()
    {
        return observableMap.entrySet();
    }

}
