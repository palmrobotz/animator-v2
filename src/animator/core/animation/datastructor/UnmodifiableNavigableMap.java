
package animator.core.animation.datastructor;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;

/**
 *
 * @author nuntipat
 */
class UnmodifiableNavigableMap<K, V> implements NavigableMap<K, V>
{
    private final NavigableMap<K, V> map;

    public UnmodifiableNavigableMap(NavigableMap<K, V> map)
    {
        if (map == null)
            throw new NullPointerException("Wrap a null NavigableMap!!!");
        
        this.map = map;
    }
    
    @Override
    public Set<K> keySet()
    {
        return Collections.unmodifiableSet(map.keySet());
    }

    @Override
    public Collection<V> values()
    {
        return Collections.unmodifiableCollection(map.values());
    }

    @Override
    public int size()
    {
        return map.size();
    }

    @Override
    public boolean isEmpty()
    {
        return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return map.containsValue(value);
    }

    @Override
    public V get(Object key)
    {
        return map.get(key);
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public V put(K key, V value)
    {
        throw new UnsupportedOperationException("This navigate map is unmodifiable."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public V remove(Object key)
    {
        throw new UnsupportedOperationException("This navigate map is unmodifiable."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public void putAll(Map<? extends K, ? extends V> m)
    {
        throw new UnsupportedOperationException("This navigate map is unmodifiable."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public void clear()
    {
        throw new UnsupportedOperationException("This navigate map is unmodifiable."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet()
    {
        return Collections.unmodifiableSet(map.entrySet());
    }

    @Override
    public Entry<K, V> lowerEntry(K key)
    {
        return UnmodifiableEntry.of(map.lowerEntry(key));
    }

    @Override
    public K lowerKey(K key)
    {
        return map.lowerKey(key);
    }

    @Override
    public Entry<K, V> floorEntry(K key)
    {
        return UnmodifiableEntry.of(map.floorEntry(key));
    }

    @Override
    public K floorKey(K key)
    {
        return map.floorKey(key);
    }

    @Override
    public Entry<K, V> ceilingEntry(K key)
    {
        return UnmodifiableEntry.of(map.ceilingEntry(key));
    }

    @Override
    public K ceilingKey(K key)
    {
        return map.ceilingKey(key);
    }

    @Override
    public Entry<K, V> higherEntry(K key)
    {
        return UnmodifiableEntry.of(map.higherEntry(key));
    }

    @Override
    public K higherKey(K key)
    {
        return map.higherKey(key);
    }

    @Override
    public Entry<K, V> firstEntry()
    {
        return UnmodifiableEntry.of(map.firstEntry());
    }

    @Override
    public Entry<K, V> lastEntry()
    {
        return UnmodifiableEntry.of(map.lastEntry());
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public Entry<K, V> pollFirstEntry()
    {
        throw new UnsupportedOperationException("This navigate map is unmodifiable.");
    }

    /**
     * Not support by this implementation 
     */
    @Override
    public Entry<K, V> pollLastEntry()
    {
        throw new UnsupportedOperationException("This navigate map is unmodifiable.");
    }

    @Override
    public NavigableMap<K, V> descendingMap()
    {
        return new UnmodifiableNavigableMap<>(map.descendingMap());
    }

    @Override
    public NavigableSet<K> navigableKeySet()
    {
        return new UnmodifiableNavigableSet<>(map.navigableKeySet());
    }

    @Override
    public NavigableSet<K> descendingKeySet()
    {
        return new UnmodifiableNavigableSet<>(map.descendingKeySet());
    }

    @Override
    public NavigableMap<K, V> subMap(K fromKey, boolean fromInclusive, K toKey, boolean toInclusive)
    {
        return new UnmodifiableNavigableMap<>(map.subMap(fromKey, fromInclusive, toKey, toInclusive));
    }

    @Override
    public NavigableMap<K, V> headMap(K toKey, boolean inclusive)
    {
        return new UnmodifiableNavigableMap<>(map.headMap(toKey, inclusive));
    }

    @Override
    public NavigableMap<K, V> tailMap(K fromKey, boolean inclusive)
    {
        return new UnmodifiableNavigableMap<>(map.tailMap(fromKey, inclusive));
    }

    @Override
    public SortedMap<K, V> subMap(K fromKey, K toKey)
    {
        return Collections.unmodifiableSortedMap(map.subMap(fromKey, toKey));
    }

    @Override
    public SortedMap<K, V> headMap(K toKey)
    {
        return Collections.unmodifiableSortedMap(map.headMap(toKey));
    }

    @Override
    public SortedMap<K, V> tailMap(K fromKey)
    {
        return Collections.unmodifiableSortedMap(map.tailMap(fromKey));
    }

    @Override
    public Comparator<? super K> comparator()
    {
        return map.comparator();
    }

    @Override
    public K firstKey()
    {
        return map.firstKey();
    }

    @Override
    public K lastKey()
    {
        return map.lastKey();
    }
}
