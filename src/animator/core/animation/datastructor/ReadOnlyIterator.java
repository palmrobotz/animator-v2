
package animator.core.animation.datastructor;

import java.util.Iterator;

/**
 *
 * @author nuntipat
 */
class ReadOnlyIterator<E> implements Iterator<E>
{
    private Iterator<E> iterator;
    
    public ReadOnlyIterator(Iterator<E> iterator)
    {
        if (iterator == null)
            throw new NullPointerException("Wrap null iterator!!!");
        
        this.iterator = iterator;
    }
    
    @Override
    public boolean hasNext()
    {
        return iterator.hasNext();
    }

    @Override
    public E next()
    {
        return iterator.next();
    }

    @Override
    public void remove()
    {
        throw new UnsupportedOperationException("This iterator is read only.");
    }
    
}
