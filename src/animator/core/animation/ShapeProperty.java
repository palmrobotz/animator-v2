
package animator.core.animation;

import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.PointData;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javafx.animation.Interpolator;
import javafx.beans.value.WritableValue;
import javafx.scene.paint.Paint;


/**
 *
 * @author Nuntipat narkthong
 */
public enum ShapeProperty
{
    SELECTION
    {
        @Override
        public PropertyValue<?> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            throw new UnsupportedOperationException("This property is not animable");
        }
        
        @Override
        public PropertyValue<?> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            throw new UnsupportedOperationException("This property is not animable");
        }
        
        @Override
        public WritableValue<?> getWritableValue(AnimatorShapeBase shape)
        {
            throw new UnsupportedOperationException("This property is not animable");
        }
    },
    X_POSITION
    {
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) X_POSITION.getWritableValue(s);
            return new PropertyValue<>(X_POSITION, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) X_POSITION.getWritableValue(s);
            if (!(value instanceof Number))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(X_POSITION, wv, (Number) value, interpolator);
        }
        
        @Override
        public WritableValue<Number> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.xProperty();
        }
    },
    Y_POSITION
    {
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) Y_POSITION.getWritableValue(s);
            return new PropertyValue<>(Y_POSITION, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) Y_POSITION.getWritableValue(s);
            if (!(value instanceof Number))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(Y_POSITION, wv, (Number) value, interpolator);
        }
        
        @Override
        public WritableValue<Number> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.yProperty();
        }
    },
    FILL_COLOR
    {
        @Override
        public PropertyValue<Paint> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Paint> wv = (WritableValue<Paint>) FILL_COLOR.getWritableValue(s);
            return new PropertyValue<>(FILL_COLOR, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Paint> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Paint> wv = (WritableValue<Paint>) FILL_COLOR.getWritableValue(s);
            if (!(value instanceof Paint))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(FILL_COLOR, wv, (Paint) value, interpolator);
        }
        
        @Override
        public WritableValue<Paint> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.fillProperty();
        }
    },
    STROKE_COLOR
    {
        @Override
        public PropertyValue<Paint> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Paint> wv = (WritableValue<Paint>) STROKE_COLOR.getWritableValue(s);
            return new PropertyValue<>(STROKE_COLOR, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Paint> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Paint> wv = (WritableValue<Paint>) STROKE_COLOR.getWritableValue(s);
            if (!(value instanceof Paint))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(STROKE_COLOR, wv, (Paint) value, interpolator);
        }
        
        @Override
        public WritableValue<Paint> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.strokeProperty();
        }
    },
    STROKE_WIDTH
    {
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) STROKE_WIDTH.getWritableValue(s);
            return new PropertyValue<>(STROKE_WIDTH, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) STROKE_WIDTH.getWritableValue(s);
            if (!(value instanceof Number))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(STROKE_WIDTH, wv, (Number) value, interpolator);
        }
        
        @Override
        public WritableValue<Number> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.strokeWidthProperty();
        }
    },
    ROTATION
    {
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) ROTATION.getWritableValue(s);
            return new PropertyValue<>(ROTATION, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) ROTATION.getWritableValue(s);
            if (!(value instanceof Number))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(ROTATION, wv, (Number) value, interpolator);
        }
        
        @Override
        public WritableValue<Number> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.rotationProperty();
        }
    },
    ALPHA
    {
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) ALPHA.getWritableValue(s);
            return new PropertyValue<>(ALPHA, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) ALPHA.getWritableValue(s);
            if (!(value instanceof Number))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(ALPHA, wv, (Number) value, interpolator);
        }
        
        @Override
        public WritableValue<Number> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.alphaProperty();
        }
    },
    OFFSET_X_POSITION
    {
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) OFFSET_X_POSITION.getWritableValue(s);
            return new PropertyValue<>(OFFSET_X_POSITION, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) OFFSET_X_POSITION.getWritableValue(s);
            if (!(value instanceof Number))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(OFFSET_X_POSITION, wv, (Number) value, interpolator);
        }
        
        @Override
        public WritableValue<Number> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.offsetXProperty();
        }
    },
    OFFSET_Y_POSITION
    {
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) OFFSET_Y_POSITION.getWritableValue(s);
            return new PropertyValue<>(OFFSET_Y_POSITION, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) OFFSET_Y_POSITION.getWritableValue(s);
            if (!(value instanceof Number))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(OFFSET_Y_POSITION, wv, (Number) value, interpolator);
        }
        
        @Override
        public WritableValue<Number> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.offsetYProperty();
        }
    },
    OFFSET_ROTATION
    {
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) OFFSET_ROTATION.getWritableValue(s);
            return new PropertyValue<>(OFFSET_ROTATION, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) OFFSET_ROTATION.getWritableValue(s);
            if (!(value instanceof Number))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(OFFSET_ROTATION, wv, (Number) value, interpolator);
        }
        
        @Override
        public WritableValue<Number> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.offsetRotationProperty();
        }
    },
    OFFSET_ALPHA
    {
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) OFFSET_ALPHA.getWritableValue(s);
            return new PropertyValue<>(OFFSET_ALPHA, wv, wv.getValue(), interpolator);
        }
        
        @Override
        public PropertyValue<Number> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            @SuppressWarnings("unchecked")
            WritableValue<Number> wv = (WritableValue<Number>) OFFSET_ALPHA.getWritableValue(s);
            if (!(value instanceof Number))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(OFFSET_ALPHA, wv, (Number) value, interpolator);
        }
        
        @Override
        public WritableValue<Number> getWritableValue(AnimatorShapeBase shape)
        {
            return shape.offsetAlphaProperty();
        }
    },
    POINT_DATA
    {
        @Override
        public PropertyValue<PointData> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator)
        {
            return new PropertyValue<>(POINT_DATA, PointData.DUMMY_POINT_DATA_PROPERTY, new PointData(s.getPointDataUnmodifiable()), interpolator);
        }
        
        @Override
        public PropertyValue<PointData> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator)
        {
            if (!(value instanceof PointData))
                throw new AssertionError("Value is not correct type");
            
            return new PropertyValue<>(POINT_DATA, PointData.DUMMY_POINT_DATA_PROPERTY, (PointData) value, interpolator);
        }
        
        @Override
        public WritableValue<PointData> getWritableValue(AnimatorShapeBase shape)
        {
            return PointData.DUMMY_POINT_DATA_PROPERTY;
        }
    };
    
    public abstract PropertyValue<?> createPropertyValue(AnimatorShapeBase s, Interpolator interpolator);
    public abstract PropertyValue<?> createPropertyValue(AnimatorShapeBase s, Object value, Interpolator interpolator);
    public abstract WritableValue<?> getWritableValue(AnimatorShapeBase shape);
    
    //private static final Set<ShapeProperty> animableProperty = Collections.unmodifiableSet(EnumSet.of(X_POSITION, Y_POSITION, FILL_COLOR));
    private static final List<ShapeProperty> animableProperty = Collections.unmodifiableList(
            Arrays.asList(X_POSITION, Y_POSITION, FILL_COLOR, STROKE_COLOR, STROKE_WIDTH, ROTATION, ALPHA
            , OFFSET_X_POSITION, OFFSET_Y_POSITION, OFFSET_ROTATION, OFFSET_ALPHA, POINT_DATA));
    
    public static List<ShapeProperty> getAnimableProperty()
    {
        return animableProperty;
    }
    
    // Might be better to implement using set (EnumSet) but we require garantee
    // traversal order to use in AnimationController
//    public static List<ShapeProperty> getAnimableProperty(Set<ShapeProperty> supportProperty)
//    {
//        List<ShapeProperty> list = new ArrayList<>(animableProperty);
//        list.retainAll(supportProperty);
//        return Collections.unmodifiableList(list);
//    }
            
    public static boolean isPropertyAnimatable(ShapeProperty sp)
    {
        return animableProperty.contains(sp);
    }
    
}