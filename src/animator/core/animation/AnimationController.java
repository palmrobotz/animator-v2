
package animator.core.animation;

import animator.core.animation.datastructor.ObservableNavigableMap;
import animator.ui.preview.Previewable;
import com.sun.javafx.animation.TickCalculation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import javafx.animation.Animation;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.collections.ObservableList;
import javafx.util.Duration;

/**
 *
 * @author Nuntipat narkthong
 */
public class AnimationController extends Animation
{
    private static final boolean DEBUG = false;
    
    // Variable use when playback
    private Iterator<AnimationModel>[] iteratorList;
    private AnimationModel[] currentAnimation;
    private AnimationModelInterpolator[] currentAnimationInterpolator;
    
    // Current tick of this animation as calculate by TickCalculation
    private LongProperty playHeadTick = new SimpleLongProperty();
    //private long playHeadTick = 0;
    
    private TimelineModel model;
    
    public AnimationController(TimelineModel model)
    {
        this.model = model;
    }

    public LongProperty playHeadTickProperty()
    {
        return playHeadTick;
    }
    
    @SuppressWarnings("unchecked")
    private void initIterator()
    {
        // @TODO could be optimize to currentTicks / 6 but this might be break in future released
        long playHeadTime = (long) TickCalculation.toDuration(playHeadTick.get()).toMillis();
        
        if (DEBUG)
            System.err.println("AnimationController : initIterator() Play head time : " + playHeadTime);
        
        ObservableList<LayerModel> timelineData = model.getDataUnmodifiable();
        
        // Initial iterator list
        iteratorList = (Iterator<AnimationModel>[]) new Iterator[timelineData.size()];
        for (int i=0; i<iteratorList.length; i++)
        {
            // Find the submap of the map. Start from the animation that has the starttime 
            // less than or equal to the play head position
            LayerModel layer = timelineData.get(i);
            // Layer is visible so we can continue
            if (layer.isVisible())
            {
                ObservableNavigableMap<Long, AnimationModel> map = layer.getDataUnmodifiable();

                Entry<Long, AnimationModel> lowEntry = map.floorEntry(playHeadTime);
                // There is animation block before the playhead position
                if (lowEntry != null)
                {
                    // We are currently in an animation that already start and isn't end yet
                    // We will include this animation in the result map iterator
                    if (lowEntry.getValue().isContainTime(playHeadTime))
                    {
                        Long lowBound = lowEntry.getKey();
                        if (lowBound != null)
                        {
                            SortedMap<Long, AnimationModel> tailMap = map.tailMap(lowBound, true);
                            iteratorList[i] = tailMap.values().iterator();
                        }
                    }
                    // There is animation before but it is already ended.
                    // We won't include that animation in the result map iterator
                    else
                    {
                        Long lowBound = lowEntry.getKey();
                        if (lowBound != null)
                        {
                            SortedMap<Long, AnimationModel> tailMap = map.tailMap(lowBound, false);
                            iteratorList[i] = tailMap.values().iterator();
                        }
                    }
                }   
                else
                {
                    // No animation before playhead current position
                    // Point iterator to the first animation after playhead
                    iteratorList[i] = map.values().iterator();
                }
            }
            // Layer isn't visible. So iterator should be null
            else
            {
                iteratorList[i] = null;
            }
        }
        
        // Initial animation list
        currentAnimation = new AnimationModel[timelineData.size()];
        for (int i=0; i<currentAnimation.length; i++)
        {
            Iterator<AnimationModel> it = iteratorList[i];
            if (it != null && it.hasNext())
            {
                currentAnimation[i] = it.next();
                // Can't prepare in advance because the shape will be put on screen
                //currentAnimation[i].prepareInterpolate();
            }
            else
            {
                currentAnimation[i] = null;
            } 
        }
        
        // Initial interpolator list
        currentAnimationInterpolator = new AnimationModelInterpolator[timelineData.size()];
        for (int i=0; i<currentAnimationInterpolator.length; i++)
        {
            if (currentAnimation[i] != null)
            {
                currentAnimationInterpolator[i] = new AnimationModelInterpolator(currentAnimation[i]);
            }
            else
            {
                currentAnimationInterpolator[i] = null;
            } 
        }
        
//        // Init variable for playback
//        current = new KeyTime[timelineData.size()][ShapeProperty.getAnimableProperty().size()];
//        next = new KeyTime[timelineData.size()][ShapeProperty.getAnimableProperty().size()];
//        keyFrameIterator = (Iterator<KeyTime>[][]) new Iterator[timelineData.size()][ShapeProperty.getAnimableProperty().size()];
//        alreadyPrepareInterpolate = new boolean[timelineData.size()];
    }
    
    @Override
    public void play()
    {
        if (DEBUG)
            System.err.println("AnimationController : play()");
        
        setCycleDuration(Duration.millis(model.getPreferredDuration()));
        cleanUp();
        initIterator();
        super.play();
    }
    
    public void jumpToTime(Duration time)
    {
        if (DEBUG)
            System.err.println("AnimationController : jumpTo() Time : " + time);
            
        //System.out.println("Jump To");
        // @TODO should move to other place
        Duration d = Duration.millis(model.getPreferredDuration());
        setCycleDuration(d);
        
        if (time == null) 
            throw new NullPointerException("Time needs to be specified.");
        
        if (time.isUnknown()) 
            throw new IllegalArgumentException("The time is invalid");
        
        final Duration totalDuration = getTotalDuration();
        // Wrap the value
        time = time.lessThan(Duration.ZERO) ? Duration.ZERO : time
                .greaterThan(totalDuration) ? totalDuration : time;
        
        //System.out.println("Time : " + time);
        //playHeadTick.set(TickCalculation.fromDuration(time));
        
        // Temporary fix for delete bug
        cleanUp();
        
        jumpTo(time);
    }
    
    @Override
    @Deprecated
    public void impl_playTo(long currentTicks, long cycleTicks)
    {
        if (DEBUG)
            System.err.println("AnimationController : impl_playTo() currentTick : " + currentTicks + " cycleTicks : " + cycleTicks);
        
        // Move playhead
        playHeadTick.set(currentTicks);
        
        // @TODO could be optimize to currentTicks / 6 but this might be break in future released
        long currentTime = (long) TickCalculation.toDuration(currentTicks).toMillis();
        
        // for all layer
        for (int i=0; i<currentAnimation.length; i++)
        {
            if (currentAnimation[i] != null)
            {
                int retVal = currentAnimation[i].isInRange(currentTime);

                // Running
                if (retVal == 0)
                {
                    if (!currentAnimationInterpolator[i].isAlreadyPrepareInterpolate())
                        currentAnimationInterpolator[i].prepareInterpolate();
                    
                    currentAnimationInterpolator[i].doInterpolate(currentTime);
//                    System.out.println("Interpolating");
                }
                // Pass away
                else if (retVal > 0)
                {
                    // The animation cover the time at endTime exactly
                    // Remove because it throw NullPointerException
                    //currentAnimationInterpolator[i].doInterpolate(currentTime);
                    currentAnimationInterpolator[i].cleanPreviewable();
                    if (iteratorList[i].hasNext())
                    {
                        currentAnimation[i] = iteratorList[i].next();
                        currentAnimationInterpolator[i] = new AnimationModelInterpolator(currentAnimation[i]);
                        // Can't prepare in advance since it will show up on screen before its time
                        //prepareInterpolate(i);
                        //doInterpolate(i, currentTime);
                    }
                    else
                    {
                        currentAnimation[i] = null;
                        currentAnimationInterpolator[i] = null;
                    }
//                    System.out.println("Clean up");
                }
                // @TODO Ignore <0 (not start yet)
                else
                {
//                    System.out.println("Not Start");
                }
            }
        }
        
//        System.out.println("------------------");
    }

    @Override
    @Deprecated
    public void impl_jumpTo(long currentTicks, long cycleTicks)
    {
        if (DEBUG)
            System.err.println("AnimationController : impl_jumpTo() currentTick : " + currentTicks + " cycleTicks : " + cycleTicks);
        
        //System.out.println("Tick : "+currentTicks);
        playHeadTick.set(currentTicks);
        // @TODO In order to maintain selection while skim we need to clean up only animtation model that change
        cleanUp();
        initIterator();
        impl_playTo(currentTicks, cycleTicks);
    }
    
    private void cleanUp()
    {
        if (DEBUG)
            System.err.println("AnimationController : cleanUp()");
        
        if (currentAnimation != null)
        {
            for (int i=0; i<currentAnimation.length; i++)
            {
                if (currentAnimation[i] != null)
                    if (currentAnimationInterpolator[i].isAlreadyPrepareInterpolate())
                        currentAnimationInterpolator[i].cleanPreviewable();
            }
        }
    }
    
    private final List<Previewable> previewWindow = new ArrayList<>();
    private final List<Previewable> immutablePreviewWindow = Collections.unmodifiableList(previewWindow);
    
    public void addPreviewableSurface(Previewable previewable)
    {
        previewWindow.add(previewable);
    }
    
    public void removePreviewableSurface(Previewable previewable)
    {
        previewWindow.remove(previewable);
    }
    
    List<Previewable> getPreviewableSurface()
    {
        return immutablePreviewWindow;
    }
    
    private class AnimationModelInterpolator extends AnimationModelInterpolatorBase
    {   
        // Variable use when playback
        private SubAnimationModel[] childAnimationModel;
        private AnimationModelInterpolatorBase[] childInterpolator;
        
        public AnimationModelInterpolator(AnimationModel am)
        {
            super(am);
        }
        
        @Override
        public void prepareInterpolate()
        {
            if (DEBUG)
                System.err.println("AnimationModelInterpolator : prepareInterpolate()");
            
            ObservableList<SubAnimationModel> sam = ((AnimationModel) am).getChildModelUnmodifiable();
            
            // Initial child animation list
            childAnimationModel = sam.toArray(new SubAnimationModel[sam.size()]);

            // Initial child interpolator list
            childInterpolator = new AnimationModelInterpolatorBase[sam.size()];
            for (int i=0; i<childInterpolator.length; i++)
            {
                childInterpolator[i] = new AnimationModelInterpolatorBase(childAnimationModel[i]);
            }
            
            // Prepare interpolate the group itself
            super.prepareInterpolate();
        }
        
        @Override
        public void doInterpolate(long millis)
        {
            if (DEBUG)
                System.err.println("AnimationModelInterpolator : doInterpolate() millis : " + millis);
            
            // We couldn't just call doInterpolate(millis) on all child
            // because each child will have separate start and end time
            
            // For all child layer
            for (int i=0; i<childAnimationModel.length; i++)
            {
                if (childAnimationModel[i] != null)
                {
                    int retVal = childAnimationModel[i].isInRange(millis);

                    // Running
                    if (retVal == 0)
                    {
                        if (!childInterpolator[i].isAlreadyPrepareInterpolate())
                            childInterpolator[i].prepareInterpolate();

                        childInterpolator[i].doInterpolate(millis);
    //                    System.out.println("Interpolating");
                    }
                    // Pass away
                    else if (retVal > 0)
                    {
                        // The animation cover the time at endTime exactly
                        // Remove because it throw NullPointerException
                        //childInterpolator[i].doInterpolate(millis);
                        childInterpolator[i].cleanPreviewable();

 //                       childAnimationModel[i] = null;
 //                       childInterpolator[i] = null;
                    }
                    // @TODO Ignore <0 (not start yet)
                    else
                    {
    //                    System.out.println("Not Start");
                    }
                }
            }
            
            super.doInterpolate(millis);
        }

        @Override
        public void cleanPreviewable()
        {
            if (DEBUG)
                System.err.println("AnimationModelInterpolator : cleanPreviewable()");
            
            // Clean all child
            for (AnimationModelInterpolatorBase amib : childInterpolator)
            {
                amib.cleanPreviewable();
            }
            
            super.cleanPreviewable();
        }
    }
    
    private class AnimationModelInterpolatorBase
    {
        protected final AbstractAnimationModel am;
        
        private boolean prepareInterpolate = false;
        
        private final KeyTime[] current = new KeyTime[ShapeProperty.getAnimableProperty().size()];
        private final KeyTime[] next = new KeyTime[ShapeProperty.getAnimableProperty().size()];
        private final Iterator<KeyTime>[] keyFrameIterator = (Iterator<KeyTime>[]) new Iterator[ShapeProperty.getAnimableProperty().size()];
        
        public AnimationModelInterpolatorBase(AbstractAnimationModel am)
        {
            this.am = am;
        }
        
        public boolean isAlreadyPrepareInterpolate()
        {
            return prepareInterpolate;
        }
        
        public void prepareInterpolate()
        {
            if (DEBUG)
                System.err.println("AnimationModelInterpolatorBase : prepareInterpolate()");
            
            for (int i=0; i<ShapeProperty.getAnimableProperty().size(); i++)
            {
                keyFrameIterator[i] = am.getKeyTimeUnmodifiable().iterator();
                current[i] = keyFrameIterator[i].hasNext() ? keyFrameIterator[i].next() : null;
                if (current[i].isPropertyChange(ShapeProperty.getAnimableProperty().get(i)))
                {
                    next[i] = keyFrameIterator[i].hasNext() ? keyFrameIterator[i].next() : null;
                    // Ensure that next keyframe has the correspond change
                    while (next[i] != null && !next[i].isPropertyChange(ShapeProperty.getAnimableProperty().get(i)))
                    {
                        next[i] = keyFrameIterator[i].hasNext() ? keyFrameIterator[i].next() : null;
                    }
                }
                else
                {
                    current[i] = null;
                    next[i] = null;
                }
            }

            for (Previewable surface : previewWindow)
            {
                surface.addShape(am.getShape());
            }

            prepareInterpolate = true;
        }
        
        public void doInterpolate(long millis)
        {
            if (DEBUG)
                System.err.println("AnimationModelInterpolatorBase : doInterpolate() millis : " + millis);
            
            for (int i =0; i<current.length; i++)
                doInterpolate(i, millis);
        }
        
        // @TODO allow to much millis
        // @TODO use tick instead
        private void doInterpolate(int i, long millis)
        {
            if (DEBUG)
                System.err.println("AnimationController : doInterpolate() : j = " + i + " millis = " + millis);
        
            if (current[i] == null)
                return;

            if (millis < current[i].getTime())
                throw new RuntimeException("Shouldn't reach here");

            ShapeProperty ap = ShapeProperty.getAnimableProperty().get(i);

            // Check if current and next still valid [current time, next time)
            // Just for sure in case that miliis go to fast
            while (next[i] != null && millis > next[i].getTime())
            {
                current[i] = next[i];
                next[i] = keyFrameIterator[i].hasNext() ? keyFrameIterator[i].next() : null;
                // Ensure that next keyframe has the correspond change
                while (next[i] != null && !next[i].isPropertyChange(ap))
                {
                    next[i] = keyFrameIterator[i].hasNext() ? keyFrameIterator[i].next() : null;
                }
            }

            // Freeze at current
            if (next[i] == null)
            {
                //Set<ShapeProperty> change = current[i][j].getPropertyChange();
                //for (AnimatableProperty ap : change)
                //{
                    current[i].getProperty(ap).getWritableValue().setValue(current[i].getProperty(ap).getValue());
                //}
            }
            // This is just for safety, we should implicitly here
            else if (current[i].getTime() <= millis && next[i].getTime() > millis)
            {
                //Set<ShapeProperty> change = current[i][j].getPropertyChange();
                double percent = (double) (millis - current[i].getTime()) / (double) (next[i].getTime() - current[i].getTime());

                // For each change property in current time, find matches in the next time
                //for (AnimatableProperty ap : change)
                //{
                    if (next[i].isPropertyChange(ap))
                        current[i].getProperty(ap).interpolate(next[i].getProperty(ap), percent);
                    // Do not remove!!!!
                    else
                        current[i].getProperty(ap).getWritableValue().setValue(current[i].getProperty(ap).getValue());
                //}
            }
        }
        
        public void cleanPreviewable()
        {
            if (DEBUG)
                System.err.println("AnimationModelInterpolatorBase : cleanPreviewable()");
            
            for (Previewable surface : previewWindow)
            {
                surface.removeShape(am.getShape());
            }
        
            prepareInterpolate = false;
        }
    }
}
