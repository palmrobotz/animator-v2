
package animator.core.animation;

import animator.core.animation.datastructor.AnimatorCollections;
import animator.core.animation.datastructor.ObservableNavigableMap;
import animator.core.animation.event.LayerModelChangedEvent;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.GroupShape;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.ReadOnlyLongWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author nuntipat
 */
public class LayerModel
{
    private ObservableNavigableMap<Long, AnimationModel> map = AnimatorCollections.observableNavigableMap(new TreeMap<Long, AnimationModel>());
    private ObservableNavigableMap<Long, AnimationModel> immutableMap = AnimatorCollections.unmodifiableObservableNavigableMap(map);
    
    //private long duration = 0L;
    private ReadOnlyLongWrapper duration = new ReadOnlyLongWrapper();
    private ReadOnlyIntegerWrapper layerIndex = new ReadOnlyIntegerWrapper();
    
    private StringProperty name = new SimpleStringProperty("Untitled");
    private BooleanProperty lock = new SimpleBooleanProperty(false);
    private BooleanProperty visible = new SimpleBooleanProperty(true);
    
    private final TimelineModel timelineModel;
    
    LayerModel(TimelineModel timelineModel)
    {
        this.timelineModel = timelineModel;
    }
    
    ObservableNavigableMap<Long, AnimationModel> getData()
    {
        return map;
    }
    
    public ObservableNavigableMap<Long, AnimationModel> getDataUnmodifiable()
    {
        return immutableMap;
    }

    public TimelineModel getTimelineModel()
    {
        return timelineModel;
    }
    
    public ReadOnlyIntegerProperty layerIndexProperty()
    {
        return layerIndex.getReadOnlyProperty();
    }
    
    public int getLayerIndex()
    {
//        ObservableList<LayerModel> list = timelineModel.getDataUnmodifiable();
//        for (int i=0; i<list.size(); i++)
//        {
//            if (this == list.get(i))
//                return i;
//        }
//        return -1;
        return layerIndex.get();
    }
    
    void setLayerIndex(int i)
    {
        layerIndex.set(i);
    }
    
    // @TODO add check
    /**
     * This method should only be use by undo / redo machanism
     * @param animationModel 
     */
    public void addAnimation(AnimationModel animationModel)
    {
        map.put(animationModel.getStartTime(), animationModel);
        updateDuration();
        getTimelineModel().fireLayerModelChangedEvent(
                new LayerModelChangedEvent(this, animationModel, 
                    LayerModelChangedEvent.Changed.ADD_ANIMATION));
    }
    
    /**
     * {@code [startTime, endTime)}
     * @param startTime
     * @param s
     * @return 
     */
    public AnimationModel addAnimation(long startTime, AnimatorShapeBase s)
    {
        if (startTime < 0)
            throw new IllegalArgumentException("Invalid time");
        
        // Find possible endtime
        Long nextKey = map.higherKey(startTime);
        if (nextKey == null)
            return addAnimation(startTime, startTime+1000, s);
        
        if (startTime == nextKey.longValue())
            throw new IllegalArgumentException("No time slot free at the given start time");

        return addAnimation(startTime, nextKey, s);
    }
    
    public AnimationModel addAnimation(long startTime, long endTime, AnimatorShapeBase s)
    {
        if (s == null)
            throw new NullPointerException("Shape is null");
        
        if (startTime < 0)
            throw new IllegalArgumentException("Invalid start time");
        
        // Check if there is any animation at start time
        Map.Entry<Long, AnimationModel> prevEntry = map.floorEntry(startTime);

        // Check if the previous animation overlap the one being add or not
        if (prevEntry != null && /*(prevEntry.getKey() == startTime ||*/ prevEntry.getValue().isContainTime(startTime)/*)*/ )
            throw new IllegalArgumentException("Animation already exist at start time");

        // Check if there is any animation at end time
        Map.Entry<Long, AnimationModel> nextEntry = map.floorEntry(endTime);
        
        // Check if the next animation overlap the one being add or not
        if (nextEntry != null && nextEntry.getValue().isContainTime(endTime))
            throw new IllegalArgumentException("Animation already exist at end time");
        
        AnimationModel am = new AnimationModel(this, startTime, endTime);
        am.addChildShape(s, startTime);
        map.put(startTime, am);
        
        updateDuration();
        
        getTimelineModel().fireLayerModelChangedEvent(new LayerModelChangedEvent(this, am, LayerModelChangedEvent.Changed.ADD_ANIMATION));
        return am;
    }
    
    public AnimationModel addAnimation(long startTime, long endTime, AnimatorShapeBase s, GroupShape gs)
    {
        if (s == null)
            throw new NullPointerException("Shape is null");
        
        if (startTime < 0)
            throw new IllegalArgumentException("Invalid start time");
        
        // Check if there is any animation at start time
        Map.Entry<Long, AnimationModel> prevEntry = map.floorEntry(startTime);

        // Check if the previous animation overlap the one being add or not
        if (prevEntry != null && /*(prevEntry.getKey() == startTime ||*/ prevEntry.getValue().isContainTime(startTime)/*)*/ )
            throw new IllegalArgumentException("Animation already exist at start time");

        // Check if there is any animation at end time
        Map.Entry<Long, AnimationModel> nextEntry = map.floorEntry(endTime);
        
        // Check if the next animation overlap the one being add or not
        if (nextEntry != null && nextEntry.getValue().isContainTime(endTime))
            throw new IllegalArgumentException("Animation already exist at end time");
        
        AnimationModel am = new AnimationModel(this, gs, startTime, endTime);
        am.addChildShape(s, startTime);
        map.put(startTime, am);
        
        updateDuration();
        
        getTimelineModel().fireLayerModelChangedEvent(new LayerModelChangedEvent(this, am, LayerModelChangedEvent.Changed.ADD_ANIMATION));
        return am;
    }
    
    
    public AnimationModel removeAnimation(long time)
    {
        if (time < 0)
            throw new IllegalArgumentException("Invalid time");
        
        // Find the animation that contain the time given
        Long startTime = map.floorKey(time);
        if (startTime != null)
        {
            // Confirm that the animation to be removed contain the time given
            AnimationModel am = map.get(startTime);
            if (am.isContainTime(startTime))
            {
                map.remove(startTime);
                updateDuration();
                return am;
            }
        }
        
        throw new IllegalArgumentException("Can't find animation at this time");
    }
    
    public boolean removeAnimation(AnimationModel animation)
    {
        if (animation == null)
            throw new NullPointerException("Animation is null");
        
        if (map.values().remove(animation))
        {
            updateDuration();
            getTimelineModel().fireLayerModelChangedEvent(new LayerModelChangedEvent(this, animation, LayerModelChangedEvent.Changed.REMOVE_ANIMATION));
            return true;
        }
        else
        {
            return false;
        }
    }

    public long getDuration()
    {
        return duration.get();
    }
    
    public ReadOnlyLongProperty durationProperty()
    {
        return duration.getReadOnlyProperty();
    }
    
    /**
     * Rely on AnimationModel to called this method when the endtime change
     */
    void updateDuration()
    {
        if (!map.isEmpty())
            duration.set(map.lastEntry().getValue().getEndTime());
        else 
            duration.set(0);
    }
    
    public AnimationModel getAnimation(long time)
    {
        if (time < 0)
            throw new IllegalArgumentException("Invalid start time");
        
        // Find the animation that contain the time given
        Long startTime = map.floorKey(time);
        if (startTime != null)
        {
            AnimationModel am = map.get(startTime);
            // Confirm that the animation to be removed contain the time given
            if (am.isContainTime(time))
            {
                return am;
            }
        }
        
        throw new IllegalArgumentException("Can't find animation at this time");
    }
    
    public boolean isAnimationExist(long time)
    {
        if (time < 0)
            throw new IllegalArgumentException("Invalid start time");
        
        // Find the animation that contain the time given
        Long startTime = map.floorKey(time);
        if (startTime != null)
        {
            AnimationModel am = map.get(startTime);
            // Confirm that the animation to be removed contain the time given
            if (am.isContainTime(time))
            {
                return true;
            }
        }
        
        return false;
    }

    public boolean isAnimationExist(long startTime, long endTime)
    {
        if (startTime < 0 || endTime <= startTime)
            throw new IllegalArgumentException("Invalid start time or end time");
        
        SortedMap<Long, AnimationModel> subMap = map.subMap(startTime, endTime);
        if (subMap.size() != 0)
            return true;
        
        // In case that the animation start before startTime
        Long leastKey = map.floorKey(startTime);
        if(leastKey != null)
        {
            return map.get(leastKey).isContainTime(startTime);
        }
        
        return false;
    }
    
    public StringProperty nameProperty()
    {
        return name;
    }
    
    public String getName()
    {
        return name.get();
    }
    
    public void setName(String s)
    {
        name.set(s);
    }
    
    public BooleanProperty lockProperty()
    {
        return lock;
    }
    
    public boolean isLock()
    {
        return lock.get();
    }
    
    public void setLock(boolean b)
    {
        lock.set(b);
    }
    
    public BooleanProperty visibleProperty()
    {
        return visible;
    }
    
    public boolean isVisible()
    {
        return visible.get();
    }
    
    public void setVisible(boolean b)
    {
        visible.set(b);
    }
}
