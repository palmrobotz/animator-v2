
package animator.core.animation;

import animator.core.shape.AnimatorShapeBase;

/**
 *
 * @author Nuntipat narkthong
 */
public class SubAnimationModel extends AbstractAnimationModel
{
    private AnimationModel parent;
    
    SubAnimationModel(AnimationModel parent, AnimatorShapeBase shape, long startTime, long endTime)
    {
        super(parent.getLayerModel(), shape, startTime, endTime);
        this.parent = parent;
    }
    
    public AnimationModel getParentAnimationModel()
    {
        return parent;
    }
    
    /**
     * 
     * @param time relative time to the start time of the parent
     * @return 
     */
    @Override
    public boolean moveToTime(long time)
    {
        long newStartTime = getStartTime() + time;
        // Check if the new time is in the range of the parent animation
        if (parent.isContainTime(newStartTime))
        {
            startTime.set(newStartTime);
            return true;
        }
        
        return false;
    }

    @Override
    public boolean setEndTime(long time)
    {
        // Check if the new end time is in the range of the parent animation
        if (time >= parent.getStartTime() && time <= parent.getEndTime())
        {
            duration.set(time - startTime.get());
            return true;
        }
        
        return false;
    }
    
}
