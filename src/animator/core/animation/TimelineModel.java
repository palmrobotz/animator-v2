package animator.core.animation;

import animator.core.animation.event.AnimationModelChangedEvent;
import animator.core.animation.event.AnimationModelChangedListener;
import animator.core.animation.event.LayerModelChangedEvent;
import animator.core.animation.event.LayerModelChangedListener;
import animator.core.animation.event.TimelineModelChangedEvent;
import animator.core.animation.event.TimelineModelChangedListener;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.ReadOnlyLongWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class use to store timeline data
 * @author Nuntipat narkthong
 */
public final class TimelineModel
{
    private final ObservableList<LayerModel> timelineData = FXCollections.observableArrayList();
    private final ObservableList<LayerModel> immutableTimelineData = FXCollections.unmodifiableObservableList(timelineData);
    
    private ReadOnlyLongWrapper preferredDuration = new ReadOnlyLongWrapper();
    
    private InvalidationListener durationInvalidate = new InvalidationListener() 
    {
        @Override
        public void invalidated(Observable o)
        {
            // Now the last value in the map will 
            long minimumDuration = 0;
            // Compute the minimum duration for this clip
            for (LayerModel tm : timelineData)
            {
                long layerDuration = tm.getDuration();
                if (layerDuration > minimumDuration)
                    minimumDuration = layerDuration;
            }
            preferredDuration.set(minimumDuration);
        }
    };
    
    public TimelineModel()
    {
        // When the list change, update all layer index
        timelineData.addListener(new InvalidationListener() 
        {
            @Override
            public void invalidated(Observable observable)
            {
                for (int i=0; i<timelineData.size(); i++)
                {
                    timelineData.get(i).setLayerIndex(i);
                }
            }
        });
    }
    
    public ObservableList<LayerModel> getDataUnmodifiable()
    {
        return immutableTimelineData;
    }
    
    public void addLayer(LayerModel layer)
    {
        timelineData.add(layer);
        layer.durationProperty().addListener(durationInvalidate);
        fireTimelineModelChangedEvent(new TimelineModelChangedEvent(this, TimelineModelChangedEvent.Changed.ADD_LAYER, layer));
    }
    
    public LayerModel newLayer()
    {
        LayerModel layer = new LayerModel(this);
        layer.durationProperty().addListener(durationInvalidate);
        timelineData.add(layer);
        fireTimelineModelChangedEvent(new TimelineModelChangedEvent(this, TimelineModelChangedEvent.Changed.ADD_LAYER, layer));
        return layer;
    }
    
    public LayerModel newLayer(int i)
    {
        LayerModel layer = new LayerModel(this);
        layer.durationProperty().addListener(durationInvalidate);
        timelineData.add(i, layer);
        fireTimelineModelChangedEvent(new TimelineModelChangedEvent(this, TimelineModelChangedEvent.Changed.ADD_LAYER, layer));
        return layer;
    }
    
    public void removeLayer(LayerModel layer)
    {
        int index = timelineData.indexOf(layer);
        timelineData.remove(index);
        layer.durationProperty().removeListener(durationInvalidate);
        fireTimelineModelChangedEvent(new TimelineModelChangedEvent(this, TimelineModelChangedEvent.Changed.REMOVE_LAYER, layer));
    }
    
    public LayerModel getLayer(int i)
    {
        return timelineData.get(i);
    }
    
    public int getLayerCount()
    {
        return timelineData.size();
    }

    // @TODO Create property for getPreferredDuration
    public long getPreferredDuration()
    {
        return preferredDuration.get();
    }
    
    public ReadOnlyLongProperty preferredDuration()
    {
        return preferredDuration.getReadOnlyProperty();
    }
    
    public long getDuration()
    {
        //return duration;
        throw new UnsupportedOperationException();
    }

    public void setDuration(long duration)
    {
        //this.duration = duration;
        throw new UnsupportedOperationException();
    }

    private final List<TimelineModelChangedListener> timelineModelChangedListener = new ArrayList<>();
    private final List<LayerModelChangedListener> layerModelChangedListener = new ArrayList<>();
    private final List<AnimationModelChangedListener> animationModelChangedListener = new ArrayList<>();
    
    public void addTimelineModelChangedListener(TimelineModelChangedListener listener)
    {
        timelineModelChangedListener.add(listener);
    }
    
    public void removeTimelineModelChangedListener(TimelineModelChangedListener listener)
    {
        timelineModelChangedListener.remove(listener);
    }
    
    void fireTimelineModelChangedEvent(TimelineModelChangedEvent event)
    {
        for (TimelineModelChangedListener listener : timelineModelChangedListener)
        {
            listener.onTimelineModelChanged(event);
        }
    }
    
    public void addLayerModelChangedListener(LayerModelChangedListener listener)
    {
        layerModelChangedListener.add(listener);
    }
    
    public void removeLayerModelChangedListener(LayerModelChangedListener listener)
    {
        layerModelChangedListener.remove(listener);
    }
    
    void fireLayerModelChangedEvent(LayerModelChangedEvent event)
    {
        for (LayerModelChangedListener listener : layerModelChangedListener)
        {
            listener.onLayerModelChanged(event);
        }
    }
    
    public void addAnimationModelChangedListener(AnimationModelChangedListener listener)
    {
        animationModelChangedListener.add(listener);
    }
    
    public void removeAnimationModelChangedListener(AnimationModelChangedListener listener)
    {
        animationModelChangedListener.remove(listener);
    }
    
    void fireAnimationModelChangedEvent(AnimationModelChangedEvent event)
    {
        for (AnimationModelChangedListener listener : animationModelChangedListener)
        {
            listener.onAnimationModelChanged(event);
        }
    }
}
