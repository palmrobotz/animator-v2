
package animator.core.shape;

import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.scene.paint.Paint;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

/**
 * An abstract adapter class for all shape. 
 * 
 * The methods in this class all throw UnsupportedOperationException.
 * This class defines empty methods for them all, so you can only have to define methods you care about.
 * 
 * Only ShapeOpacity getter/setter and property is implement to return the AnimatorShapeBase itself opacity
 * This is appropriate for object that could be transparent as a whole.
 * 
 * @author Nuntipat narkthong
 */
public class AnimatorShapeBaseAdapter extends AnimatorShapeBase
{
    @Override
    public AnimatorShape getType()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public Paint getFill()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setFill(Paint p)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ObjectProperty<Paint> fillProperty()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Paint getStroke()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setStroke(Paint d)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ObjectProperty<Paint> strokeProperty()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public double getStrokeWidth()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setStrokeWidth(double d)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public DoubleProperty strokeWidthProperty()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public StrokeLineCap getStrokeLineCap()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setStrokeLineCap(StrokeLineCap value)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ObjectProperty<StrokeLineCap> strokeLineCapProperty()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public StrokeLineJoin getStrokeLineJoin()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setStrokeLineJoin(StrokeLineJoin value)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ObjectProperty<StrokeLineJoin> strokeLineJoinProperty()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public AnimatorShapeBase deepCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public AnimatorShapeBase referenceCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public AnimatorShapeBase immutableReferenceCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getShapeOpacity()
    {
        return getOpacity();
    }

    @Override
    public void setShapeOpacity(double d)
    {
        setOpacity(d);
    }

    @Override
    public DoubleProperty shapeOpacityProperty()
    {
        return opacityProperty();
    }
   
}
