package animator.core.shape;

import animator.core.animation.ShapeProperty;
import animator.core.shape.util.DragHelper;
import animator.core.shape.util.DragHelperCallback;
import static animator.core.shape.util.ShapeTheme.*;
import animator.ui.Tool;
import animator.ui.ToolBox;
import java.util.Arrays;
import java.util.List;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.PathElement;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

/**
 *
 * @author Nuntipat narkthong
 */
public class Path extends AnimatorShapeBase
{
    private final Group boundGroup = new Group();
    private final javafx.scene.shape.Path path = new javafx.scene.shape.Path();
    
    private boolean closed = false;
    
    private final ReadOnlyObjectProperty<Tool> toolSelected = ToolBox.getInstance().toolSelectProperty();

    public Path(double x, double y)
    {
        javafx.scene.shape.Path boundingPath = applyBoundTheme(new javafx.scene.shape.Path());
        Bindings.bindContentBidirectional(path.getElements(), boundingPath.getElements());

        /* Create first point */
        final MoveTo moveTo = new MoveTo(x, y);
        path.getElements().add(moveTo);

        javafx.scene.shape.Rectangle startPoint = EDGE_POINT_BUILDER.build();
        startPoint.layoutXProperty().bind(moveTo.xProperty());
        startPoint.layoutYProperty().bind(moveTo.yProperty());
        DragHelper.INSTANCE.makeNodeDragable(startPoint, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                moveTo.setX(moveTo.getX() + deltaX);
                moveTo.setY(moveTo.getY() + deltaY);
            }
        });

        //No need this part any more - use double click to close path instead
        // Detect path close
        /*startPoint.setOnMouseClicked(new EventHandler<MouseEvent>()
         {
         @Override
         public void handle(MouseEvent event)
         {
         createClosePathEndPoint(moveTo);
         path.getElements().add(new ClosePath());
         closed = true;
         }
         });*/
        boundGroup.getChildren().addAll(boundingPath, startPoint);

        /* Keep all point for XML */
        addPointData(Arrays.asList(moveTo.xProperty(), moveTo.yProperty()));

        /* Initialize shape */
        boundGroup.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        getChildren().addAll(path, boundGroup);

        /* Recieve forward mouse event when this object is being selected */
        addEventHandler(MouseEvent.ANY, new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent event)
            {
                if (toolSelected.getValue() == Tool.PATH && event.getEventType() == MouseEvent.MOUSE_PRESSED)
                {
                    if (!closed)
                    {
                        double x = event.getX(), y = event.getY();
                        createPoint(x, y, x, y, x, y);
                    }
                }
                if (event.getClickCount() == 2)
                {
                    System.out.println("Double clicked");
                    if (closed == false)
                    {
                        createClosePathEndPoint(moveTo);
                        path.getElements().add(new ClosePath());
                        closed = true;
                    }
                }

                event.consume();
            }
        });
        
        setSupportProperty(ShapeProperty.SELECTION, ShapeProperty.X_POSITION, ShapeProperty.Y_POSITION
                , ShapeProperty.FILL_COLOR, ShapeProperty.STROKE_COLOR, ShapeProperty.STROKE_WIDTH
                , ShapeProperty.ROTATION, ShapeProperty.ALPHA, ShapeProperty.POINT_DATA);
    
        initShapeWithShape(path);
    }

    private void createPoint(double cx1, double cy1, double cx2, double cy2, double x, double y)
    {
        if (closed)
        {
            throw new AssertionError("Should not be called after close");
        }

        final PathElement pathLastElement = path.getElements().get(path.getElements().size() - 1);
        /* need to change location to control point1 to last Anchor location*/
        if (pathLastElement instanceof CubicCurveTo)
        {
            CubicCurveTo lastCubic = (CubicCurveTo) pathLastElement;
            cx1 = lastCubic.getX();
            cy1 = lastCubic.getY();
        }
        if (pathLastElement instanceof MoveTo)
        {
            MoveTo lastMove = (MoveTo) pathLastElement;
            cx1 = lastMove.getX();
            cy1 = lastMove.getY();
        }
        final CubicCurveTo cubicTo = new CubicCurveTo(cx1, cy1, cx2, cy2, x, y);

        path.getElements().add(cubicTo);

        // Dummy Property
//        DoubleProperty controlX1 = new SimpleDoubleProperty(cx1);
//        DoubleProperty controlY1 = new SimpleDoubleProperty(cy1);
//        DoubleProperty controlX2 = new SimpleDoubleProperty(cy2);
//        DoubleProperty controlY2 = new SimpleDoubleProperty(cy2);


        javafx.scene.shape.Rectangle edgePoint = EDGE_POINT_BUILDER.build();
        javafx.scene.shape.Rectangle control1Point = EDGE_POINT_BUILDER.build();
        javafx.scene.shape.Rectangle control2Point = EDGE_POINT_BUILDER.build();

        javafx.scene.shape.Line control1Line = LINE_BUILDER.build();
        javafx.scene.shape.Line control2Line = LINE_BUILDER.build();

        edgePoint.layoutXProperty().bind(cubicTo.xProperty());
        edgePoint.layoutYProperty().bind(cubicTo.yProperty());
        control1Point.layoutXProperty().bind(cubicTo.controlX1Property());
        control1Point.layoutYProperty().bind(cubicTo.controlY1Property());
        control2Point.layoutXProperty().bind(cubicTo.controlX2Property());
        control2Point.layoutYProperty().bind(cubicTo.controlY2Property());

        //bind with previous anchor
        if (pathLastElement instanceof CubicCurveTo)
        {
            CubicCurveTo lastCubic = (CubicCurveTo) pathLastElement;
            control1Line.startXProperty().bind(lastCubic.xProperty());
            control1Line.startYProperty().bind(lastCubic.yProperty());
        }
        if (pathLastElement instanceof MoveTo)
        {
            MoveTo lastMove = (MoveTo) pathLastElement;
            control1Line.startXProperty().bind(lastMove.xProperty());
            control1Line.startYProperty().bind(lastMove.yProperty());
        }
        control1Line.endXProperty().bind(cubicTo.controlX1Property());
        control1Line.endYProperty().bind(cubicTo.controlY1Property());

        control2Line.startXProperty().bind(cubicTo.xProperty());
        control2Line.startYProperty().bind(cubicTo.yProperty());
        control2Line.endXProperty().bind(cubicTo.controlX2Property());
        control2Line.endYProperty().bind(cubicTo.controlY2Property());

        DragHelper.INSTANCE.makeNodeDragable(edgePoint, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                int index = path.getElements().indexOf(cubicTo);
                PathElement pathNextElement = path.getElements().get(index + 1);
                cubicTo.setX(cubicTo.getX() + deltaX);
                cubicTo.setY(cubicTo.getY() + deltaY);
                cubicTo.setControlX2(cubicTo.getControlX2() + deltaX);
                cubicTo.setControlY2(cubicTo.getControlY2() + deltaY);
                if (pathNextElement instanceof CubicCurveTo)
                {
                    CubicCurveTo nextCubic = (CubicCurveTo) pathNextElement;
                    nextCubic.setControlX1(nextCubic.getControlX1() + deltaX);
                    nextCubic.setControlY1(nextCubic.getControlY1() + deltaY);
                }
            }
        });
        DragHelper.INSTANCE.makeNodeDragable(control1Point, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                cubicTo.setControlX1(cubicTo.getControlX1() + deltaX);
                cubicTo.setControlY1(cubicTo.getControlY1() + deltaY);
                /*if(pathLastElement instanceof CubicCurveTo){
                 CubicCurveTo lastCubic = (CubicCurveTo)pathLastElement;
                 lastCubic.setControlX2(lastCubic.getControlX2()-deltaX);
                 lastCubic.setControlY2(lastCubic.getControlY2()-deltaY);
                 }else{// this mean pathLastElement is MoveTo => first element
                 //move controlX2 of last element
                    
                 }*/
            }
        });
        DragHelper.INSTANCE.makeNodeDragable(control2Point, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                cubicTo.setControlX2(cubicTo.getControlX2() + deltaX);
                cubicTo.setControlY2(cubicTo.getControlY2() + deltaY);
            }
        });
        boundGroup.getChildren().add(control1Line);
        boundGroup.getChildren().add(control2Line);
        boundGroup.getChildren().add(edgePoint);
        boundGroup.getChildren().add(control1Point);
        boundGroup.getChildren().add(control2Point);

        // Keep all point for XML
        addPointData(Arrays.asList(cubicTo.controlX1Property(), cubicTo.controlY1Property()
                , cubicTo.controlX2Property(), cubicTo.controlY2Property(), cubicTo.xProperty(), cubicTo.yProperty()));
    }

    private void createClosePathEndPoint(MoveTo firstPoint)
    {
        if (closed)
        {
            throw new AssertionError("Should not be called after close");
        }

        double x = firstPoint.getX(), y = firstPoint.getY();
        final PathElement pathLastElement = path.getElements().get(path.getElements().size() - 1);
        /* need to change location to control point1 to last Anchor location*/
        double cx1 = 0;
        double cy1 = 0;
        if (pathLastElement instanceof CubicCurveTo)
        {
            CubicCurveTo lastCubic = (CubicCurveTo) pathLastElement;
            cx1 = lastCubic.getX();
            cy1 = lastCubic.getY();
        }
        if (pathLastElement instanceof MoveTo)
        {
            MoveTo lastMove = (MoveTo) pathLastElement;
            cx1 = lastMove.getX();
            cy1 = lastMove.getY();
        }

        final CubicCurveTo cubicTo = new CubicCurveTo(cx1, cy1, x, y, x, y);
        cubicTo.xProperty().bindBidirectional(firstPoint.xProperty());
        cubicTo.yProperty().bindBidirectional(firstPoint.yProperty());
        path.getElements().add(cubicTo);

        // Dummy Property
//        DoubleProperty controlX1 = cubicTo.controlX1Property();
//        DoubleProperty controlY1 = cubicTo.controlY1Property();
//        DoubleProperty controlX2 = cubicTo.controlX2Property();
//        DoubleProperty controlY2 = cubicTo.controlY2Property();


        javafx.scene.shape.Rectangle edgePoint = EDGE_POINT_BUILDER.build();
        javafx.scene.shape.Rectangle control1Point = EDGE_POINT_BUILDER.build();
        javafx.scene.shape.Rectangle control2Point = EDGE_POINT_BUILDER.build();

        javafx.scene.shape.Line control1Line = LINE_BUILDER.build();
        javafx.scene.shape.Line control2Line = LINE_BUILDER.build();

        edgePoint.layoutXProperty().bind(cubicTo.xProperty());
        edgePoint.layoutYProperty().bind(cubicTo.yProperty());
        control1Point.layoutXProperty().bind(cubicTo.controlX1Property());
        control1Point.layoutYProperty().bind(cubicTo.controlY1Property());
        control2Point.layoutXProperty().bind(cubicTo.controlX2Property());
        control2Point.layoutYProperty().bind(cubicTo.controlY2Property());

        //bind with previous anchor
        if (pathLastElement instanceof CubicCurveTo)
        {
            CubicCurveTo lastCubic = (CubicCurveTo) pathLastElement;
            control1Line.startXProperty().bind(lastCubic.xProperty());
            control1Line.startYProperty().bind(lastCubic.yProperty());
        }
        if (pathLastElement instanceof MoveTo)
        {
            MoveTo lastMove = (MoveTo) pathLastElement;
            control1Line.startXProperty().bind(lastMove.xProperty());
            control1Line.startYProperty().bind(lastMove.yProperty());
        }
        control1Line.endXProperty().bind(cubicTo.controlX1Property());
        control1Line.endYProperty().bind(cubicTo.controlY1Property());

        control2Line.startXProperty().bind(cubicTo.xProperty());
        control2Line.startYProperty().bind(cubicTo.yProperty());
        control2Line.endXProperty().bind(cubicTo.controlX2Property());
        control2Line.endYProperty().bind(cubicTo.controlY2Property());

        DragHelper.INSTANCE.makeNodeDragable(edgePoint, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {

                int index = path.getElements().indexOf(cubicTo);
                PathElement pathNextElement = path.getElements().get(1);
                cubicTo.setX(cubicTo.getX() + deltaX);
                cubicTo.setY(cubicTo.getY() + deltaY);
                cubicTo.setControlX2(cubicTo.getControlX2() + deltaX);
                cubicTo.setControlY2(cubicTo.getControlY2() + deltaY);
                if (pathNextElement instanceof CubicCurveTo)
                {
                    CubicCurveTo nextCubic = (CubicCurveTo) pathNextElement;
                    nextCubic.setControlX1(nextCubic.getControlX1() + deltaX);
                    nextCubic.setControlY1(nextCubic.getControlY1() + deltaY);
                }
            }
        });
        DragHelper.INSTANCE.makeNodeDragable(control1Point, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                cubicTo.setControlX1(cubicTo.getControlX1() + deltaX);
                cubicTo.setControlY1(cubicTo.getControlY1() + deltaY);
                /*if(pathLastElement instanceof CubicCurveTo){
                 CubicCurveTo lastCubic = (CubicCurveTo)pathLastElement;
                 lastCubic.setControlX2(lastCubic.getControlX2()-deltaX);
                 lastCubic.setControlY2(lastCubic.getControlY2()-deltaY);
                 }else{// this mean pathLastElement is MoveTo => first element
                 //move controlX2 of last element
                 CubicCurveTo lastElement = (CubicCurveTo)path.getElements().get(path.getElements().size()-1);
                 MoveTo firstElement = (MoveTo)path.getElements().get(0);
                 }*/

            }
        });
        DragHelper.INSTANCE.makeNodeDragable(control2Point, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                System.err.println("test2");
                cubicTo.setControlX2(cubicTo.getControlX2() + deltaX);
                cubicTo.setControlY2(cubicTo.getControlY2() + deltaY);
            }
        });

        javafx.scene.shape.Rectangle secondAnchorControl1rect = (javafx.scene.shape.Rectangle) boundGroup.getChildren().get(5);
        /**
         * ***************************
         * This part of code is use for bind control point at first point
         *
         * //need to override second anchor's controlx1 onDrag to bind with last
         * anchor's controlx2!!!!!!!!!!!!!!!!!!!!
         *
         *
         *
         * //secondAnchorControl1rect.removeEventHandler(MouseEvent.MOUSE_PRESSED,
         * secondAnchorControl1rect.onMousePressedProperty().getValue());
         * //secondAnchorControl1rect.removeEventHandler(MouseEvent.MOUSE_DRAGGED,
         * secondAnchorControl1rect.onMouseDraggedProperty().getValue()); final
         * CubicCurveTo secondAnchor = (CubicCurveTo)path.getElements().get(1);
         * DragHelper.INSTANCE.makeNodeDragable(secondAnchorControl1rect, new
         * DragHelperCallback() {
         *
         * @Override public void onDragged(double deltaX, double deltaY) {
         * secondAnchor.setControlX1(secondAnchor.getControlX1()+deltaX);
         * secondAnchor.setControlY1(secondAnchor.getControlY1()+deltaY);
         * cubicTo.setControlX2(cubicTo.getControlX2()-deltaX);
         * cubicTo.setControlY2(cubicTo.getControlY2()-deltaY); } });
         */
        boundGroup.getChildren().add(control1Line);
        boundGroup.getChildren().add(control2Line);
        boundGroup.getChildren().add(edgePoint);
        boundGroup.getChildren().add(control1Point);
        boundGroup.getChildren().add(control2Point);

        //move secondAnchorcontrol1 to top
        boundGroup.getChildren().remove(secondAnchorControl1rect);
        boundGroup.getChildren().add(secondAnchorControl1rect);
        
        // Keep all point for XML
        addPointData(Arrays.asList(cubicTo.controlX1Property(), cubicTo.controlY1Property()
                , cubicTo.controlX2Property(), cubicTo.controlY2Property(), cubicTo.xProperty(), cubicTo.yProperty()));
    }

    @Override
    public AnimatorShape getType()
    {
        return AnimatorShape.PATH;
    }

    @Override
    public Paint getFill()
    {
        return path.getFill();
    }

    @Override
    public void setFill(Paint p)
    {
        path.setFill(p);
    }

    @Override
    public ObjectProperty<Paint> fillProperty()
    {
        return path.fillProperty();
    }

    @Override
    public Paint getStroke()
    {
        return path.getStroke();
    }

    @Override
    public void setStroke(Paint p)
    {
        path.setStroke(p);
    }

    @Override
    public ObjectProperty<Paint> strokeProperty()
    {
        return path.strokeProperty();
    }

    @Override
    public double getShapeOpacity()
    {
        return path.getOpacity();
    }

    @Override
    public void setShapeOpacity(double d)
    {
        path.setOpacity(d);
    }

    @Override
    public DoubleProperty shapeOpacityProperty()
    {
        return path.opacityProperty();
    }

    @Override
    public double getStrokeWidth()
    {
        return path.getStrokeWidth();
    }

    @Override
    public void setStrokeWidth(double d)
    {
        path.setStrokeWidth(d);
    }

    @Override
    public DoubleProperty strokeWidthProperty()
    {
        return path.strokeWidthProperty();
    }

    @Override
    public StrokeLineCap getStrokeLineCap()
    {
        return path.getStrokeLineCap();
    }

    @Override
    public void setStrokeLineCap(StrokeLineCap value)
    {
        path.setStrokeLineCap(value);
    }

    @Override
    public ObjectProperty<StrokeLineCap> strokeLineCapProperty()
    {
        return path.strokeLineCapProperty();
    }

    @Override
    public StrokeLineJoin getStrokeLineJoin()
    {
        return path.getStrokeLineJoin();
    }

    @Override
    public void setStrokeLineJoin(StrokeLineJoin value)
    {
        path.setStrokeLineJoin(value);
    }

    @Override
    public ObjectProperty<StrokeLineJoin> strokeLineJoinProperty()
    {
        return path.strokeLineJoinProperty();
    }

    /**
     * Should be use for saving capability
     *
     * @param point
     * @return
     */
    public static Path createFromPoint(List<List<DoubleProperty>> point)
    {
        List<DoubleProperty> firstPoint = point.get(0);
        Path path = new Path(firstPoint.get(0).get(), firstPoint.get(1).get());

        List<DoubleProperty> p = null;
        for (int i = 1; i < point.size() - 1; i++)
        {
            p = point.get(i);
            path.createPoint(p.get(0).get(), p.get(1).get(), p.get(2).get(), p.get(3).get(), p.get(4).get(), p.get(5).get());
        }

        // Check if this is a close path or not
        if ((firstPoint.get(0).get() == p.get(4).get()) && (firstPoint.get(1).get() == p.get(5).get()))
        {
            path.createClosePathEndPoint((MoveTo) path.path.getElements().get(0));
            path.path.getElements().add(new ClosePath());
            path.closed = true;
        }
        else
        {
            p = point.get(point.size() - 1);
            path.createPoint(p.get(0).get(), p.get(1).get(), p.get(2).get(), p.get(3).get(), p.get(4).get(), p.get(5).get());
        }

        return path;
    }

    @Override
    public Path deepCopy()
    {
        List<List<DoubleProperty> > list = getPointDataUnmodifiable();
        
        List<DoubleProperty> firstPoint = list.get(0);
        Path p = new Path(firstPoint.get(0).get(), firstPoint.get(1).get());
        for (int i = 1; i < list.size() - 1; i++)
        {
            List<DoubleProperty> point = list.get(i);
            double x = point.get(0).get(), y = point.get(1).get();
            p.createPoint(x, y, x, y, x, y);
        }

        if (closed)
        {
            p.createClosePathEndPoint((MoveTo) p.path.getElements().get(0));
            p.path.getElements().add(new ClosePath());
            p.closed = true;
        }
        else
        {
            List<DoubleProperty> point = list.get(list.size() - 1);
            double x = point.get(0).get(), y = point.get(1).get();
            p.createPoint(x, y, x, y, x, y);
        }

        return p;
    }

    @Override
    public Path referenceCopy()
    {
        Path p = deepCopy();

        MoveTo m1 = (MoveTo) path.getElements().get(0);
        MoveTo m2 = (MoveTo) p.path.getElements().get(0);
        m1.xProperty().bindBidirectional(m2.xProperty());
        m1.yProperty().bindBidirectional(m2.yProperty());
        for (int i = 1; i < (closed ? getPointDataUnmodifiable().size() - 1 : getPointDataUnmodifiable().size()); i++)
        {
            CubicCurveTo cct1 = (CubicCurveTo) path.getElements().get(i);
            CubicCurveTo cct2 = (CubicCurveTo) p.path.getElements().get(i);
            cct1.controlX1Property().bindBidirectional(cct2.controlX1Property());
            cct1.controlY1Property().bindBidirectional(cct2.controlY1Property());
            cct1.controlX2Property().bindBidirectional(cct2.controlX2Property());
            cct1.controlY2Property().bindBidirectional(cct2.controlY2Property());
            cct1.xProperty().bindBidirectional(cct2.xProperty());
            cct1.yProperty().bindBidirectional(cct2.yProperty());
        }

        return p;
    }

    @Override
    public Path immutableReferenceCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
