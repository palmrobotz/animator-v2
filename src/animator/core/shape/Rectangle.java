
package animator.core.shape;

import animator.core.animation.ShapeProperty;
import animator.core.shape.util.DragHelper;
import animator.core.shape.util.DragHelperCallback;
import static animator.core.shape.util.ShapeTheme.*;
import java.util.Arrays;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.PathBuilder;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

/**
 *
 * @author Nuntipat narkthong
 */
public class Rectangle extends AnimatorShapeBase
{   
    private final javafx.scene.shape.Path rectangle;
    
    private DoubleProperty x;
    private DoubleProperty y;
    private DoubleProperty width;
    private DoubleProperty height;
    
    public Rectangle()
    {
        this(0, 0, 0, 0);
    }

    public Rectangle(double width, double height)
    {
        this(0, 0, width, height);
    }

    // Main Constructor
    public Rectangle(double x, double y, double width, double height)
    {
        this.x = new SimpleDoubleProperty(x);
        this.y = new SimpleDoubleProperty(y);
        this.width = new SimpleDoubleProperty(width);
        this.height = new SimpleDoubleProperty(height);
        
        // Create the path element
        final MoveTo topLeftPoint = new MoveTo();
        topLeftPoint.xProperty().bind(this.x);
        topLeftPoint.yProperty().bind(this.y);
        addPointData(Arrays.asList(topLeftPoint.xProperty(),
                                    topLeftPoint.yProperty()));
        
        final CubicCurveTo topRightPoint = new CubicCurveTo();
        topRightPoint.xProperty().bind(this.x.add(this.width));
        topRightPoint.yProperty().bind(this.y);
        topRightPoint.controlX1Property().bind(this.x);
        topRightPoint.controlY1Property().bind(this.y);
        topRightPoint.controlX2Property().bind(this.x.add(this.width));
        topRightPoint.controlY2Property().bind(this.y);
        addPointData(Arrays.asList(topRightPoint.controlX1Property(),
                                    topRightPoint.controlY1Property(),
                                    topRightPoint.controlX2Property(),
                                    topRightPoint.controlY2Property(),
                                    topRightPoint.xProperty(),
                                    topRightPoint.yProperty()));
        
        
        final CubicCurveTo bottomRightPoint = new CubicCurveTo();
        bottomRightPoint.xProperty().bind(this.x.add(this.width));
        bottomRightPoint.yProperty().bind(this.y.add(this.height));
        bottomRightPoint.controlX1Property().bind(this.x.add(this.width));
        bottomRightPoint.controlY1Property().bind(this.y);
        bottomRightPoint.controlX2Property().bind(this.x.add(this.width));
        bottomRightPoint.controlY2Property().bind(this.y.add(this.height));
        addPointData(Arrays.asList(bottomRightPoint.controlX1Property(),
                                    bottomRightPoint.controlY1Property(),
                                    bottomRightPoint.controlX2Property(),
                                    bottomRightPoint.controlY2Property(),
                                    bottomRightPoint.xProperty(),
                                    bottomRightPoint.yProperty()));
        
                
        final CubicCurveTo bottomLeftPoint = new CubicCurveTo();
        bottomLeftPoint.xProperty().bind(this.x);
        bottomLeftPoint.yProperty().bind(this.y.add(this.height));
        bottomLeftPoint.controlX1Property().bind(this.x.add(this.width));
        bottomLeftPoint.controlY1Property().bind(this.y.add(this.height));
        bottomLeftPoint.controlX2Property().bind(this.x);
        bottomLeftPoint.controlY2Property().bind(this.y.add(this.height));
        addPointData(Arrays.asList(bottomLeftPoint.controlX1Property(),
                                    bottomLeftPoint.controlY1Property(),
                                    bottomLeftPoint.controlX2Property(),
                                    bottomLeftPoint.controlY2Property(),
                                    bottomLeftPoint.xProperty(),
                                    bottomLeftPoint.yProperty()));
                
        final CubicCurveTo returnFirstPoint = new CubicCurveTo();
        returnFirstPoint.xProperty().bind(this.x);
        returnFirstPoint.yProperty().bind(this.y);
        returnFirstPoint.controlX1Property().bind(this.x);
        returnFirstPoint.controlY1Property().bind(this.y.add(this.height));
        returnFirstPoint.controlX2Property().bind(this.x);
        returnFirstPoint.controlY2Property().bind(this.y);
        addPointData(Arrays.asList(returnFirstPoint.controlX1Property(),
                                    returnFirstPoint.controlY1Property(),
                                    returnFirstPoint.controlX2Property(),
                                    returnFirstPoint.controlY2Property(),
                                    returnFirstPoint.xProperty(),
                                    returnFirstPoint.yProperty()));
        
        
        // Create path builder so we can create many path instance
        PathBuilder pathBuilder = PathBuilder.create().elements(topLeftPoint, topRightPoint, bottomRightPoint, bottomLeftPoint,returnFirstPoint, new ClosePath());
        
        // Create the shape
        rectangle = pathBuilder.build();
        
        // Edge point
        
        javafx.scene.shape.Rectangle topLeft = EDGE_POINT_BUILDER.build();
        topLeft.layoutXProperty().bind(this.x);
        topLeft.layoutYProperty().bind(this.y);
        topLeft.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(topLeft, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Rectangle.this.x.set(Rectangle.this.x.get() + deltaX);
                Rectangle.this.width.set(Rectangle.this.width.get() - deltaX);
                
                Rectangle.this.y.set(Rectangle.this.y.get() + deltaY);
                Rectangle.this.height.set(Rectangle.this.height.get() - deltaY);
            }          
        });
        
        javafx.scene.shape.Rectangle topRight = EDGE_POINT_BUILDER.build();
        topRight.layoutXProperty().bind(this.x.add(this.width));
        topRight.layoutYProperty().bind(this.y);
        topRight.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(topRight, new DragHelperCallback() 
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Rectangle.this.width.set(Rectangle.this.width.get() + deltaX);
                
                Rectangle.this.y.set(Rectangle.this.y.get() + deltaY);
                Rectangle.this.height.set(Rectangle.this.height.get() - deltaY);
            }
        });
        
        javafx.scene.shape.Rectangle bottomLeft = EDGE_POINT_BUILDER.build();
        bottomLeft.layoutXProperty().bind(this.x);
        bottomLeft.layoutYProperty().bind(this.y.add(this.height));
        bottomLeft.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(bottomLeft, new DragHelperCallback() 
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Rectangle.this.x.set(Rectangle.this.x.get() + deltaX);
                Rectangle.this.width.set(Rectangle.this.width.get() - deltaX);
                
                Rectangle.this.height.set(Rectangle.this.height.get() + deltaY); 
            }
        });
        
        javafx.scene.shape.Rectangle bottomRight = EDGE_POINT_BUILDER.build();
        bottomRight.layoutXProperty().bind(this.x.add(this.width));
        bottomRight.layoutYProperty().bind(this.y.add(this.height));
        bottomRight.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(bottomRight, new DragHelperCallback() 
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Rectangle.this.width.set(Rectangle.this.width.get() + deltaX);
                Rectangle.this.height.set(Rectangle.this.height.get() + deltaY); 
            }
        });
        
        // Bounding box show when select
        javafx.scene.shape.Path bound = applyBoundTheme(pathBuilder.build());
        bound.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        
        // Registration point
//        javafx.scene.shape.Rectangle regisPoint = REGIS_POINT_BUILDER.build();
//        DragHelper.INSTANCE.makeDragable(bottomRight, new DragHelperCallback() 
//        {
//            @Override
//            public void onDragged(double deltaX, double deltaY)
//            {
//                
//            }
//        });
        
        // Add all element to the group
        getChildren().addAll(rectangle, bound, topLeft, topRight, bottomLeft, bottomRight/*, regisPoint*/);
        
        setSupportProperty(ShapeProperty.SELECTION, ShapeProperty.X_POSITION, ShapeProperty.Y_POSITION
                , ShapeProperty.FILL_COLOR, ShapeProperty.STROKE_COLOR, ShapeProperty.STROKE_WIDTH
                , ShapeProperty.ROTATION, ShapeProperty.ALPHA/*, ShapeProperty.POINT_DATA*/);
    
        initShapeWithShape(rectangle);
    }
    
    public Rectangle(double width, double height, Paint fill)
    {
        this(width, height);
        rectangle.setFill(fill);
    }
   
    @Override
    public AnimatorShape getType()
    {
        return AnimatorShape.RECTANGLE;
    }
    
    public double getWidth()
    {
        return width.get();
    }
    
    public void setWidth(double d)
    {
        width.set(d);
    }
    
    public DoubleProperty widthProperty()
    {
        return width;
    }
    
    public double getHeight()
    {
        return height.get();
    }
    
    public void setHeight(double d)
    {
        height.set(d);
    }
    
    public DoubleProperty heightProperty()
    {
        return height;
    }
    
    @Override
    public ObjectProperty<Paint> fillProperty()
    {
        return rectangle.fillProperty();
    }

    @Override
    public Paint getFill()
    {
        return rectangle.getFill();
    }

    @Override
    public void setFill(Paint p)
    {
        rectangle.setFill(p);
    }
    
    @Override
    public Paint getStroke()
    {
        return rectangle.getStroke();
    }

    @Override
    public void setStroke(Paint p)
    {
        rectangle.setStroke(p);
    }

    @Override
    public ObjectProperty<Paint> strokeProperty()
    {
        return rectangle.strokeProperty();
    }
    
    @Override
    public double getShapeOpacity()
    {
        return rectangle.getOpacity();
    }

    @Override
    public void setShapeOpacity(double d)
    {
        rectangle.setOpacity(d);
    }

    @Override
    public DoubleProperty shapeOpacityProperty()
    {
        return rectangle.opacityProperty();
    }
    
    @Override
    public double getStrokeWidth()
    {
        return rectangle.getStrokeWidth();
    }

    @Override
    public void setStrokeWidth(double d)
    {
        rectangle.setStrokeWidth(d);
    }

    @Override
    public DoubleProperty strokeWidthProperty()
    {
        return rectangle.strokeWidthProperty();
    }

    @Override
    public StrokeLineCap getStrokeLineCap()
    {
        return rectangle.getStrokeLineCap();
    }

    @Override
    public void setStrokeLineCap(StrokeLineCap value)
    {
        rectangle.setStrokeLineCap(value);
    }

    @Override
    public ObjectProperty<StrokeLineCap> strokeLineCapProperty()
    {
        return rectangle.strokeLineCapProperty();
    }

    @Override
    public StrokeLineJoin getStrokeLineJoin()
    {
        return rectangle.getStrokeLineJoin();
    }

    @Override
    public void setStrokeLineJoin(StrokeLineJoin value)
    {
        rectangle.setStrokeLineJoin(value);
    }

    @Override
    public ObjectProperty<StrokeLineJoin> strokeLineJoinProperty()
    {
        return rectangle.strokeLineJoinProperty();
    }

    @Override
    public Rectangle deepCopy()
    {
        return new Rectangle(x.get(), y.get(), width.get(), height.get());
    }

    @Override
    public Rectangle referenceCopy()
    {
        Rectangle r = new Rectangle();
        r.x.bindBidirectional(x);
        r.y.bindBidirectional(y);
        r.width.bindBidirectional(width);
        r.height.bindBidirectional(height);
        r.fillProperty().bindBidirectional(fillProperty());
        return r;
    }

    @Override
    public Rectangle immutableReferenceCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
}
