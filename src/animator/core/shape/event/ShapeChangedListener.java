
package animator.core.shape.event;

/**
 *
 * @author Nuntipat narkthong
 */
public interface ShapeChangedListener
{
    public void onShapeChanged(ShapeChangedEvent e);
}
