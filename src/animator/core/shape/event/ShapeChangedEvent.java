
package animator.core.shape.event;

import animator.core.animation.ShapeProperty;
import animator.core.shape.AnimatorShapeBase;
import java.util.EventObject;

/**
 *
 * @author Nuntipat narkthong
 */
public class ShapeChangedEvent extends EventObject
{
    private final ShapeProperty changedProperty;
    
    public ShapeChangedEvent(AnimatorShapeBase asb, ShapeProperty sp)
    {
        super(asb);
        changedProperty = sp;
    }
    
    @Override
    public AnimatorShapeBase getSource()
    {
        return (AnimatorShapeBase) super.getSource();
    }

    public ShapeProperty getChangedProperty()
    {
        return changedProperty;
    }
}
