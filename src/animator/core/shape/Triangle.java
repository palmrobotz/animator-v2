
package animator.core.shape;

import animator.core.animation.ShapeProperty;
import animator.core.shape.util.DragHelper;
import animator.core.shape.util.DragHelperCallback;
import static animator.core.shape.util.ShapeTheme.*;
import java.util.Arrays;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.PathBuilder;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

/**
 *
 * @author Nuntipat narkthong
 */
public class Triangle extends AnimatorShapeBase
{   
    private final javafx.scene.shape.Path triangle;
    
    private DoubleProperty x;
    private DoubleProperty y;
    private DoubleProperty width;
    private DoubleProperty height;
    
    public Triangle()
    {
        this(0, 0, 0, 0);
    }

    public Triangle(double width, double height)
    {
        this(0, 0, width, height);
    }

    // Main Constructor
    public Triangle(double x, double y, double width, double height)
    {   
        this.x = new SimpleDoubleProperty(x);
        this.y = new SimpleDoubleProperty(y);
        this.width = new SimpleDoubleProperty(width);
        this.height = new SimpleDoubleProperty(height);
        
        // Create the path element
        final MoveTo topMiddlePoint = new MoveTo();
        topMiddlePoint.xProperty().bind((this.x.add(width)).divide(2));
        topMiddlePoint.yProperty().bind(this.y);
        addPointData(Arrays.asList(topMiddlePoint.xProperty(),topMiddlePoint.yProperty()));
        
        final CubicCurveTo bottomRightPoint = new CubicCurveTo();
        bottomRightPoint.xProperty().bind(this.x.add(this.width));
        bottomRightPoint.yProperty().bind(this.y.add(this.height));
        bottomRightPoint.controlX1Property().bind(this.x.add(width).divide(2));
        bottomRightPoint.controlY1Property().bind(this.y);
        bottomRightPoint.controlX2Property().bind(this.x.add(this.width));
        bottomRightPoint.controlY2Property().bind(this.y.add(this.height));
        addPointData(Arrays.asList(bottomRightPoint.xProperty(),
                                    bottomRightPoint.yProperty(),
                                    bottomRightPoint.controlX1Property(),
                                    bottomRightPoint.controlY1Property(),
                                    bottomRightPoint.controlX2Property(),
                                    bottomRightPoint.controlY2Property()));
        
        final CubicCurveTo bottomLeftPoint = new CubicCurveTo();
        bottomLeftPoint.xProperty().bind(this.x);
        bottomLeftPoint.yProperty().bind(this.y.add(this.height));
        bottomLeftPoint.controlX1Property().bind(this.x.add(this.width));
        bottomLeftPoint.controlY1Property().bind(this.y.add(this.height));
        bottomLeftPoint.controlX2Property().bind(this.x);
        bottomLeftPoint.controlY2Property().bind(this.y.add(this.height));
        addPointData(Arrays.asList(bottomLeftPoint.xProperty(),
                                    bottomLeftPoint.yProperty(),
                                    bottomLeftPoint.controlX1Property(),
                                    bottomLeftPoint.controlY1Property(),
                                    bottomLeftPoint.controlX2Property(),
                                    bottomLeftPoint.controlY2Property()));
                
        final CubicCurveTo returnFirstPoint = new CubicCurveTo();
        returnFirstPoint.xProperty().bind(this.x.add(width).divide(2));
        returnFirstPoint.yProperty().bind(this.y);
        returnFirstPoint.controlX1Property().bind(this.x);
        returnFirstPoint.controlY1Property().bind(this.y.add(this.height));
        returnFirstPoint.controlX2Property().bind(this.x.add(width).divide(2));
        returnFirstPoint.controlY2Property().bind(this.y);
        addPointData(Arrays.asList(returnFirstPoint.xProperty(),
                                    returnFirstPoint.yProperty(),
                                    returnFirstPoint.controlX1Property(),
                                    returnFirstPoint.controlY1Property(),
                                    returnFirstPoint.controlX2Property(),
                                    returnFirstPoint.controlY2Property()));
        
        
        // Create path builder so we can create many path instance
        PathBuilder pathBuilder = PathBuilder.create().elements(topMiddlePoint, bottomRightPoint, bottomLeftPoint,returnFirstPoint, new ClosePath());
        
        // Create the shape
        triangle = pathBuilder.build();
        
        // Edge point
        
        javafx.scene.shape.Rectangle topLeft = EDGE_POINT_BUILDER.build();
        topLeft.layoutXProperty().bind(this.x);
        topLeft.layoutYProperty().bind(this.y);
        topLeft.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(topLeft, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Triangle.this.x.set(Triangle.this.x.get() + deltaX);
                Triangle.this.width.set(Triangle.this.width.get() - deltaX);
                
                Triangle.this.y.set(Triangle.this.y.get() + deltaY);
                Triangle.this.height.set(Triangle.this.height.get() - deltaY);
                
            }          
        });
        
        javafx.scene.shape.Rectangle topRight = EDGE_POINT_BUILDER.build();
        topRight.layoutXProperty().bind(this.x.add(this.width));
        topRight.layoutYProperty().bind(this.y);
        topRight.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(topRight, new DragHelperCallback() 
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Triangle.this.width.set(Triangle.this.width.get() + deltaX);
                Triangle.this.x.set(Triangle.this.x.get() + deltaX);
                Triangle.this.y.set(Triangle.this.y.get() + deltaY);
                Triangle.this.height.set(Triangle.this.height.get() - deltaY);
            }
        });
        
        javafx.scene.shape.Rectangle bottomLeft = EDGE_POINT_BUILDER.build();
        bottomLeft.layoutXProperty().bind(this.x);
        bottomLeft.layoutYProperty().bind(this.y.add(this.height));
        bottomLeft.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(bottomLeft, new DragHelperCallback() 
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Triangle.this.x.set(Triangle.this.x.get() + deltaX);
                Triangle.this.width.set(Triangle.this.width.get() - deltaX);
                
                Triangle.this.height.set(Triangle.this.height.get() + deltaY); 
            }
        });
        
        javafx.scene.shape.Rectangle bottomRight = EDGE_POINT_BUILDER.build();
        bottomRight.layoutXProperty().bind(this.x.add(this.width));
        bottomRight.layoutYProperty().bind(this.y.add(this.height));
        bottomRight.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(bottomRight, new DragHelperCallback() 
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Triangle.this.width.set(Triangle.this.width.get() + deltaX);
                Triangle.this.height.set(Triangle.this.height.get() + deltaY);
            }
        });
        
        // Bounding box show when select
        javafx.scene.shape.Rectangle bound = applyBoundTheme(new javafx.scene.shape.Rectangle(x, y, width, height));
        bound.xProperty().bind(this.x);
        bound.yProperty().bind(this.y);
        bound.widthProperty().bind(this.width);
        bound.heightProperty().bind(this.height);
        bound.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        
        // Add all element to the group
        getChildren().addAll(triangle, bound, topLeft, topRight, bottomLeft, bottomRight);
        
        setSupportProperty(ShapeProperty.SELECTION, ShapeProperty.X_POSITION, ShapeProperty.Y_POSITION
                , ShapeProperty.FILL_COLOR, ShapeProperty.STROKE_COLOR, ShapeProperty.STROKE_WIDTH
                , ShapeProperty.ROTATION, ShapeProperty.ALPHA/*, ShapeProperty.POINT_DATA*/);
    
        initShapeWithShape(triangle);
    }
    
    public Triangle(double width, double height, Paint fill)
    {
        this(width, height);
        triangle.setFill(fill);
    }
    
    @Override
    public AnimatorShape getType()
    {
        return AnimatorShape.TRIANGLE;
    }
   
    public double getWidth()
    {
        return width.get();
    }
    
    public void setWidth(double d)
    {
        width.set(d);
    }
    
    public DoubleProperty widthProperty()
    {
        return width;
    }
    
    public double getHeight()
    {
        return height.get();
    }
    
    public void setHeight(double d)
    {
        height.set(d);
    }
    
    public DoubleProperty heightProperty()
    {
        return height;
    }
    
    @Override
    public ObjectProperty<Paint> fillProperty()
    {
        return triangle.fillProperty();
    }

    @Override
    public Paint getFill()
    {
        return triangle.getFill();
    }

    @Override
    public void setFill(Paint p)
    {
        triangle.setFill(p);
    }
    
    @Override
    public Paint getStroke()
    {
        return triangle.getStroke();
    }

    @Override
    public void setStroke(Paint p)
    {
        triangle.setStroke(p);
    }

    @Override
    public ObjectProperty<Paint> strokeProperty()
    {
        return triangle.strokeProperty();
    }
    
    @Override
    public double getShapeOpacity()
    {
        return triangle.getOpacity();
    }

    @Override
    public void setShapeOpacity(double d)
    {
        triangle.setOpacity(d);
    }

    @Override
    public DoubleProperty shapeOpacityProperty()
    {
        return triangle.opacityProperty();
    }
    
    @Override
    public double getStrokeWidth()
    {
        return triangle.getStrokeWidth();
    }

    @Override
    public void setStrokeWidth(double d)
    {
        triangle.setStrokeWidth(d);
    }

    @Override
    public DoubleProperty strokeWidthProperty()
    {
        return triangle.strokeWidthProperty();
    }

    @Override
    public StrokeLineCap getStrokeLineCap()
    {
        return triangle.getStrokeLineCap();
    }

    @Override
    public void setStrokeLineCap(StrokeLineCap value)
    {
        triangle.setStrokeLineCap(value);
    }

    @Override
    public ObjectProperty<StrokeLineCap> strokeLineCapProperty()
    {
        return triangle.strokeLineCapProperty();
    }

    @Override
    public StrokeLineJoin getStrokeLineJoin()
    {
        return triangle.getStrokeLineJoin();
    }

    @Override
    public void setStrokeLineJoin(StrokeLineJoin value)
    {
        triangle.setStrokeLineJoin(value);
    }

    @Override
    public ObjectProperty<StrokeLineJoin> strokeLineJoinProperty()
    {
        return triangle.strokeLineJoinProperty();
    }

    @Override
    public Triangle deepCopy()
    {
        return new Triangle(x.get(), y.get(), width.get(), height.get());
    }

    @Override
    public Triangle referenceCopy()
    {
        Triangle t = new Triangle();
        t.x.bindBidirectional(x);
        t.y.bindBidirectional(y);
        t.width.bindBidirectional(width);
        t.height.bindBidirectional(height);
        return t;
    }

    @Override
    public Triangle immutableReferenceCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
}
