
package animator.core.shape;

import animator.core.animation.ShapeProperty;
import animator.core.shape.util.DragHelper;
import animator.core.shape.util.DragHelperCallback;
import static animator.core.shape.util.ShapeTheme.*;
import java.util.Arrays;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.PathBuilder;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

/**
 *
 * @author Nuntipat narkthong
 */
public class Circle extends AnimatorShapeBase
{   
    private final javafx.scene.shape.Path circle;
    
    private DoubleProperty x;
    private DoubleProperty y;
    private DoubleProperty width;
    private DoubleProperty height;
    
    public Circle()
    {
        this(0, 0, 0, 0);
    }

    public Circle(double width, double height)
    {
        this(0, 0, width, height);
    }

    // Main Constructor
    public Circle(double x, double y, double width, double height)
    {
        double kappa=0.5522847498307933984022516322796; 
        this.x = new SimpleDoubleProperty(x);
        this.y = new SimpleDoubleProperty(y);
        this.width = new SimpleDoubleProperty(width);
        this.height = new SimpleDoubleProperty(height);
        
        // Create the path element
        final MoveTo left = new MoveTo();
        left.xProperty().bind(this.x);
        left.yProperty().bind(this.y.add(this.height.divide(2)));//y+(height/2)
        addPointData(Arrays.asList(left.xProperty(),
                                    left.yProperty()));
        
        final CubicCurveTo top = new CubicCurveTo();
        top.xProperty().bind(this.x.add(this.width.divide(2)));
        top.yProperty().bind(this.y);
        top.controlX1Property().bind(this.x);
        top.controlY1Property().bind(this.y.add(this.height.divide(2)).subtract(this.height.multiply(kappa).divide(2)));//y+(height/2)-(height*kappa/2)
        top.controlX2Property().bind(this.x.add(this.width.divide(2)).subtract(this.width.multiply(kappa).divide(2)));//x+(width/2)-(width*kappa/2)
        top.controlY2Property().bind(this.y);
        addPointData(Arrays.asList(top.controlX1Property(),
                                    top.controlY1Property(),
                                    top.controlX2Property(),
                                    top.controlY2Property(),
                                    top.xProperty(),
                                    top.yProperty()));
        
        
        final CubicCurveTo right = new CubicCurveTo();
        right.xProperty().bind(this.x.add(this.width));
        right.yProperty().bind(this.y.add(this.height.divide(2)));
        right.controlX1Property().bind(this.x.add(this.width.divide(2)).add(this.width.multiply(kappa).divide(2)));//x+(width/2)+(width*kappa/2)
        right.controlY1Property().bind(this.y);
        right.controlX2Property().bind(this.x.add(this.width));
        right.controlY2Property().bind(this.y.add(this.height.divide(2)).subtract(this.height.multiply(kappa).divide(2)));//y+(height/2)-(height*kappa/2)
        addPointData(Arrays.asList(right.controlX1Property(),
                                    right.controlY1Property(),
                                    right.controlX2Property(),
                                    right.controlY2Property(),
                                    right.xProperty(),
                                    right.yProperty()));
        
                
        final CubicCurveTo bottom = new CubicCurveTo();
        bottom.xProperty().bind(this.x.add(this.width.divide(2)));
        bottom.yProperty().bind(this.y.add(this.height));
        bottom.controlX1Property().bind(this.x.add(this.width));
        bottom.controlY1Property().bind(this.y.add(this.height.divide(2)).add(this.height.multiply(kappa).divide(2)));//y+(height/2)+(height*kappa/2)
        bottom.controlX2Property().bind(this.x.add(this.width.divide(2)).add(this.width.multiply(kappa).divide(2)));//x+(width/2)+(width*kappa/2)
        bottom.controlY2Property().bind(this.y.add(this.height));
        addPointData(Arrays.asList(bottom.controlX1Property(),
                                    bottom.controlY1Property(),
                                    bottom.controlX2Property(),
                                    bottom.controlY2Property(),
                                    bottom.xProperty(),
                                    bottom.yProperty()));
                
        final CubicCurveTo returnFirstPoint = new CubicCurveTo();
        returnFirstPoint.xProperty().bind(this.x);
        returnFirstPoint.yProperty().bind(this.y.add(this.height.divide(2)));//y+(height/2)
        returnFirstPoint.controlX1Property().bind(this.x.add(this.width.divide(2)).subtract(this.width.multiply(kappa).divide(2)));//x+(width/2)-(width*kappa/2)
        returnFirstPoint.controlY1Property().bind(this.y.add(this.height));
        returnFirstPoint.controlX2Property().bind(this.x);
        returnFirstPoint.controlY2Property().bind(this.y.add(this.height.divide(2)).add(this.height.multiply(kappa).divide(2)));//y+(height/2)+(height*kappa/2)
        addPointData(Arrays.asList(returnFirstPoint.controlX1Property(),
                                    returnFirstPoint.controlY1Property(),
                                    returnFirstPoint.controlX2Property(),
                                    returnFirstPoint.controlY2Property(),
                                    returnFirstPoint.xProperty(),
                                    returnFirstPoint.yProperty()));
        
        
        // Create path builder so we can create many path instance
        PathBuilder pathBuilder = PathBuilder.create().elements(left, top, right, bottom,returnFirstPoint, new ClosePath());
        
        // Create the shape
        circle = pathBuilder.build();
        
        // Edge point
        
        javafx.scene.shape.Rectangle topLeft = EDGE_POINT_BUILDER.build();
        topLeft.layoutXProperty().bind(this.x);
        topLeft.layoutYProperty().bind(this.y);
        topLeft.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(topLeft, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Circle.this.x.set(Circle.this.x.get() + deltaX);
                Circle.this.width.set(Circle.this.width.get() - deltaX);
                
                Circle.this.y.set(Circle.this.y.get() + deltaY);
                Circle.this.height.set(Circle.this.height.get() - deltaY);
            }          
        });
        
        javafx.scene.shape.Rectangle topRight = EDGE_POINT_BUILDER.build();
        topRight.layoutXProperty().bind(this.x.add(this.width));
        topRight.layoutYProperty().bind(this.y);
        topRight.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(topRight, new DragHelperCallback() 
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Circle.this.width.set(Circle.this.width.get() + deltaX);
                
                Circle.this.y.set(Circle.this.y.get() + deltaY);
                Circle.this.height.set(Circle.this.height.get() - deltaY);
            }
        });
        
        javafx.scene.shape.Rectangle bottomLeft = EDGE_POINT_BUILDER.build();
        bottomLeft.layoutXProperty().bind(this.x);
        bottomLeft.layoutYProperty().bind(this.y.add(this.height));
        bottomLeft.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(bottomLeft, new DragHelperCallback() 
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Circle.this.x.set(Circle.this.x.get() + deltaX);
                Circle.this.width.set(Circle.this.width.get() - deltaX);
                
                Circle.this.height.set(Circle.this.height.get() + deltaY); 
            }
        });
        
        javafx.scene.shape.Rectangle bottomRight = EDGE_POINT_BUILDER.build();
        bottomRight.layoutXProperty().bind(this.x.add(this.width));
        bottomRight.layoutYProperty().bind(this.y.add(this.height));
        bottomRight.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(bottomRight, new DragHelperCallback() 
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                Circle.this.width.set(Circle.this.width.get() + deltaX);
                Circle.this.height.set(Circle.this.height.get() + deltaY); 
            }
        });
        
        // Bounding box show when select
        javafx.scene.shape.Path bound = applyBoundTheme(pathBuilder.build());
        bound.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        
        // Registration point
//        javafx.scene.shape.Rectangle regisPoint = REGIS_POINT_BUILDER.build();
//        DragHelper.INSTANCE.makeDragable(bottomRight, new DragHelperCallback() 
//        {
//            @Override
//            public void onDragged(double deltaX, double deltaY)
//            {
//                
//            }
//        });
        
        // Add all element to the group
        getChildren().addAll(circle, bound, topLeft, topRight, bottomLeft, bottomRight/*, regisPoint*/);
        
        setSupportProperty(ShapeProperty.SELECTION, ShapeProperty.X_POSITION, ShapeProperty.Y_POSITION
                , ShapeProperty.FILL_COLOR, ShapeProperty.STROKE_COLOR, ShapeProperty.STROKE_WIDTH
                , ShapeProperty.ROTATION, ShapeProperty.ALPHA/*, ShapeProperty.POINT_DATA*/);
    
        initShapeWithShape(circle);
    }
    
    public Circle(double width, double height, Paint fill)
    {
        this(width, height);
        circle.setFill(fill);
    }
   
    @Override
    public AnimatorShape getType()
    {
        return AnimatorShape.CIRCLE;
    }
    
    public double getWidth()
    {
        return width.get();
    }
    
    public void setWidth(double d)
    {
        width.set(d);
    }
    
    public DoubleProperty widthProperty()
    {
        return width;
    }
    
    public double getHeight()
    {
        return height.get();
    }
    
    public void setHeight(double d)
    {
        height.set(d);
    }
    
    public DoubleProperty heightProperty()
    {
        return height;
    }
    
    @Override
    public ObjectProperty<Paint> fillProperty()
    {
        return circle.fillProperty();
    }

    @Override
    public Paint getFill()
    {
        return circle.getFill();
    }

    @Override
    public void setFill(Paint p)
    {
        circle.setFill(p);
    }
    
    @Override
    public Paint getStroke()
    {
        return circle.getStroke();
    }

    @Override
    public void setStroke(Paint p)
    {
        circle.setStroke(p);
    }

    @Override
    public ObjectProperty<Paint> strokeProperty()
    {
        return circle.strokeProperty();
    }
    
    @Override
    public double getShapeOpacity()
    {
        return circle.getOpacity();
    }

    @Override
    public void setShapeOpacity(double d)
    {
        circle.setOpacity(d);
    }

    @Override
    public DoubleProperty shapeOpacityProperty()
    {
        return circle.opacityProperty();
    }
    
    @Override
    public double getStrokeWidth()
    {
        return circle.getStrokeWidth();
    }

    @Override
    public void setStrokeWidth(double d)
    {
        circle.setStrokeWidth(d);
    }

    @Override
    public DoubleProperty strokeWidthProperty()
    {
        return circle.strokeWidthProperty();
    }

    @Override
    public StrokeLineCap getStrokeLineCap()
    {
        return circle.getStrokeLineCap();
    }

    @Override
    public void setStrokeLineCap(StrokeLineCap value)
    {
        circle.setStrokeLineCap(value);
    }

    @Override
    public ObjectProperty<StrokeLineCap> strokeLineCapProperty()
    {
        return circle.strokeLineCapProperty();
    }

    @Override
    public StrokeLineJoin getStrokeLineJoin()
    {
        return circle.getStrokeLineJoin();
    }

    @Override
    public void setStrokeLineJoin(StrokeLineJoin value)
    {
        circle.setStrokeLineJoin(value);
    }

    @Override
    public ObjectProperty<StrokeLineJoin> strokeLineJoinProperty()
    {
        return circle.strokeLineJoinProperty();
    }

    @Override
    public Circle deepCopy()
    {
        return new Circle(x.get(), y.get(), width.get(), height.get());
    }

    @Override
    public Circle referenceCopy()
    {
        Circle r = new Circle();
        r.x.bindBidirectional(x);
        r.y.bindBidirectional(y);
        r.width.bindBidirectional(width);
        r.height.bindBidirectional(height);
        r.fillProperty().bindBidirectional(fillProperty());
        return r;
    }

    @Override
    public Circle immutableReferenceCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
