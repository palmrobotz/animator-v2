
package animator.core.shape;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.animation.Interpolatable;
import javafx.animation.Interpolator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 *
 * @author Nuntipat narkthong
 */
public class PointData implements Interpolatable<PointData>
{
    private final List<List<DoubleProperty>> point;
    private final List<List<Double>> pointDeepCopy;
    
    public static final PointData DUMMY_POINT_DATA = new PointData(Collections.<List<DoubleProperty>>emptyList());
    public static final ObjectProperty<PointData> DUMMY_POINT_DATA_PROPERTY = new SimpleObjectProperty<>(DUMMY_POINT_DATA);
    
    public PointData(List<List<DoubleProperty>> point)
    {
        this.point = point;
        this.pointDeepCopy = new ArrayList<>();
        
        for (int i=0; i<point.size(); i++)
        {
            List<Double> list = new ArrayList<>();
            
            for (int j=0; j<point.get(i).size(); j++)
            {
                list.add(point.get(i).get(j).doubleValue());
            }
            
            pointDeepCopy.add(list);
        }
    }
    
    public List<List<Double>> getPointList()
    {
        return pointDeepCopy;
    }
    
    public List<List<DoubleProperty>> getPoint()
    {
        return point;
    }
    
    @Override
    public PointData interpolate(PointData t, double d)
    {
        for (int i=0; i<pointDeepCopy.size(); i++)
        {
            for (int j=0; j<pointDeepCopy.get(i).size(); j++)
            {
                double interpolate = Interpolator.LINEAR.interpolate(pointDeepCopy.get(i).get(j).doubleValue()
                        , t.pointDeepCopy.get(i).get(j).doubleValue(), d);
                point.get(i).get(j).set(interpolate);
            }
        }
        
        return DUMMY_POINT_DATA;
    }
}
