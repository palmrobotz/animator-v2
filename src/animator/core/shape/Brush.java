
package animator.core.shape;

import animator.core.animation.ShapeProperty;
import animator.core.shape.util.FitBezierCurve;
import static animator.core.shape.util.ShapeTheme.*;
import animator.ui.Tool;
import animator.ui.ToolBox;
import java.util.Arrays;
import java.util.List;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.PathElement;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

/**
 *
 * @author Nuntipat narkthong
 */
public class Brush extends AnimatorShapeBase
{   
    private FitBezierCurve fitCurve = new FitBezierCurve();
    private final javafx.scene.shape.Path path = fitCurve.getPath();
    
    private ObservableList<PathElement> pathElement = null;
    
    private boolean finish = false;
    
    private final ReadOnlyObjectProperty<Tool> toolSelected = ToolBox.getInstance().toolSelectProperty();
    
    public Brush(double x, double y)
    {
        javafx.scene.shape.Path boundingPath = applyBoundTheme(new javafx.scene.shape.Path());
        Bindings.bindContentBidirectional(path.getElements(), boundingPath.getElements());
        
        /* Create first point */
        final MoveTo moveTo = new MoveTo(x, y);
        fitCurve.addPoint(new Point2D(x, y));
        
        /* Initialize shape */
        boundingPath.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        getChildren().addAll(path, boundingPath);
    
        /* Recieve forward mouse event when this object is being selected */
        addEventHandler(MouseEvent.ANY, new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent event)
            {
                if (!finish && toolSelected.getValue() == Tool.BRUSH && event.getEventType() == MouseEvent.MOUSE_DRAGGED)
                {
                    double x = event.getX(), y = event.getY();
                    fitCurve.addPoint(new Point2D(x, y));
                    event.consume();
                }
                else if (!finish && event.getEventType() == MouseEvent.MOUSE_RELEASED)
                {
                    // Lazy instantiate the path element
                    if (pathElement == null)
                    {
                        pathElement = path.getElements();
                        
                        clearPointData();
        
                        MoveTo mt = (MoveTo) pathElement.get(0);
                        addPointData(Arrays.asList((DoubleProperty) new SimpleDoubleProperty(mt.getX())
                                , (DoubleProperty) new SimpleDoubleProperty(mt.getY())));

                        for (int i=1; i<pathElement.size(); i++)
                        {
                            CubicCurveTo cct = (CubicCurveTo) pathElement.get(i);
                            addPointData(Arrays.asList((DoubleProperty) new SimpleDoubleProperty(cct.getControlX1())
                                    , (DoubleProperty) new SimpleDoubleProperty(cct.getControlY1())
                                    , (DoubleProperty) new SimpleDoubleProperty(cct.getControlX2())
                                    , (DoubleProperty) new SimpleDoubleProperty(cct.getControlY2())
                                    , (DoubleProperty) new SimpleDoubleProperty(cct.getX())
                                    , (DoubleProperty) new SimpleDoubleProperty(cct.getY()))
                                    );
                        }
                    }
                    
                    finish = true;
                    setDraggable(true);
                    fitCurve = null;
                }
            }
        });
        
        setDraggable(false);
        setSupportProperty(ShapeProperty.SELECTION, ShapeProperty.X_POSITION, ShapeProperty.Y_POSITION
                , ShapeProperty.FILL_COLOR, ShapeProperty.STROKE_COLOR, ShapeProperty.STROKE_WIDTH
                , ShapeProperty.ROTATION, ShapeProperty.ALPHA/*, ShapeProperty.POINT_DATA*/);
    
        initShapeWithShape(path);
    }
    
    private void createPoint(double cx1, double cy1, double cx2, double cy2, double x, double y)
    {        
        final CubicCurveTo cubicTo = new CubicCurveTo(cx1, cy1, cx2, cy2, x, y);
        path.getElements().add(cubicTo);
    }
    
    @Override
    public AnimatorShape getType()
    {
        return AnimatorShape.BRUSH;
    }
    
    @Override
    public Paint getFill()
    {
        return path.getFill();
    }

    @Override
    public void setFill(Paint p)
    {
        path.setFill(p);
    }

    @Override
    public ObjectProperty<Paint> fillProperty()
    {
        return path.fillProperty();
    }

    @Override
    public Paint getStroke()
    {
        return path.getStroke();
    }

    @Override
    public void setStroke(Paint p)
    {
        path.setStroke(p);
    }

    @Override
    public ObjectProperty<Paint> strokeProperty()
    {
        return path.strokeProperty();
    }

    @Override
    public double getShapeOpacity()
    {
        return path.getOpacity();
    }

    @Override
    public void setShapeOpacity(double d)
    {
        path.setOpacity(d);
    }

    @Override
    public DoubleProperty shapeOpacityProperty()
    {
        return path.opacityProperty();
    }
    
    @Override
    public double getStrokeWidth()
    {
        return path.getStrokeWidth();
    }

    @Override
    public void setStrokeWidth(double d)
    {
        path.setStrokeWidth(d);
    }

    @Override
    public DoubleProperty strokeWidthProperty()
    {
        return path.strokeWidthProperty();
    }

    @Override
    public StrokeLineCap getStrokeLineCap()
    {
        return path.getStrokeLineCap();
    }

    @Override
    public void setStrokeLineCap(StrokeLineCap value)
    {
        path.setStrokeLineCap(value);
    }

    @Override
    public ObjectProperty<StrokeLineCap> strokeLineCapProperty()
    {
        return path.strokeLineCapProperty();
    }

    @Override
    public StrokeLineJoin getStrokeLineJoin()
    {
        return path.getStrokeLineJoin();
    }

    @Override
    public void setStrokeLineJoin(StrokeLineJoin value)
    {
        path.setStrokeLineJoin(value);
    }

    @Override
    public ObjectProperty<StrokeLineJoin> strokeLineJoinProperty()
    {
        return path.strokeLineJoinProperty();
    }
    
    /**
     * Should be use for saving capability
     * @param point
     * @return 
     */
    public static Brush createFromPoint(List<List<DoubleProperty> > point)
    {
        List<DoubleProperty> firstPoint = point.get(0);
        Brush b = new Brush(firstPoint.get(0).get(), firstPoint.get(1).get());
        
        List<DoubleProperty> p;
        for (int i=1; i<point.size(); i++)
        {
            p = point.get(i);
            b.createPoint(p.get(0).get(), p.get(1).get()
                    , p.get(2).get(), p.get(3).get()
                    , p.get(4).get(), p.get(5).get());
        }
        
        return b;
    }

    @Override
    public Brush deepCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public Brush referenceCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Brush immutableReferenceCopy()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
