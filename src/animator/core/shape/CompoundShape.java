
package animator.core.shape;

import animator.core.animation.ShapeProperty;
import javafx.beans.property.DoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;

/**
 *
 * @author Nuntipat narkthong
 */
public class CompoundShape extends AnimatorShapeBaseAdapter
{
    // @TODO Just to block this class from being used now
    private CompoundShape()
    {
        setSelect(true);
        setSupportProperty(ShapeProperty.SELECTION, ShapeProperty.X_POSITION, ShapeProperty.Y_POSITION);
    }

    public void addShape(AnimatorShapeBase asb)
    {
        super.getChildren().add(asb);
        asb.selectProperty().bindBidirectional(selectProperty());
    }
    
    public boolean removeShape(AnimatorShapeBase asb)
    {
        asb.selectProperty().unbindBidirectional(selectProperty());
        return super.getChildren().remove(asb);
    }
    
    @Override
    public ObservableList<Node> getChildren()
    {
        System.err.println("Shouldn't call this method");
        return super.getChildren();
        //throw new UnsupportedOperationException("Shouldn't edit the underly data manually");
    }
   
    @Override
    public CompoundShape referenceCopy()
    {
        CompoundShape shape = new CompoundShape();
        for (Node asb : getChildrenUnmodifiable())
        {
            if (asb instanceof AnimatorShapeBase)
                shape.addShape(((AnimatorShapeBase) asb).referenceCopy());
            else
                throw new IllegalStateException("Some shape is not an instance of AnimatorShapeBase");
        }
        return shape;
    }
}
