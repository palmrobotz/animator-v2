
package animator.core.shape.util;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.ImageTranscoder;

/**
 *
 * @author Xephrt
 */
public class SVGConverter
{
    private Image image;

    public SVGConverter(String svgFile, float width, float height) throws FileNotFoundException
    {
        FileInputStream fileIn = new FileInputStream(svgFile);

        SVGConverter.SVGtoImgeTranscoder transcoder = new SVGConverter.SVGtoImgeTranscoder();
        transcoder.addTranscodingHint(ImageTranscoder.KEY_WIDTH, width);
        transcoder.addTranscodingHint(ImageTranscoder.KEY_HEIGHT, height);

        TranscoderInput input = new TranscoderInput(fileIn);
        try
        {
            transcoder.transcode(input, null);
        }
        catch (TranscoderException ex)
        {
            System.err.println("TranscoderException throw some unknown error.");
        }

        BufferedImage bimage = transcoder.getImage();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try
        {
            ImageIO.write((RenderedImage) bimage, "png", out);
        }
        catch (IOException ex)
        {
            Logger.getLogger(SVGConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        try
        {
            out.flush();
        }
        catch (IOException ex)
        {
            Logger.getLogger(SVGConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        image = new javafx.scene.image.Image(in);
    }

    public Image getImage()
    {
        return image;
    }

    private class SVGtoImgeTranscoder extends ImageTranscoder
    {
        private BufferedImage image = null;

        @Override
        public BufferedImage createImage(int width, int height)
        {
            image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            return image;
        }

        @Override
        public void writeImage(BufferedImage bi, TranscoderOutput to) throws TranscoderException
        {
            throw new UnsupportedOperationException("Not supported writeImahe yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public BufferedImage getImage()
        {
            return image;
        }
    }
}
