
package animator.core.shape.util;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.LineBuilder;
import javafx.scene.shape.RectangleBuilder;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeType;

/**
 *
 * @author Nuntipat narkthong
 */
public final class ShapeTheme
{
    // Suppress default constructor for non instantiability
    private ShapeTheme()
    {
        throw new InstantiationError("Shouldn't create an instance of this class");
    }
    
    public static final int EDGE_POINT_DIMEN = 5;
    public static final int REGIS_POINT_DIMEN = 5;
    public static final int LINE_WIDTH = 1;
    public static final RectangleBuilder<?> EDGE_POINT_BUILDER = RectangleBuilder.create()
                                                                .x(- EDGE_POINT_DIMEN / 2)
                                                                .y(- EDGE_POINT_DIMEN / 2)
                                                                .width(EDGE_POINT_DIMEN)
                                                                .height(EDGE_POINT_DIMEN)
                                                                .stroke(Color.DARKBLUE)
                                                                .fill(Color.BLUE);
    public static final RectangleBuilder<?> REGIS_POINT_BUILDER = RectangleBuilder.create()
                                                                .x(- REGIS_POINT_DIMEN / 2)
                                                                .y(- REGIS_POINT_DIMEN / 2)
                                                                .width(REGIS_POINT_DIMEN)
                                                                .height(REGIS_POINT_DIMEN)
                                                                .stroke(Color.DARKBLUE)
                                                                .fill(Color.ALICEBLUE)
                                                                .rotate(45);
    public static final LineBuilder<?> LINE_BUILDER = LineBuilder.create()
            .startX(EDGE_POINT_DIMEN)
            .startY(EDGE_POINT_DIMEN)
            .endX(EDGE_POINT_DIMEN)
            .endY(EDGE_POINT_DIMEN)
            .stroke(Color.DARKBLUE)
            .strokeWidth(LINE_WIDTH);
    
                                                                
    
    public static final Paint BOUND_STROKE_COLOR = Color.BLUE;
    
    public static <T extends Shape> T applyBoundTheme(T n)
    {
        n.setStroke(BOUND_STROKE_COLOR);
        n.setStrokeType(StrokeType.OUTSIDE);
        n.setFill(Color.TRANSPARENT);
        return n;
    }
}
