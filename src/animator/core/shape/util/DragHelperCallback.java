
package animator.core.shape.util;

/**
 *
 * @author Nuntipat narkthong
 */
public interface DragHelperCallback
{
    public void onDragged(double deltaX, double deltaY);
}
