package animator.core.shape.util;

import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Point2D;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

/**
 *
 * @author Nattaphat
 */
public class FitBezierCurve
{
    private List<Node> bezier = new ArrayList<>();
    private Path path = new Path();

    //add point to bezier path and auto curve-fitting
    public void addPoint(Point2D p)
    {
        Node newNode = new Node(p);
        if (bezier.isEmpty())
        {
            newNode.id = 0;
        }
        else
        {
            newNode.id = bezier.get(bezier.size() - 1).id + 1;
        }
        bezier.add(newNode);
        fitCurve();
    }

    //get node from given index 
    public Node get(int index)
    {
        return bezier.get(index);
    }

    //get size of path
    public int size()
    {
        return bezier.size();
    }

//    //clear path
//    public void clear()
//    {
//        bezier.clear();
//    }

    //get Path from bezier path
    public Path getPath()
    {        
        return path;
    }

    public void smooth(int smoothness)
    {
        for (int i = 0; i < bezier.size(); i++)
        {
            while (i < bezier.size() && bezier.get(i).id % smoothness != 0)
            {
                bezier.remove(i);

            }
        }
        for (int k = 1; k < bezier.size() - 1; k++)
        {
            calculateControlPoint(k);
        }

    }

    private void fitCurve()
    {
        if (bezier.size() >= 3)
        {
            int i = bezier.size();
            calculateControlPoint(i - 2);
        }
    }

    private void calculateControlPoint(int index)
    {
        Node current = bezier.get(index);

        Point2D p1 = bezier.get(index - 1).anchor;    //previous point
        Point2D p2 = bezier.get(index).anchor;        // current point - point that we want to generate control point
        Point2D p3 = bezier.get(index + 1).anchor;    // next point
        //find point that locate at the center of p1p2 and p2p3
        Point2D p1p2 = center(p1, p2);
        Point2D p2p3 = center(p2, p3);
        double ratio = p1.distance(p2.getX(), p2.getY()) / (p1.distance(p2.getX(), p2.getY()) + p2.distance(p3.getX(), p3.getY()));
        Point2D Q = pointWithRatio(p1p2, p2p3, ratio);
        double leftDiffx = p1p2.getX() - Q.getX();
        double leftDiffy = p1p2.getY() - Q.getY();
        double rightDiffx = p2p3.getX() - Q.getX();
        double rightDiffy = p2p3.getY() - Q.getY();

        Point2D leftControl = new Point2D(p2.getX() + leftDiffx, p2.getY() + leftDiffy);
        Point2D rightControl = new Point2D(p2.getX() + rightDiffx, p2.getY() + rightDiffy);

        current.leftControl = leftControl;
        current.rightControl = rightControl;
        
        if(index-1==0)//first point
        {
            path.getElements().add(new MoveTo(bezier.get(0).anchor.getX(), bezier.get(0).anchor.getY()));
        }
        
        addPointToPath(bezier.get(index-1),bezier.get(index));
    }
    
    private void addPointToPath(Node previous, Node current)
    {
        //if previous have no right control
            if (previous.rightControl == null)
            {
                //if current have no left control
                if (current.leftControl == null)
                {
                    path.getElements().add(new LineTo(current.anchor.getX(), current.anchor.getY()));
                }
                else
                {//current have left control
                    //curve.getElements().add(new QuadCurveTo(current.leftControl.getX(), current.leftControl.getY(), current.anchor.getX(), current.anchor.getY()));
                    path.getElements().add(new CubicCurveTo(current.leftControl.getX(), current.leftControl.getY(), current.leftControl.getX(), current.leftControl.getY(), current.anchor.getX(), current.anchor.getY()));
                }
            }
            else
            {
                //previous have right control
                //if current have no left control
                if (current.leftControl == null)
                {
                    //curve.getElements().add(new QuadCurveTo(previous.rightControl.getX(), previous.rightControl.getY(), current.anchor.getX(), current.anchor.getY()));
                    path.getElements().add(new CubicCurveTo(previous.rightControl.getX(), previous.rightControl.getY(), previous.rightControl.getX(), previous.rightControl.getY(), current.anchor.getX(), current.anchor.getY()));
                }
                else
                {
                    //current have left control
                    path.getElements().add(new CubicCurveTo(previous.rightControl.getX(), previous.rightControl.getY()
                            , current.leftControl.getX(), current.leftControl.getY(), current.anchor.getX(), current.anchor.getY()));
                }
            }
    }

    private Point2D center(Point2D p1, Point2D p2)
    {
        Point2D center = new Point2D(((p2.getX() + p1.getX()) / 2), ((p2.getY() + p1.getY()) / 2));
        return center;
    }

    private Point2D pointWithRatio(Point2D p1, Point2D p2, double r)
    {
        //error here
        double deltaX = p2.getX() - p1.getX();
        double deltaY = p2.getY() - p1.getY();
     
        Point2D p = new Point2D(p1.getX() + (deltaX * r), p1.getY() + (deltaY * r));
        return p;
    }

    private static class Node
    {
        private Point2D anchor;
        private Point2D leftControl;
        private Point2D rightControl;
        private int id;

        public Node(Point2D p)
        {
            anchor = p;
        }
    }
}
