
package animator.core.shape.util;

import animator.core.shape.AnimatorShapeBase;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Nuntipat narkthong
 */
public class DragHelper
{
    public static final DragHelper INSTANCE = new DragHelper();
        
    // Use for drag capability
    private double initialX = 0, initialY = 0;
    private double initialDragX = 0, initialDragY = 0;
    private double deltaX = 0, deltaY = 0;
    
    // Suppress default constructor for non-instantiability
    private DragHelper()
    {
        
    }
    
    /**
     * 
     * @param n 
     */
    public void makeNodeDragable(Node n)
    {
        // Prevent duplicate handler
        n.removeEventHandler(MouseEvent.MOUSE_PRESSED, nodePressed);
        n.removeEventHandler(MouseEvent.MOUSE_DRAGGED, nodeDragged);
        
        // Detect a mouse click and record as initial position
        n.addEventHandler(MouseEvent.MOUSE_PRESSED, nodePressed);
        // Detect a mouse drag and find drag delta then save change
        n.addEventHandler(MouseEvent.MOUSE_DRAGGED, nodeDragged);
    }
    
    public void makeNodeNonDragable(Node n)
    {
        // Detect a mouse click and record as initial position
        n.removeEventHandler(MouseEvent.MOUSE_PRESSED, nodePressed);
        // Detect a mouse drag and find drag delta then save change
        n.removeEventHandler(MouseEvent.MOUSE_DRAGGED, nodeDragged);
    }
    
    public void makeNodeDragable(Node n, final DragHelperCallback callback)
    {
        // Detect a mouse click and record as initial position
        n.addEventHandler(MouseEvent.MOUSE_PRESSED, nodePressed);
        // Detect a mouse drag and find drag delta then save change
        n.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                //System.err.println("Drag");
                deltaX = t.getX() - initialDragX;
                deltaY = t.getY() - initialDragY;

                callback.onDragged(deltaX, deltaY);
                t.consume();
            }   
        });
    }
    
    /**
     * 
     * @param n
     * @param handler the event handler created by {@code createNodeDragEventHandler(DragHelperCallback)}
     */
    public void makeNodeDragable(Node n, EventHandler<MouseEvent> handler)
    {
        // Prevent duplicate handler
        n.removeEventHandler(MouseEvent.MOUSE_PRESSED, nodePressed);
        n.removeEventHandler(MouseEvent.MOUSE_DRAGGED, handler);
        
        // Detect a mouse click and record as initial position
        n.addEventHandler(MouseEvent.MOUSE_PRESSED, nodePressed);
        // Detect a mouse drag and find drag delta then save change
        n.addEventHandler(MouseEvent.MOUSE_DRAGGED, handler);
    }
    
    /**
     * 
     * @param n
     * @param handler the event handler created by {@code createNodeDragEventHandler(DragHelperCallback)}
     * this should be the same instance that is used when call {@code makeNodeDragable(Node, EventHandler<MouseEvent>)}
     */
    public void makeNodeNonDragable(Node n, EventHandler<MouseEvent> handler)
    {
        n.removeEventHandler(MouseEvent.MOUSE_PRESSED, nodePressed);
        n.removeEventHandler(MouseEvent.MOUSE_DRAGGED, handler);
    } 
    
    /**
     * Create the EventHandler for use with {@code makeNodeDragable(Node, EventHandler<MouseEvent>)}
     * and {@code makeNodeNonDragable(Node, EventHandler<MouseEvent>)}
     * @param callback
     * @return the event handler created
     */
    public EventHandler<MouseEvent> createNodeDragEventHandler(final DragHelperCallback callback)
    {
        return new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                //System.err.println("Drag");
                deltaX = t.getX() - initialDragX;
                deltaY = t.getY() - initialDragY;

                callback.onDragged(deltaX, deltaY);
                t.consume();
            }   
        };
    }
    
    private EventHandler<MouseEvent> nodePressed = new EventHandler<MouseEvent>()
    {
        @Override
        public void handle(MouseEvent t)
        {
            Node n = (Node) t.getSource();
            //System.err.println("Drag press");
            initialX = n.getLayoutX();
            initialY = n.getLayoutY();

            initialDragX = t.getX();
            initialDragY = t.getY();

            deltaX = 0;
            deltaY = 0;
            
            t.consume();
        }   
    };
    
    private EventHandler<MouseEvent> nodeDragged = new EventHandler<MouseEvent>()
    {
        @Override
        public void handle(MouseEvent t)
        {
            //System.err.println("Drag drag");
            deltaX = t.getX() - initialDragX;
            deltaY = t.getY() - initialDragY;

            ((Node) t.getSource()).setLayoutX(initialX + deltaX);
            ((Node) t.getSource()).setLayoutY(initialY + deltaY);
            
            t.consume();
        }   
    };
    
    /**
     * 
     * @param n 
     */
    public void makeAnimatorShapeDragable(AnimatorShapeBase n)
    {
        // Prevent duplicate handler
        n.removeEventHandler(MouseEvent.MOUSE_PRESSED, shapePressed);
        n.removeEventHandler(MouseEvent.MOUSE_DRAGGED, shapeDragged);
        
        // Detect a mouse click and record as initial position
        n.addEventHandler(MouseEvent.MOUSE_PRESSED, shapePressed);
        // Detect a mouse drag and find drag delta then save change
        n.addEventHandler(MouseEvent.MOUSE_DRAGGED, shapeDragged);
    }
    
    public void makeAnimatorShapeNonDragable(AnimatorShapeBase n)
    {
        // Detect a mouse click and record as initial position
        n.removeEventHandler(MouseEvent.MOUSE_PRESSED, shapePressed);
        // Detect a mouse drag and find drag delta then save change
        n.removeEventHandler(MouseEvent.MOUSE_DRAGGED, shapeDragged);
    }
    
    private EventHandler<MouseEvent> shapePressed = new EventHandler<MouseEvent>()
    {
        @Override
        public void handle(MouseEvent t)
        {
            AnimatorShapeBase n = (AnimatorShapeBase) t.getSource();
            //System.err.println("Drag press");
            initialX = n.getX();
            initialY = n.getY();

            initialDragX = t.getX();
            initialDragY = t.getY();

            deltaX = 0;
            deltaY = 0;
            
            t.consume();
        }   
    };
    
    private EventHandler<MouseEvent> shapeDragged = new EventHandler<MouseEvent>()
    {
        @Override
        public void handle(MouseEvent t)
        {
            //System.err.println("Drag drag");
            deltaX = t.getX() - initialDragX;
            deltaY = t.getY() - initialDragY;

            ((AnimatorShapeBase) t.getSource()).setX(initialX + deltaX);
            ((AnimatorShapeBase) t.getSource()).setY(initialY + deltaY);
            
            t.consume();
        }   
    };
}
