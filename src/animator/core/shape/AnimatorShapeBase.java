
package animator.core.shape;

import animator.core.animation.AbstractAnimationModel;
import animator.core.animation.ShapeProperty;
import animator.core.shape.event.ShapeChangedEvent;
import animator.core.shape.event.ShapeChangedListener;
import animator.core.shape.util.DragHelper;
import animator.ui.Tool;
import animator.ui.ToolBox;
import animator.ui.preview.Previewable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;

/**
 *
 * @author Nuntipat narkthong
 */
public abstract class AnimatorShapeBase extends Group
{
    private final StringProperty name = new SimpleStringProperty("Untitled");
    private final BooleanProperty select = new SimpleBooleanProperty(false);
    private final BooleanProperty drawBound = new SimpleBooleanProperty(true);
    private final BooleanProperty draggable = new SimpleBooleanProperty(true);
 
    /* layoutX, layoutY, rotate, opacity setter of the object shouldn't be call directly 
     * this property should be set via x, y, rotation, alpha */
    private final DoubleProperty x = new SimpleDoubleProperty(0);
    private final DoubleProperty y = new SimpleDoubleProperty(0);
    private final DoubleProperty rotation = new SimpleDoubleProperty(0);
    private final DoubleProperty alpha = new SimpleDoubleProperty(1);
    private final DoubleProperty offsetX = new SimpleDoubleProperty(0);
    private final DoubleProperty offsetY = new SimpleDoubleProperty(0);
    private final DoubleProperty offsetRotation = new SimpleDoubleProperty(0);
    private final DoubleProperty offsetAlpha = new SimpleDoubleProperty(1);
    
    /* Bezier path of the shape, use for shape that support tween shape */
    private final List<List<DoubleProperty>> pointData = new ArrayList<>();
    private final List<List<DoubleProperty>> unmodifiablePointData = Collections.unmodifiableList(pointData);
    
    private final EnumSet<ShapeProperty> supportProperty = EnumSet.noneOf(ShapeProperty.class);
    private final Set<ShapeProperty> unmodifiableSupportProperty = Collections.unmodifiableSet(supportProperty);
    private final List<ShapeProperty> animableSupportProperty = new ArrayList<>();
    private final List<ShapeProperty> unmodifiableAnimableSupportProperty = Collections.unmodifiableList(animableSupportProperty);

    private AbstractAnimationModel animationModel;
    private final List<ShapeChangedListener> changedListener = new CopyOnWriteArrayList<>();
    private final ObjectProperty<Previewable> previewWindow = new SimpleObjectProperty<>(null);
    
    private final ToolBox toolSelected = ToolBox.getInstance();
    
    public AnimatorShapeBase()
    {
        // Bind layout x and y, rotate, opacity
        layoutXProperty().bind(x.add(offsetX));
        layoutYProperty().bind(y.add(offsetY));
        rotateProperty().bind(rotation.add(offsetRotation));
        //System.err.println("Don't forget to fix alpha");
        //shapeOpacityProperty().bind(alpha.add(offsetAlpha));

        toolSelected.toolSelectProperty().addListener(new ChangeListener<Tool>()
        {
            @Override
            public void changed(ObservableValue<? extends Tool> ov, Tool t, Tool t1)
            {
                if (isDraggable() && toolSelected.getToolSelect() == Tool.SELECT)
                {
                    DragHelper.INSTANCE.makeAnimatorShapeDragable(AnimatorShapeBase.this);
                }
                else
                {
                    DragHelper.INSTANCE.makeAnimatorShapeNonDragable(AnimatorShapeBase.this);
                }
            }
        });
        draggable.addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1)
            {
                if (t1.booleanValue())
                    DragHelper.INSTANCE.makeAnimatorShapeDragable(AnimatorShapeBase.this);
                else
                    DragHelper.INSTANCE.makeAnimatorShapeNonDragable(AnimatorShapeBase.this);
            }
        });
        
        if (isDraggable() && toolSelected.getToolSelect() == Tool.SELECT)
        {
            DragHelper.INSTANCE.makeAnimatorShapeDragable(AnimatorShapeBase.this);
        }
        
        // @TODO This code is not working in brush
        // Make this shape dragable and detect drag
//        BooleanBinding drag = toolSelected.isEqualTo(Tool.SELECT).and(draggable);
//        drag.addListener(new ChangeListener<Boolean>()
//        {
//            @Override
//            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)
//            {
//                System.out.println(AnimatorShapeBase.this + " Drag and tool change from " + oldValue + " to " + newValue);
//                if (newValue.booleanValue())
//                    DragHelper.INSTANCE.makeAnimatorShapeDragable(AnimatorShapeBase.this);
//                else
//                    DragHelper.INSTANCE.makeAnimatorShapeNonDragable(AnimatorShapeBase.this);
//            }  
//        });
    }
    
    /**
     * Every subclass of this class should call this method
     * @param s the {@code javafx.scene.shape} instance of the AnimatorShapeBase or
     * null if the implementation doesn't contain any shape such as {@code RasterImage}
     */
    protected void initShapeWithShape(Shape s)
    {
        s.setStrokeType(StrokeType.INSIDE);
        s.opacityProperty().bind(alpha.multiply(offsetAlpha));
    }
    
    /**
     * Every subclass of this class should call this method
     * @param s the {@code javafx.scene.shape} instance of the AnimatorShapeBase or
     * null if the implementation doesn't contain any shape such as {@code RasterImage}
     */
    protected void initShapeWithNode(Node n)
    {
        n.opacityProperty().bind(alpha.multiply(offsetAlpha));
    }
    
    public final String getName()
    {
        return name.get();
    }
    
    public final void setName(String s)
    {
        name.set(s);
    }
    
    public final StringProperty nameProperty()
    {
        return name;
    }
        
    public boolean isSelect()
    {
        return select.get();
    }

    public void setSelect(boolean b)
    {
        select.set(b);
    }

    public BooleanProperty selectProperty()
    {
        return select;
    }

    public final boolean isDrawBound()
    {
        return drawBound.get();
    }
    
    public final void setDrawBound(boolean b)
    {
        drawBound.set(b);
    }
    
    public final BooleanProperty drawBoundProperty()
    {
        return drawBound;
    }
    
    public final boolean isDraggable()
    {
        return draggable.get();
    }
    
    public final void setDraggable(boolean b)
    {
        draggable.set(b);
    }
    
    public final BooleanProperty draggableProperty()
    {
        return draggable;
    }
    
    public double getX()
    {
        return x.get();
    }
    
    public void setX(double d)
    {
        x.set(d);
    }
    
    public DoubleProperty xProperty()
    {
        return x;
    }
    
    public double getY()
    {
        return y.get();
    }
    
    public void setY(double d)
    {
        y.set(d);
    }
    
    public DoubleProperty yProperty()
    {
        return y;
    }
    
    public double getOffsetX()
    {
        return offsetX.get();
    }
    
    public void setOffsetX(double d)
    {
        offsetX.set(d);
    }
    
    public DoubleProperty offsetXProperty()
    {
        return offsetX;
    }
    
    public double getOffsetY()
    {
        return offsetY.get();
    }
    
    public void setOffsetY(double d)
    {
        offsetY.set(d);
    }
    
    public DoubleProperty offsetYProperty()
    {
        return offsetY;
    }
    
    public double getRotation()
    {
        return rotation.get();
    }
    
    public void setRotation(double d)
    {
        rotation.set(d);
    }
    
    public DoubleProperty rotationProperty()
    {
        return rotation;
    }
    
    public double getOffsetRotation()
    {
        return offsetRotation.get();
    }
    
    public void setOffsetRotation(double d)
    {
        offsetRotation.set(d);
    }
    
    public DoubleProperty offsetRotationProperty()
    {
        return offsetRotation;
    }
    
    public double getAlpha()
    {
        return alpha.get();
    }
    
    public void setAlpha(double d)
    {
        alpha.set(d);
    }
    
    public DoubleProperty alphaProperty()
    {
        return alpha;
    }
    
    public double getOffsetAlpha()
    {
        return offsetAlpha.get();
    }
    
    public void setOffsetAlpha(double d)
    {
        offsetAlpha.set(d);
    }
    
    public DoubleProperty offsetAlphaProperty()
    {
        return offsetAlpha;
    }
    
//    public final List<List<DoubleProperty>> getPointData()
//    {
//        return pointData;
//    }
    
    public final void addPointData(List<DoubleProperty> e)
    {
        addControlPointPropertyListener(e);
        pointData.add(e);
        fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.POINT_DATA));
    }
    
    public final void addPointData(int index, List<DoubleProperty> e)
    {
        addControlPointPropertyListener(e);
        pointData.add(index, e);
        fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.POINT_DATA));
    }
    
    public final boolean removePointData(List<DoubleProperty> e)
    {
        if (pointData.remove(e))
        {
            removeControlPointPropertyListener(e);
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.POINT_DATA));
            return true;
        }
        
        return false;
   }
    
    public final List<DoubleProperty> removePointData(int index)
    {
        List<DoubleProperty> list = pointData.remove(index);
        removeControlPointPropertyListener(list);
        fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.POINT_DATA));
        return list;
    }
    
    public void clearPointData()
    {
        for (List<DoubleProperty> list : pointData)
        {
            removeControlPointPropertyListener(list);
        }
        pointData.clear();
        fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.POINT_DATA));
    }
    
    public final List<List<DoubleProperty>> getPointDataUnmodifiable()
    {
        return unmodifiablePointData;
    }
    
    public final List<List<DoubleProperty>> getPointDataModifiable()
    {
        return pointData;
    }
    
    
    private void addControlPointPropertyListener(List<DoubleProperty> e)
    {
        for (DoubleProperty dp : e)
        {
            dp.addListener(controlPointChangedListener);
        }
    }
    
    private void removeControlPointPropertyListener(List<DoubleProperty> e)
    {
        for (DoubleProperty dp : e)
        {
            dp.removeListener(controlPointChangedListener);
        }
    }
    
    /** 
     * Fire event so that the KeyTime will be create to record path change 
     */
    private ChangeListener<Number> controlPointChangedListener = new ChangeListener<Number>()
    {
        @Override
        public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
        {
            if (getSupportProperty().contains(ShapeProperty.POINT_DATA))
                fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.POINT_DATA));
        }
    };
    
    public final AbstractAnimationModel getAnimationModel()
    {
        return animationModel;
    }
    
    public final void setAnimationModel(AbstractAnimationModel model)
    {
        animationModel = model;
        
        // Not allow changing the shape (disable) when the layer is locked
        // This behavior is only when the shape is in some AbstractAnimationModel
        if (animationModel != null)
        {
            // Not accept any mouse event when the layer is locked
            disableProperty().bind(model.getLayerModel().lockProperty());
        }
        else
        {
            // Shape not in the AnimationModel so we don't bind the lock property
            disableProperty().unbind();
        }
    }
    
    public Previewable getPreviewWindow()
    {
        return previewWindow.get();
    }
    
    public void setPreviewWindow(Previewable previewable)
    {
        previewWindow.set(previewable);
    }
    
    public ObjectProperty<Previewable> previewWindowProperty()
    {
        return previewWindow;
    }
    
    public void addShapeChangedListener(ShapeChangedListener listener)
    {
        changedListener.add(listener);
    }
    
    public void removeShapeChangedListener(ShapeChangedListener listener)
    {
        changedListener.remove(listener);
    }
    
    protected void fireShapeChanged(ShapeChangedEvent e)
    {
//        ShapeChangedListener[] tmp = changedListener.toArray(new ShapeChangedListener[0]);
//        for (ShapeChangedListener listener : tmp)
//        {
//            listener.onShapeChanged(e);
//        }
        // Not fire shape changed event if shape is not selected
        if (select.get())
        {
            for (ShapeChangedListener listener : changedListener)
            {
                listener.onShapeChanged(e);
            }
        }
    }
    
    private ChangeListener<Boolean> selectChangedListener = new ChangeListener<Boolean>()
    {
        @Override
        public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1)
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.SELECTION));
        }   
    };
    private ChangeListener<Paint> fillChangedListener = new ChangeListener<Paint>() 
    {
        @Override
        public void changed(ObservableValue<? extends Paint> ov, Paint t, Paint t1)
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.FILL_COLOR));
        }
    };
    private ChangeListener<Paint> strokeColorChangedListener = new ChangeListener<Paint>() 
    {
        @Override
        public void changed(ObservableValue<? extends Paint> ov, Paint t, Paint t1)
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.STROKE_COLOR));
        }
    };
    private ChangeListener<Number> strokeWidthChangedListener = new ChangeListener<Number>() 
    {
        @Override
        public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.STROKE_WIDTH));
        }
    };
    private ChangeListener<Number> xChangedListener = new ChangeListener<Number>()
    {
        @Override
        public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.X_POSITION));
        }          
    };
    private ChangeListener<Number> yChangedListener = new ChangeListener<Number>()
    {
        @Override
        public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.Y_POSITION));
        }          
    };
    private ChangeListener<Number> offsetXChangedListener = new ChangeListener<Number>()
    {
        @Override
        public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.OFFSET_X_POSITION));
        }          
    };
    private ChangeListener<Number> offsetYChangedListener = new ChangeListener<Number>()
    {
        @Override
        public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.OFFSET_Y_POSITION));
        }          
    };
    private ChangeListener<Number> rotateChangedListener = new ChangeListener<Number>()
    {
        @Override
        public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.ROTATION));
        }          
    };
    private ChangeListener<Number> alphaChangedListener = new ChangeListener<Number>()
    {
        @Override
        public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
        {
            fireShapeChanged(new ShapeChangedEvent(AnimatorShapeBase.this, ShapeProperty.ALPHA));
        }          
    };
    private EventHandler<MouseEvent> shapePress = new EventHandler<MouseEvent>()
    {
        // @TODO Change select layer in timeline
        @Override
        public void handle(MouseEvent t)
        {
            select.set(true);
            
            // Consume the event so the event is not continue traverse in the event
            // dispatch chain
            // For example when the shape is in a preview window if we not consume, we
            // will not be able to select (select then deselect)
            t.consume();
        }
    };
    private EventHandler<MouseEvent> shapeRelease = new EventHandler<MouseEvent>() 
    {
        @Override
        public void handle(MouseEvent t)
        {
            // Just want the shape to consume this event so this event is not propergate
            // to the preview screen
            t.consume();
        }
    };

    public Set<ShapeProperty> getSupportProperty()
    {
        return unmodifiableSupportProperty;
    }

    public List<ShapeProperty> getAnimableSupportProperty()
    {
        return unmodifiableAnimableSupportProperty;
    }
    
    /**
     * Subclass should call this method to set the desired support property.
     * Override this method to public to allow user change this setting
     * @param property property to support
     */
    protected void setSupportProperty(ShapeProperty... property)
    {
        clearEvent();
        
        supportProperty.clear();
        supportProperty.addAll(Arrays.asList(property));
        
        // We need to maintain order to make AnimationController work properly
        animableSupportProperty.clear();
        animableSupportProperty.addAll(ShapeProperty.getAnimableProperty());
        animableSupportProperty.retainAll(supportProperty);
        
        initialEvent();
    }
    
    private void initialEvent()
    {
        if (supportProperty.contains(ShapeProperty.SELECTION))
        {
            addEventHandler(MouseEvent.MOUSE_PRESSED, shapePress);
            addEventHandler(MouseEvent.MOUSE_RELEASED, shapeRelease);
            select.addListener(selectChangedListener);
        }
        if (supportProperty.contains(ShapeProperty.FILL_COLOR))
            fillProperty().addListener(fillChangedListener);
        if (supportProperty.contains(ShapeProperty.STROKE_COLOR))
            strokeProperty().addListener(strokeColorChangedListener);
        if (supportProperty.contains(ShapeProperty.STROKE_WIDTH))
            strokeWidthProperty().addListener(strokeWidthChangedListener);
        if (supportProperty.contains(ShapeProperty.X_POSITION))
            xProperty().addListener(xChangedListener);
        if (supportProperty.contains(ShapeProperty.Y_POSITION))
            yProperty().addListener(yChangedListener);
        if (supportProperty.contains(ShapeProperty.OFFSET_X_POSITION))
            offsetXProperty().addListener(offsetXChangedListener);
        if (supportProperty.contains(ShapeProperty.OFFSET_Y_POSITION))
            offsetYProperty().addListener(offsetYChangedListener);
        if (supportProperty.contains(ShapeProperty.ROTATION))
            rotateProperty().addListener(rotateChangedListener);
        if (supportProperty.contains(ShapeProperty.ALPHA))
            shapeOpacityProperty().addListener(alphaChangedListener);
        
        // Point data property will maintain itself
    }
    
    private void clearEvent()
    {
        if (supportProperty.contains(ShapeProperty.SELECTION))
        {
            removeEventHandler(MouseEvent.MOUSE_PRESSED, shapePress);
            removeEventHandler(MouseEvent.MOUSE_RELEASED, shapeRelease);
            select.removeListener(selectChangedListener);
        }
        if (supportProperty.contains(ShapeProperty.FILL_COLOR))
            fillProperty().removeListener(fillChangedListener);
        if (supportProperty.contains(ShapeProperty.STROKE_COLOR))
            strokeProperty().removeListener(strokeColorChangedListener);
        if (supportProperty.contains(ShapeProperty.STROKE_WIDTH))
            strokeWidthProperty().removeListener(strokeWidthChangedListener);
        if (supportProperty.contains(ShapeProperty.X_POSITION))
            xProperty().removeListener(xChangedListener);
        if (supportProperty.contains(ShapeProperty.Y_POSITION))
            yProperty().removeListener(yChangedListener);
        if (supportProperty.contains(ShapeProperty.OFFSET_X_POSITION))
            offsetXProperty().removeListener(offsetXChangedListener);
        if (supportProperty.contains(ShapeProperty.OFFSET_Y_POSITION))
            offsetYProperty().removeListener(offsetYChangedListener);
        if (supportProperty.contains(ShapeProperty.ROTATION))
            rotateProperty().removeListener(rotateChangedListener);
        if (supportProperty.contains(ShapeProperty.ALPHA))
            shapeOpacityProperty().removeListener(alphaChangedListener);
        
        // Point data property will maintain itself
    }
    
    public abstract AnimatorShape getType();
    
    public abstract Paint getFill();
    public abstract void setFill(Paint p);
    public abstract ObjectProperty<Paint> fillProperty();
    
    public abstract Paint getStroke();
    public abstract void setStroke(Paint p);
    public abstract ObjectProperty<Paint> strokeProperty();
    
    /* We can not use opacity property of the group directly like we do with rotation
     * because the bounding etc. shoule not get opacity with the shape */
    public abstract double getShapeOpacity();
    public abstract void setShapeOpacity(double d);
    public abstract DoubleProperty shapeOpacityProperty();
    
    public abstract double getStrokeWidth();
    public abstract void setStrokeWidth(double d);
    public abstract DoubleProperty strokeWidthProperty();
    
    public abstract StrokeLineCap getStrokeLineCap();
    public abstract void setStrokeLineCap(StrokeLineCap value);
    public abstract ObjectProperty<StrokeLineCap> strokeLineCapProperty();
    
    public abstract StrokeLineJoin getStrokeLineJoin();
    public abstract void setStrokeLineJoin(StrokeLineJoin value);
    public abstract ObjectProperty<StrokeLineJoin> strokeLineJoinProperty();
    
    public abstract AnimatorShapeBase deepCopy();
    public abstract AnimatorShapeBase referenceCopy();
    public abstract AnimatorShapeBase immutableReferenceCopy();

}
