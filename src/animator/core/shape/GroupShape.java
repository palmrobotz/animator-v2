
package animator.core.shape;

import animator.core.animation.ShapeProperty;
import animator.core.shape.util.DragHelper;
import animator.core.shape.util.DragHelperCallback;
import animator.core.shape.util.ShapeTheme;
import static animator.core.shape.util.ShapeTheme.applyBoundTheme;
import animator.ui.Tool;
import animator.ui.ToolBox;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 *
 * @author Nuntipat narkthong
 */
public class GroupShape extends AnimatorShapeBaseAdapter
{   
    // Can't not use getChildren().xxx since we don't want to show it on screen
    // instead we just want to keep the instance to the shape
    private List<AnimatorShapeBase> shape = new ArrayList<>();
    
    private javafx.scene.shape.Rectangle boundRect;
    private javafx.scene.shape.Rectangle registrationPoint;
    
    private static final int REGIS_WIDTH = 4;
    
    // Injected property
    private final ToolBox toolSelected = ToolBox.getInstance();
    
    // Use for derigister the handler
    private EventHandler<MouseEvent> dragEventHandler = 
            DragHelper.INSTANCE.createNodeDragEventHandler(new DragHelperCallback()
    {
        @Override
        public void onDragged(double deltaX, double deltaY)
        {
            setOffsetX(getOffsetX() + deltaX);
            setOffsetY(getOffsetY() + deltaY);
        }         
    });
    
    public GroupShape()
    {
        // Bounding box show when select
        boundRect = applyBoundTheme(new javafx.scene.shape.Rectangle());
        boundRect.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        
        final javafx.scene.shape.Rectangle body = new javafx.scene.shape.Rectangle();
        //body.setFill(Color.color(0.5, 0.5, 0.5, 0.5));
        body.setFill(Color.TRANSPARENT);
        body.xProperty().bind(boundRect.xProperty());
        body.yProperty().bind(boundRect.yProperty());
        body.widthProperty().bind(boundRect.widthProperty());
        body.heightProperty().bind(boundRect.heightProperty());
        
        registrationPoint = new javafx.scene.shape.Rectangle();
        registrationPoint.setWidth(REGIS_WIDTH);
        registrationPoint.setHeight(REGIS_WIDTH);
        registrationPoint.setFill(ShapeTheme.BOUND_STROKE_COLOR);
        registrationPoint.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        
        getChildren().addAll(body, boundRect, registrationPoint);
        
        // Toggle between drag and undragable normally this is handle by AnimationShapeBase
        // however we use custom drag algorithm so we need to handle it
        
        toolSelected.toolSelectProperty().addListener(new ChangeListener<Tool>()
        {
            @Override
            public void changed(ObservableValue<? extends Tool> ov, Tool t, Tool t1)
            {
                if (toolSelected.getToolSelect() == Tool.SELECT)
                {
                    DragHelper.INSTANCE.makeNodeDragable(GroupShape.this, dragEventHandler);
                }
                else
                {
                    DragHelper.INSTANCE.makeNodeNonDragable(GroupShape.this, dragEventHandler);
                }
            }
        });
        
        if (toolSelected.getToolSelect() == Tool.SELECT)
        {
            DragHelper.INSTANCE.makeNodeDragable(GroupShape.this, dragEventHandler);
        }
        
        setDraggable(false);
        setSupportProperty(ShapeProperty.SELECTION, ShapeProperty.OFFSET_X_POSITION, ShapeProperty.OFFSET_Y_POSITION
                , ShapeProperty.OFFSET_ROTATION, ShapeProperty.OFFSET_ALPHA);
    }
    
    public void addShape(AnimatorShapeBase asb)
    {
        asb.offsetXProperty().bind(offsetXProperty());
        asb.offsetYProperty().bind(offsetYProperty());
        asb.offsetRotationProperty().bind(offsetRotationProperty());
        asb.offsetAlphaProperty().bind(offsetAlphaProperty());
        
        // Not allow mouse event to pass through every children when the group is accepting mouse event
        asb.mouseTransparentProperty().bind(mouseTransparentProperty().not());
        
        asb.boundsInParentProperty().addListener(boundsChange);
        shape.add(asb);
        calculateBound();
    }
    
    public void removeShape(AnimatorShapeBase asb)
    {
        if (shape.remove(asb))
        {
            asb.offsetXProperty().unbind();
            asb.offsetYProperty().unbind();
            asb.offsetRotationProperty().unbind();
            asb.offsetAlphaProperty().unbind();

            asb.mouseTransparentProperty().unbind();
            asb.boundsInParentProperty().removeListener(boundsChange);
            
            asb.setOffsetX(0);
            asb.setOffsetY(0);
            
            calculateBound();
        }
    }
    
    private ChangeListener<Bounds> boundsChange = new ChangeListener<Bounds>()
    {
        @Override
        public void changed(ObservableValue<? extends Bounds> ov, Bounds t, Bounds t1)
        {
            calculateBound();
        }
    };
    
    private void calculateBound()
    {
        Bounds b = shape.get(0).getBoundsInParent();
        
        double x1 = b.getMinX();
        double x2 = b.getMaxX();
        double y1 = b.getMinY();
        double y2 = b.getMaxY();
        
        for (AnimatorShapeBase asb : shape)
        {
            b = asb.getBoundsInParent();
            
            x1 = Math.min(x1, b.getMinX());
            x2 = Math.max(x2, b.getMaxX());
            y1 = Math.min(y1, b.getMinY());
            y2 = Math.max(y2, b.getMaxY());
        }
        
        // The object bind it's offset x/y with this GroupShape when we calculate
        // bound we will get bound that include the offset. The GroupShape also
        // has it's own offset so we need to subtract offset from the bound so
        // we won't get double offset
        setX(x1 - getOffsetX());
        setY(y1 - getOffsetY());
        boundRect.setWidth(x2-x1);
        boundRect.setHeight(y2-y1);
        registrationPoint.setX((x2 - x1- REGIS_WIDTH) / 2);
        registrationPoint.setY((y2 - y1- REGIS_WIDTH) / 2);
    }
}
