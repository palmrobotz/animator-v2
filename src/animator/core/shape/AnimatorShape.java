package animator.core.shape;

import javafx.scene.paint.Paint;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

/**
 *
 * @author Nuntipat narkthong
 */
public enum AnimatorShape
{

    RECTANGLE
    {
        @Override
        public AnimatorShapeBase createShape(double x, double y, double width, double height, Paint stroke, Paint fill, StrokeLineJoin joint, StrokeLineCap cap, Double strokeSize)
        {
            Rectangle r = new Rectangle(x, y, width, height);
            r.setFill(fill);
            r.setStroke(stroke);
            r.setStrokeWidth(strokeSize);
            r.setStrokeLineCap(cap);
            r.setStrokeLineJoin(joint);
            return r;
        }
    },
    TRIANGLE
    {
        @Override
        public AnimatorShapeBase createShape(double x, double y, double width, double height, Paint stroke, Paint fill, StrokeLineJoin joint, StrokeLineCap cap, Double strokeSize)
        {
            Triangle t = new Triangle(x, y, width, height);
            t.setFill(fill);
            t.setStroke(stroke);
            t.setStrokeWidth(strokeSize);
            t.setStrokeLineCap(cap);
            t.setStrokeLineJoin(joint);
            return t;
        }
    },
    CIRCLE
    {
        @Override
        public AnimatorShapeBase createShape(double x, double y, double width, double height, Paint stroke, Paint fill, StrokeLineJoin joint, StrokeLineCap cap, Double strokeSize)
        {
            Circle r = new Circle(x, y, width, height);
            r.setFill(fill);
            r.setStroke(stroke);
            r.setStrokeWidth(strokeSize);
            r.setStrokeLineCap(cap);
            r.setStrokeLineJoin(joint);
            return r;
        }
    },
    PATH
    {
        @Override
        public AnimatorShapeBase createShape(double x, double y, double width, double height, Paint stroke, Paint fill, StrokeLineJoin joint, StrokeLineCap cap, Double strokeSize)
        {
            Path p = new Path(x, y);
            p.setFill(fill);
            p.setStroke(stroke);
            p.setStrokeWidth(strokeSize);
            p.setStrokeLineCap(cap);
            p.setStrokeLineJoin(joint);
            return p;
        }
    },
    BRUSH
    {
        @Override
        public AnimatorShapeBase createShape(double x, double y, double width, double height, Paint stroke, Paint fill, StrokeLineJoin joint, StrokeLineCap cap, Double strokeSize)
        {
            Brush b = new Brush(x, y);
            b.setFill(fill);
            b.setStroke(stroke);
            b.setStrokeWidth(strokeSize);
            b.setStrokeLineCap(cap);
            b.setStrokeLineJoin(joint);
            return b;
        }
    },
    RASTERIMAGE
    {
        @Override
        public AnimatorShapeBase createShape(double x, double y, double width, double height, Paint stroke, Paint fill, StrokeLineJoin joint, StrokeLineCap cap, Double strokeSize)
        {
            throw new UnsupportedOperationException();
        }
    },
    
    SVGIMAGE
    {
        @Override
        public AnimatorShapeBase createShape(double x, double y, double width, double height, Paint stroke, Paint fill, StrokeLineJoin joint, StrokeLineCap cap, Double strokeSize)
        {
            throw new UnsupportedOperationException();
        }
    };

    public abstract AnimatorShapeBase createShape(double x, double y, double width, double height, Paint stroke, Paint fill, StrokeLineJoin joint, StrokeLineCap cap, Double strokeSize);
}
