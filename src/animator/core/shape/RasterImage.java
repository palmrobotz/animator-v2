package animator.core.shape;

import animator.core.animation.ShapeProperty;
import animator.core.shape.util.DragHelper;
import animator.core.shape.util.DragHelperCallback;
import static animator.core.shape.util.ShapeTheme.EDGE_POINT_BUILDER;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 *
 * @author Xephrt
 */
public class RasterImage extends AnimatorShapeBaseAdapter
{
    private DoubleProperty x;
    private DoubleProperty y;
    private DoubleProperty width;
    private DoubleProperty height;
    private ImageView imgView;
    private String imagePath;

    public RasterImage(String uri)
    {
        imagePath = uri;
        
        Image img = new Image("file://localhost" + uri);
        imgView = new ImageView(img);

        imgView.setFitHeight(img.getHeight());
        imgView.setFitWidth(img.getWidth());

        x = imgView.xProperty();
        y = imgView.yProperty();
        height = imgView.fitHeightProperty();
        width = imgView.fitWidthProperty();

        final ContextMenu cm = new ContextMenu();

        final MenuItem flipX = new MenuItem("Flip Horizontal");

        flipX.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent t)
            {
                if (imgView.getScaleX() == 1)
                {
                    imgView.setScaleX(-1);
                }
                else
                {
                    imgView.setScaleX(1);
                }
            }
        });

        final MenuItem flipY = new MenuItem("Flip Vertical");
        flipY.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent t)
            {
                if (imgView.getScaleY() == 1)
                {
                    imgView.setScaleY(-1);
                }
                else
                {
                    imgView.setScaleY(1);
                }
            }
        });

        cm.getItems().addAll(flipX, flipY);

        final javafx.scene.shape.Rectangle bound = new javafx.scene.shape.Rectangle(imgView.getFitWidth(), imgView.getFitHeight());
        bound.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        bound.setFill(Color.TRANSPARENT);
        bound.setStroke(Color.BLUE);
        bound.setStrokeWidth(1);
        bound.widthProperty().bind(width);
        bound.heightProperty().bind(height);
        bound.xProperty().bind(x);
        bound.yProperty().bind(y);

        final javafx.scene.shape.Rectangle transArea = new javafx.scene.shape.Rectangle(imgView.getFitWidth(), imgView.getFitHeight());
        transArea.setFill(Color.TRANSPARENT);
        transArea.widthProperty().bind(width);
        transArea.heightProperty().bind(height);
        transArea.xProperty().bind(x);
        transArea.yProperty().bind(y);
        transArea.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                if (t.getButton() == MouseButton.SECONDARY)
                {
                    cm.show(bound, t.getScreenX(), t.getScreenY());
                }
            }
        });

        javafx.scene.shape.Rectangle topLeft = EDGE_POINT_BUILDER.build();
        topLeft.layoutXProperty().bind(this.x);
        topLeft.layoutYProperty().bind(this.y);
        topLeft.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(topLeft, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                if (RasterImage.this.width.get() - deltaX <= 0)
                {
                }
                else
                {
                    RasterImage.this.x.set(RasterImage.this.x.get() + deltaX);
                    RasterImage.this.width.set(RasterImage.this.width.get() - deltaX);
                }

                if (RasterImage.this.height.get() - deltaY <= 0)
                {
                }
                else
                {
                    RasterImage.this.y.set(RasterImage.this.y.get() + deltaY);
                    RasterImage.this.height.set(RasterImage.this.height.get() - deltaY);
                }
            }
        });

        javafx.scene.shape.Rectangle topRight = EDGE_POINT_BUILDER.build();
        topRight.layoutXProperty().bind(this.x.add(this.width));
        topRight.layoutYProperty().bind(this.y);
        topRight.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(topRight, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                if (RasterImage.this.width.get() + deltaX <= 0)
                {
                }
                else
                {
                    RasterImage.this.width.set(RasterImage.this.width.get() + deltaX);
                }

                if (RasterImage.this.height.get() - deltaY <= 0)
                {
                }
                else
                {
                    RasterImage.this.y.set(RasterImage.this.y.get() + deltaY);
                    RasterImage.this.height.set(RasterImage.this.height.get() - deltaY);
                }
            }
        });

        javafx.scene.shape.Rectangle bottomLeft = EDGE_POINT_BUILDER.build();
        bottomLeft.layoutXProperty().bind(this.x);
        bottomLeft.layoutYProperty().bind(this.y.add(this.height));
        bottomLeft.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(bottomLeft, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                if (RasterImage.this.width.get() - deltaX <= 0)
                {
                }
                else
                {
                    RasterImage.this.x.set(RasterImage.this.x.get() + deltaX);
                    RasterImage.this.width.set(RasterImage.this.width.get() - deltaX);
                }

                if (RasterImage.this.height.get() + deltaY <= 0)
                {
                }
                else
                {
                    RasterImage.this.height.set(RasterImage.this.height.get() + deltaY);
                }
            }
        });

        javafx.scene.shape.Rectangle bottomRight = EDGE_POINT_BUILDER.build();
        bottomRight.layoutXProperty().bind(this.x.add(this.width));
        bottomRight.layoutYProperty().bind(this.y.add(this.height));
        bottomRight.visibleProperty().bind(selectProperty().and(drawBoundProperty()));
        DragHelper.INSTANCE.makeNodeDragable(bottomRight, new DragHelperCallback()
        {
            @Override
            public void onDragged(double deltaX, double deltaY)
            {
                if (!(RasterImage.this.width.get() + deltaX <= 0))
                {
                    RasterImage.this.width.set(RasterImage.this.width.get() + deltaX);
                }
                if (!(RasterImage.this.height.get() + deltaY <= 0))
                {
                    RasterImage.this.height.set(RasterImage.this.height.get() + deltaY);
                }
            }
        });

        getChildren().addAll(imgView, bound, transArea, topLeft, topRight, bottomLeft, bottomRight);
        setSupportProperty(ShapeProperty.SELECTION, ShapeProperty.X_POSITION, ShapeProperty.Y_POSITION
                , ShapeProperty.ROTATION, ShapeProperty.ALPHA);

        initShapeWithNode(imgView);
    }

    @Override
    public AnimatorShape getType()
    {
        return AnimatorShape.RASTERIMAGE;
    }

    @Override
    public double getAlpha()
    {
        return imgView.getOpacity();
    }

    @Override
    public void setAlpha(double d)
    {
        imgView.setOpacity(d);
    }

    @Override
    public DoubleProperty alphaProperty()
    {
        return imgView.opacityProperty();
    }
    
    public String getFilePath()
    {
        return imagePath;
    }
}
