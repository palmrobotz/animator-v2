
package animator.core.command;

import animator.core.animation.AnimationModel;
import animator.core.animation.LayerModel;
import animator.core.shape.AnimatorShapeBase;

/**
 *
 * @author Nuntipat narkthong
 */
public class NewAnimationBlockCommand extends Command
{
    private AnimationModel animationModel;
    
    private LayerModel layerModel;
    private AnimatorShapeBase shape;
    private long startTime;
    private long endTime;
    
    public NewAnimationBlockCommand(LayerModel model, AnimatorShapeBase asb, long startTime, long endTime)
    {
        super("New animation block");
        this.layerModel = model;
        this.shape = asb;
        this.startTime = startTime;
        this.endTime = endTime;
    }
    
    @Override
    public void undo()
    {
        layerModel.removeAnimation(animationModel);
    }

    @Override
    public void redo()
    {
        if (animationModel == null)
            animationModel = layerModel.addAnimation(startTime, endTime, shape);
        else
            layerModel.addAnimation(animationModel);
    }

    public AnimationModel getAnimationModel()
    {
        if (animationModel == null)
            animationModel = layerModel.addAnimation(startTime, endTime, shape);
        
        return animationModel;
    }
    
}
