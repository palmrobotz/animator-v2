package animator.core.command;

import animator.core.animation.ShapeProperty;
import animator.core.animation.AnimationModel;
import animator.core.animation.KeyTime;
import animator.core.animation.LayerModel;
import animator.core.animation.PropertyValue;
import animator.core.animation.SubAnimationModel;
import animator.core.animation.TimelineModel;
import animator.core.shape.AnimatorShape;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.GroupShape;
import animator.core.shape.Path;
import animator.core.shape.PointData;
import animator.core.shape.RasterImage;
import animator.core.shape.SVGImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javafx.animation.Interpolator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Xephrt
 */
public class Loader
{

    private String loadFile;
    private TimelineModel timeline;

    public Loader(String file, TimelineModel t)
    {
        loadFile = file;
        timeline = t;
    }

    public void setLoadFile(String f)
    {
        loadFile = f;
    }

    public void loadFile()
    {
        clearTimeline();
        loadToAnimator(createAnimatorSaveCollection());
    }

    private AnimatorData createAnimatorSaveCollection()
    {
        AnimatorData animatorData = null;
        TimeLineData timeLineData = null;
        try
        {
            LayerData layerData = null;
            GroupData groupData = null;
            AnimationData animationData = null;
            ShapeData shapeData = null;
            KeyTimeData keyTimeData = null;
            KeyTimeData groupKeyTimeData =null;

            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            InputStream in = new FileInputStream(loadFile);
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

            while (eventReader.hasNext())
            {
                XMLEvent event = eventReader.nextEvent();

                if (event.isStartElement())
                {
                    StartElement startElement = event.asStartElement();
                    if (startElement.getName().getLocalPart().matches("Animator"))
                    {
                        animatorData = new AnimatorData();
                    }
                    else if (startElement.getName().getLocalPart().matches("Timeline"))
                    {
                        timeLineData = new TimeLineData();
                    }
                    else if (startElement.getName().getLocalPart().matches("Layer"))
                    {
                        layerData = new LayerData();

                        Iterator<Attribute> attributes = startElement.getAttributes();
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            if (attribute.getName().toString().equals("ID"))
                            {
                                layerData.setID(Integer.valueOf(attribute.getValue()));
                            }
                        }

                    }
                    else if (startElement.getName().getLocalPart().matches("Group"))
                    {
                        groupData = new GroupData();
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            switch (attribute.getName().toString())
                            {
                                case "Start":
                                    groupData.setStartTime(Double.valueOf(attribute.getValue()));
                                    break;
                                case "End":
                                    groupData.setEndTime(Double.valueOf(attribute.getValue()));
                                    break;
                            }
                        }
                        
                    }
                    else if (startElement.getName().getLocalPart().matches("GroupKeytime"))
                    {
                        groupKeyTimeData = new KeyTimeData();
                        
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            if (attribute.getName().toString().equals("Time"))
                            {
                                groupKeyTimeData.setTime(Double.valueOf(attribute.getValue()));
                            }
                        } 
                    }
                    else if (startElement.getName().getLocalPart().matches("GroupProperty"))
                    {
                        String key = null;
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            if (attribute.getName().toString().equals("Type"))
                            {
                                key = attribute.getValue();
                            }
                        }
                        event = eventReader.nextEvent();

                        groupKeyTimeData.getPropertyList().add(createProperty(groupData.getGroupShape(), key, event.asCharacters().getData()));

                    }
                    else if (startElement.getName().getLocalPart().matches("Animation"))
                    {
                        animationData = new AnimationData();
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            switch (attribute.getName().toString())
                            {
                                case "Type":
                                    animationData.setType(attribute.getValue());
                                    break;
                                case "Start":
                                    animationData.setStartTime(Double.valueOf(attribute.getValue()));
                                    break;
                                case "End":
                                    animationData.setEndTime(Double.valueOf(attribute.getValue()));
                                    break;
                            }
                        }
                    }
                    else if (startElement.getName().getLocalPart().matches("Shape"))
                    {
                        shapeData = new ShapeData();

                    }
                    else if (startElement.getName().getLocalPart().matches("Image"))
                    {
                        event = eventReader.nextEvent();
                        animationData.setFilePath(event.asCharacters().getData());
                    }
                    else if (startElement.getName().getLocalPart().matches("Path"))
                    {
                        event = eventReader.nextEvent();
                        shapeData.setPointList(pathPointGen(event.asCharacters().getData()));
                    }
                    else if (startElement.getName().getLocalPart().matches("Keytime"))
                    {
                        keyTimeData = new KeyTimeData();

                        Iterator<Attribute> attributes = startElement.getAttributes();
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            if (attribute.getName().toString().equals("Time"))
                            {
                                keyTimeData.setTime(Double.valueOf(attribute.getValue()));
                            }
                        }

                    }
                    else if (startElement.getName().getLocalPart().matches("Property"))
                    {
                        String key = null;
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        while (attributes.hasNext())
                        {
                            Attribute attribute = attributes.next();
                            if (attribute.getName().toString().equals("Type"))
                            {
                                key = attribute.getValue();
                            }
                        }
                        event = eventReader.nextEvent();
                        keyTimeData.getPropertyList().add(createProperty(animationData, key, event.asCharacters().getData()));

                    }
                }
                else if (event.isEndElement())
                {
                    EndElement endElement = event.asEndElement();

                    if (endElement.getName().getLocalPart().matches("Animator"))
                    {
                        //Nothing to do here
                    }
                    else if (endElement.getName().getLocalPart().matches("Timeline"))
                    {
                        animatorData.setTimeLineData(timeLineData);
                        //timeLineData=null;
                    }
                    else if (endElement.getName().getLocalPart().matches("Layer"))
                    {
                        timeLineData.addLayerToList(layerData);
                        layerData = null;
                    }
                    else if (endElement.getName().getLocalPart().matches("Group"))
                    {
                        layerData.addGroupToList(groupData);
                        groupData = null;
                    }
                    else if (endElement.getName().getLocalPart().matches("GroupKeytime"))
                    {
                        groupData.addKeyTimeToList(groupKeyTimeData);
                        groupKeyTimeData = null;
                    }
                    else if (endElement.getName().getLocalPart().matches("GroupProperty"))
                    {
                    }
                    else if (endElement.getName().getLocalPart().matches("Animation"))
                    {
                        groupData.addAnimationToList(animationData);
                        animationData = null;
                        shapeData = null;
                    }
                    else if(endElement.getName().getLocalPart().matches("Image"))
                    {
                        
                        if("RASTERIMAGE".equals(animationData.getType()))
                        {
                        animationData.setAnimatorShapeBase(new RasterImage(animationData.getFilePath()));
                        }
                        else if("SVGIMAGE".equals(animationData.getType()))
                        {
                            
                        animationData.setAnimatorShapeBase(new SVGImage(animationData.getFilePath()));
                        }
                        
                    }
                    else if (endElement.getName().getLocalPart().matches("Shape"))
                    {
                        animationData.setShapeData(shapeData);
                        animationData.setAnimatorShapeBase(Path.createFromPoint(shapeData.getPath()));
                        
                    }
                    else if (endElement.getName().getLocalPart().matches("Path"))
                    {
                        //Path already added in the StartElement 
                    }
                    else if (endElement.getName().getLocalPart().matches("Keytime"))
                    {

                        animationData.addKeyTimeToList(keyTimeData);
                        keyTimeData = null;
                        

                    }
                    else if (endElement.getName().getLocalPart().matches("Property"))
                    {
                    }
                }

            }
        }
        catch (FileNotFoundException | XMLStreamException e)
        {
            System.err.println("Loading Error");
        }
        return animatorData;
    }

    private void loadToAnimator(AnimatorData a)
    { 
        //Generate Layer
        for (int i = 0; i < a.getTimeLineData().getLayerList().size(); i++)
        {
            LayerModel lm = timeline.newLayer();

            //Generate Animation
            
            for (int j = 0; j < a.getTimeLineData().getLayerList().get(i).getGroupList().size(); j++)
            {
                
                GroupData gd = a.getTimeLineData().getLayerList().get(i).getGroupList().get(j);
                
                AnimationModel am = lm.addAnimation((long)(gd.getStartTime()),(long)(gd.getEndTime()), gd.getAnimationList().get(0).getAnimatorShapeBase(),gd.getGroupShape());
                
                SubAnimationModel firstSub = am.getChildModelModifiable().get(0);
                
                for(int num=0;num<gd.getAnimationList().get(0).getKeyTimeList().size();num++)
                {
                    KeyTime keyTime = new KeyTime(am, (long)(gd.getAnimationList().get(0).getKeyTimeList().get(num).getTime()));
                    for(int num2=0;num2<gd.getAnimationList().get(0).getKeyTimeList().get(num).getPropertyList().size();num2++)
                    {
                        System.out.println(gd.getAnimationList().get(0).getKeyTimeList().get(num).getPropertyList().get(num2).getValue());
                        keyTime.addProperty(gd.getAnimationList().get(0).getKeyTimeList().get(num).getPropertyList().get(num2));
                    }
                    firstSub.addKeyTime(keyTime);
                }
                for (int m=1;m<gd.getAnimationList().size();m++)
                {
                    SubAnimationModel sa = am.addChildShape(gd.getAnimationList().get(m).getAnimatorShapeBase(), (long)gd.getAnimationList().get(m).getStartTime());
                        
                    for(int num=0;num<gd.getAnimationList().get(m).getKeyTimeList().size();num++)
                    {
                        KeyTime keyTime = new KeyTime(am, (long)(gd.getAnimationList().get(m).getKeyTimeList().get(num).getTime()));
                        for(int num2=0;num2<gd.getAnimationList().get(m).getKeyTimeList().get(num).getPropertyList().size();num2++)
                        {
                            keyTime.addProperty(gd.getAnimationList().get(m).getKeyTimeList().get(num).getPropertyList().get(num2));
                        }
                        sa.addKeyTime(keyTime);
                    }
                }
                //add keytime for group
                for (int k=0;k<gd.getKeyTimeList().size();k++)
                {
                    KeyTime gKt = new KeyTime(am,(long)(gd.getKeyTimeList().get(k).getTime()));
                    for(int l=0;l<gd.getKeyTimeList().get(k).getPropertyList().size();l++)
                    {
                        gKt.addProperty(gd.getKeyTimeList().get(k).getPropertyList().get(l));
                        System.out.println(gd.getKeyTimeList().get(k).getPropertyList().get(l).getValue());
                    }
                    am.addKeyTime(gKt);
                }
                
            }
        }
        
    }

    private void clearTimeline()
    {
        while (timeline.getLayerCount() != 0)
        {
            //timeline.getLayer(0).removeAnimation(timeline.getLayer(0).getDuration());
            timeline.removeLayer(timeline.getLayer(0));
        }
    }

    private List<List<DoubleProperty>> pathPointGen(String s)
    {
        List<List<DoubleProperty>> pointList = new LinkedList<>();
        String lines[] = s.split("\\|");


        for (int i = 0; i < lines.length; i++)
        {

            String point[] = lines[i].split(",");


            pointList.add(new LinkedList<DoubleProperty>());
            for (int j = 0; j < point.length; j++)
            {
                pointList.get(i).add(
                        new SimpleDoubleProperty(Double.valueOf(point[j])));
            }
        }

        return pointList;
    }
    
    private PropertyValue createProperty(GroupShape gs, String key, String value)
    {
        PropertyValue p = null;
        
        if (key.matches("X_POSITION"))
        {
            p = ShapeProperty.X_POSITION.createPropertyValue(gs, Double.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("Y_POSITION"))
        {
            p = ShapeProperty.Y_POSITION.createPropertyValue(gs, Double.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("FILL_COLOR"))
        {
            p = ShapeProperty.FILL_COLOR.createPropertyValue(gs, Color.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("STROKE_COLOR"))
        {
            p = ShapeProperty.STROKE_COLOR.createPropertyValue(gs, Color.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("STROKE_WIDTH"))
        {
            p = ShapeProperty.STROKE_WIDTH.createPropertyValue(gs, Double.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("ROTATION"))
        {
            p = ShapeProperty.ROTATION.createPropertyValue(gs,Double.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("ALPHA"))
        {
            p = ShapeProperty.ALPHA.createPropertyValue(gs, Double.valueOf(value), Interpolator.LINEAR);
        }
        else if(key.matches("OFFSET_X_POSITION"))
        {
            p = ShapeProperty.OFFSET_X_POSITION.createPropertyValue(gs, Double.valueOf(value),Interpolator.LINEAR);
        }
        else if(key.matches("OFFSET_Y_POSITION"))
        {
            p = ShapeProperty.OFFSET_Y_POSITION.createPropertyValue(gs, Double.valueOf(value),Interpolator.LINEAR);
        }
        else if(key.matches("OFFSET_ROTATION"))
        {
            p = ShapeProperty.OFFSET_ROTATION.createPropertyValue(gs, Double.valueOf(value),Interpolator.LINEAR);
        }
        else if(key.matches("OFFSET_ALPHA"))
        {
            p = ShapeProperty.OFFSET_ALPHA.createPropertyValue(gs, Double.valueOf(value),Interpolator.LINEAR);
        }
        else if(key.matches("POINT_DATA"))
        {
            List<List<DoubleProperty>> point = pathPointGen(value);
            PointData pointData = new PointData(point);
            p = ShapeProperty.POINT_DATA.createPropertyValue(gs, pointData,Interpolator.LINEAR);       
        }
        
        return p;
    }
    
    private PropertyValue createProperty(AnimationData ad, String key, String value)
    {
        PropertyValue p = null;
        
        if (key.matches("X_POSITION"))
        {
            p = ShapeProperty.X_POSITION.createPropertyValue(ad.getAnimatorShapeBase(), Double.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("Y_POSITION"))
        {
            p = ShapeProperty.Y_POSITION.createPropertyValue(ad.getAnimatorShapeBase(), Double.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("FILL_COLOR"))
        {
            p = ShapeProperty.FILL_COLOR.createPropertyValue(ad.getAnimatorShapeBase(), Color.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("STROKE_COLOR"))
        {
            p = ShapeProperty.STROKE_COLOR.createPropertyValue(ad.getAnimatorShapeBase(), Color.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("STROKE_WIDTH"))
        {
            p = ShapeProperty.STROKE_WIDTH.createPropertyValue(ad.getAnimatorShapeBase(), Double.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("ROTATION"))
        {
            p = ShapeProperty.ROTATION.createPropertyValue(ad.getAnimatorShapeBase(), Double.valueOf(value), Interpolator.LINEAR);
        }
        else if (key.matches("ALPHA"))
        {
            p = ShapeProperty.ALPHA.createPropertyValue(ad.getAnimatorShapeBase(), Double.valueOf(value), Interpolator.LINEAR);
        }
        else if(key.matches("OFFSET_X_POSITION"))
        {
            p = ShapeProperty.OFFSET_X_POSITION.createPropertyValue(ad.getAnimatorShapeBase(), Double.valueOf(value),Interpolator.LINEAR);
        }
        else if(key.matches("OFFSET_Y_POSITION"))
        {
            p = ShapeProperty.OFFSET_Y_POSITION.createPropertyValue(ad.getAnimatorShapeBase(), Double.valueOf(value),Interpolator.LINEAR);
        }
        else if(key.matches("OFFSET_ROTATION"))
        {
            p = ShapeProperty.OFFSET_ROTATION.createPropertyValue(ad.getAnimatorShapeBase(), Double.valueOf(value),Interpolator.LINEAR);
        }
        else if(key.matches("OFFSET_ALPHA"))
        {
            p = ShapeProperty.OFFSET_ALPHA.createPropertyValue(ad.getAnimatorShapeBase(), Double.valueOf(value),Interpolator.LINEAR);
        }
        else if(key.matches("POINT_DATA"))
        {
            Path shapeBase = (Path)ad.getAnimatorShapeBase();
            List<List<DoubleProperty>> pointOriginal = shapeBase.getPointDataModifiable();
            List<List<DoubleProperty>> point = pathPointGen(value);
            for(int i=0;i<pointOriginal.size();i++)
            {
                for(int j=0;j<pointOriginal.get(i).size();j++)
                {
                    pointOriginal.get(i).get(j).set(point.get(i).get(j).doubleValue());
                }
            }
            PointData pointData = new PointData(pointOriginal);
            p = ShapeProperty.POINT_DATA.createPropertyValue(ad.getAnimatorShapeBase(), pointData,Interpolator.LINEAR);       
        }
        
        return p;
    }

    private class ShapeData implements Cloneable
    {

        private List<List<DoubleProperty>> path;

        @Override
        public Object clone()
        {
            ShapeData obj = new ShapeData();
            obj.setPointList(this.path);

            return obj;
        }

        public ShapeData()
        {
            path = new LinkedList<>();
        }

        public List<List<DoubleProperty>> getPath()
        {
            return path;
        }

        public void setPointList(List<List<DoubleProperty>> p)
        {
            this.path = p;
        }

        public void setPointListClone(List<List<DoubleProperty>> p)
        {
            for (int i = 0; i < p.size(); i++)
            {
                this.path.add(new LinkedList<DoubleProperty>());
                for (int j = 0; j < p.get(i).size(); j++)
                {
                    this.path.get(i).add(new SimpleDoubleProperty(p.get(i).get(j).get()));
                }
            }
        }
    }

    private class KeyTimeData implements Cloneable
    {

        private double time;
        private List<PropertyValue> property;

        @Override
        public Object clone()
        {
            KeyTimeData obj = new KeyTimeData();
            obj.setTime(this.time);
            obj.setPropertyList(this.property);

            return obj;
        }

        public KeyTimeData()
        {
            time = -1;
            property = new LinkedList<>();
        }

        public double getTime()
        {
            return time;
        }

        public void setTime(double t)
        {
            time = t;
        }

        public List<PropertyValue> getPropertyList()
        {
            return property;
        }

        public void setPropertyList(List<PropertyValue> p)
        {
            this.property = p;
        }
    }

    private class AnimationData implements Cloneable
    {
        private AnimatorShapeBase asb;
        private double startTime;
        private double endTime;
        private ShapeData shape;
        private String type;
        private String uri;
        private List<KeyTimeData> keyTimeList;

        public AnimationData()
        {
            startTime = -1;
            endTime = -1;
            shape = new ShapeData();
            type = null;
            uri = null;
            keyTimeList = new LinkedList<>();
        }

        @Override
        public Object clone()
        {
            AnimationData obj = new AnimationData();
            obj.setStartTime(this.startTime);
            obj.setEndTime(this.endTime);
            obj.setShapeData(this.shape);
            obj.setKeyTimeList(this.keyTimeList);

            return obj;
        }

        public void setKeyTimeList(List<KeyTimeData> k)
        {
            for (int i = 0; i < k.size(); i++)
            {
                this.keyTimeList.add((KeyTimeData) k.get(i).clone());
            }
        }
        
        public String getFilePath()
        {
            if("SVGIMAGE".equals(type) || "RASTERIMAGE".equals(type))
            {
                return uri;
            }
            else
            {
                return "This is Animation did not have file path";
            }
        }
        
        public void setFilePath(String s)
        {
            uri=s;
        }
        
        public String getType()
        {
            return type;
        }
        
        public void setType(String s)
        {
            type=s;
        }

        public double getStartTime()
        {
            return startTime;
        }

        public void setStartTime(double t)
        {
            startTime = t;
        }

        public double getEndTime()
        {
            return endTime;
        }

        public void setEndTime(double t)
        {
            endTime = t;
        }

        public AnimatorShapeBase getAnimatorShapeBase()
        {
            return asb;
        }

        public void setAnimatorShapeBase(AnimatorShapeBase asb)
        {
            this.asb = asb;
        }

        public ShapeData getShapeData()
        {
            return shape;
        }

        public void setShapeData(ShapeData s)
        {
            //this.shape = (ShapeData) s.clone();
            this.shape = s;
        }

        public List<KeyTimeData> getKeyTimeList()
        {
            return keyTimeList;
        }

        public void addKeyTimeToList(KeyTimeData k)
        {
            //keyTimeList.add((KeyTimeData) k.clone());
            keyTimeList.add(k);
        }
    }
    
    private static class GroupData
    {
        private GroupShape groupShape;
        private List<KeyTimeData> keyTimeList;
        private List<AnimationData> animationList;
        private double startTime;
        private double endTime;
        
        public GroupData()
        {
            startTime = -1;
            endTime = -1;
            keyTimeList = new LinkedList<>();
            animationList = new LinkedList<>();
            groupShape = new GroupShape();
        }
        
        public GroupShape getGroupShape()
        {
            return groupShape;
        }
        public double getStartTime()
        {
            return startTime;
        }

        public void setStartTime(double t)
        {
            startTime = t;
        }

        public double getEndTime()
        {
            return endTime;
        }

        public void setEndTime(double t)
        {
            endTime = t;
        }
        
        public List<AnimationData> getAnimationList()
        {
            return animationList;
        }

        public void setAnimationList(List<AnimationData> a)
        {
            for (int i = 0; i < a.size(); i++)
            {
                this.animationList.add((AnimationData) a.get(i).clone());
            }
        }

        public void addAnimationToList(AnimationData a)
        {
            //animationList.add((AnimationData) a.clone());
            animationList.add(a);
        }
        
        public List<KeyTimeData> getKeyTimeList()
        {
            return keyTimeList;
        }

        public void addKeyTimeToList(KeyTimeData k)
        {
            //keyTimeList.add((KeyTimeData) k.clone());
            keyTimeList.add(k);
        }

        private void setGroupData(GroupShape gs)
        {
            groupShape=gs;
        }
        
    }

    private static class LayerData implements Cloneable
    {

        private int ID;
        private List<GroupData> groupList;
/*
        @Override
        public Object clone()
        {
            LayerData obj = new LayerData();
            obj.setID(this.ID);
            obj.setAnimationList(this.animationList);

            return obj;
        }*/

        public LayerData()
        {
            ID = -1;
            groupList = new LinkedList<>();
        }

        public int getID()
        {
            return ID;
        }

        public void setID(int i)
        {
            ID = i;
        }

        public List<GroupData> getGroupList()
        {
            return groupList;
        }


        public void addGroupToList(GroupData g)
        {
            //animationList.add((AnimationData) a.clone());
            groupList.add(g);
        }
    }

    private class TimeLineData implements Cloneable
    {

        private List<LayerData> layerList;

        public TimeLineData()
        {
            layerList = new LinkedList<>();
        }

        public List<LayerData> getLayerList()
        {
            return layerList;
        }

        public void addLayerToList(LayerData l)
        {
            //this.layerList.add((LayerData) l.clone());
            this.layerList.add(l);
        }
    }

    private class AnimatorData
    {

        private TimeLineData timeline;

        public AnimatorData()
        {
            timeline = new TimeLineData();
        }

        public TimeLineData getTimeLineData()
        {
            return timeline;
        }

        public void setTimeLineData(TimeLineData t)
        {
            //this.timeline = (TimeLineData) t.clone();
            this.timeline = t;
        }
    }
}
