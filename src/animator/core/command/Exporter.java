package animator.core.command;

import animator.core.animation.AbstractAnimationModel;
import animator.core.animation.ShapeProperty;
import animator.core.animation.AnimationModel;
import animator.core.animation.KeyTime;
import animator.core.animation.LayerModel;
import animator.core.animation.SubAnimationModel;
import animator.core.animation.TimelineModel;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.Path;
import animator.core.shape.PointData;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Nattaphat
 */
public class Exporter
{

    private String saveFile;
    private TimelineModel timeline;

    public Exporter(TimelineModel model)
    {
        timeline = model;
    }

    public void setFile(String file)
    {
        this.saveFile = file;
    }

    public void saveFile() throws Exception
    {
        XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();
        XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(new FileOutputStream(saveFile));
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t");

        //StartDocument startDocument = eventFactory.createStartDocument();
        //eventWriter.add(startDocument);

////////////////////////////////Animator Tag Start//////////////////////////////        
        StartElement doctype = eventFactory.createStartElement("", "", "!DOCTYPE HTML");
        StartElement html = eventFactory.createStartElement("", "", "html");
        StartElement body = eventFactory.createStartElement("", "", "body");
        StartElement svg = eventFactory.createStartElement("", "", "svg");
        Attribute xmlns = eventFactory.createAttribute("xmlns", "http://www.w3.org/2000/svg");
        Attribute xmlnsVersion = eventFactory.createAttribute("version", "1.1");

        eventWriter.add(doctype);
        eventWriter.add(endline);
        eventWriter.add(html);
        eventWriter.add(endline);
        eventWriter.add(body);
        eventWriter.add(endline);
        eventWriter.add(svg);
        eventWriter.add(xmlns);
        eventWriter.add(xmlnsVersion);
        eventWriter.add(endline);

        for (int i = 0; i < timeline.getLayerCount(); i++)
        {
            createLayerNode(eventWriter, timeline.getLayer(i));
        }
        eventWriter.add(eventFactory.createEndElement("", "", "svg"));
        eventWriter.add(endline);
        eventWriter.add(eventFactory.createEndElement("", "", "body"));
        eventWriter.add(endline);
        eventWriter.add(eventFactory.createEndElement("", "", "html"));
        eventWriter.add(endline);
        
        eventWriter.flush();
        eventWriter.close();
    }

    private void createLayerNode(XMLEventWriter eventWriter, LayerModel layer) throws XMLStreamException
    {
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t");

        eventWriter.add(eventFactory.createStartElement("", "", "g"));
        eventWriter.add(eventFactory.createAttribute("id", String.valueOf(layer.getLayerIndex())));
        for (AnimationModel animation : layer.getDataUnmodifiable().values())
        {
            createGroupNode(eventWriter, animation);
        }

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "g"));
        eventWriter.add(endline);
    }

    private void createGroupNode(XMLEventWriter eventWriter, AnimationModel animation) throws XMLStreamException
    {
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t");

        eventWriter.add(eventFactory.createStartElement("", "", "g"));
        for (SubAnimationModel subAnimation : animation.getChildModelUnmodifiable())
        {
            createPathNode(eventWriter, subAnimation);
        }
        //createAnimation node here
        createAnimationNode(eventWriter, animation);
        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "g"));
        eventWriter.add(endline);
    }

    private void createPathNode(XMLEventWriter eventWriter, SubAnimationModel animation) throws XMLStreamException
    {
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t");

        AnimatorShapeBase shape = animation.getShape();
        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "path"));

        StringBuilder path = new StringBuilder();
        List<List<DoubleProperty>> data = null;
        if(shape instanceof Path){
            PointData dummy = (PointData) animation.getKeyTimeUnmodifiable().get(0).getProperty(ShapeProperty.POINT_DATA).getValue();
            data = dummy.getPoint();
            /*
            //convert to List<List<Double>>
            List<List<DoubleProperty>> newList = new ArrayList<>();
            for(List<Double> list : dummy2){
                List<DoubleProperty> newInnerList = new ArrayList<>();
                for(Double value:list){
                    DoubleProperty newValue = new SimpleDoubleProperty(value);
                    newInnerList.add(newValue);
                }
                newList.add(newInnerList);
            }
            data = newList;*/
        }else
        {
            data = shape.getPointDataUnmodifiable();
        }
        path.append("M");
        for (int i = 0; i < data.size(); i++)
        {
            for (int y = 0; y < data.get(i).size(); y++)
            {
                path.append(data.get(i).get(y).getValue().toString());
                if (y + 1 < data.get(i).size())
                {
                    path.append(" ");
                }
            }
            if (i + 1 < data.size())
            {
                path.append("C");
            }
        }
        eventWriter.add(eventFactory.createAttribute("d", path.toString()));
        eventWriter.add(eventFactory.createAttribute("stroke-width", String.valueOf(shape.getStrokeWidth())));
        eventWriter.add(eventFactory.createAttribute("stroke", colorParse(shape.getStroke().toString())));
        eventWriter.add(eventFactory.createAttribute("visibility", "hidden"));
        //eventWriter.add(eventFactory.createAttribute("fill",colorParse(shape.getFill().toString())));
        //eventWriter.add(eventFactory.createAttribute("opacity",String.valueOf(Color.valueOf(shape.getFill().toString()).getOpacity())));
        //eventWriter.add(eventFactory.createAttribute("transform", "translate("+shape.getLayoutX()+","+shape.getLayoutY()+")"));
        eventWriter.add(endline);

        //createAnimation node here
        createAnimationNode(eventWriter, animation);

        eventWriter.add(eventFactory.createEndElement("", "", "path"));
        eventWriter.add(endline);

    }

    private void createAnimationNode(XMLEventWriter eventWriter, AbstractAnimationModel animation) throws XMLStreamException
    {
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t");

        //set object visible only in timeline block
        eventWriter.add(eventFactory.createStartElement("", "", "set"));
        eventWriter.add(eventFactory.createAttribute("attributeType", "CSS"));
        eventWriter.add(eventFactory.createAttribute("attributeName", "visibility"));
        eventWriter.add(eventFactory.createAttribute("to", "visible"));
        eventWriter.add(eventFactory.createAttribute("begin", String.valueOf((double) animation.getStartTime() / 1000)));
        eventWriter.add(eventFactory.createAttribute("end", String.valueOf((double) animation.getEndTime() / 1000)));
        eventWriter.add(eventFactory.createEndElement("", "", "set"));
        eventWriter.add(endline);

        //create each type of animation in keytime
        //translate
        KeyTimeIterator xKeytimeIterator = null;
        KeyTimeIterator yKeytimeIterator = null;
        if (animation instanceof SubAnimationModel)
        {
            xKeytimeIterator = new KeyTimeIterator(animation.getKeyTimeUnmodifiable(), ShapeProperty.X_POSITION);
            yKeytimeIterator = new KeyTimeIterator(animation.getKeyTimeUnmodifiable(), ShapeProperty.Y_POSITION);
        }
        else
        {
            xKeytimeIterator = new KeyTimeIterator(animation.getKeyTimeUnmodifiable(), ShapeProperty.OFFSET_X_POSITION);
            yKeytimeIterator = new KeyTimeIterator(animation.getKeyTimeUnmodifiable(), ShapeProperty.OFFSET_Y_POSITION);
        }
        KeyTime xKeytime = null;
        KeyTime yKeytime = null;
        String x = "";
        String y = "";
        String time = "";
        int i = 0;
        while (xKeytimeIterator.hasNext())
        {
            if (i == 0)
            {
                xKeytime = xKeytimeIterator.next();
                yKeytime = yKeytimeIterator.next();
                if (animation instanceof SubAnimationModel)
                {
                    x = xKeytime.getProperty(ShapeProperty.X_POSITION).getValue().toString();
                    y = yKeytime.getProperty(ShapeProperty.Y_POSITION).getValue().toString();
                }
                else
                {
                    x = xKeytime.getProperty(ShapeProperty.OFFSET_X_POSITION).getValue().toString();
                    y = yKeytime.getProperty(ShapeProperty.OFFSET_Y_POSITION).getValue().toString();
                }

                time = String.valueOf((double) xKeytime.getTime() / 1000);
                if (!xKeytimeIterator.hasNext())
                {

                    eventWriter.add(eventFactory.createStartElement("", "", "animateTransform"));
                    eventWriter.add(eventFactory.createAttribute("attributeType", "XML"));
                    eventWriter.add(eventFactory.createAttribute("attributeName", "transform"));
                    eventWriter.add(eventFactory.createAttribute("type", "translate"));
                    eventWriter.add(eventFactory.createAttribute("begin", time + "s"));
                    eventWriter.add(eventFactory.createAttribute("from", x + "," + y));
                    eventWriter.add(eventFactory.createAttribute("to", x + "," + y));
                    String dur = String.valueOf((double) animation.getEndTime() / 1000);
                    eventWriter.add(eventFactory.createAttribute("dur", dur + "s"));
                    eventWriter.add(eventFactory.createAttribute("fill", "freeze"));
                    eventWriter.add(eventFactory.createEndElement("", "", "animateTransform"));
                    eventWriter.add(endline);
                }
            }
            if (xKeytimeIterator.hasNext())
            {
                eventWriter.add(eventFactory.createStartElement("", "", "animateTransform"));
                eventWriter.add(eventFactory.createAttribute("attributeType", "XML"));
                eventWriter.add(eventFactory.createAttribute("attributeName", "transform"));
                eventWriter.add(eventFactory.createAttribute("type", "translate"));
                eventWriter.add(eventFactory.createAttribute("begin", time + "s"));
                eventWriter.add(eventFactory.createAttribute("from", x + "," + y));
                xKeytime = xKeytimeIterator.next();
                yKeytime = yKeytimeIterator.next();
                if (animation instanceof SubAnimationModel)
                {
                    x = xKeytime.getProperty(ShapeProperty.X_POSITION).getValue().toString();
                    y = yKeytime.getProperty(ShapeProperty.Y_POSITION).getValue().toString();
                }
                else
                {
                    x = xKeytime.getProperty(ShapeProperty.OFFSET_X_POSITION).getValue().toString();
                    y = yKeytime.getProperty(ShapeProperty.OFFSET_Y_POSITION).getValue().toString();
                }
                double startTime = Double.parseDouble(time);
                time = String.valueOf((double) xKeytime.getTime() / 1000);
                double endTime = Double.parseDouble(time);
                String dur = String.valueOf(endTime - startTime);
                eventWriter.add(eventFactory.createAttribute("to", x + "," + y));
                eventWriter.add(eventFactory.createAttribute("dur", dur + "s"));
                eventWriter.add(eventFactory.createAttribute("fill", "freeze"));
                eventWriter.add(eventFactory.createEndElement("", "", "animateTransform"));
                eventWriter.add(endline);
            }
            i++;
        }
        
        //fill color
        KeyTimeIterator fillKeytimeIterator = new KeyTimeIterator(animation.getKeyTimeUnmodifiable(), ShapeProperty.FILL_COLOR);
        KeyTime fillKeytime = null;
        String color = "";
        String opacity = "";
        time = "";
        i = 0;
        while (fillKeytimeIterator.hasNext())
        {
            if (i == 0)
            {
                fillKeytime = fillKeytimeIterator.next();
                color = colorParse(fillKeytime.getProperty(ShapeProperty.FILL_COLOR).getValue().toString());
                opacity = opacityParse(fillKeytime.getProperty(ShapeProperty.FILL_COLOR).getValue().toString());
                time = String.valueOf((double) fillKeytime.getTime() / 1000);
                if (!fillKeytimeIterator.hasNext())
                {
                    String dur = String.valueOf((double) animation.getEndTime() / 1000);

                    eventWriter.add(eventFactory.createStartElement("", "", "animateColor"));
                    eventWriter.add(eventFactory.createAttribute("attributeType", "XML"));
                    eventWriter.add(eventFactory.createAttribute("attributeName", "fill"));
                    eventWriter.add(eventFactory.createAttribute("begin", time + "s"));
                    eventWriter.add(eventFactory.createAttribute("from", color));
                    eventWriter.add(eventFactory.createAttribute("to", color));
                    eventWriter.add(eventFactory.createAttribute("dur", dur + "s"));
                    eventWriter.add(eventFactory.createAttribute("fill", "freeze"));
                    eventWriter.add(eventFactory.createEndElement("", "", "animateColor"));
                    eventWriter.add(endline);

                    eventWriter.add(eventFactory.createStartElement("", "", "animate"));
                    eventWriter.add(eventFactory.createAttribute("attributeType", "CSS"));
                    eventWriter.add(eventFactory.createAttribute("attributeName", "opacity"));
                    eventWriter.add(eventFactory.createAttribute("begin", time + "s"));
                    eventWriter.add(eventFactory.createAttribute("from", opacity));
                    eventWriter.add(eventFactory.createAttribute("to", opacity));
                    eventWriter.add(eventFactory.createAttribute("dur", dur + "s"));
                    eventWriter.add(eventFactory.createAttribute("fill", "freeze"));
                    eventWriter.add(eventFactory.createEndElement("", "", "animate"));
                    eventWriter.add(endline);
                }
            }
            if (fillKeytimeIterator.hasNext())
            {
                KeyTime nextFillKeytime = fillKeytimeIterator.next();
                String nextColor = colorParse(nextFillKeytime.getProperty(ShapeProperty.FILL_COLOR).getValue().toString());
                String nextOpacity = opacityParse(nextFillKeytime.getProperty(ShapeProperty.FILL_COLOR).getValue().toString());
                String nextTime = String.valueOf((double) nextFillKeytime.getTime() / 1000);
                double startTime = Double.parseDouble(time);
                double endTime = Double.parseDouble(nextTime);
                String dur = String.valueOf(endTime - startTime);
                eventWriter.add(eventFactory.createStartElement("", "", "animateColor"));
                eventWriter.add(eventFactory.createAttribute("attributeType", "XML"));
                eventWriter.add(eventFactory.createAttribute("attributeName", "fill"));
                eventWriter.add(eventFactory.createAttribute("begin", time + "s"));
                eventWriter.add(eventFactory.createAttribute("from", color));
                eventWriter.add(eventFactory.createAttribute("to", nextColor));
                eventWriter.add(eventFactory.createAttribute("dur", dur + "s"));
                eventWriter.add(eventFactory.createAttribute("fill", "freeze"));
                eventWriter.add(eventFactory.createEndElement("", "", "animateColor"));
                eventWriter.add(endline);

                eventWriter.add(eventFactory.createStartElement("", "", "animate"));
                eventWriter.add(eventFactory.createAttribute("attributeType", "CSS"));
                eventWriter.add(eventFactory.createAttribute("attributeName", "opacity"));
                eventWriter.add(eventFactory.createAttribute("begin", time + "s"));
                eventWriter.add(eventFactory.createAttribute("from", opacity));
                eventWriter.add(eventFactory.createAttribute("to", nextOpacity));
                eventWriter.add(eventFactory.createAttribute("dur", dur + "s"));
                eventWriter.add(eventFactory.createAttribute("fill", "freeze"));
                eventWriter.add(eventFactory.createEndElement("", "", "animate"));
                eventWriter.add(endline);

                time = nextTime;
                color = nextColor;
                opacity = nextOpacity;
            }
            i++;
        }
        
        //shape
        KeyTimeIterator shapeKeytimeIterator = new KeyTimeIterator(animation.getKeyTimeUnmodifiable(), ShapeProperty.POINT_DATA);
        KeyTime shapeKeytime;
        StringBuilder path = new StringBuilder();
        List<List<Double>> data;
        time = "";
        i = 0;
        while (shapeKeytimeIterator.hasNext())
        {
            if (i == 0)
            {
                System.out.println("1");
                shapeKeytime = shapeKeytimeIterator.next();
                PointData pData = (PointData)shapeKeytime.getProperty(ShapeProperty.POINT_DATA).getValue();
                data = pData.getPointList();
                
                time = String.valueOf((double) shapeKeytime.getTime() / 1000);
                if (!shapeKeytimeIterator.hasNext())
                {
                    String dur = String.valueOf((double) animation.getEndTime() / 1000);

                    path.append("M");
                    for (int j = 0; j < data.size(); j++)
                    {
                        for (int k = 0; k < data.get(j).size(); k++)
                        {
                            path.append(data.get(j).get(k).toString());
                            if (k + 1 < data.get(j).size())
                            {
                                path.append(" ");
                            }
                        }
                        if (j + 1 < data.size())
                        {
                            path.append("C");
                        }
                    }
                    
                    eventWriter.add(eventFactory.createStartElement("", "", "animate"));
                    eventWriter.add(eventFactory.createAttribute("attributeName", "d"));
                    eventWriter.add(eventFactory.createAttribute("begin", time + "s"));
                    eventWriter.add(eventFactory.createAttribute("to", path.toString()));
                    eventWriter.add(eventFactory.createAttribute("dur", dur + "s"));
                    eventWriter.add(eventFactory.createAttribute("fill", "freeze"));
                    eventWriter.add(eventFactory.createEndElement("", "", "animate"));
                    eventWriter.add(endline);
                }
            }
            if (shapeKeytimeIterator.hasNext())
            {
                System.out.println("2");
                KeyTime nextShapeKeytime = shapeKeytimeIterator.next();
                StringBuilder nextPath = new StringBuilder();
                List<List<Double>> nextData;
                PointData pData = (PointData)nextShapeKeytime.getProperty(ShapeProperty.POINT_DATA).getValue();
                nextData = pData.getPointList();
                
                String nextTime = String.valueOf((double) nextShapeKeytime.getTime() / 1000);
                double startTime = Double.parseDouble(time);
                double endTime = Double.parseDouble(nextTime);
                String dur = String.valueOf(endTime - startTime);
                
                nextPath.append("M");
                for (int j = 0; j < nextData.size(); j++)
                {
                    for (int k = 0; k < nextData.get(j).size(); k++)
                    {
                        nextPath.append(nextData.get(j).get(k).toString());
                        if (k + 1 < nextData.get(j).size())
                        {
                            nextPath.append(" ");
                        }
                    }
                    if (j + 1 < nextData.size())
                    {
                        nextPath.append("C");
                    }
                }
                    
                eventWriter.add(eventFactory.createStartElement("", "", "animate"));
                eventWriter.add(eventFactory.createAttribute("attributeName", "d"));
                eventWriter.add(eventFactory.createAttribute("begin", time + "s"));
                eventWriter.add(eventFactory.createAttribute("to", nextPath.toString()));
                eventWriter.add(eventFactory.createAttribute("dur", dur + "s"));
                eventWriter.add(eventFactory.createAttribute("fill", "freeze"));
                eventWriter.add(eventFactory.createEndElement("", "", "animate"));
                eventWriter.add(endline);


                time = nextTime;
                path = nextPath;
                data = nextData;
            }
            i++;
        }

    }

    private String colorParse(String s)
    {
        String newColor = s.substring(0, s.length() - 2);
        return newColor.replace("0x", "#");
    }

    private String opacityParse(String s)
    {
        Color c = Color.web(s);
        String opacity = String.valueOf(c.getOpacity());
        return opacity;
    }

    private static class KeyTimeIterator implements Iterator<KeyTime>
    {

        private final ObservableList<KeyTime> l;
        private final ListIterator<KeyTime> e;
        private final ShapeProperty p;

        public KeyTimeIterator(ObservableList<KeyTime> list, ShapeProperty property)
        {
            l = list;
            p = property;
            e = l.listIterator();
        }

        @Override
        public boolean hasNext()
        {
            ListIterator<KeyTime> tmp_e = l.listIterator(e.nextIndex());
            while (tmp_e.hasNext())
            {
                KeyTime next = tmp_e.next();
                if (next.isPropertyChange(p))
                {
                    //roll back iterator to first index
                    return true;
                }
            }
            return false;
        }

        @Override
        public KeyTime next()
        {
            while (e.hasNext())
            {
                KeyTime next = e.next();
                if (next.isPropertyChange(p))
                {
                    return next;
                }
            }
            return null;
        }

        @Override
        public void remove()
        {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
