
package animator.core.command;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Nuntipat narkthong
 */
public class CommandCenter
{
    /**
     * The only one instance of this class.
     */
    public static final CommandCenter INSTANCE = new CommandCenter();
    
    /**
     * List of the {@code command} instance.
     */
    private List<Command> command = new ArrayList<>();
    /** 
     * This field is use to keep track of the current position in the list.
     * When undo, the current position will be in the middle of the list, thus
     * indicated that we can redo otherwise this will be equal to {@code command.size()}.
     */
    private int currentPosition = 0;
    
    /* Suppress default constructor for noninstantiability */
    private CommandCenter()
    {
        
    }
    
    /**
     * Add new {@code Command} to the command center.
     * This method will execute {@code c.redo()} once.
     * @param c the {@code c.redo()} to be added
     */
    public void addCommand(Command c)
    {
        // If we have command that will be erase
        if (currentPosition < command.size())
        {
            for (int i=currentPosition; i<command.size(); i++)
                command.remove(i);
        }
        
        currentPosition++;
        command.add(c);
        c.redo();
    }
    
    /**
     * Undo and revert change made by the latest {@code Command}.
     * @return true if there is {@code Command} available to undo to otherwise return false
     */
    public boolean undo()
    {
        if (canUndo())
        {
            currentPosition--;
            command.get(currentPosition).undo();
            return true;
        }
        return false;
    }
    
    /**
     * Redo and execute the next {@code Command}.
     * @return true if there is {@code Command} available to redo to otherwise return false
     */
    public boolean redo()
    {
        if (canRedo())
        {
            command.get(currentPosition).redo();
            currentPosition++;
            return true;
        }
        return false;
    }

    /**
     * Check if we can redo.
     * @return true if there is {@code Command} available to undo to otherwise return false
     */
    public boolean canUndo()
    {
        return !command.isEmpty();
    }
    
    /**
     * Check if we can redo.
     * @return true if there is {@code Command} available to redo to otherwise return false
     */
    public boolean canRedo()
    {
        return currentPosition < command.size();
    }
}
