
package animator.core.command;

import animator.core.animation.LayerModel;
import animator.core.animation.TimelineModel;
import java.util.List;

/**
 *
 * @author Nuntipat narkthong
 */
public class RemoveLayerTimelineCommand extends Command
{
    // @TODO Maybe we don't need timeline model since layer know its timelinemodel
    private TimelineModel timeline;
    private List<LayerModel> layer;
    
    public RemoveLayerTimelineCommand(TimelineModel timelineModel, List<LayerModel> layerModel)
    {
        super("Remove layer");
        this.timeline = timelineModel;
        this.layer = layerModel;
    }
    
    @Override
    public void undo()
    {
        for (LayerModel l : layer)
            timeline.addLayer(l);
    }

    @Override
    public void redo()
    {
        for (LayerModel l : layer)
            timeline.removeLayer(l);
    }
}
