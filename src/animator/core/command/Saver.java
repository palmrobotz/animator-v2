package animator.core.command;

import animator.core.animation.ShapeProperty;
import animator.core.animation.AnimationModel;
import animator.core.animation.KeyTime;
import animator.core.animation.LayerModel;
import animator.core.animation.PropertyValue;
import animator.core.animation.SubAnimationModel;
import animator.core.animation.TimelineModel;
import animator.core.shape.AnimatorShape;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.PointData;
import animator.core.shape.RasterImage;
import animator.core.shape.SVGImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Xephrt
 */
public class Saver
{

    private String saveFile;
    private TimelineModel timeline;

    public Saver(TimelineModel model)
    {
        timeline = model;
    }

    public void setFile(String file)
    {
        this.saveFile = file;
    }

    public void saveFile() throws Exception
    {
        XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();
        XMLEventWriter eventWriter = outputFactory.createXMLEventWriter(new FileOutputStream(saveFile));
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t");

        //StartDocument startDocument = eventFactory.createStartDocument();
        //eventWriter.add(startDocument);

////////////////////////////////Animator Tag Start//////////////////////////////        
        StartElement animatorTitle = eventFactory.createStartElement("", "", "Animator");
        Attribute animatorVersion = eventFactory.createAttribute("Version", "0.0");
        eventWriter.add(animatorTitle);
        eventWriter.add(animatorVersion);
        eventWriter.add(endline);

////////////////////////////////Timeline Tag Start//////////////////////////////
        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Timeline"));
        eventWriter.add(endline);

        for (int i = 0; i < timeline.getLayerCount(); i++)
        {
            createLayerNode(eventWriter, timeline.getLayer(i));
        }

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "Timeline"));
        eventWriter.add(endline);
////////////////////////////////Timeline Tag End////////////////////////////////

        eventWriter.add(eventFactory.createEndElement("", "", "Animator"));
        eventWriter.add(endline);
        //eventWriter.add(eventFactory.createEndDocument());
        eventWriter.flush();
        eventWriter.close();

////////////////////////////////Animator Tag End////////////////////////////////
    }

    private void createLayerNode(XMLEventWriter eventWriter, LayerModel layer) throws XMLStreamException
    {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Layer"));
        eventWriter.add(eventFactory.createAttribute("ID", String.valueOf(layer.getLayerIndex())));
        eventWriter.add(endline);

        for (AnimationModel group : layer.getDataUnmodifiable().values())
        {
            createGroupNode(eventWriter, group);
        }

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "Layer"));
        eventWriter.add(endline);

    }
    
    private void createGroupNode(XMLEventWriter eventWriter, AnimationModel group) throws XMLStreamException
    {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Group"));
        eventWriter.add(eventFactory.createAttribute("Start", String.valueOf(group.getStartTime())));
        eventWriter.add(eventFactory.createAttribute("End", String.valueOf(group.getEndTime())));
        eventWriter.add(endline);
        //create keytime for group
        for (int i=0;i<group.getKeyTimeUnmodifiable().size();i++)
        {
            createGroupKeyTimeNode(eventWriter,group.getKeyTimeUnmodifiable().get(i));
        }
        //create sub animation fro group
        for (int i=0;i<group.getChildModelUnmodifiable().size();i++)
        {
            createAnimationNode(eventWriter, group.getChildModelUnmodifiable().get(i));
        }
        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "Group"));
        eventWriter.add(endline);
    }


    private void createAnimationNode(XMLEventWriter eventWriter, SubAnimationModel animation) throws XMLStreamException
    {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Animation"));
        eventWriter.add(eventFactory.createAttribute("Type", String.valueOf(animation.getShape().getType())));
        eventWriter.add(eventFactory.createAttribute("Start", String.valueOf(animation.getStartTime())));
        eventWriter.add(eventFactory.createAttribute("End", String.valueOf(animation.getEndTime())));
        eventWriter.add(endline);

        if(animation.getShape().getType() == AnimatorShape.RASTERIMAGE)
        {
            createImageNode(eventWriter, animation.getShape());
        }
        else if(animation.getShape().getType() == AnimatorShape.SVGIMAGE)
        {
            createSvgImageNode(eventWriter, animation.getShape());
        }
        else
        {
            createShapeNode(eventWriter, animation.getShape());
        }
        for (int k = 0; k < animation.getKeyTimeUnmodifiable().size(); k++)
        {
            createKeyTimeNode(eventWriter, animation.getKeyTimeUnmodifiable().get(k));
        }

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "Animation"));
        eventWriter.add(endline);
    }
    
    
    private void createImageNode(XMLEventWriter eventWriter, AnimatorShapeBase image) throws XMLStreamException
    {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Image"));
        
        File f = new File(((RasterImage)image).getFilePath());
        try {
            eventWriter.add(eventFactory.createCharacters(f.getCanonicalPath()));
        } catch (IOException ex) {
            System.err.println("No path");
        }

        eventWriter.add(eventFactory.createEndElement("", "", "Image"));
        eventWriter.add(endline);
    }
    
    private void createSvgImageNode(XMLEventWriter eventWriter, AnimatorShapeBase image) throws XMLStreamException
    {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Image"));
        
        File f = new File(((SVGImage)image).getFilePath());
        try {
            eventWriter.add(eventFactory.createCharacters(f.getCanonicalPath()));
        } catch (IOException ex) {
            System.err.println("No path");
        }

        eventWriter.add(eventFactory.createEndElement("", "", "Image"));
        eventWriter.add(endline);
    }

    private void createShapeNode(XMLEventWriter eventWriter, AnimatorShapeBase shape) throws XMLStreamException
    {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Shape"));
        eventWriter.add(endline);
        
        createPointNode(eventWriter, shape.getPointDataUnmodifiable());

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "Shape"));
        eventWriter.add(endline);
    }

    private void createKeyTimeNode(XMLEventWriter eventWriter, KeyTime keytime) throws XMLStreamException
    {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Keytime"));
        eventWriter.add(eventFactory.createAttribute("Time", String.valueOf(keytime.getTime())));
        eventWriter.add(endline);

        for (Entry<ShapeProperty, PropertyValue<?>> e : keytime.getMapData().entrySet())
        {
            createPropertyNode(eventWriter, e);
        }

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "Keytime"));
        eventWriter.add(endline);
    }
    
    private void createGroupKeyTimeNode(XMLEventWriter eventWriter, KeyTime keytime) throws XMLStreamException
    {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "GroupKeytime"));
        eventWriter.add(eventFactory.createAttribute("Time", String.valueOf(keytime.getTime())));
        eventWriter.add(endline);

        for (Entry<ShapeProperty, PropertyValue<?>> e : keytime.getMapData().entrySet())
        {
            createGroupPropertyNode(eventWriter, e);
        }

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "GroupKeytime"));
        eventWriter.add(endline);
    }
    
    private void createGroupPropertyNode(XMLEventWriter eventWriter, Entry<ShapeProperty, PropertyValue<?>> e) throws XMLStreamException
    {
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "GroupProperty"));
        eventWriter.add(eventFactory.createAttribute("Type", e.getKey().name()));

        if("POINT_DATA".equals(e.getKey().name()))
        {
            System.out.print(e.getValue().getWritableValue());
        }
        else
        {
        eventWriter.add(eventFactory.createCharacters(e.getValue().getValue().toString()));
        }
        eventWriter.add(eventFactory.createEndElement("", "", "GroupProperty"));
        eventWriter.add(endline);
    }

 
    
    private void createPointNode(XMLEventWriter eventWriter, List<List<DoubleProperty>> data) throws XMLStreamException
    {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Path"));
        eventWriter.add(endline);

        eventWriter.add(tab);
        for (int i = 0; i < data.size(); i++)
        {
            for (int y = 0; y < data.get(i).size(); y++)
            {
                eventWriter.add(eventFactory.createCharacters(
                        data.get(i).get(y).getValue().toString()));
                if (y + 1 < data.get(i).size())
                {
                    eventWriter.add(eventFactory.createCharacters(","));
                }
            }
            if (i + 1 < data.size())
            {
                eventWriter.add(eventFactory.createCharacters("|"));
            }
        }
        eventWriter.add(endline);

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createEndElement("", "", "Path"));
        eventWriter.add(endline);
    }

    private void createPropertyNode(XMLEventWriter eventWriter, Entry<ShapeProperty, PropertyValue<?>> e) throws XMLStreamException
    {
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent endline = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t\t\t\t\t\t");

        eventWriter.add(tab);
        eventWriter.add(eventFactory.createStartElement("", "", "Property"));
        eventWriter.add(eventFactory.createAttribute("Type", e.getKey().name()));

        if("POINT_DATA".equals(e.getKey().name()))
        {
           PropertyValue<PointData> pointData = (PropertyValue<PointData>) e.getValue();
           createPointPropertyNode(eventWriter, pointData.getValue().getPointList());
        }
        else
        {
        eventWriter.add(eventFactory.createCharacters(e.getValue().getValue().toString()));
        }
        
        eventWriter.add(eventFactory.createEndElement("", "", "Property"));
        eventWriter.add(endline);
    }
    
    private void createPointPropertyNode(XMLEventWriter eventWriter, List<List<Double>> data) throws XMLStreamException
    {
       XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        
       for (int i = 0; i < data.size(); i++)
        {
            for (int y = 0; y < data.get(i).size(); y++)
            {
                eventWriter.add(eventFactory.createCharacters(
                        data.get(i).get(y).toString()));
                if (y + 1 < data.get(i).size())
                {
                    eventWriter.add(eventFactory.createCharacters(","));
                }
            }
            if (i + 1 < data.size())
            {
                eventWriter.add(eventFactory.createCharacters("|"));
            }
        } 
    }
}
