
package animator.core.command;

import animator.core.animation.LayerModel;
import animator.core.animation.TimelineModel;

/**
 *
 * @author Nuntipat narkthong
 */
public class AddLayerTimelineCommand extends Command
{
    private TimelineModel timeline;
    private LayerModel layer;
    
    public AddLayerTimelineCommand(TimelineModel model)
    {
        super("Add new layer");
        this.timeline = model;
    }
    
    @Override
    public void undo()
    {
        timeline.removeLayer(layer);
    }

    @Override
    public void redo()
    {
        if (layer == null)
            layer = timeline.newLayer();
        else
            timeline.addLayer(layer);
    }
    
}
