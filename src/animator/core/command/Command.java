
package animator.core.command;

/**
 * Base class for all command object use in the {@code CommandCenter}.
 * @author Nuntipat narkthong
 */
public abstract class Command
{
    private String description;
    
    /**
     * Constructor of {@code Command}
     * @param description a string describe what's this command do. This will be
     * use when showing this command in the menubar or in the history window.
     */
    public Command(String description)
    {
        this.description = description;
    }
    
    /**
     * Get this command description
     * @return a string describe what's this command do. This will be use when 
     * showing this command in the menubar or in the history window.
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * Method to be called in order to revert change made by this {@code Command} object.
     */
    public abstract void undo();
    /**
     * Method to be called for this {@code Command} object to do some change.
     */
    public abstract void redo();
}
