
package animator.ui;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Nuntipat narkthong
 */
public class ToolBox extends ToolBar
{
    private static final ToolBox INSTANCE = new ToolBox();

    private final ReadOnlyObjectWrapper<Tool> toolSelected = new ReadOnlyObjectWrapper<>(Tool.SELECT);
    
    private ToolBox()
    {
        final ToggleButton select = new ToggleButton();
        select.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/animator/selector.png"))));
        
        final ToggleButton rectangle = new ToggleButton();
        rectangle.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/animator/rectangle.png"))));
        
        final ToggleButton triangle = new ToggleButton();
        triangle.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/animator/triangle.png"))));
        
        final ToggleButton circle = new ToggleButton();
        circle.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/animator/circle.png"))));
        
        final ToggleButton path = new ToggleButton();
        path.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/animator/path.png"))));
        
        final ToggleButton brush = new ToggleButton();
        brush.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/animator/brush.png"))));
        
        final ToggleButton zoom = new ToggleButton();
        zoom.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/animator/zoomin.png"))));
        
        final ToggleGroup toggleGroup = new ToggleGroup();
        select.setToggleGroup(toggleGroup);
        rectangle.setToggleGroup(toggleGroup);
        triangle.setToggleGroup(toggleGroup);
        circle.setToggleGroup(toggleGroup);
        path.setToggleGroup(toggleGroup);
        brush.setToggleGroup(toggleGroup);
        zoom.setToggleGroup(toggleGroup);
        
        toggleGroup.selectToggle(select);
        toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>()
        {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1)
            {
                if(t1 == null)
                    toggleGroup.selectToggle(t);
                else if (t1 == select)
                    toolSelected.set(Tool.SELECT);
                else if (t1 == rectangle)
                    toolSelected.set(Tool.RECTANGLE);
                else if (t1 == triangle)
                    toolSelected.set(Tool.TRIANGLE);
                else if (t1 == circle)
                    toolSelected.set(Tool.CIRCLE);
                else if (t1 == path)
                    toolSelected.set(Tool.PATH);
                else if (t1 == brush)
                    toolSelected.set(Tool.BRUSH);
                else if (t1 == zoom)
                    toolSelected.set(Tool.ZOOM);
                else 
                    throw new RuntimeException("Unknown tool");
            }           
        });
        setOrientation(Orientation.VERTICAL);
        getItems().addAll(select, rectangle, triangle, circle, path, brush, zoom);
    }
    
    public static ToolBox getInstance()
    {
        return INSTANCE;
    }
    
    public ReadOnlyObjectProperty<Tool> toolSelectProperty()
    {
        return toolSelected.getReadOnlyProperty();
    }
    
    public Tool getToolSelect()
    {
        return toolSelected.get();
    }
}
