package animator.ui;

import animator.core.animation.ShapeProperty;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.event.ShapeChangedEvent;
import animator.core.shape.event.ShapeChangedListener;
import animator.ui.color.ColorMixer;
import java.util.List;
import javafx.beans.binding.When;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyDoubleWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;

/**
 *
 * @author Xephrt
 */
public class StrokeConfig extends Region
{
    private static final StrokeConfig INSTANCE = new StrokeConfig();
    
    private ReadOnlyObjectProperty<List<AnimatorShapeBase>> selectedShape;
    
    private final ReadOnlyBooleanWrapper enable = new ReadOnlyBooleanWrapper(false);
    // Export property
    private final ReadOnlyDoubleWrapper width = new ReadOnlyDoubleWrapper(1);
    private final ReadOnlyObjectProperty<StrokeLineJoin> joint;
    private final ReadOnlyObjectProperty<StrokeLineCap> cap;
    private final ReadOnlyObjectProperty<Tool> toolSelected = ToolBox.getInstance().toolSelectProperty();
    private final ObjectProperty<Color> bgColor = ColorMixer.getInstance().bgColorProperty();
    private final ObjectProperty<Color> stColor = ColorMixer.getInstance().fgColorProperty();
    private final Slider sizeSlider;

    private StrokeConfig()
    {
        DoubleProperty sliderVal;
        DoubleProperty numFieldVal;

        final Label sizeLabel = new Label("Width");
        sizeSlider = new Slider();
        sizeSlider.setMaxWidth(80);
        sizeSlider.setMax(10);
        sliderVal = sizeSlider.valueProperty();

        final NumberField sizeNum = new NumberField();
        sizeNum.setMaxWidth(40);
        sizeNum.setMax(10);
        numFieldVal = sizeNum.valueProperty();

        sliderVal.bindBidirectional(numFieldVal);
        width.bind(sliderVal);
        width.addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
            {
                for (AnimatorShapeBase asb : selectedShape.get())
                {
                    if (asb.getSupportProperty().contains(ShapeProperty.STROKE_WIDTH))
                        asb.setStrokeWidth(t1.doubleValue());
                }
            }
        });

        HBox sizeChanger = new HBox();
        sizeChanger.getChildren().addAll(sizeSlider, sizeNum);
        sizeChanger.setSpacing(2);
        sizeChanger.setAlignment(Pos.CENTER);

        final Label jointLabel = new Label("Joint");
        final ChoiceBox<StrokeLineJoin> jointChoice = new ChoiceBox<>();
        jointChoice.setItems(FXCollections.observableArrayList(StrokeLineJoin.values()));
        jointChoice.getSelectionModel().selectFirst();
        joint = jointChoice.getSelectionModel().selectedItemProperty();

        final Label capLabel = new Label("Cap");
        final ChoiceBox<StrokeLineCap> capChoice = new ChoiceBox<>();
        capChoice.setItems(FXCollections.observableArrayList(StrokeLineCap.values()));
        capChoice.getSelectionModel().selectFirst();
        cap = capChoice.getSelectionModel().selectedItemProperty();

        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setHgap(4);

        grid.add(sizeLabel, 1, 0);
        grid.add(sizeChanger, 2, 0);

        grid.add(jointLabel, 1, 1);
        grid.add(jointChoice, 2, 1);

        grid.add(capLabel, 1, 2);
        grid.add(capChoice, 2, 2);


        final PreviewBox pvB = new PreviewBox();

        disableProperty().bind(enable.not());
        //disableProperty().bind(toolSelected.isEqualTo(Tool.SELECT));
        //pvB.disableProperty().bind(toolSelected.isEqualTo(Tool.SELECT));

        //grid.add(pvB, 0, 0, 1, 3);
        /*
         Rectangle reg = new Rectangle();
         reg.setWidth(50);
         reg.setHeight(50);
         */
        HBox root = new HBox();
        root.setSpacing(5);
        root.getChildren().addAll(pvB, grid);
        root.setPadding(new Insets(5, 10, 5, 10));
        root.setAlignment(Pos.CENTER);
        this.getChildren().add(root);
    }
    private ChangeListener<List<AnimatorShapeBase>> selectShapeChanged = new ChangeListener<List<AnimatorShapeBase>>()
    {
        @Override
        public void changed(ObservableValue<? extends List<AnimatorShapeBase>> ov, List<AnimatorShapeBase> oldShape, List<AnimatorShapeBase> newShape)
        {
            // Remove binding with old Shape
            if (!oldShape.isEmpty())
            {
                for (AnimatorShapeBase asb : oldShape)
                {
                    asb.removeShapeChangedListener(shapeChangedListener);
                }
            }

            // Bind with new Shape
            if (!newShape.isEmpty())
            {
                // @TODO Fix since now initial the slider to the stroke width of the last shape
                for (AnimatorShapeBase asb : newShape)
                {
                    if (asb.getSupportProperty().contains(ShapeProperty.STROKE_WIDTH))
                    {
                        asb.addShapeChangedListener(shapeChangedListener);
                        sizeSlider.setValue(asb.getStrokeWidth());
                    }
                }
                
                enable.set(true);
            }
            else
            {
                sizeSlider.setValue(0);

                enable.set(false);
            }
        }
    };
    private ShapeChangedListener shapeChangedListener = new ShapeChangedListener()
    {
        @Override
        public void onShapeChanged(ShapeChangedEvent e)
        {
            if (e.getChangedProperty() == ShapeProperty.STROKE_WIDTH)
            {
                sizeSlider.setValue(e.getSource().getStrokeWidth());
            }

        }
    };

    public void setSelectShapeProperty(ReadOnlyObjectProperty<List<AnimatorShapeBase>> property)
    {
        if (selectedShape != null)
            throw new IllegalStateException("Selection property shouldn't change through out the program execution period");
            
        selectedShape = property;
        selectedShape.addListener(selectShapeChanged);
    }

    public static StrokeConfig getInstance()
    {
        return INSTANCE;
    }

    public ReadOnlyObjectProperty<StrokeLineJoin> jointProperty()
    {
        return joint;
    }

    public StrokeLineJoin getJointProperty()
    {
        return joint.get();
    }

    public ReadOnlyObjectProperty<StrokeLineCap> capProperty()
    {
        return cap;
    }

    public StrokeLineCap getCapProperty()
    {
        return cap.get();
    }

    public ReadOnlyDoubleProperty strokeWidthProperty()
    {
        return width.getReadOnlyProperty();
    }

    public double getStrokeWidth()
    {
        return width.get();
    }

    /**
     *
     */
    private class PreviewBox extends HBox
    {

        private Rectangle frame;

        public PreviewBox()
        {
            frame = new Rectangle();
            frame.setFill(Color.TRANSPARENT);
            frame.strokeProperty().bind(new When(disableProperty())
                    .then(Color.rgb(200, 200, 200, 0.5))
                    .otherwise(Color.rgb(200, 200, 200, 1)));
            frame.setStrokeWidth(1);
            frame.setWidth(73);
            frame.setHeight(73);

            final Path rect = new Path();
            rect.getElements().add(new MoveTo(10, 10));
            rect.getElements().add(new LineTo(50, 10));
            rect.getElements().add(new LineTo(50, 50));
            rect.getElements().add(new LineTo(10, 50));
            rect.getElements().add(new ClosePath());
            rect.setFill(bgColor.getValue());
            rect.setStroke(stColor.getValue());
            rect.setStrokeWidth(width.getValue());
            rect.setStrokeLineJoin(StrokeLineJoin.MITER);
            rect.setStrokeLineCap(StrokeLineCap.BUTT);
            rect.setStrokeType(StrokeType.CENTERED);
            rect.setVisible(false);

            final Path triangle = new Path();
            triangle.getElements().add(new MoveTo(0, 50));
            triangle.getElements().add(new LineTo((0 + 50) / 2, 0));
            triangle.getElements().add(new LineTo(50, 50));
            triangle.getElements().add(new ClosePath());
            triangle.setFill(bgColor.getValue());
            triangle.setStroke(stColor.getValue());
            triangle.setStrokeWidth(width.getValue());
            triangle.setStrokeLineJoin(StrokeLineJoin.MITER);
            triangle.setStrokeLineCap(StrokeLineCap.BUTT);
            triangle.setStrokeType(StrokeType.CENTERED);
            triangle.setVisible(false);

            final Path path = new Path();
            path.getElements().add(new MoveTo(0, 0));
            path.getElements().add(new CubicCurveTo(50, 0, 0, 50, 50, 50));
            path.setFill(bgColor.getValue());
            path.setStroke(stColor.getValue());
            path.setStrokeWidth(width.getValue());
            path.setStrokeLineJoin(StrokeLineJoin.MITER);
            path.setStrokeLineCap(StrokeLineCap.BUTT);
            path.setStrokeType(StrokeType.CENTERED);
            path.setVisible(false);


            StackPane g = new StackPane();
            g.getChildren().addAll(frame, rect, triangle, path);
            g.setAlignment(Pos.CENTER);

            toolSelected.addListener(new ChangeListener<Tool>()
            {
                @Override
                public void changed(ObservableValue<? extends Tool> ov, Tool t, Tool t1)
                {
                    if (t1.equals(Tool.SELECT))
                    {
                        rect.setVisible(false);
                        triangle.setVisible(false);
                        path.setVisible(false);
                    }
                    else if (t1.equals(Tool.RECTANGLE))
                    {
                        rect.setVisible(true);
                        triangle.setVisible(false);
                        path.setVisible(false);
                    }
                    else if (t1.equals(Tool.TRIANGLE))
                    {
                        rect.setVisible(false);
                        triangle.setVisible(true);
                        path.setVisible(false);
                    }
                    else if (t1.equals(Tool.PATH))
                    {
                        rect.setVisible(false);
                        triangle.setVisible(false);
                        path.setVisible(true);
                    }
                }
            });


            width.addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
                {

                    rect.setStrokeWidth(t1.intValue());
                    triangle.setStrokeWidth(t1.intValue());
                    path.setStrokeWidth(t1.intValue());
                }
            });

            joint.addListener(new ChangeListener<StrokeLineJoin>()
            {
                @Override
                public void changed(ObservableValue<? extends StrokeLineJoin> ov, StrokeLineJoin t, StrokeLineJoin t1)
                {
                    if (t1.equals(StrokeLineJoin.MITER))
                    {
                        rect.setStrokeLineJoin(StrokeLineJoin.MITER);
                        triangle.setStrokeLineJoin(StrokeLineJoin.MITER);
                        path.setStrokeLineJoin(StrokeLineJoin.MITER);
                    }
                    else if (t1.equals(StrokeLineJoin.BEVEL))
                    {
                        rect.setStrokeLineJoin(StrokeLineJoin.BEVEL);
                        triangle.setStrokeLineJoin(StrokeLineJoin.BEVEL);
                        path.setStrokeLineJoin(StrokeLineJoin.BEVEL);
                    }
                    else if (t1.equals(StrokeLineJoin.ROUND))
                    {
                        rect.setStrokeLineJoin(StrokeLineJoin.ROUND);
                        triangle.setStrokeLineJoin(StrokeLineJoin.ROUND);
                        path.setStrokeLineJoin(StrokeLineJoin.ROUND);
                    }
                }
            });

            cap.addListener(new ChangeListener<StrokeLineCap>()
            {
                @Override
                public void changed(ObservableValue<? extends StrokeLineCap> ov, StrokeLineCap t, StrokeLineCap t1)
                {
                    if (t1.equals(StrokeLineCap.BUTT))
                    {
                        rect.setStrokeLineCap(StrokeLineCap.BUTT);
                        triangle.setStrokeLineCap(StrokeLineCap.BUTT);
                        path.setStrokeLineCap(StrokeLineCap.BUTT);
                    }
                    else if (t1.equals(StrokeLineCap.ROUND))
                    {
                        rect.setStrokeLineCap(StrokeLineCap.ROUND);
                        triangle.setStrokeLineCap(StrokeLineCap.ROUND);
                        path.setStrokeLineCap(StrokeLineCap.ROUND);
                    }
                    else if (t1.equals(StrokeLineCap.SQUARE))
                    {
                        rect.setStrokeLineCap(StrokeLineCap.SQUARE);
                        triangle.setStrokeLineCap(StrokeLineCap.SQUARE);
                        path.setStrokeLineCap(StrokeLineCap.SQUARE);
                    }
                }
            });

            bgColor.addListener(new ChangeListener<Color>()
            {
                @Override
                public void changed(ObservableValue<? extends Color> ov, Color t, Color t1)
                {
                    rect.setFill(t1);
                    triangle.setFill(t1);
                    path.setFill(t1);
                }
            });

            stColor.addListener(new ChangeListener<Color>()
            {
                @Override
                public void changed(ObservableValue<? extends Color> ov, Color t, Color t1)
                {
                    rect.setStroke(t1);
                    triangle.setStroke(t1);
                    path.setFill(t1);
                }
            });


            this.getChildren().add(g);
            this.setAlignment(Pos.CENTER);
        }
    }
}
