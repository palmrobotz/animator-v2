package animator.ui;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Xephrt
 */
public class NumberField extends TextField
{

    private final double defualtMax = 100;
    private ChangeListener<Number> defualtDPListener;
    private ChangeListener<String> defualtTextListener;
    private final StringProperty sp = this.textProperty();
    private final DoubleProperty dp = new SimpleDoubleProperty(0);

    public NumberField()
    {
        setText("0");
        this.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent t)
            {
                char ar[] = t.getCharacter().toCharArray();
                char ch = ar[t.getCharacter().toCharArray().length - 1];
                if (!(ch >= '0' && ch <= '9'))
                {
                    System.out.println("The char you entered is not a number");
                    t.consume();
                }
            }
        });

        defualtTextListener = new ChangeListener<String>()
        {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1)
            {
                if (t1.isEmpty())
                {
                    setText("0");
                    dp.set(0);
                }
                else
                {
                    Integer val = Integer.parseInt(t1);
                    if (val <= defualtMax)
                    {
                        setText(String.valueOf(val));
                        dp.set(Double.valueOf(t1));
                    }
                    else
                    {
                        setText(t);
                        dp.set(Double.valueOf(t));
                    }
                }
            }
        };

        this.textProperty().addListener(defualtTextListener);

        defualtDPListener = new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
            {
                if (t1.doubleValue() <= defualtMax)
                {
                    setText(String.valueOf(t1.intValue()));
                    dp.set(t1.intValue());
                }
                else
                {
                    setText(String.valueOf(t1.intValue()));
                    dp.set(t.intValue());
                }
            }
        };

        dp.addListener(defualtDPListener);

    }

    public Double getValue()
    {
        return dp.getValue();
    }

    public DoubleProperty valueProperty()
    {
        return dp;
    }

    public void setMax(double maxNum)
    {
        final double num = maxNum;
        this.textProperty().removeListener(defualtTextListener);
        defualtTextListener = new ChangeListener<String>()
        {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1)
            {
                if (t1.isEmpty())
                {
                    setText("0");
                    dp.set(0);
                }
                else
                {
                    Integer val = Integer.parseInt(t1);
                    if (val <= num)
                    {
                        setText(String.valueOf(val));
                        dp.set(Double.valueOf(t1));
                    }
                    else
                    {
                        setText(t);
                        dp.set(Double.valueOf(t));
                    }
                }

            }
        };
        this.textProperty().addListener(defualtTextListener);
        dp.removeListener(defualtDPListener);
        defualtDPListener = new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
            {
                if (t1.doubleValue() <= num)
                {
                    setText(String.valueOf(t1.intValue()));
                    dp.set(t1.intValue());
                }
                else
                {
                    setText(String.valueOf(t1.intValue()));
                    dp.set(t.intValue());
                }
            }
        };

        dp.addListener(defualtDPListener);
    }
}
