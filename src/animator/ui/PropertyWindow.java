
package animator.ui;

import animator.core.animation.ShapeProperty;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.GroupShape;
import animator.core.shape.event.ShapeChangedEvent;
import animator.core.shape.event.ShapeChangedListener;
import java.math.BigDecimal;
import java.util.List;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;

/**
 *
 * @author nuntipat
 */
public class PropertyWindow extends Region
{
    private ReadOnlyObjectProperty<List<AnimatorShapeBase> > selectedShape;
    
    private final NumberTextField xTextField;
    private final NumberTextField yTextField;
    private final NumberTextField rotateTextField;
    private final NumberTextField alphaTextField;
    
    private static final double TEXTFIELD_MAX_WIDTH = 70;
    
    private final ReadOnlyBooleanWrapper enable = new ReadOnlyBooleanWrapper(false);
    
    public PropertyWindow(ReadOnlyObjectProperty<List<AnimatorShapeBase> > selectedShapeProperty)
    {
        selectedShape = selectedShapeProperty;
        selectedShape.addListener(selectShapeChanged);
        
        
        xTextField = new NumberTextField();
        xTextField.setMaxWidth(TEXTFIELD_MAX_WIDTH);
        xTextField.numberProperty().addListener(new ChangeListener<BigDecimal>()
        {
            @Override
            public void changed(ObservableValue<? extends BigDecimal> ov, BigDecimal t, BigDecimal t1)
            {
                if (selectedShape.get().size() == 1)
                {
                    AnimatorShapeBase asb = selectedShape.get().get(0);
                    if (asb instanceof GroupShape)
                        asb.setOffsetX(t1.doubleValue() - asb.getX());  // The value in the text field is the sum of x and offset x
                    else
                        asb.setX(t1.doubleValue() - asb.getOffsetX());
                }
            }
        });
        yTextField = new NumberTextField();
        yTextField.setMaxWidth(TEXTFIELD_MAX_WIDTH);
        yTextField.numberProperty().addListener(new ChangeListener<BigDecimal>()
        {
            @Override
            public void changed(ObservableValue<? extends BigDecimal> ov, BigDecimal t, BigDecimal t1)
            {
                if (selectedShape.get().size() == 1)
                {
                    AnimatorShapeBase asb = selectedShape.get().get(0);
                    if (asb instanceof GroupShape)
                        asb.setOffsetY(t1.doubleValue() - asb.getY());
                    else
                        asb.setY(t1.doubleValue() - asb.getOffsetY());
                }
            }
        });
        rotateTextField = new NumberTextField();
        rotateTextField.setMaxWidth(TEXTFIELD_MAX_WIDTH);
        rotateTextField.numberProperty().addListener(new ChangeListener<BigDecimal>()
        {
            @Override
            public void changed(ObservableValue<? extends BigDecimal> ov, BigDecimal t, BigDecimal t1)
            {
                if (selectedShape.get().size() == 1)
                {
                    AnimatorShapeBase asb = selectedShape.get().get(0);
                    if (asb instanceof GroupShape)
                        asb.setOffsetRotation(t1.doubleValue());
                    else
                        asb.setRotation(t1.doubleValue());
                }
            }
        });
        alphaTextField = new NumberTextField();
        alphaTextField.setMaxWidth(TEXTFIELD_MAX_WIDTH);
        alphaTextField.numberProperty().addListener(new ChangeListener<BigDecimal>()
        {
            @Override
            public void changed(ObservableValue<? extends BigDecimal> ov, BigDecimal t, BigDecimal t1)
            {
                if (selectedShape.get().size() == 1)
                {
                    AnimatorShapeBase asb = selectedShape.get().get(0);
                    if (asb instanceof GroupShape)
                        asb.setOffsetAlpha(t1.doubleValue());
                    else
                        asb.setAlpha(t1.doubleValue());
                }
            }
        });
        
        Label xLabel = new Label("X");
        xLabel.setLabelFor(xTextField);
        
        Label yLabel = new Label("Y");
        yLabel.setLabelFor(yTextField);
        
        Label rotateLabel = new Label("Rotate");
        yLabel.setLabelFor(rotateTextField);
        
        Label alphaLabel = new Label("Alpha");
        yLabel.setLabelFor(alphaTextField);
        
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(5);
        gridPane.setPadding(new Insets(10));
        gridPane.disableProperty().bind(enable.not());
        gridPane.add(xLabel, 0, 0);
        gridPane.add(xTextField, 1, 0);
        gridPane.add(yLabel, 2, 0);
        gridPane.add(yTextField, 3, 0);
        gridPane.add(rotateLabel, 0, 1);
        gridPane.add(rotateTextField, 1, 1);
        gridPane.add(alphaLabel, 2, 1);
        gridPane.add(alphaTextField, 3, 1);
        
        getChildren().add(gridPane);
    }
    
    private ChangeListener<List<AnimatorShapeBase> > selectShapeChanged = new ChangeListener<List<AnimatorShapeBase> >() 
    {
        @Override
        public void changed(ObservableValue<? extends List<AnimatorShapeBase> > ov, List<AnimatorShapeBase> oldShape, List<AnimatorShapeBase> newShape)
        {
            // Remove binding with old Shape
            if (!oldShape.isEmpty())
            {
                oldShape.get(0).removeShapeChangedListener(shapeChangedListener);
            }
            
            // Bind with new Shape
            if (newShape.size() == 1)
            {
                AnimatorShapeBase asb = newShape.get(0);
                
                xTextField.setText(String.valueOf(asb.getX() + asb.getOffsetX()));
                yTextField.setText(String.valueOf(asb.getY() + asb.getOffsetY()));
                if (asb instanceof GroupShape)
                {
                    rotateTextField.setText(String.valueOf(asb.getOffsetRotation()));
                    alphaTextField.setText(String.valueOf(asb.getOffsetAlpha()));
                }
                else
                {
                    rotateTextField.setText(String.valueOf(asb.getRotation()));
                    alphaTextField.setText(String.valueOf(asb.getAlpha()));
                }
                
                asb.addShapeChangedListener(shapeChangedListener);
                
                enable.set(true);
            }
            else
            {
                xTextField.setText("");
                yTextField.setText("");
                rotateTextField.setText("");
                alphaTextField.setText("");
                
                enable.set(false);
            }
        }
    };
    
    private ShapeChangedListener shapeChangedListener = new ShapeChangedListener()
    {
        @Override
        public void onShapeChanged(ShapeChangedEvent e)
        {
            if (e.getChangedProperty() == ShapeProperty.X_POSITION || e.getChangedProperty() == ShapeProperty.OFFSET_X_POSITION)
            {
                xTextField.setText(String.valueOf(e.getSource().getX() + e.getSource().getOffsetX()));
            }
            else if (e.getChangedProperty() == ShapeProperty.Y_POSITION || e.getChangedProperty() == ShapeProperty.OFFSET_Y_POSITION)
            {
                yTextField.setText(String.valueOf(e.getSource().getY() + e.getSource().getOffsetY()));
            }
            else if (e.getChangedProperty() == ShapeProperty.ROTATION)
            {
                rotateTextField.setText(String.valueOf(e.getSource().getRotation()));
            }
            else if (e.getChangedProperty() == ShapeProperty.ALPHA)
            {
                alphaTextField.setText(String.valueOf(e.getSource().getAlpha()));
            }
            else if (e.getChangedProperty() == ShapeProperty.OFFSET_ROTATION)
            {
                rotateTextField.setText(String.valueOf(e.getSource().getOffsetRotation()));
            }
            else if (e.getChangedProperty() == ShapeProperty.OFFSET_ALPHA)
            {
                alphaTextField.setText(String.valueOf(e.getSource().getOffsetAlpha()));
            }
        }      
    };

//    @Override
//    public ReadOnlyObjectProperty<AnimatorShapeBase> selectObjectProperty()
//    {
//        return selectedShape;
//    }
//
//    @Override
//    public void setSelectObjectProperty(ReadOnlyObjectProperty<AnimatorShapeBase> property)
//    {
//        // Remove listener from the old property
//        if (selectedShape != null)
//            selectedShape.removeListener(selectShapeChanged);
//        
//        selectedShape = property;
//        
//        // Add listener to the new property if it isn't null
//        if (selectedShape != null)
//            selectedShape.addListener(selectShapeChanged);
//    }
}
