
package animator.ui.color;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;

/**
 *
 * @author ShockGoRat
 */
public class ColorSwatches extends Region
{
    private ObjectProperty<Color> selectedColor = new SimpleObjectProperty<>();
    private IntegerProperty selectedPalette = new SimpleIntegerProperty(0);

    private static final String[] filename = new String[]
    {
        "/animator/standard.txt",
        "/animator/Skintone.txt",
        "/animator/Pastel.txt"
    };

    public ColorSwatches()
    {
        final GridPane[] colorgrid = new GridPane[3];
        colorgrid[0] = createColorGrid(0);  // standard
        colorgrid[1] = createColorGrid(1);  //skintone
        colorgrid[2] = createColorGrid(2);  //pastel

        colorgrid[0].setVisible(true);
        colorgrid[1].setVisible(false);
        colorgrid[2].setVisible(false);

        StackPane s = new StackPane();
        s.getChildren().addAll(colorgrid);

        VBox v = new VBox();
        v.setPadding(new Insets(10));
        v.setSpacing(5);
        v.getChildren().addAll(s);

        selectedPalette.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                if(t1.intValue()==0)
                {
                    colorgrid[0].setVisible(true);
                    colorgrid[1].setVisible(false);
                    colorgrid[2].setVisible(false);
                }
                else if(t1.intValue()==1)
                {
                    colorgrid[0].setVisible(false);
                    colorgrid[1].setVisible(true);
                    colorgrid[2].setVisible(false);
                }
                else if(t1.intValue()==2)
                {
                    colorgrid[0].setVisible(false);
                    colorgrid[1].setVisible(false);
                    colorgrid[2].setVisible(true);                    
                }
            }
        });
        
        getChildren().add(v);
    }

    public ObjectProperty<Color> selectedColorProperty()
    {
        return selectedColor;
    }

    public IntegerProperty selectedPaletteProperty()
    {
        return selectedPalette;
    }

    // Create the swatch for the text file index given
    private GridPane createColorGrid(int type)
    {
        GridPane gridPane = new GridPane();

        int col = 0, row = 0;
        double r, g, b;
        
        InputStream is = getClass().getResourceAsStream(filename[type]);
        if (is == null)
            throw new RuntimeException("File not include in jar file!!!");
            
        try (Scanner sc = new Scanner(is))
        {
            sc.nextInt(); // skip first value
            while (sc.hasNextInt())
            {
                r = sc.nextDouble() / 255;
                g = sc.nextDouble() / 255;
                b = sc.nextDouble() / 255;

                gridPane.add(RectangleBuilder
                        .create()
                        .width(15)
                        .height(15)
                        .fill(Color.color(r, g, b))
                        .stroke(Color.BLACK)
                        .strokeWidth(0.3)
                        .onMouseClicked(cellListener)
                        .build()
                        , col, row);

                col++;
                if (col >= 16)
                {
                    row++;
                    col = 0;
                }
            }
        }

        return gridPane;
    }

    // Listener for each cell in the swatches
    private EventHandler<MouseEvent> cellListener = new EventHandler<MouseEvent>()
    {
        @Override
        public void handle(MouseEvent t)
        {
            Rectangle r = (Rectangle) t.getSource();
            selectedColor.set((Color) r.getFill());
        }
    };
}
