
package animator.ui.color;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.scene.paint.Color;

/**
 *
 * @author nuntipat
 */
public interface ColorRecievable
{
    public ReadOnlyObjectProperty<Color> fillColorProperty();
    public void setFillColorProperty(ReadOnlyObjectProperty<Color> property);
    
    public ReadOnlyObjectProperty<Color> strokeColorProperty();
    public void setStrokeColorProperty(ReadOnlyObjectProperty<Color> property);
}
