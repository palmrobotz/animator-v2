package animator.ui.color;

import java.math.BigDecimal;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;

/**
 *
 * @author Xephrt
 */
public class ColorSlider extends Region
{

    private final double OFFSET = 7;
    private double max = 100;
    private double min = 0;
    private DoubleProperty value = new SimpleDoubleProperty();
    private Rectangle bar = new Rectangle();
    private Rectangle emptyBar = new Rectangle();
    private Circle thumb = new Circle();
    private double mutiplyer = 1;
    private boolean decimalMode = true;
    private ObjectProperty<BigDecimal> colorValProperty = new SimpleObjectProperty<>();
    
    public ColorSlider()
    {
        initialBar();
        initialThumb();
        Group g = new Group();
        g.getChildren().addAll(emptyBar, bar, thumb);

        this.getChildren().add(g);
        
        value.addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                if(decimalMode==true)
                    colorValProperty.setValue(BigDecimal.valueOf((ColorSlider.this.mutiplyer*t1.doubleValue())));
                else
                    colorValProperty.setValue(BigDecimal.valueOf((int)(ColorSlider.this.mutiplyer*t1.doubleValue())));
            }
        });
        colorValProperty.addListener(new ChangeListener<BigDecimal>() {

            @Override
            public void changed(ObservableValue<? extends BigDecimal> ov, BigDecimal t, BigDecimal t1) {
                value.set(t1.doubleValue()/ColorSlider.this.mutiplyer);
            }
        });
    }

    public ColorSlider(double maxVal, double minVal)
    {
        max = maxVal;
        min = minVal;

        initialBar();
        initialThumb();

        Group g = new Group();
        // @TODO not sure if forget empty bar or not
        g.getChildren().addAll(bar, thumb);
        
        this.getChildren().add(g);
    }

    private void initialBar()
    {
        bar.setY(OFFSET / 2);
        bar.setX(OFFSET);
        bar.setWidth(120);
        bar.setHeight(8);
        bar.setArcHeight(10);
        bar.setArcWidth(10);
        bar.setStroke(Color.rgb(20, 20, 20));

        LinearGradient lg = LinearGradient.valueOf("linear-gradient(#b9b9b9 0%, #c2c2c2 20%, #afafaf 80%, #c8c8c8 100%)");

        bar.setFill(lg);
        bar.setStrokeLineJoin(StrokeLineJoin.ROUND);
        bar.setStrokeType(StrokeType.INSIDE);

        bar.setOnMousePressed(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                thumb.setCenterX(t.getX());
                value.set((((max - min) * getPercent()) / 100) + min);
            }
        });

        Image img = new Image("/animator/blank.png");
        ImagePattern imgP = new ImagePattern(img, 0, 0, 6, 6, false);

        emptyBar.setY(OFFSET / 2);
        emptyBar.setX(OFFSET);
        emptyBar.setWidth(bar.getWidth());
        emptyBar.setHeight(bar.getHeight());
        emptyBar.setArcHeight(bar.getArcHeight());
        emptyBar.setArcWidth(bar.getArcWidth());
        emptyBar.setFill(imgP);
    }

    private void initialThumb()
    {
        thumb.setCenterX(bar.getLayoutX() + OFFSET);
        thumb.setCenterY(bar.getLayoutY() + (bar.getHeight() / 2) + OFFSET / 2);
        thumb.setRadius(OFFSET);

        final RadialGradient rgGreen = RadialGradient.valueOf("radial-gradient(center 50% -40%, radius 200%, #b8ee36 45%, #80c800 50%)");
        final RadialGradient rgWhite = RadialGradient.valueOf("radial-gradient(center 50% -40%, radius 200%, #ebebeb 45%, #d7d7d7 50%)");
        final RadialGradient rgWhiteOver = RadialGradient.valueOf("radial-gradient(center 50% -40%, radius 200%, #fefefe 45%, #eaeaea 50%)");

        thumb.setFill(rgWhite);
        thumb.setStroke(Color.BLACK);

        thumb.setOnMouseDragged(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                double maxPos = bar.getWidth() + OFFSET;
                double minPos = OFFSET;
                //System.out.println("Val:" + getVal());
                value.set((((max - min) * getPercent()) / 100) + min);
                if (!(t.getX() > maxPos) && !(t.getX() < minPos))
                {
                    thumb.setCenterX(t.getX());
                }
                else if (t.getX() > maxPos)
                {
                    thumb.setCenterX(maxPos);
                }
                else if (t.getX() < minPos)
                {
                    thumb.setCenterX(minPos);
                }

            }
        });

        thumb.setOnMouseEntered(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                thumb.setFill(rgWhiteOver);
            }
        });

        thumb.setOnMouseExited(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                thumb.setFill(rgWhite);
            }
        });
    }

    public double getPercent()
    {
        double posPercent;
        posPercent = ((thumb.getCenterX() - OFFSET) * (100)) / (bar.getWidth());

        return posPercent;
    }

    public void setMax(double val)
    {
        max = val;
    }

    public void setMin(double val)
    {
        min = val;
    }

    public void setRange(double maxVal, double minVal)
    {
        max = maxVal;
        min = minVal;
    }

    // @TODO Did not Test!!
    public void setBarWidth(double width)
    {
        double beforePer = getPercent();
        bar.setWidth(width);
        emptyBar.setWidth(width);
        setPercent(beforePer);
    }

    public double getVal()
    {
        return (((max - min) * getPercent()) / 100) + min;
    }

    public DoubleProperty valueProperty()
    {
        return value;
    }
    
    public ObjectProperty<BigDecimal> colorValueProperty()
    {
        return colorValProperty;
    }


    
    public void setValue(double val)
    {
        if (val >= min && val <= max)
        {
            double percent = ((val - min) * 100) / max - min;
            double xPosition = ((percent * (bar.getWidth())) / 100) + OFFSET;
            thumb.setCenterX(xPosition);
            value.set((((max - min) * getPercent()) / 100) + min);
        }
        else
        {
            throw new IllegalArgumentException("Fancy slider,value out of range.");
        }

    }

    // @TODO Did not Test!!
    public void setPercent(double percent)
    {
        if (percent >= 0 && percent <= 100)
        {
            double xPosition = ((percent * (bar.getWidth())) / 100) + OFFSET;
            thumb.setCenterX(xPosition);
            value.set((((max - min) * getPercent()) / 100) + min);
        }
        else
        {
            throw new IllegalArgumentException("Fancy slider,percent value out of range.");
        }

    }

    // Not safe to have 2 version of paintBar (Paint is superclass of Color)
//    public void paintBar(Color c)
//    {
//        bar.setFill(c);
//    }

    public void setMultiplyer(double d)
    {
        mutiplyer=d;
    }
    
    public void setDecimalMode(boolean b)
    {
        decimalMode=b;
    }
    
    public void paintBar(Color c1, Color c2)
    {
        Stop[] stops = new Stop[]
        {
            new Stop(0, c1), new Stop(1, c2)
        };
        LinearGradient lg = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
        bar.setFill(lg);
    }

    public void paintBar(Paint p)
    {
        bar.setFill(p);
    }

    public void paintBarHue(double s, double b)
    {
        Stop[] stops = new Stop[]
        {
            new Stop(0, Color.hsb(0, s, b)), new Stop(0.167, Color.hsb(60, s, b)), new Stop(0.334, Color.hsb(120, s, b)), new Stop(0.501, Color.hsb(180, s, b)), new Stop(0.668, Color.hsb(240, s, b)), new Stop(0.835, Color.hsb(300, s, b)), new Stop(1, Color.hsb(360, s, b))
        };
        LinearGradient lg = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
        bar.setFill(lg);
    }

//    public void paintThumb(Color c)
//    {
//        thumb.setFill(c);
//    }

    public void paintThumb(Paint p)
    {
        thumb.setFill(p);
    }
}
