
package animator.ui.color;

import animator.core.shape.AnimatorShapeBase;
import animator.ui.NumberSpinner;
import java.math.BigDecimal;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 *
 * @author Xephrt
 */
public class ColorMixer extends Region
{    
    private static final ColorMixer INSTANCE = new ColorMixer();
    
    private final ColorTwinBox colorBox = new ColorTwinBox();

    private ReadOnlyObjectProperty<List<AnimatorShapeBase> > selectedShape;
    
    private ColorMixer()
    {
////////////////////////Start of Tab////////////////////////////////        
        final TabPane allTab = new TabPane();
        allTab.setSide(Side.RIGHT);
        allTab.setTabMaxHeight(10);
        allTab.setTabMaxWidth(20);
        
        final Tab rgbTab = new Tab("RGB");
        rgbTab.setClosable(false);
        
        final Tab hsbTab = new Tab("HSB");
        hsbTab.setClosable(false);
        
        final Tab swatchTab = new Tab("Swatch");
        swatchTab.setClosable(false);
        
        allTab.getTabs().addAll(rgbTab,hsbTab);
//////////////////////// End of Tab ////////////////////////////////        
////////////////////////Start of RGB////////////////////////////////
        final Label rLabel = new Label("Red");
        //rLabel.setTextFill(Color.rgb(176, 176, 176));
        final Label gLabel = new Label("Green");
        //gLabel.setTextFill(Color.rgb(176, 176, 176));
        final Label bLabel = new Label("Blue");
        //bLabel.setTextFill(Color.rgb(176, 176, 176));
        final Label aLabel = new Label("Alpha");
        //aLabel.setTextFill(Color.rgb(176, 176, 176));

        final ColorSlider rSlider = new ColorSlider();
        rSlider.setMax(1);
        rSlider.setMin(0);
        rSlider.setValue(1);
        rSlider.setBarWidth(85);
        rSlider.paintBar(new Color(0, 1, 1, 1),new Color(1, 1, 1, 1));
        rSlider.setMultiplyer(255);
        rSlider.setDecimalMode(false);


        final ColorSlider gSlider = new ColorSlider();
        gSlider.setMax(1);
        gSlider.setMin(0);
        gSlider.setValue(1);
        gSlider.setBarWidth(85);
        gSlider.paintBar(new Color(1, 0, 1, 1),new Color(1, 1, 1, 1));
        gSlider.setMultiplyer(255);
        gSlider.setDecimalMode(false);
       
        
        final ColorSlider bSlider = new ColorSlider();
        bSlider.setMax(1);
        bSlider.setMin(0);
        bSlider.setValue(1);
        bSlider.setBarWidth(85);
        bSlider.paintBar(new Color(1, 1, 0, 1),new Color(1, 1, 1, 1));
        bSlider.setMultiplyer(255);
        bSlider.setDecimalMode(false);
        
        final ColorSlider aSlider = new ColorSlider();
        aSlider.setMax(1);
        aSlider.setMin(0);
        aSlider.setValue(1);
        aSlider.setBarWidth(85);
        aSlider.paintBar(new Color(1, 1, 1, 0),new Color(1, 1, 1, 1));
        aSlider.setMultiplyer(1);
        aSlider.setDecimalMode(true);
        
        final NumberSpinner rSpinner =  new NumberSpinner();
        rSpinner.setMaxWidth(48);
        rSpinner.setMaxHeight(10);
        rSpinner.setMaxVal(255);
        rSpinner.setMinVal(0);
        rSpinner.setAllowDecimal(false);
        rSpinner.numberProperty().bindBidirectional(rSlider.colorValueProperty());
        rSpinner.setNumber(BigDecimal.valueOf(255*rSlider.getVal()));
        
        final NumberSpinner gSpinner =  new NumberSpinner();
        gSpinner.setMaxWidth(48);
        gSpinner.setMaxHeight(10);
        gSpinner.setMaxVal(255);
        gSpinner.setMinVal(0);
        gSpinner.setAllowDecimal(false);
        gSpinner.numberProperty().bindBidirectional(gSlider.colorValueProperty());
        gSpinner.setNumber(BigDecimal.valueOf(255*gSlider.getVal()));
        
        final NumberSpinner bSpinner =  new NumberSpinner();
        bSpinner.setMaxWidth(48);
        bSpinner.setMaxHeight(10);
        bSpinner.setMaxVal(255);
        bSpinner.setMinVal(0);
        bSpinner.setAllowDecimal(false);
        bSpinner.numberProperty().bindBidirectional(bSlider.colorValueProperty());
        bSpinner.setNumber(BigDecimal.valueOf(255*bSlider.getVal()));
        
        final NumberSpinner aSpinner =  new NumberSpinner();
        aSpinner.setMaxWidth(48);
        aSpinner.setMaxHeight(10);
        aSpinner.setMaxVal(1);
        aSpinner.setMinVal(0);
        aSpinner.setAllowDecimal(true);
        aSpinner.numberProperty().bindBidirectional(aSlider.colorValueProperty());
        aSpinner.setNumber(BigDecimal.valueOf(aSlider.getVal()));
        
        final GridPane grid = new GridPane();
        grid.setHgap(5);
        grid.setVgap(7);
        grid.setPadding(new Insets(0, 10, 0, 10));
        
        grid.add(rLabel, 0, 0);
        grid.add(gLabel, 0, 1);
        grid.add(bLabel, 0, 2);
        grid.add(aLabel, 0, 3);
        
        grid.add(rSlider, 1, 0);
        grid.add(gSlider, 1, 1);
        grid.add(bSlider, 1, 2);
        grid.add(aSlider, 1, 3);
        
        grid.add(rSpinner, 2, 0);
        grid.add(gSpinner, 2, 1);
        grid.add(bSpinner, 2, 2);
        grid.add(aSpinner, 2, 3);
        
        VBox gridVBox = new VBox();
        gridVBox.getChildren().add(grid);
        gridVBox.setAlignment(Pos.CENTER);
        
        rgbTab.setContent(gridVBox);
//////////////////////// End of RGB ////////////////////////////////
////////////////////////Start of HSB////////////////////////////////
        Label hLabel = new Label("Hue");
        Label sLabel = new Label("Saturation");
        Label brLabel = new Label("Brightness");
        brLabel.setMinWidth(70);
        
        final ColorSlider hSlider = new ColorSlider();
        hSlider.setRange(360, 0);
        hSlider.paintBarHue(1,1);
        
        final ColorSlider sSlider = new ColorSlider();
        sSlider.setRange(1, 0);
        sSlider.setValue(1);
        sSlider.paintBar(Color.WHITE, Color.RED);
        
        final ColorSlider brSlider = new ColorSlider();
        brSlider.setRange(1, 0);
        brSlider.setValue(1);
        
        final GridPane gridHSB = new GridPane();
        gridHSB.setHgap(10);
        gridHSB.setVgap(10);
        gridHSB.setPadding(new Insets(0, 10, 0, 10));
        
        gridHSB.add(hLabel, 0, 0);
        gridHSB.add(sLabel, 0, 1);
        gridHSB.add(brLabel, 0, 2);
        //gridHSB.add(aLabel, 0, 3);
        
        gridHSB.add(hSlider, 1, 0);
        gridHSB.add(sSlider, 1, 1);
        gridHSB.add(brSlider, 1, 2);
        //gridHSB.add(aSlider, 1, 3);
        
        VBox gridHSBVBox = new VBox();
        gridHSBVBox.getChildren().add(gridHSB);
        gridHSBVBox.setAlignment(Pos.CENTER);
        
        hsbTab.setContent(gridHSBVBox);
        
//////////////////////// End of HSB ////////////////////////////////
        
////////////////////////Start of ColorBox///////////////////////////
        //Use to chage size (Just in case)
        //colorBox.setScaleX(1);
        //colorBox.setScaleY(1);
        
        VBox v = new VBox();
        v.setAlignment(Pos.CENTER);
        v.getChildren().add(colorBox);        
//////////////////////// End of ColorBox ///////////////////////////
        
//////////////////////// Start of Event ////////////////////////////
        final ObjectProperty<Color> bgColor = colorBox.bgColorProperty();
        bgColor.addListener(new ChangeListener<Color>() {

            @Override
            public void changed(ObservableValue<? extends Color> ov, Color t, Color t1)
            {
            if(rgbTab.isSelected())
                {
                rSlider.setValue(t1.getRed());
                gSlider.setValue(t1.getGreen());
                bSlider.setValue(t1.getBlue());
                aSlider.setValue(t1.getOpacity());
                }
            else if(hsbTab.isSelected())
                {
                    SingleSelectionModel<Tab> selectionModel = allTab.getSelectionModel();
                    selectionModel.select(0);
                    selectionModel.select(1);
                }
            }
        });
        
        final ObjectProperty<Color> stColor = colorBox.fgColorProperty();
        stColor.addListener(new ChangeListener<Color>() {

            @Override
            public void changed(ObservableValue<? extends Color> ov, Color t, Color t1)
            {
            if(rgbTab.isSelected())
                {
                rSlider.setValue(t1.getRed());
                gSlider.setValue(t1.getGreen());
                bSlider.setValue(t1.getBlue());
                aSlider.setValue(t1.getOpacity());
                }
            else if(hsbTab.isSelected())
                {
                SingleSelectionModel<Tab> selectionModel = allTab.getSelectionModel();
                    selectionModel.select(0);
                    selectionModel.select(1);   
                }
            }
        });        
        
        final IntegerProperty box = colorBox.selectedBoxProperty();
        box.addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
            {
                Color c = colorBox.getCurrentColor();
                
                if(rgbTab.isSelected())
                {
                    System.out.println("RGB selected");
                    rSlider.setValue(c.getRed());
                    gSlider.setValue(c.getGreen());
                    bSlider.setValue(c.getBlue());
                    aSlider.setValue(c.getOpacity());
                }
                else if(hsbTab.isSelected())
                {
                    System.out.println("HSB selected");
                    hSlider.setValue(c.getHue());
                    sSlider.setValue(c.getSaturation());
                    brSlider.setValue(c.getBrightness());
                }
            }
        });
        
        final DoubleProperty rVal = rSlider.valueProperty();
        final DoubleProperty gVal = gSlider.valueProperty();
        final DoubleProperty bVal = bSlider.valueProperty();
        final DoubleProperty aVal = aSlider.valueProperty();
        
        rVal.addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
            {
                colorBox.setCurrentColorRGB(rVal.getValue(), gVal.getValue(), bVal.getValue(), aVal.getValue());
                gSlider.paintBar(new Color(rVal.getValue(), 0, bVal.getValue(), 1), new Color(rVal.getValue(), 1, bVal.getValue(), 1));
                bSlider.paintBar(new Color(rVal.getValue(), gVal.getValue(), 0, 1), new Color(rVal.getValue(), gVal.getValue(), 1, 1));
                aSlider.paintBar(new Color(0, 0, 0, 0),new Color(rVal.getValue(), gVal.getValue(), bVal.getValue(), 1));
            }
        });
        
        gVal.addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
            {
                colorBox.setCurrentColorRGB(rVal.getValue(), gVal.getValue(), bVal.getValue(), aVal.getValue());
                rSlider.paintBar(new Color(0, gVal.getValue(), bVal.getValue(), 1), new Color(1, gVal.getValue(), bVal.getValue(), 1));
                bSlider.paintBar(new Color(rVal.getValue(), gVal.getValue(), 0, 1), new Color(rVal.getValue(), gVal.getValue(), 1, 1));
                aSlider.paintBar(new Color(0, 0, 0, 0),new Color(rVal.getValue(), gVal.getValue(), bVal.getValue(), 1));
            }
        });
        
        bVal.addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
            {
                colorBox.setCurrentColorRGB(rVal.getValue(), gVal.getValue(), bVal.getValue(), aVal.getValue());
                rSlider.paintBar(new Color(0, gVal.getValue(), bVal.getValue(), 1), new Color(1, gVal.getValue(), bVal.getValue(), 1));
                gSlider.paintBar(new Color(rVal.getValue(), 0, bVal.getValue(), 1), new Color(rVal.getValue(), 1, bVal.getValue(), 1));
                aSlider.paintBar(new Color(0, 0, 0, 0),new Color(rVal.getValue(), gVal.getValue(), bVal.getValue(), 1));
            }
        });
        
        aVal.addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
            {
                if(rgbTab.isSelected())
                    colorBox.setCurrentColorRGB(rVal.getValue(), gVal.getValue(), bVal.getValue(), aVal.getValue());
                else if(hsbTab.isSelected())
                    colorBox.setCurrentColorHSB(hSlider.getVal(), sSlider.getVal(), brSlider.getVal(), aVal.getValue());
            }
        }); 
        
        final DoubleProperty hVal = hSlider.valueProperty();
        final DoubleProperty sVal = sSlider.valueProperty();
        final DoubleProperty brVal = brSlider.valueProperty();
        
        hVal.addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
            {
                colorBox.setCurrentColorHSB(hVal.getValue(), sVal.getValue(), brVal.getValue(),aVal.getValue());
                sSlider.paintBar(Color.hsb(hSlider.getVal(), 0, brSlider.getVal()), Color.hsb(hSlider.getVal(), 1, brSlider.getVal()));
                brSlider.paintBar(Color.hsb(hSlider.getVal(), sSlider.getVal(), 0), Color.hsb(hSlider.getVal(), sSlider.getVal(), 1));
                aSlider.paintBar(new Color(0, 0, 0, 0), Color.hsb(hSlider.getVal(), sSlider.getVal(), brSlider.getVal(),1));
            }
        });
        
        sVal.addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
            {
                colorBox.setCurrentColorHSB(hVal.getValue(), sVal.getValue(), brVal.getValue(),aVal.getValue());
                brSlider.paintBar(Color.hsb(hSlider.getVal(), sSlider.getVal(), 0), Color.hsb(hSlider.getVal(), sSlider.getVal(), 1));
                hSlider.paintBarHue(sSlider.getVal(), brSlider.getVal());
                aSlider.paintBar(new Color(0, 0, 0, 0), Color.hsb(hSlider.getVal(), sSlider.getVal(), brSlider.getVal(),1));
            }
        });
        
        brVal.addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
            {
                colorBox.setCurrentColorHSB(hVal.getValue(), sVal.getValue(), brVal.getValue(),aVal.getValue());
                sSlider.paintBar(Color.hsb(hSlider.getVal(), 0, brSlider.getVal()), Color.hsb(hSlider.getVal(), 1, brSlider.getVal()));
                hSlider.paintBarHue(sSlider.getVal(), brSlider.getVal());
                aSlider.paintBar(new Color(0, 0, 0, 0), Color.hsb(hSlider.getVal(), sSlider.getVal(), brSlider.getVal(),1));
            }
        });
        
        allTab.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>()
        {
            @Override
            public void changed(ObservableValue<? extends Tab> tab, Tab oldTab, Tab newTab)
            {
                Color c = colorBox.getCurrentColor();
                if(newTab.equals(rgbTab))
                {
                    rSlider.setValue(c.getRed());
                    gSlider.setValue(c.getGreen());
                    bSlider.setValue(c.getBlue());
                    aSlider.setValue(c.getOpacity());
                    grid.add(aLabel, 0, 3);
                    grid.add(aSlider, 1, 3);
                }
                else if(newTab.equals(hsbTab))
                {
                    if(c.getBrightness()!=0&&c.getSaturation()!=0)
                        if(c.getHue()!=0&&c.getHue()!=360)
                            hSlider.setValue(c.getHue());
                    
                    if(c.getBrightness()!=0)
                        sSlider.setValue(c.getSaturation());
                    
                    brSlider.setValue(c.getBrightness());
                    gridHSB.add(aLabel, 0, 3);
                    gridHSB.add(aSlider, 1, 3);
                }
            }
        });
        
//////////////////////// End of Event ////////////////////////////
        
        HBox root = new HBox();
        //root.getChildren().addAll(v,allTab);
        root.getChildren().addAll(v,grid);
        root.setPadding(new Insets(5, 10, 5, 10));
        root.setAlignment(Pos.CENTER);
        
        this.getChildren().add(root);
        
        initEvent();
    }
    
    public static ColorMixer getInstance()
    {
        return INSTANCE;
    }
    
    private void initEvent()
    {
        colorBox.bgColorProperty().addListener(new ChangeListener<Color>()
        {
            @Override
            public void changed(ObservableValue<? extends Color> ov, Color t, Color t1)
            {
                List<AnimatorShapeBase> shape = selectedShape.get();
                for (AnimatorShapeBase s : shape)
                    s.setFill(t1);
            }           
        });
    }
    
    // @TODO Perth Review
    public Color getCurrentColor()
    {
        return colorBox.getCurrentColor();
    }
    
    public void setCurrentColor(Color c)
    {
        colorBox.setCurrentColor(c);
    }
    
    // @TODO Perth Review
    public Color getBackgroundColor()
    {
        return colorBox.getBackgroundColor();
    }
    
    // @TODO Perth Review
    public Color getForegroundColor()
    {
        return colorBox.getForegroundColor();
    }
    
    // @TODO Perth Review
    public ObjectProperty<Color> bgColorProperty()
    {
        return colorBox.bgColorProperty();
    }
    
    // @TODO Perth Review
    public ObjectProperty<Color> fgColorProperty()
    {
        return colorBox.fgColorProperty();
    }

//    public ReadOnlyObjectProperty<List<AnimatorShapeBase> > selectObjectProperty()
//    {
//        return selectedShape;
//    }

    public void setSelectShapeProperty(ReadOnlyObjectProperty<List<AnimatorShapeBase> > property)
    {
        selectedShape = property;
    }
    
//    public void attachColorRecievable(ColorRecievable cr)
//    {
//        cr.setFillColorProperty(colorBox.fgColorProperty());
//        cr.setStrokeColorProperty(colorBox.bgColorProperty());
//    }
}
