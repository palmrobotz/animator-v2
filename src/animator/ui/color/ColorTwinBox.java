package animator.ui.color;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.layout.Region;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;

/**
 *
 * @author Xephrt
 */
public class ColorTwinBox extends Region
{
    private final IntegerProperty selectedBox = new SimpleIntegerProperty(0);

    private ObjectProperty<Color> bgColor = new SimpleObjectProperty<>(new Color(1.0, 1.0, 1.0, 1.0));
    private ObjectProperty<Color> fgColor = new SimpleObjectProperty<>(new Color(0, 0, 0, 1.0));
    
    public ColorTwinBox()
    {
        Image img = new Image(getClass().getResourceAsStream("/animator/nocolor.png"));

        final Rectangle emptyBG = new Rectangle();
        emptyBG.setFill(new ImagePattern(img));
        emptyBG.setWidth(50);
        emptyBG.setHeight(50);
        emptyBG.setArcWidth(15);
        emptyBG.setArcHeight(15);
        emptyBG.setX(0);
        emptyBG.setY(0);

        final Rectangle emptyS = new Rectangle();
        emptyS.setFill(new ImagePattern(img));
        emptyS.setWidth(50);
        emptyS.setHeight(50);
        emptyS.setArcWidth(15);
        emptyS.setArcHeight(15);
        emptyS.setX(25);
        emptyS.setY(25);

        final Rectangle BGBox = createBackgroundColor();
        final Rectangle SBox = createStrokeColor();

        BGBox.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                emptyBG.toFront();
                BGBox.toFront();
                selectedBox.set(0);
            }
        });

        SBox.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                emptyS.toFront();
                SBox.toFront();
                selectedBox.set(1);
            }
        });

        Group g = new Group();
        g.getChildren().addAll(BGBox, SBox, emptyBG, emptyS);

        this.getChildren().add(g);

        emptyS.toFront();
        SBox.toFront();
        emptyBG.toFront();
        BGBox.toFront();
    }
    
    private Rectangle createBackgroundColor()
    {
        Rectangle r = new Rectangle();
        r.setHeight(50);
        r.setWidth(50);
        r.fillProperty().bind(bgColor);
        r.setX(0);
        r.setY(0);
        r.setArcWidth(15);
        r.setArcHeight(15);
        r.setStroke(Color.color(0.4, 0.4, 0.4));
        r.setStrokeLineJoin(StrokeLineJoin.ROUND);
        r.setStrokeType(StrokeType.INSIDE);
        r.setStrokeWidth(2);
        return r;
    }

    private Rectangle createStrokeColor()
    {
        Rectangle r = new Rectangle();
        r.setHeight(50);
        r.setWidth(50);
        r.fillProperty().bind(fgColor);
        r.setX(25);
        r.setY(25);
        r.setArcWidth(15);
        r.setArcHeight(15);
        r.setStroke(Color.color(0.4, 0.4, 0.4));
        r.setStrokeLineJoin(StrokeLineJoin.ROUND);
        r.setStrokeType(StrokeType.INSIDE);
        r.setStrokeWidth(2);
        return r;
    }

    public IntegerProperty selectedBoxProperty()
    {
        return selectedBox;
    }

    public Color getCurrentColor()
    {
        if (selectedBox.get() == 0)
        {
            return bgColor.get();
        }
        else
        {
            return fgColor.get();
        }
    }
    
    public void setCurrentColor(Color c)
    {
        if (selectedBox.get() == 0)
        {
            bgColor.set(c);
        }
        else
        {
            fgColor.set(c);
        }
    }
    
    // @TODO Perth Review
    public void setCurrentColorRGB(double r, double g, double b, double a)
    {
        if (selectedBox.getValue() == 0)
        {
            bgColor.set(new Color(r, g, b, a));
        }
        else
        {
            fgColor.set(new Color(r, g, b, a));
        }
    }
    
    // @TODO Perth Review
    public void setCurrentColorHSB(double h, double s, double b, double a)
    {
        if (selectedBox.getValue() == 0)
        {
            bgColor.set(Color.hsb(h, s, b, a));
        }
        else
        {
            fgColor.set(Color.hsb(h, s, b, a));
        }
    }
    
    // @TODO Perth Review
    public Color getBackgroundColor()
    {
        return bgColor.get();
    }

    // @TODO Perth Review
    public Color getForegroundColor()
    {
        return fgColor.get();
    }
    
    // @TODO Perth Review
    public ObjectProperty<Color> bgColorProperty()
    {
        return bgColor;
    }
    
    // @TODO Perth Review
    public ObjectProperty<Color> fgColorProperty()
    {
        return fgColor;
    } 
}