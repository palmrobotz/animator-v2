
package animator.ui.preview;

import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.CompoundShape;
import animator.ui.StrokeConfig;
import animator.ui.Tool;
import animator.ui.ToolBox;
import animator.ui.color.ColorMixer;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;

/**
 * This preview window is used to edit the CompoundShape.
 * It can contain only one CompoundShape by set the CompoundShape property.
 * All of these (add/remove/clearShape) methods throw UnSupportOperationException to prevent illegal 
 * access to violate the class invariance.
 * @author Nuntipat narkthong
 */
public class CompoundEditablePreviewWindow extends EditablePreviewWindow
{
    private ObjectProperty<CompoundShape> shape = new SimpleObjectProperty<>(null);
    
    public CompoundEditablePreviewWindow(double width, double height, Paint bgColor)
    {
        super(width, height, bgColor);
        
        final ColorMixer colorMixer = ColorMixer.getInstance();
        final StrokeConfig strokeConfig = StrokeConfig.getInstance();
        final ReadOnlyObjectProperty<Tool> toolSelected = ToolBox.getInstance().toolSelectProperty();
        
        // Support Drawing
        addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent event)
            {
                double initX = mapXToPreviewWindowCoordinate(event.getX());
                double initY = mapYToPreviewWindowCoordinate(event.getY());
                
                if (toolSelected.get().hasCorrespondShape())
                {
                    AnimatorShapeBase asb = toolSelected.get().asAnimatorShape().createShape(0, 0, 0, 0, colorMixer.getForegroundColor(), colorMixer.getBackgroundColor()
                                    , strokeConfig.getJointProperty(), strokeConfig.getCapProperty(), strokeConfig.getStrokeWidth());
                    asb.setX(initX);
                    asb.setY(initY);
                    asb.setSelect(true);
                    if (shape.get() == null)
                        throw new RuntimeException("We're in CompoundEPW without setting compound shape");
                    else
                        shape.get().addShape(asb);
                }
            }
        });
        
        // Allow only one CompoundShape in the preview window
        shape.addListener(new ChangeListener<CompoundShape>()
        {
            @Override
            public void changed(ObservableValue<? extends CompoundShape> ov, CompoundShape oldShape, CompoundShape newShape)
            {
                if (oldShape != null)
                {
                    CompoundEditablePreviewWindow.super.removeShape(oldShape);
                }
                else if (newShape != null)
                {
                    CompoundEditablePreviewWindow.super.addShape(newShape);
                }
            }            
        });
    }

    @Override
    protected void onShapeDelete(AnimatorShapeBase asb)
    {
        shape.get().removeShape(asb);
    }

    public CompoundShape getCompoundShape()
    {
        return shape.get();
    }
    
    public void setCompoundShape(CompoundShape s)
    {
        shape.set(s);
    }
    
    public ObjectProperty<CompoundShape> compoundShapeProperty()
    {
        return shape;
    }
    
    @Override
    public void addShape(AnimatorShapeBase s)
    {
        throw new UnsupportedOperationException("All shape operations of CompoundEditablePreviewWindow should be done though get/setCompoundShape method and property");
    }

    @Override
    public boolean removeShape(AnimatorShapeBase s)
    {
        throw new UnsupportedOperationException("All shape operations of CompoundEditablePreviewWindow should be done though get/setCompoundShape method and property");
    }

    @Override
    public void clearShape()
    {
        throw new UnsupportedOperationException("All shape operations of CompoundEditablePreviewWindow should be done though get/setCompoundShape method and property");
    }
}
