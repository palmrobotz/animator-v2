
package animator.ui.preview.event;

/**
 *
 * @author Nuntipat narkthong
 */
public interface EditablePreviewWindowActionListener
{
    public void actionPerformed(EditablePreviewWindowActionEvent e);
}
