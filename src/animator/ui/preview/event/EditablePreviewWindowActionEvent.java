
package animator.ui.preview.event;

import animator.core.shape.AnimatorShapeBase;
import animator.ui.preview.EditablePreviewWindow;
import java.util.EventObject;

/**
 *
 * @author Nuntipat narkthong
 */
public class EditablePreviewWindowActionEvent extends EventObject
{
    public static enum Action { ADD_SHAPE, REMOVE_SHAPE };
    
    private Action action;
    private AnimatorShapeBase shape;
    
    public EditablePreviewWindowActionEvent(EditablePreviewWindow source, Action action, AnimatorShapeBase shape)
    {
        super(source);
        this.action = action;
        this.shape = shape;
    }

    @Override
    public EditablePreviewWindow getSource()
    {
        return (EditablePreviewWindow) super.getSource();
    }

    public Action getAction()
    {
        return action;
    }

    public AnimatorShapeBase getShape()
    {
        return shape;
    }
}
