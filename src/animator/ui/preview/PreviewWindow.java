
package animator.ui.preview;

import animator.core.shape.AnimatorShapeBase;
import java.util.Iterator;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 * Class for the PreviewWindow
 * @author Nuntipat narkthong
 */
public class PreviewWindow extends ScrollPane implements Previewable
{
    // TODO: Fix to use
    // Default possible scroll value
    //private static final int[] scale = {30, 50, 75, 100};
    //private static final ObservableList<String> zoomOptions = FXCollections.<String>observableArrayList("30%", "50%", "75%", "100%");
    
    // Preview rectangle
    protected final Rectangle previewBound;
    private final Group preview;
    protected final HBox hBox;
    
    //private ReadOnlyLongProperty currentTime = null;
    private final IntegerProperty selectedLayer = null;
    
    private final DoubleProperty previewWidth;
    private final DoubleProperty previewHeight;
    private final ObjectProperty<Paint> previewBgColor; 
    
    private final DoubleProperty zoomX;
    private final DoubleProperty zoomY;
    private final BooleanProperty border = new SimpleBooleanProperty(true);

    public PreviewWindow(double width, double height, Paint bgColor)
    {
        //previewWidth = new SimpleDoubleProperty(width);
        //previewHeight = new SimpleDoubleProperty(height);
        previewBgColor = new SimpleObjectProperty<>(bgColor);
        
        previewBound = new Rectangle(width, height, bgColor);
        previewWidth = previewBound.widthProperty();
        previewHeight = previewBound.heightProperty();
        
        // Wrap in a group to scroll by visual bound not the real size
        // See the document of the scrollpane
        preview = new Group(previewBound);
        //final Group wrapGroup = new Group(preview);
                
        hBox = HBoxBuilder.create()
                .alignment(Pos.CENTER)
                .style("-fx-background-color: #AAAAAA;")
                .padding(Insets.EMPTY)
                .spacing(0)
                .children(preview)
                .build();
        
        // Wrap a preview rectangle into a ScrollPane
        setFitToHeight(true);
        setFitToWidth(true);
        setContent(hBox);
                
        border.addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1)
            {
                if (t1.booleanValue())
                {
                    hBox.getChildren().clear();
                    hBox.getChildren().add(preview);
                    setContent(hBox);
                }
                else
                {
                    setContent(preview);
                }
            }        
        });
        
        zoomX = preview.scaleXProperty();
        zoomY = preview.scaleYProperty();
        
        // @TODO Beware of width and height change
        //setPrefSize(width, height);
    }
        
    /**
     * Map Preview Window Origin ((0,0) is at top left corner including grey area)
     * to the preview rectangle origin ((0,0) is at the top left of preview rectangle
     * excluding the grey area)
     * @param x
     * @return 
     */
    public double mapXToPreviewWindowCoordinate(double x)
    {
        return (x - preview.getBoundsInParent().getMinX()) * (1 / zoomX.get());
    }
    
    public double mapYToPreviewWindowCoordinate(double y)
    {
        return (y - preview.getBoundsInParent().getMinY()) * (1 / zoomY.get());
    }
    
    public DoubleProperty zoomXProperty()
    {
        return zoomX;
    }
    
    public double getZoomX()
    {
        return zoomX.get();
    }
    
    public void setZoomX(double percentage)
    {
        zoomX.set(percentage);
    }
    
    public DoubleProperty zoomYProperty()
    {
        return zoomY;
    }
    
    public double getZoomY()
    {
        return zoomY.get();
    }
    
    public void setZoomY(double percentage)
    {
        zoomY.set(percentage);
    }
    
    public void setZoom(double percentage)
    {
        zoomX.set(percentage);
        zoomY.set(percentage);
    }
    
    public BooleanProperty borderProperty()
    {
        return border;
    }
    
    public boolean getBorder()
    {
        return border.get();
    }
    
    public void setBorder(boolean b)
    {
        border.set(b);
    }
    
    public DoubleProperty previewWidthProperty()
    {
        return previewWidth;
    }

    public DoubleProperty previewHeightProperty()
    {
        return previewHeight;
    }

    public ObjectProperty<Paint> previewBgColorProperty()
    {
        return previewBgColor;
    }
    
    public double getPreviewWidth()
    {
        return previewWidth.get();
    }
    
    public double getPreviewHeight()
    {
        return previewHeight.get();
    }
    
    public Paint getPreviewBgColor()
    {
        return previewBgColor.get();
    }
    
    @Override
    public void addShape(AnimatorShapeBase s)
    {
        s.setPreviewWindow(this);
        preview.getChildren().add(s);
    }

    @Override
    public boolean removeShape(AnimatorShapeBase s)
    {
        if (preview.getChildren().remove(s))
        {
            s.setPreviewWindow(null);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public ObservableList<Node> getShapeUnmodifiable()
    {
        return preview.getChildrenUnmodifiable();
    }
    
    @Override
    public void clearShape()
    {
        Iterator<Node> it = preview.getChildren().iterator();
        while (it.hasNext())
        {
            Node n = it.next();
            if (n instanceof AnimatorShapeBase)
            {   
                ((AnimatorShapeBase) n).setPreviewWindow(null);
                it.remove();
            }
        }
    }
    
    // @TODO connect closer
//    @Override
//    public void setCurrentTimeProperty(ReadOnlyLongProperty property)
//    {
//        currentTime = property;
//    }
//
//    @Override
//    public long getTime()
//    {
//        if (currentTime != null)
//            return currentTime.get();
//        // @TODO We might want to return some thing more meaningful
//        else
//            return -1;
//    }

//    @Override
//    public int getSelectedLayerIndex()
//    {
//        return selectedLayer.get();
//    }
//
//    @Override
//    public void setSelectedLayerProperty(IntegerProperty property)
//    {
//        selectedLayer = property;
//    }
    
}
