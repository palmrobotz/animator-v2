
package animator.ui.preview;

import animator.core.shape.AnimatorShapeBase;

/**
 *
 * @author nuntipat
 */
public interface Previewable
{
    public void addShape(AnimatorShapeBase s);
    public boolean removeShape(AnimatorShapeBase s);
    public void clearShape();
    
    //public long getTime();
    //public void setCurrentTimeProperty(ReadOnlyLongProperty property);
    
    // @TODO not a best way I think
//    public int getSelectedLayerIndex();
//    public void setSelectedLayerProperty(IntegerProperty property);
}
