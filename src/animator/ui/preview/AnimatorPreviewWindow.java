
package animator.ui.preview;

import javafx.beans.binding.When;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 *
 * @author Nuntipat narkthong
 */
public class AnimatorPreviewWindow extends BorderPane
{
    private final BooleanProperty closeUp = new SimpleBooleanProperty(false);
    
    private final EditablePreviewWindow previewWindow;
    private final CompoundEditablePreviewWindow compoundWindow;
    
    private final DoubleProperty zoom = new SimpleDoubleProperty(1);
    private final ReadOnlyObjectWrapper<EditablePreviewWindow> currentWindow = new ReadOnlyObjectWrapper<>();
    
    public AnimatorPreviewWindow(double width, double height, Paint bgColor)
    {
        Button stage = new Button("Stage"); 
        // Exit from close up mode
        stage.setOnAction(new EventHandler<ActionEvent>() 
        {
            @Override
            public void handle(ActionEvent t)
            {
                setCloseUp(false);
            }
        });
        stage.visibleProperty().bind(closeUp);
        
        ToolBar modeSelect = new ToolBar(stage);
        modeSelect.visibleProperty().bind(closeUp);
        
        previewWindow = new EditablePreviewWindow(width, height, Color.WHITE);
        previewWindow.visibleProperty().bind(closeUp.not());
        previewWindow.zoomXProperty().bind(zoom);
        previewWindow.zoomYProperty().bind(zoom);
        
        compoundWindow = new CompoundEditablePreviewWindow(width, height, Color.WHITE);
        compoundWindow.visibleProperty().bind(closeUp);
        compoundWindow.zoomXProperty().bind(zoom);
        compoundWindow.zoomYProperty().bind(zoom);
        
        StackPane pane = new StackPane();
        pane.getChildren().addAll(previewWindow, compoundWindow);
        
        // Can not use setTop(...) because the space will be reserved even ... is not visible
        topProperty().bind(new When(closeUp).then(modeSelect).otherwise((ToolBar) null));
        setCenter(pane);
        
        currentWindow.bind(new When(closeUp).then((EditablePreviewWindow) compoundWindow).otherwise(previewWindow));
    }
    
    /**
     * Get preview window use for display the main timeline
     * @return 
     */
    public EditablePreviewWindow getPreviewWindow()
    {
        return previewWindow;
    }
    
    /**
     * Get preview window use for edit compound shape
     * @return 
     */
    public CompoundEditablePreviewWindow getCompoundPreviewWindow()
    {
        return compoundWindow;
    }
    
    public EditablePreviewWindow getCurrentPreviewWindow()
    {
        return currentWindow.get();
    }
    
    public BooleanProperty closeUpProperty()
    {
        return closeUp;
    }
    
    public boolean isCloseUp()
    {
        return closeUp.get();
    }
    
    public void setCloseUp(boolean b)
    {
        closeUp.set(b);
    }
    
    public DoubleProperty zoomProperty()
    {
        return zoom;
    }
    
    public double getZoom()
    {
        return zoom.get();
    }
    
    public void setZoom(double percentage)
    {
        zoom.set(percentage);
    }
}
