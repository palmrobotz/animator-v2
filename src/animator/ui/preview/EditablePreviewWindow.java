
package animator.ui.preview;

import animator.core.animation.ShapeProperty;
import animator.core.animation.TimelineModel;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.event.ShapeChangedEvent;
import animator.core.shape.event.ShapeChangedListener;
import animator.ui.Tool;
import animator.ui.ToolBox;
import animator.ui.preview.event.EditablePreviewWindowActionEvent;
import animator.ui.preview.event.EditablePreviewWindowActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;

/**
 *
 * @author Nuntipat narkthong
 */
public class EditablePreviewWindow extends PreviewWindow/* implements EditablePreviewable*/
{
    private ReadOnlyObjectWrapper<List<AnimatorShapeBase> > selectedShape = 
            new ReadOnlyObjectWrapper<>(Collections.<AnimatorShapeBase>emptyList());
    
    // Listener
    private List<EditablePreviewWindowActionListener> actionListener = new ArrayList<>();
    private List<ShapeChangedListener> shapeChangedListener = new ArrayList<>();
    
    private boolean allowMultipleSelection = false;
    //private KeyEvent currentPressKeyEvent = null;
    
    // Use for create new shape
//    private TimelineModel timelineModel;
    
    public EditablePreviewWindow(double width, double height, Paint bgColor)
    {
        super(width, height, bgColor);
        
        final ReadOnlyObjectProperty<Tool> toolSelected = ToolBox.getInstance().toolSelectProperty();
        addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent event)
            {
                List<AnimatorShapeBase> shape = selectedShape.get();
                
                if (!shape.isEmpty())
                {
                    if (event.getTarget() == previewBound || event.getTarget() == hBox)
                    {
                        if (toolSelected.get() != Tool.SELECT)
                        {
                            for (AnimatorShapeBase s : shape)
                                Event.fireEvent(s, event);
                        }
                        else
                        {
                            if (event.getEventType() == MouseEvent.MOUSE_PRESSED)
                            {
                                clearShapeSelection();
                            }
                            else
                            {
                                event.consume();
                            }
                        }
                    }
                }
                
//                if (event.getEventType() == MouseEvent.MOUSE_PRESSED)
//                {
//                    if (event.getTarget() == previewBound || event.getTarget() == hBox)
//                    {
//                        if (shape != null)
//                        {
//                            // Deselect the current select object if use select tool
//                            if (toolSelected.get() == Tool.SELECT)
//                            {
//                                shape.setSelect(false);
//                                selectedShape.set(null);
//                            }
//                            else
//                            {
//                                Event.fireEvent(shape, event);
//                                //shape.getMouseEvent((MouseEvent) event.copyFor(preview, shape));
//                            }
//                        }
//                    }
//                }
            }   
        });
        
        // Detect shift key for multiple selection
        setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent t)
            {
                //currentPressKeyEvent = t;
                if (t.isShiftDown())
                {
                    allowMultipleSelection = true;
                }
            }
        });
        // Detect keyboard event
        setOnKeyReleased(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent t)
            {
                // Clear stored press keyevent
                allowMultipleSelection = false;
                
                if (t.getCode() == KeyCode.DELETE || t.getCode() == KeyCode.BACK_SPACE)
                {
                    List<AnimatorShapeBase> shape = selectedShape.get();
                    if (!shape.isEmpty())
                    {
                        for (AnimatorShapeBase s : shape)
                        {
                            s.setSelect(false);
                            fireEditablePreviewWindowActionEvent(new EditablePreviewWindowActionEvent(
                                    EditablePreviewWindow.this, EditablePreviewWindowActionEvent.Action.REMOVE_SHAPE
                                    , s));
                            onShapeDelete(s);
                        }
                        selectedShape.set(Collections.<AnimatorShapeBase>emptyList());
                    }
                }
            }        
        });
    }
    
    /**
     * Allow subclass to do something when the shape is removed from this preview window
     * @param shape 
     */
    protected void onShapeDelete(AnimatorShapeBase shape)
    {
        
    }
    
//    @Override
//    public TimelineModel getTimelineModel()
//    {
//        return timelineModel;
//    }
//    
//    @Override
//    public void setTimelineModel(TimelineModel model)
//    {
//        timelineModel = model;
//    }

    /**
     * Add Shape to this preview window.
     * If the shape is selected before added, this shape will be the only shape selected
     * in the preview window after this method is executed.
     * @param s 
     */
    @Override
    public void addShape(AnimatorShapeBase s)
    {
        super.addShape(s);
        s.addShapeChangedListener(shapeChanged);
        if (s.isSelect())
        {
            List<AnimatorShapeBase> currentSelect = selectedShape.get();
            if (!currentSelect.isEmpty())
            {
                for (AnimatorShapeBase asb : currentSelect)
                    asb.setSelect(false);
                //selectedShape.get().clear();
            }
            selectedShape.set(Arrays.asList(s));
        }
    }
    
    @Override
    public boolean removeShape(AnimatorShapeBase s)
    {
        if (super.removeShape(s))
        {
            s.removeShapeChangedListener(shapeChanged);
            // @TODO maintain selection while skim
            // Un select if s is being selected
            if (selectedShape.get().contains(s))
            {
                s.setSelect(false);
                // recreate select list
                List<AnimatorShapeBase> list = new ArrayList<>(selectedShape.get());
                list.remove(s);
                selectedShape.set(Collections.unmodifiableList(list));
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    
    // Forward ShapeChangedEvent of the contain shape to the other class
    private final ShapeChangedListener shapeChanged = new ShapeChangedListener() 
    {
        @Override
        public void onShapeChanged(ShapeChangedEvent e)
        {
            if (e.getChangedProperty() == ShapeProperty.SELECTION)
            {
                List<AnimatorShapeBase> selectedShapeList = selectedShape.get();
                // Select
                if (e.getSource().isSelect())
                {
                    // if shift is press, do multiple selection
                    if (allowMultipleSelection)
                    {
                        // recreate select list
                        List<AnimatorShapeBase> list = new ArrayList<>(selectedShapeList);
                        list.add(e.getSource());
                        selectedShape.set(Collections.unmodifiableList(list));
                    }
                    // shift not press
                    else
                    {
                        if (!selectedShapeList.isEmpty())
                        {
                            for (AnimatorShapeBase s : selectedShapeList)
                                s.setSelect(false);
                        }

                        selectedShape.set(Arrays.asList(e.getSource()));
                    }
                }
                // Deselect
                else
                {
                    if (selectedShapeList.contains(e.getSource()))
                    {
                        e.getSource().setSelect(false);
                        // recreate select list
                        List<AnimatorShapeBase> list = new ArrayList<>(selectedShapeList);
                        list.remove(e.getSource());
                        selectedShape.set(Collections.unmodifiableList(list));
                    }
                    else
                    {
                        throw new RuntimeException("Shape being deselect hasn't been selected before");
                    }
                }
            }
            fireShapeChangedListener(e);
        }
    };
    
    public void clearShapeSelection()
    {
        for (AnimatorShapeBase s : selectedShape.get())
            s.setSelect(false);
        selectedShape.set(Collections.<AnimatorShapeBase>emptyList());
    }
    
    public boolean addSelectedShape(AnimatorShapeBase shape)
    {
        if (getShapeUnmodifiable().contains(shape))
        {
            boolean oldAllowMultipleSelectionValue = allowMultipleSelection;
            allowMultipleSelection = true;
            shape.setSelect(true);
            allowMultipleSelection = oldAllowMultipleSelectionValue;
            return true;
        }
        else
        {
            throw new IllegalArgumentException("Shape to be force select not contain in this EPW");
        }
    }
    
    public boolean setSelectedShape(AnimatorShapeBase shape)
    {
        if (getShapeUnmodifiable().contains(shape))
        {
            clearShapeSelection();
            shape.setSelect(true);
            return true;
        }
        else
        {
            throw new IllegalArgumentException("Shape to be force select not contain in this EPW");
        }
    }
    
    public boolean setSelectedShape(List<AnimatorShapeBase> shape)
    {
        // Split into two loops so we can fail atomically (no partial selection occur
        // if some shape in the list doesn't contain in the EPW)
        
        for (AnimatorShapeBase asb: shape)
        {
             if (!getShapeUnmodifiable().contains(asb))
                 throw new IllegalArgumentException("Shape to be force select not contain in this EPW");
        }
        
        clearShapeSelection();

        // We should keep the old value so we can revert back to true if the value has
        // been true before, otherwise we will normally set the value back to false
        // which will break if user is holding shift key
        
        boolean oldAllowMultipleSelectionValue = allowMultipleSelection;
        
        allowMultipleSelection = true;
        for (AnimatorShapeBase asb: shape)
        {
            asb.setSelect(true);
        }
        allowMultipleSelection = oldAllowMultipleSelectionValue;
        
        return true;
    }
    
    public List<AnimatorShapeBase> getSelectedShape()
    {
        return selectedShape.get();
    }
    
    public ReadOnlyObjectProperty<List<AnimatorShapeBase> > selectedShapeProperty()
    {
        return selectedShape.getReadOnlyProperty();
    }
    
    public void addShapeChangedListener(ShapeChangedListener listener)
    {
        shapeChangedListener.add(listener);
    }
    
    public void removeShapeChangedListener(ShapeChangedListener listener)
    {
        shapeChangedListener.remove(listener);
    }
    
    private void fireShapeChangedListener(ShapeChangedEvent e)
    {
        for (ShapeChangedListener listener : shapeChangedListener)
        {
            listener.onShapeChanged(e);
        }
    }
    
    public void addEditablePreviewWindowActionListener(EditablePreviewWindowActionListener listener)
    {
        actionListener.add(listener);
    }
    
    public void removeEditablePreviewWindowActionListener(EditablePreviewWindowActionListener listener)
    {
        actionListener.remove(listener);
    }
    
    private void fireEditablePreviewWindowActionEvent(EditablePreviewWindowActionEvent e)
    {
        for (EditablePreviewWindowActionListener listener : actionListener)
        {
            listener.actionPerformed(e);
        }
    }
    
//    public void attachSelectionRecievable(SelectionRecievable sr)
//    {
//        sr.setSelectObjectProperty(selectedShape.getReadOnlyProperty());
//    }
    
//    @Override
//    public ReadOnlyObjectProperty<Color> fillColorProperty()
//    {
//        return fillColor;
//    }
//
//    @Override
//    public void setFillColorProperty(ReadOnlyObjectProperty<Color> property)
//    {
//        fillColor = property;
//    }
//
//    @Override
//    public ReadOnlyObjectProperty<Color> strokeColorProperty()
//    {
//        return strokeColor;
//    }
//
//    @Override
//    public void setStrokeColorProperty(ReadOnlyObjectProperty<Color> property)
//    {
//        strokeColor = property;
//    }
//    
//    public void setToolSelectedProperty(ReadOnlyObjectProperty<Tool> property)
//    {
//        toolSelected = property;
//    }

}
