
package animator.ui;

import animator.core.shape.AnimatorShapeBase;
import animator.ui.preview.AnimatorPreviewWindow;
import animator.ui.preview.PreviewWindow;
import java.util.List;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 *
 * @author Nuntipat narkthong
 */
public class Library extends VBox
{
    private final ObservableList<AnimatorShapeBase> shapeLibrary = FXCollections.observableArrayList();
    private final ObservableList<AnimatorShapeBase> unmodifiableShapeLibrary = 
            FXCollections.unmodifiableObservableList(shapeLibrary);
    
    private AnimatorShapeBase tableSelectedShape = null;
    private int selectedIndex = -1;
    
    // Injected Property
    private final AnimatorPreviewWindow apw;
    private final ReadOnlyObjectProperty<List<AnimatorShapeBase> > selectedShape;
    
    public Library(AnimatorPreviewWindow apw, ReadOnlyObjectProperty<List<AnimatorShapeBase> > selectedShapeProperty)
    {
        this.apw = apw;
        this.selectedShape = selectedShapeProperty;
        
        // Create preview window
        final PreviewWindow preview = new PreviewWindow(640, 480, Color.WHITE);
        preview.setZoom(0.2);
        
        // Create shape table
        final TableView<AnimatorShapeBase> table = new TableView<>();
        table.setPrefHeight(100);
        table.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<AnimatorShapeBase>()
        {
            @Override
            public void changed(ObservableValue<? extends AnimatorShapeBase> observable, AnimatorShapeBase oldValue, AnimatorShapeBase newValue)
            {
                tableSelectedShape = newValue;
                selectedIndex = table.getSelectionModel().getSelectedIndex();
                
                if (oldValue != null)
                {
                    //preview.removeShape(oldValue);
                }
                if (newValue != null)
                {
                    //AnimatorShapeBase cloneShape = newValue;
                    //cloneShape.setLayoutX(0);
                    //cloneShape.setLayoutY(0);
                    //preview.addShape(cloneShape);
                }
            }            
        });
        table.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent event) 
            {
                if (event.getClickCount() > 1) 
                {
                    if (tableSelectedShape != null)
                    {
                        //Library.this.apw.getCompoundPreviewWindow().setCompoundShape((CompoundShape) tableSelectedShape);
                        Library.this.apw.setCloseUp(true);
                    }
                }
            }
        });
        table.setOnDragDetected(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                // drag was detected, start a drag-and-drop gesture allow any transfer mode
                Dragboard db = table.startDragAndDrop(TransferMode.ANY);

                // Put a string on a dragboard
                ClipboardContent content = new ClipboardContent();
                content.putString(String.valueOf(selectedIndex));
                db.setContent(content);

                t.consume();
            }          
        });
        // Get DragDone however no need now
        
        TableColumn<AnimatorShapeBase, String> shapeNameColumn = new TableColumn<>("Name");
        shapeNameColumn.setCellValueFactory(new PropertyValueFactory<AnimatorShapeBase, String>("name"));
        
        table.setItems(shapeLibrary);
        table.getColumns().add(shapeNameColumn);
        
        // Menu
        Button newButton = new Button("N");
        newButton.setOnAction(new EventHandler<ActionEvent>() 
        {
            @Override
            public void handle(ActionEvent t)
            {
                //shapeLibrary.add(new CompoundShape());
                throw new UnsupportedOperationException("Not support now");
            }
        });
        Button addButton = new Button("+");
        addButton.setPrefSize(20, 20);
        addButton.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                if (selectedShape.get() != null)
                {
                    // Should create a immutableReferenceCopy instead
                    //shapeLibrary.add(selectedShape.get().referenceCopy());
                    throw new UnsupportedOperationException("Not support now");
                }
            }       
        });
        Button deleteButton = new Button("-");
        deleteButton.setPrefSize(20, 20);
        deleteButton.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent t)
            {
                AnimatorShapeBase shape = table.getSelectionModel().getSelectedItem();
                if (shape != null)
                {
                    shapeLibrary.remove(shape);
                }
            }
        });
        
        // Layout everything
        VBox vBox  = new VBox(5);
        vBox.setFillWidth(true);
        vBox.getChildren().addAll(newButton, addButton, deleteButton);
        
        HBox hBox = new HBox(5);
        hBox.getChildren().addAll(table, vBox);
        
        getChildren().addAll(preview, hBox);
    }
    
//    public ObservableList<AnimatorShapeBase> getShape()
//    {
//        return unmodifiableShapeLibrary;
//    }
    
    public AnimatorShapeBase getShape(int i)
    {
        return shapeLibrary.get(i)/*.referenceCopy()*/;
    }
}
