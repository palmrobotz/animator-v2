
package animator.ui;

import animator.core.animation.AbstractAnimationModel;
import animator.core.animation.ShapeProperty;
import animator.core.command.CommandCenter;
import animator.core.command.NewAnimationBlockCommand;
import animator.core.animation.AnimationModel;
import animator.core.animation.KeyTime;
import animator.core.animation.LayerModel;
import animator.core.animation.PropertyValue;
import animator.ui.preview.EditablePreviewWindow;
import animator.ui.color.ColorMixer;
import animator.ui.color.ColorSwatches;
import animator.core.animation.TimelineModel;
import animator.core.command.Exporter;
import animator.core.command.Loader;
import animator.core.command.Saver;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.RasterImage;
import animator.core.shape.SVGImage;
import animator.core.shape.event.ShapeChangedEvent;
import animator.core.shape.event.ShapeChangedListener;
import animator.ui.preview.AnimatorPreviewWindow;
import animator.ui.preview.event.EditablePreviewWindowActionEvent;
import animator.ui.preview.event.EditablePreviewWindowActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javafx.animation.Interpolator;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 *
 * @author Nuntipat narkthong
 */
public class Animator extends Application
{
    private double initX = 0, initY = 0, endX = 0, endY = 0;
    
    // Store the current KeyEvent so we can check which keyboard key is pressed
    private KeyEvent currentKeyEvent = null;
    
    @Override
    public void start(final Stage primaryStage)
    {
        final ToolBox toolBox = ToolBox.getInstance();
       
        //final EditablePreviewWindow previewWindow = new EditablePreviewWindow(640, 480, Color.WHITE, sideBar.toolSelectProperty());
        final AnimatorPreviewWindow apw = new AnimatorPreviewWindow(640, 480, Color.WHITE);
        final EditablePreviewWindow previewWindow = apw.getPreviewWindow();
        
        TimelineModel timelineModel = new TimelineModel();
        final Timeline timeline = new Timeline(timelineModel);
        timelineModel.newLayer();
               
        // Auto select shape when select layer or animation block
        timeline.selectedShapeProperty().addListener(new ChangeListener<List<AnimatorShapeBase>>()
        {
            @Override
            public void changed(ObservableValue<? extends List<AnimatorShapeBase>> ov, List<AnimatorShapeBase> t, List<AnimatorShapeBase> t1)
            {
                List<AnimatorShapeBase> shapeList = new ArrayList<>();
                
                if (timeline.getSelectedLayer().isEmpty() && timeline.getSelectedAnimationModel().isEmpty())
                {
                    previewWindow.clearShapeSelection();
                }
                else
                {
                    for (LayerModel lm : timeline.getSelectedLayer())
                    {
                        if (lm.isAnimationExist(timeline.getCurrentTime()))
                        {
                            AnimationModel am = lm.getAnimation(timeline.getCurrentTime());
                            shapeList.add(am.getShape());
                        }
                    }
                    
                    for (AbstractAnimationModel am : timeline.getSelectedAnimationModel())
                    {
                        if (am.isContainTime(timeline.getCurrentTime()) && !shapeList.contains(am.getShape()))
                            shapeList.add(am.getShape());
                    }
                    
                    previewWindow.setSelectedShape(shapeList);
                }
            }
        });
        // Select layer and block when the shape is selected
        previewWindow.selectedShapeProperty().addListener(new ChangeListener<List<AnimatorShapeBase>>()
        {
            @Override
            public void changed(ObservableValue<? extends List<AnimatorShapeBase>> ov, List<AnimatorShapeBase> t, List<AnimatorShapeBase> t1)
            {
                for (AnimatorShapeBase asb : t1)
                {
                    timeline.setSelectedAnimationModel(asb.getAnimationModel());
                }
            }
        });
        
        final double[] scale = {0.3, 0.5, 0.75, 1};
        final ObservableList<String> zoomOptions = FXCollections.<String>observableArrayList("30%", "50%", "75%", "100%");
        
        // Implement the zoom functionality 
        final ComboBox<String> comboBox = new ComboBox<>(zoomOptions);
        SingleSelectionModel<String> comboBoxModel = comboBox.getSelectionModel();
        comboBox.setEditable(true);        
        comboBox.valueProperty().addListener(new ChangeListener<String>() 
        {
            @Override 
            public void changed(ObservableValue ov, String t, String t1) 
            {        
                try
                {
                    double value;
                    if (t1.endsWith("%"))
                        value = Double.parseDouble(t1.substring(0, t1.length()-1));
                    else      
                        value = Double.parseDouble(t1);
                    if (value > 0)
                    {
                        apw.setZoom(value / 100.0);
                    }
                }
                catch (NumberFormatException e)
                {
                    comboBox.setValue(t);
                }
            }    
        });
        comboBoxModel.select("100%");
        comboBoxModel.selectedIndexProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number oldValue, Number newValue)
            {
                apw.setZoom(scale[newValue.intValue()]);
            }
        });
        apw.zoomProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
            {
                comboBox.setValue(String.format("%.1f", t1.doubleValue() * 100) + "%");
            }
        });
        
        
        // Wrap the combobox in a Toolbar
        //ToolBar controlBar = new ToolBar(comboBox);
        //controlBar.setId("ControlToolBar");
        
        AnchorPane preview = new AnchorPane();
        preview.getChildren().addAll(apw,comboBox);
        AnchorPane.setBottomAnchor(comboBox, 15.0);
        AnchorPane.setRightAnchor(comboBox, 15.0);
        AnchorPane.setBottomAnchor(apw, 0.0);
        AnchorPane.setLeftAnchor(apw, 0.0);
        AnchorPane.setRightAnchor(apw, 0.0);
        AnchorPane.setTopAnchor(apw, 0.0);
        
        
        // Only allow the scrollPane (fix the size of the toolBar)
        //VBox.setVgrow(apw, Priority.ALWAYS);
        //VBox.setVgrow(controlBar, Priority.NEVER);
        
        //setPrefSize(800, 600);
        
        
        
        
        //PreviewWindow previewWindow = new PreviewWindow();
        //previewWindow.setCurrentTimeProperty(timeline.currentTimeProperty());
        //previewWindow.setSelectedLayerProperty(timeline.selectedLayerProperty());
        //previewWindow.setTimelineModel(t);
        
        // Attach preview window to the AnimationController
        timeline.getController().addPreviewableSurface(previewWindow);
        
        
        HBox toolAndpreview =  new HBox();
        toolAndpreview.getChildren().addAll(toolBox, preview);
        
        SplitPane splitPane = new SplitPane();
        splitPane.setOrientation(Orientation.VERTICAL);
        splitPane.getItems().addAll(toolAndpreview, timeline);
        splitPane.setDividerPositions(0.8);
        
        final ColorMixer colorMixer = ColorMixer.getInstance();
        colorMixer.setSelectShapeProperty(previewWindow.selectedShapeProperty());
        //colorMixer.attachColorRecievable(previewWindow);
        final ColorSwatches swatches = new ColorSwatches();
        swatches.selectedColorProperty().addListener(new ChangeListener<Color>()
        {
            @Override
            public void changed(ObservableValue<? extends Color> ov, Color t, Color t1)
            {
                colorMixer.setCurrentColor(t1);
            }           
        });
        
        final StrokeConfig strokeConfig = StrokeConfig.getInstance();
        strokeConfig.setSelectShapeProperty(previewWindow.selectedShapeProperty());
        // @TODO Bind selectedShapeProperty together and use apw.selected...
        final Library library = new Library(apw, previewWindow.selectedShapeProperty());
        
        PropertyWindow propertyWindow = new PropertyWindow(previewWindow.selectedShapeProperty());
        
        // Attach selection recievable
        //previewWindow.attachSelectionRecievable(propertyWindow);
        //previewWindow.attachSelectionRecievable(colorMixer);
        
        VBox property = new VBox();
        
        CustomTitlePane propertyTitle = new CustomTitlePane("Property", propertyWindow, false);
        CustomTitlePane mixerTitle = new CustomTitlePane("Mixer", colorMixer, false);
        CustomTitlePane swatchTitle = new CustomTitlePane("Swatch", swatches, true);
        
        final CheckMenuItem standard = new CheckMenuItem("Standard");
        final CheckMenuItem skintone = new CheckMenuItem("Skintone");
        final CheckMenuItem pastel = new CheckMenuItem("Pastel");
        
        standard.setSelected(true);
        
        standard.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                standard.setSelected(true);
                skintone.setSelected(false);
                pastel.setSelected(false);
                swatches.selectedPaletteProperty().set(0);
            }
        });
        
        skintone.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                standard.setSelected(false);
                skintone.setSelected(true);
                pastel.setSelected(false);        
                swatches.selectedPaletteProperty().set(1);
            }
        });
        
        pastel.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                standard.setSelected(false);
                skintone.setSelected(false);
                pastel.setSelected(true); 
                swatches.selectedPaletteProperty().set(2);
            }
        });
        
        swatchTitle.getContextualMenu().getItems().addAll(standard,pastel,skintone);
        
        CustomTitlePane strokeConfigTitle = new CustomTitlePane("Stroke", strokeConfig, false);
//        CustomTitlePane libaryTitle = new CustomTitlePane("Library", library, false);
        
        property.getChildren().addAll(propertyTitle
                , mixerTitle
                , swatchTitle
                , strokeConfigTitle
                /*, libaryTitle*/);

        
        
        Menu fileMenu = new Menu("File");
        
        MenuItem newfile = new MenuItem("New");
        newfile.setAccelerator(KeyCombination.keyCombination("Shortcut+N"));
        newfile.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent t)
            {
                while (timeline.getModel().getLayerCount() != 0)
                {
            //timeline.getLayer(0).removeAnimation(timeline.getLayer(0).getDuration());
                timeline.getModel().removeLayer(timeline.getModel().getLayer(0));
                }
                timeline.getController().jumpToTime(Duration.ZERO);
                timeline.getModel().newLayer();
            }   
        });
        
        MenuItem save = new MenuItem("Save");
        save.setAccelerator(KeyCombination.keyCombination("Shortcut+S"));
        save.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent t)
            {
                FileChooser fileChooser = new FileChooser();
                //fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Animator Beta 0.1", "*.anim"));
                File file = fileChooser.showSaveDialog(primaryStage);
                if (file != null)
                {
                    Saver saveFile = new Saver(timeline.getModel());
                    saveFile.setFile(file.getAbsolutePath());
                    try
                    {
                        saveFile.saveFile();
                    }
                    catch (Exception e)
                    {
                        System.err.println("Can't save file");
                    }
                }
            }   
        });
        
        MenuItem open = new MenuItem("Open");
        open.setAccelerator(KeyCombination.keyCombination("Shortcut+O"));
        open.setOnAction(new EventHandler<ActionEvent>() 
        {
            @Override
            public void handle(ActionEvent t)
            {
                FileChooser fileChooser = new FileChooser();
                //fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Animator Beta 0.1", "*.anim"));
                File file = fileChooser.showOpenDialog(primaryStage);
                if (file != null)
                {
                    Loader load = new Loader(file.getAbsolutePath(), timeline.getModel());
                    load.loadFile();
                    timeline.getController().jumpToTime(Duration.ZERO);
                }
            }
        });
        
        MenuItem importImage = new MenuItem("Import Image");
        importImage.setAccelerator(KeyCombination.keyCombination("Shortcut+I"));
        importImage.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent t)
            {
                try
                {
                    FileChooser fileChoser = new FileChooser();
                    File file = fileChoser.showOpenDialog(primaryStage);
                    System.err.println(file.getPath());
                    
                    if (timeline.getSelectedLayer() == null)
                    {
                        throw new RuntimeException("Selected layer is null");
                    }
                    if (previewWindow.getSelectedShape() == null)
                    {
                        throw new RuntimeException("Select shape is null");
                    }

                    // Clear selection
                    previewWindow.clearShapeSelection();
                    // @TODO add clear selection for the timeline
                    
                    if (!timeline.getSelectedLayer().isEmpty()/* && previewWindow.getSelectedShape().isEmpty()*/)
                    {
                        List<LayerModel> list = timeline.getSelectedLayer();
                        if (list.size() > 1)
                        {
                            System.err.println("Select more than 1 layer when draw, will chose the first one selected");
                        }

                        LayerModel layerModel = list.get(0);
                        if (layerModel.isLock())
                        {
                            return;
                        }

                        AnimatorShapeBase asb = new RasterImage(file.getPath());
                        asb.setX(0);
                        asb.setY(0);
                        asb.setSelect(true);
                        CommandCenter.INSTANCE.addCommand(new NewAnimationBlockCommand(layerModel, asb, timeline.getCurrentTime(), timeline.getCurrentTime() + 5000));
                    }
                    else if (timeline.getSelectedLayer().isEmpty())
                    {
                        timeline.getModel().newLayer();
                        LayerModel layerModel = timeline.getModel().getLayer(timeline.getModel().getLayerCount() - 1);

                        AnimatorShapeBase asb = new RasterImage(file.getPath());
                        asb.setX(0);
                        asb.setY(0);
                        asb.setSelect(true);
                        CommandCenter.INSTANCE.addCommand(new NewAnimationBlockCommand(layerModel, asb, timeline.getCurrentTime(), timeline.getCurrentTime() + 5000));
                    }
                }
                catch (NullPointerException e)
                {
                    // @TODO Should show dialog box
                    System.err.println("Can't open file");
                }
           }
        });
        
        MenuItem importSVG = new MenuItem("Import SVG");
        importSVG.setOnAction(new EventHandler<ActionEvent>() 
        {
            @Override
            public void handle(ActionEvent t)
            {
                FileChooser fileChoser = new FileChooser();
                File file = fileChoser.showOpenDialog(primaryStage);
                
                // No file choosen
                if (file == null)
                    return;

                LayerModel layerModel;
                
                if (!timeline.getSelectedLayer().isEmpty())
                {
                    List<LayerModel> list = timeline.getSelectedLayer();
                    if (list.size() > 1)
                        System.err.println("Select more than 1 layer when draw, will chose the first one selected");

                    layerModel = list.get(0);
                    
                    // If the layer is lock do nothing
                    if (layerModel.isLock())
                        return;
                }
                else
                {
                    layerModel = timeline.getModel().newLayer();
                }
                
                AnimatorShapeBase asb;
                try 
                {
                    asb = new SVGImage(file.getPath());      
                    asb.setX(0);
                    asb.setY(0);
                    asb.setSelect(true);
                    CommandCenter.INSTANCE.addCommand(new NewAnimationBlockCommand(layerModel, asb, timeline.getCurrentTime(), timeline.getCurrentTime() + 5000));
                } 
                catch (FileNotFoundException ex) 
                {
                    System.err.println("Cannot import this SVG. (The image file is not found.)");
                }
            }
        });
        
        MenuItem export = new MenuItem("Export");
        export.setAccelerator(KeyCombination.keyCombination("Shortcut+E"));
        export.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent t)
            {
                FileChooser fileChooser = new FileChooser();
                fileChooser.getExtensionFilters().add(
                    new ExtensionFilter("HTML5", "*.html"));
                
                //fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Animator Beta 0.1", "*.anim"));
                File file = fileChooser.showSaveDialog(primaryStage);
                if (file != null)
                {
                    
                    Exporter saveFile = new Exporter(timeline.getModel());
                    saveFile.setFile(file.getAbsolutePath());
                    try
                    {
                        saveFile.saveFile();
                    }
                    catch (Exception e)
                    {
                        System.err.println("Can't save file");
                    }
                }
            }   
        });
        fileMenu.getItems().addAll(newfile, save, open, importImage, importSVG, export);
        Menu editMenu = new Menu("Edit");
        MenuItem undoMenuItem = new MenuItem("Undo");
        undoMenuItem.setAccelerator(KeyCombination.keyCombination("Shortcut+Z"));
        undoMenuItem.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent t)
            {
                if (CommandCenter.INSTANCE.canUndo())
                    CommandCenter.INSTANCE.undo();
            }   
        });
        MenuItem redoMenuItem = new MenuItem("Redo");
        redoMenuItem.setAccelerator(KeyCombination.keyCombination("Shortcut+Y"));
        redoMenuItem.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent t)
            {
                if (CommandCenter.INSTANCE.canRedo())
                    CommandCenter.INSTANCE.redo();
            }   
        });
        editMenu.getItems().addAll(undoMenuItem, redoMenuItem);
        
        MenuBar menuBar = new MenuBar();
        menuBar.setUseSystemMenuBar(true);
        menuBar.getMenus().addAll(fileMenu, editMenu);
        
        ToolBar titleBar = createTitleBar(primaryStage);
        
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(VBoxBuilder.create().children(titleBar, menuBar).build());
        borderPane.setCenter(splitPane);
        borderPane.setRight(property);
        
        previewWindow.addEditablePreviewWindowActionListener(new EditablePreviewWindowActionListener() 
        {
            @Override
            public void actionPerformed(EditablePreviewWindowActionEvent e)
            {
                if (e.getAction() == EditablePreviewWindowActionEvent.Action.REMOVE_SHAPE)
                {
                    // @TODO fix this because we shouldn't cast manually
                    AnimationModel animationModel = (AnimationModel) e.getShape().getAnimationModel();
                    LayerModel layerModel = animationModel.getLayerModel();
                    if (!layerModel.isLock())
                        if (!layerModel.removeAnimation(animationModel))
                            System.err.println("Animation not remove");
                }
            }
        });
        previewWindow.addShapeChangedListener(new ShapeChangedListener() 
        {
            @Override
            public void onShapeChanged(ShapeChangedEvent e)
            {
                AnimatorShapeBase selectedShape = e.getSource();
                AbstractAnimationModel animationModel = selectedShape.getAnimationModel();
                if (animationModel.getLayerModel().isLock())
                    return;
                
                if (ShapeProperty.isPropertyAnimatable(e.getChangedProperty()))
                {
                    PropertyValue<?> pv = e.getChangedProperty().createPropertyValue(selectedShape, Interpolator.LINEAR);
                    KeyTime kt = new KeyTime(animationModel, timeline.getCurrentTime());
                    kt.addProperty(pv);
                    // If keytime add fail revert change for fialure atomic (automatic since move timeline will override change)
                    animationModel.addKeyTime(kt);
                }
            }
        });
        // @TODO use apw.setOnMousePressed instead to allow drawing and drop to all EPW
        // Support drawing 
        previewWindow.setOnMousePressed(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent event)
            {
                if (toolBox.getToolSelect() == Tool.SELECT)
                {
                    // Do nothing
                } 
                else if (toolBox.getToolSelect() == Tool.ZOOM)
                {
                    if (currentKeyEvent != null && currentKeyEvent.isAltDown())
                        apw.setZoom(apw.getZoom() - 0.05);
                    else
                        apw.setZoom(apw.getZoom() + 0.05);
                }
                else if (toolBox.getToolSelect().hasCorrespondShape())
                {
                    initX = previewWindow.mapXToPreviewWindowCoordinate(event.getX());
                    initY = previewWindow.mapYToPreviewWindowCoordinate(event.getY());

                    // Sometime program throw NullPointerException here?
                    if (timeline.getSelectedLayer() == null)
                        throw new RuntimeException("Selected layer is null");
                    if (previewWindow.getSelectedShape() == null)
                        throw new RuntimeException("Select shape is null");

                    // Can't auto deselect because of path
                    // @TODO (FIXED) It should be ok to draw when other object is selected (auto deselect)
                    if (!timeline.getSelectedLayer().isEmpty() && previewWindow.getSelectedShape().isEmpty())
                    {
                        List<LayerModel> list = timeline.getSelectedLayer();
                        if (list.size() > 1)
                            System.err.println("Select more than 1 layer when draw, will chose the first one selected");

                        LayerModel layerModel = list.get(0);
                        if (layerModel.isLock())
                            return;
                        
                        // If there is a block selected, add new child shape
                        if (timeline.getSelectedAnimationModel().size() == 1)
                        {
                            AbstractAnimationModel aam = timeline.getSelectedAnimationModel().get(0);
                            if (aam instanceof AnimationModel)
                            {
                                AnimationModel am = (AnimationModel) aam;
                                // Allow drawing if the playhead position in in the block selecting
                                if (am.isContainTime(timeline.getCurrentTime()))
                                {
                                    AnimatorShapeBase asb = toolBox.getToolSelect().asAnimatorShape().createShape(0, 0, 0, 0, colorMixer.getForegroundColor(), colorMixer.getBackgroundColor()
                                            , strokeConfig.getJointProperty(), strokeConfig.getCapProperty(), strokeConfig.getStrokeWidth());
                                    asb.setX(initX);
                                    asb.setY(initY);
                                    asb.setSelect(true);
                                    
                                    am.addChildShape(asb, timeline.getCurrentTime());
                                }
                            }
                        }
                        // If no block selected, create new one
                        else
                        {
                            AnimatorShapeBase asb = toolBox.getToolSelect().asAnimatorShape().createShape(0, 0, 0, 0, colorMixer.getForegroundColor(), colorMixer.getBackgroundColor()
                                            , strokeConfig.getJointProperty(), strokeConfig.getCapProperty(), strokeConfig.getStrokeWidth());
                            asb.setX(initX);
                            asb.setY(initY);
                            asb.setSelect(true);
                            
                            CommandCenter.INSTANCE.addCommand(new NewAnimationBlockCommand(layerModel
                                        ,asb, timeline.getCurrentTime(), timeline.getCurrentTime() + 5000));
                        }
                    }
                }
            }
        });
        // Support Drag&Drop
        previewWindow.setOnDragOver(new EventHandler<DragEvent>()
        {
            @Override
            public void handle(DragEvent t)
            {
                /* data is dragged over the target */
                /* accept it only if it is not dragged from the same node 
                 * and if it has a string data */
                if (t.getGestureSource() != previewWindow &&
                        t.getDragboard().hasString()) 
                {
                    /* allow for both copying and moving, whatever user chooses */
                    t.acceptTransferModes(TransferMode.ANY);
                }

                t.consume();
            }          
        });
        // Show visual feedback
        //previewWindow.setOnDragEntered();
        //previewWindow.setOnDragExited();
        // Accept drag&drop
        previewWindow.setOnDragDropped(new EventHandler<DragEvent>() 
        {
            @Override
            public void handle(DragEvent event) 
            {
                /* data dropped */
                /* if there is a string data on dragboard, read it and use it */
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasString()) 
                {
                    // @TODO It should be ok to draw when other object is selected (auto deselect)
                    if (!timeline.getSelectedLayer().isEmpty() && previewWindow.getSelectedShape().isEmpty())
                    {
                        List<LayerModel> list = timeline.getSelectedLayer();
                        if (list.size() > 1)
                            System.err.println("Select more than 1 layer when draw, will chose the first one selected");

                        LayerModel layerModel = list.get(0);
                        if (layerModel.isLock())
                            return;

                        AnimatorShapeBase asb = library.getShape(Integer.parseInt(db.getString()));
                        asb.setX(initX);
                        asb.setY(initY);
                        asb.setSelect(true);
                        CommandCenter.INSTANCE.addCommand(new NewAnimationBlockCommand(layerModel
                                    ,asb, timeline.getCurrentTime(), timeline.getCurrentTime() + 5000));
                    }
                    success = true;
                }
                /* let the source know whether the string was successfully 
                 * transferred and used */
                event.setDropCompleted(success);

                event.consume();
             }
        });
        
        //Will be move to css file later if I can find the borderpane tag.
        borderPane.getRight().setStyle("-fx-background-color:rgb(60,60,60)");
        
        Scene primaryScene = new Scene(borderPane);
        primaryScene.getStylesheets().add(getClass().getResource("/animator/animatorStyle.css").toString());
        
        // Transparent scene and stage
        //primaryScene.setFill(Color.);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        
        primaryStage.setMinWidth(1280);
        primaryStage.setMinHeight(800);
        primaryStage.setTitle("Animator");
        primaryStage.setScene(primaryScene);
        primaryStage.show();
        
        // @TODO Acting to ToolBar and Keyboard Event by changing mouse pointer
        primaryScene.setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent t)
            {
                currentKeyEvent = t;
            }
        });
        primaryScene.setOnKeyReleased(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent t)
            {
                currentKeyEvent = null;
            }
        });
        
        previewWindow.setPrefWidth(timeline.getWidth()-toolBox.getWidth());
        timeline.widthProperty().addListener(new ChangeListener<Number>() 
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) 
            {
                previewWindow.setPrefWidth(t1.doubleValue()-toolBox.getWidth());
            }
        });
        // Ugly way to look for the scroll bar
//        for (Node node: timeline.listView.lookupAll(".scroll-bar")) 
//        {
//            if (node instanceof ScrollBar) 
//            {
//                final ScrollBar bar = (ScrollBar) node;
//                if (bar.getOrientation() == Orientation.HORIZONTAL)
//                    timeline.setScrollListener(bar);
//            }
//        }
    }
    
    private double stageDragDeltaX, stageDragDeltaY;
    private Rectangle2D backupWindowBounds;
    
    private ToolBar createTitleBar(final Stage stage)
    {
        ImageView logo = new ImageView(new Image(getClass().getResourceAsStream("/animator/logo.png")));
        ImageView minimize = new ImageView(new Image(getClass().getResourceAsStream("/animator/minimize.png")));
        minimize.setPickOnBounds(true);
        minimize.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                stage.setIconified(true);
            }   
        });
        ImageView maximize = new ImageView(new Image(getClass().getResourceAsStream("/animator/maximize.png")));
        maximize.setPickOnBounds(true);
        maximize.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                final double stageY = stage.getY();
                final Screen screen = Screen.getScreensForRectangle(stage.getX(), stageY, 1, 1).get(0);      // line 42
                Rectangle2D bounds = screen.getVisualBounds();
                if (bounds.getMinX() == stage.getX() && bounds.getMinY() == stageY
                        && bounds.getWidth() == stage.getWidth() && bounds.getHeight() == stage.getHeight())
                {
                    if (backupWindowBounds != null)
                    {
                        stage.setX(backupWindowBounds.getMinX());
                        stage.setY(backupWindowBounds.getMinY());
                        stage.setWidth(backupWindowBounds.getWidth());
                        stage.setHeight(backupWindowBounds.getHeight());
                    }
                }
                else
                {
                    backupWindowBounds = new Rectangle2D(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight());
                    final double newStageY = screen.getVisualBounds().getMinY();
                    stage.setX(screen.getVisualBounds().getMinX());
                    stage.setY(newStageY);
                    stage.setWidth(screen.getVisualBounds().getWidth());
                    stage.setHeight(screen.getVisualBounds().getHeight());
                }
            }   
        });
        ImageView close = new ImageView(new Image(getClass().getResourceAsStream("/animator/close.png")));
        close.setPickOnBounds(true);
        close.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                Platform.exit();
            }   
        });
        ImageView fullscreen = new ImageView(new Image(getClass().getResourceAsStream("/animator/fullscreen.png")));
        fullscreen.setPickOnBounds(true);
        fullscreen.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                stage.setFullScreen(!stage.isFullScreen());
            }   
        });
        
        HBox titleBarContent = new HBox(10);
        titleBarContent.getChildren().addAll(logo, new Label("Animator"), minimize, maximize, close, fullscreen);
        
        ToolBar titleBar = new ToolBar();
        titleBar.setPrefHeight(25);
        titleBar.setMinHeight(25);
        titleBar.setMaxHeight(25);
        titleBar.getItems().add(titleBarContent);
        
        titleBar.setOnMousePressed(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {
                // record a delta distance for the drag and drop operation.
                stageDragDeltaX = stage.getX() - mouseEvent.getScreenX();
                stageDragDeltaY = stage.getY() - mouseEvent.getScreenY();
            }
        });
        titleBar.setOnMouseDragged(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {
                stage.setX(mouseEvent.getScreenX() + stageDragDeltaX);
                stage.setY(mouseEvent.getScreenY() + stageDragDeltaY);
            }
        });
        
        return titleBar;
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }
}
