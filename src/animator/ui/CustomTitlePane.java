package animator.ui;

import javafx.beans.property.BooleanProperty;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

/**
 *
 * @author Xephrt
 */
public class CustomTitlePane extends TitledPane
{
    private final ContextMenu cm;
    private final Label titledLabel;
    //final Circle c;
    private final ImageView imgV;

    public CustomTitlePane(String s, Node n, Boolean b)
    {
        this.setContent(n);
        this.setCollapsible(false);
        HBox wrapper = new HBox();
        wrapper.setAlignment(Pos.CENTER);

        final Path p = new Path();
        p.getElements().addAll(new MoveTo(0, 0), new LineTo(20, 0), new LineTo(10, 15), new ClosePath());
        p.setFill(Color.WHITE);
        p.setStrokeWidth(0);
        p.setScaleX(0.55);
        p.setScaleY(0.55);

        p.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<Event>()
        {
            @Override
            public void handle(Event t)
            {
                if (CustomTitlePane.this.isExpanded())
                {
                    CustomTitlePane.this.setCollapsible(true);
                    CustomTitlePane.this.setExpanded(false);
                    CustomTitlePane.this.setCollapsible(false);
                    p.setRotate(270);
                }
                else
                {
                    CustomTitlePane.this.setCollapsible(true);
                    CustomTitlePane.this.setExpanded(true);
                    CustomTitlePane.this.setCollapsible(false);
                    p.setRotate(0);
                }
            }
        });

        titledLabel = new Label(s);
        titledLabel.setTextFill(Color.WHITE);
        titledLabel.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                if (CustomTitlePane.this.isExpanded())
                {
                    CustomTitlePane.this.setCollapsible(true);
                    CustomTitlePane.this.setExpanded(false);
                    CustomTitlePane.this.setCollapsible(false);
                    p.setRotate(270);
                }
                else
                {
                    CustomTitlePane.this.setCollapsible(true);
                    CustomTitlePane.this.setExpanded(true);
                    CustomTitlePane.this.setCollapsible(false);
                    p.setRotate(0);
                }
            }
        });

        final Region space = new Region();
        space.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                if (CustomTitlePane.this.isExpanded())
                {
                    CustomTitlePane.this.setCollapsible(true);
                    CustomTitlePane.this.setExpanded(false);
                    CustomTitlePane.this.setCollapsible(false);
                    p.setRotate(270);
                }
                else
                {
                    CustomTitlePane.this.setCollapsible(true);
                    CustomTitlePane.this.setExpanded(true);
                    CustomTitlePane.this.setCollapsible(false);
                    p.setRotate(0);
                }
            }
        });
        space.setPrefWidth(200);

        cm = new ContextMenu();

        Image img = new Image("/animator/contextual.png");
        imgV = new ImageView(img);
        imgV.setVisible(b);
        imgV.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent t)
            {
                if (!(cm.getItems().isEmpty()))
                {
                    cm.show(imgV, t.getScreenX(), t.getScreenY());
                }
            }
        });
        HBox labelAndContext = new HBox();
        labelAndContext.getChildren().addAll(titledLabel, space, imgV);
        labelAndContext.setSpacing(5);
        labelAndContext.setAlignment(Pos.CENTER);

        wrapper.getChildren().addAll(p, labelAndContext);

        this.setGraphic(wrapper);

        //
        //
        //If titled pane width continue increace or decrease and never stop this is the reason
        //
        //
        /*
         this.widthProperty().addListener(new ChangeListener<Number>() {

         @Override
         public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
         if(t1.doubleValue()-120>=0)
         {
         //space.setPrefWidth(t1.doubleValue()-125);
         System.out.println(t1.doubleValue());
         }
         else
         {
         space.setPrefWidth(0);
         }
         }
         });*/

    }

    public Label getTextLabel(String s)
    {
        return titledLabel;
    }

    public ContextMenu getContextualMenu()
    {
        return cm;
    }

    public BooleanProperty contextMenuVisibleProperty()
    {
        return imgV.visibleProperty();
    }
    
    public boolean isContextMenuVisible()
    {
        return imgV.isVisible();
    }
    
    public void setContextMenuVisible(boolean b)
    {
        imgV.setVisible(b);
    }
}
