
package animator.ui;

import animator.core.shape.AnimatorShape;

/**
 *
 * @author Nuntipat narkthong
 */
public enum Tool
{
    NONE
    {
        @Override
        public boolean hasCorrespondShape()
        {
            return false;
        }
        
        @Override
        public AnimatorShape asAnimatorShape()
        {
            throw new IllegalStateException("No correspond shape");
        }
    },
    SELECT
    {
        @Override
        public boolean hasCorrespondShape()
        {
            return false;
        }
        
        @Override
        public AnimatorShape asAnimatorShape()
        {
            throw new IllegalStateException("No correspond shape");
        }
    }, 
    RECTANGLE
    {
        @Override
        public boolean hasCorrespondShape()
        {
            return true;
        }
        
        @Override
        public AnimatorShape asAnimatorShape()
        {
            return AnimatorShape.RECTANGLE;
        }
    }, 
    TRIANGLE
    {
        @Override
        public boolean hasCorrespondShape()
        {
            return true;
        }
        
        @Override
        public AnimatorShape asAnimatorShape()
        {
            return AnimatorShape.TRIANGLE;
        }
    }, 
    CIRCLE
    {
        @Override
        public boolean hasCorrespondShape()
        {
            return true;
        }
        
        @Override
        public AnimatorShape asAnimatorShape()
        {
            return AnimatorShape.CIRCLE;
        }
    },
    PATH
    {
        @Override
        public boolean hasCorrespondShape()
        {
            return true;
        }
        
        @Override
        public AnimatorShape asAnimatorShape()
        {
            return AnimatorShape.PATH;
        }
    }, 
    BRUSH
    {
        @Override
        public boolean hasCorrespondShape()
        {
            return true;
        }
        
        @Override
        public AnimatorShape asAnimatorShape()
        {
            return AnimatorShape.BRUSH;
        }
    },
    ZOOM
    {
        @Override
        public boolean hasCorrespondShape()
        {
            return false;
        }
        
        @Override
        public AnimatorShape asAnimatorShape()
        {
            throw new IllegalStateException("No correspond shape");
        }
    };
    
    public abstract boolean hasCorrespondShape();
    public abstract AnimatorShape asAnimatorShape();
}
