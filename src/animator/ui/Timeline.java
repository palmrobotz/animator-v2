
package animator.ui;

import animator.core.animation.AbstractAnimationModel;
import animator.core.command.AddLayerTimelineCommand;
import animator.core.command.CommandCenter;
import animator.core.command.RemoveLayerTimelineCommand;
import animator.core.animation.AnimationController;
import animator.core.animation.AnimationModel;
import animator.core.animation.KeyTime;
import animator.core.animation.LayerModel;
import animator.core.animation.ShapeProperty;
import animator.core.animation.SubAnimationModel;
import animator.core.animation.TimelineModel;
import animator.core.animation.event.AnimationModelChangedEvent;
import animator.core.animation.event.AnimationModelChangedListener;
import animator.core.animation.event.LayerModelChangedEvent;
import animator.core.animation.event.LayerModelChangedListener;
import animator.core.animation.event.TimelineModelChangedEvent;
import animator.core.animation.event.TimelineModelChangedListener;
import animator.core.shape.AnimatorShapeBase;
import animator.core.shape.AnimatorShapeBaseAdapter;
import animator.core.shape.GroupShape;
import animator.core.shape.event.ShapeChangedEvent;
import animator.core.shape.event.ShapeChangedListener;
import animator.ui.preview.EditablePreviewWindow;
import com.sun.javafx.animation.TickCalculation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyLongProperty;
import javafx.beans.property.ReadOnlyLongWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.LinearGradientBuilder;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcBuilder;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.RectangleBuilder;
import javafx.util.Duration;

/**
 *
 * 
 * @author Nuntipat narkthong
 */
public class Timeline extends BorderPane
{
    /* Helper class */
    private TimelineModel model;
    private AnimationController controller;
    
    /* Non expose property */
    private DoubleProperty playHeadPosition = new SimpleDoubleProperty();   // real x position of the playhead center line (not care the scrollbar)
    private DoubleProperty realAnimationViewWidth = new SimpleDoubleProperty();
    private IntegerProperty widthPerSecond = new SimpleIntegerProperty(DEFAULT_BLOCK_WIDTH_PER_SEC);
    
    /* Expose property */
    private ReadOnlyLongWrapper currentTime = new ReadOnlyLongWrapper();    // Shouldn't be set manually (bind to playHeadPosition)
    //private IntegerProperty selectedLayer = new SimpleIntegerProperty(-1);
    private ReadOnlyObjectWrapper<List<LayerModel> > selectedLayerModel = new ReadOnlyObjectWrapper<>(Collections.<LayerModel>emptyList());
    private ReadOnlyObjectWrapper<List<AbstractAnimationModel> > selectedAnimationModel = new ReadOnlyObjectWrapper<>(Collections.<AbstractAnimationModel>emptyList());
    private ReadOnlyObjectProperty<List<AnimatorShapeBase> > selectedShape; 
    
    /* UI element that need wide access level  */
    private AnimationView animationView;
    private Property property;
    private VBox timelineArea;
    private TimeRuler timeRuler;
    private PlayHead playHead;
    
    /* Current keyevent */
    private KeyEvent currentPressKeyEvent = null;
    
    /**
     * The constructor for {@code Timeline}
     * @param model the model behind this {@code Timeline}
     */
    public Timeline(TimelineModel model)
    {
        super();
        this.model = model;
        this.controller = new AnimationController(model);
        
        //ToolBar timelineMenu = createToolBar();
        
        timelineArea = new VBox();
        timelineArea.setPadding(Insets.EMPTY);
        timelineArea.setSpacing(0);
        
        timeRuler = new TimeRuler();
         
        animationView = new AnimationView();
        animationView.hvalueProperty().addListener(timeRuler.scrollHorizonChanged);
        VBox.setVgrow(animationView, Priority.ALWAYS);
        
        timelineArea.getChildren().addAll(new Group(timeRuler), animationView);    
        
        playHead = new PlayHead(heightProperty());
        animationView.hvalueProperty().addListener(playHead.scrollHorizonChanged);
        
        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll(timelineArea, playHead);
        
        property = new Property();
        
        // Layout everything into the borderpane
        //setTop(timelineMenu);
        setLeft(property);
        getLeft().setStyle("-fx-background-color:rgb(87,87,87)");
        setCenter(stackPane);
        //getCenter().setStyle("-fx-background-color:rgb(87,87,87)");
        
        initTimelineEvent();
    }

    /**
     * Get the {@code AnimationController}
     * @return the controller being use for control the animation
     */
    public AnimationController getController()
    {
        return controller;
    }

    /**
     * 
     * @return 
     */
    public TimelineModel getModel()
    {
        return model;
    }

    /**
     * 
     * @return 
     */
    public ReadOnlyObjectProperty<List<LayerModel> > selectedLayerProperty()
    {
        return selectedLayerModel.getReadOnlyProperty();
    }
    
    public List<LayerModel> getSelectedLayer()
    {
        return selectedLayerModel.get();
    }
    
    public ReadOnlyObjectProperty<List<AbstractAnimationModel> > selectedAnimationModelProperty()
    {
        return selectedAnimationModel.getReadOnlyProperty();
    }
    
    public List<AbstractAnimationModel> getSelectedAnimationModel()
    {
        return selectedAnimationModel.get();
    }
    
    /**
     * Use to select the AnimationBlock when the shape is selected in the EditablePreviewWindow
     * @param am 
     */
    public void setSelectedAnimationModel(AbstractAnimationModel am)
    {
        AnimationBlock ab = animationView.blockMap.get(am);
        if (ab != null)
            ab.setSelect(true);
        // @TODO Uncomment this after fix selection problem and create sub animation block
        //else
        //    throw new IllegalStateException("Can't find this AnimationModel in Timeline view");
    }
    
    public ReadOnlyObjectProperty<List<AnimatorShapeBase> > selectedShapeProperty()
    {
        return selectedShape;
    }
    
    public List<AnimatorShapeBase> getSelectShape()
    {
        return selectedShape.get();
    }
    
    /**
     * 
     * @return 
     */
    public ReadOnlyLongProperty currentTimeProperty()
    {
        return currentTime.getReadOnlyProperty();
    }
    
    public long getCurrentTime()
    {
        return currentTime.get();
    }
    
    /**
     * 
     */
    public void zoomIn()
    {
        widthPerSecond.set(widthPerSecond.get() + 10);
    }
    
    /**
     * 
     */
    public void zoomOut()
    {
        widthPerSecond.set(widthPerSecond.get() - 10);
    }
    
    /*
     * Layout method
     */

    private void initTimelineEvent()
    {
        model.addTimelineModelChangedListener(new TimelineModelChangedListener() 
        {
            @Override
            public void onTimelineModelChanged(TimelineModelChangedEvent e)
            {
                switch (e.getChange())
                {
                    case ADD_LAYER:
                        animationView.addLayer(e.getModel());
                        property.addLayer(e.getModel());
                        break;
                    case REMOVE_LAYER:
                        animationView.removeLayer(e.getModel());
                        property.removeLayer(e.getModel());
                        break;
                    case MOVE_LAYER:
                        break;
                }
            }
        });
        model.addLayerModelChangedListener(new LayerModelChangedListener() 
        {
            @Override
            public void onLayerModelChanged(LayerModelChangedEvent e)
            {
                switch (e.getChange())
                {
                    case ADD_ANIMATION:
                        animationView.addAnimationBlock(e.getAnimationModel());
                        calculateRealAnimationViewWidth();
                        break;
                    case REMOVE_ANIMATION:
                        animationView.removeAnimationBlock(e.getAnimationModel());
                        break;
                    case MOVE_ANIMATION:
                        break;
                }
                // @TODO Not sure if we introduce new bug
                // @TODO Fix bug that some time we need to change playhead position manually when add new block
                controller.jumpToTime(Duration.seconds(playHeadPosition.get() / widthPerSecond.get()));
            }
        });
        model.addAnimationModelChangedListener(new AnimationModelChangedListener() 
        {
            @Override
            public void onAnimationModelChanged(AnimationModelChangedEvent e)
            {
                switch (e.getChange())
                {
                    case ADD_KEYTIME:
                        animationView.addKeyTime(e.getSource(), e.getKeyTime());
                        break;
                    case REMOVE_KEYTIME:
                        break;
                    case MOVE_KEYTIME:
                        break;
                    case STARTTIME:
                        break;
                    case ENDTIME:
                        break;
                }
            }
        });
        
        // Change the playhead position when playback
        controller.playHeadTickProperty().addListener(new ChangeListener<Number>()
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
            {
                Duration toDuration = TickCalculation.toDuration(t1.longValue());
                //System.out.println("Listener : " + toDuration);
                //System.out.println("Duration : " + toDuration.toSeconds() + " Play head : " + (playHeadPosition.get() / widthPerSecond.get()));
                if (toDuration.toSeconds() == playHeadPosition.get() / widthPerSecond.get())
                {
                //    System.out.println("        Controller change and not change playhead");
                }
                else
                {
                //    System.out.println("        Controller change and change playhead");
                    playHeadPosition.set(toDuration.toSeconds() * widthPerSecond.get());
                }
            }   
        });
        
        currentTime.bind(playHeadPosition.divide(widthPerSecond).multiply(1000));
        
        // Detect shift key for multiple selection
        setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent t)
            {
                currentPressKeyEvent = t;
            }
        });
        // Detect keyboard event
        setOnKeyReleased(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent t)
            {
                // Clear stored press keyevent
                currentPressKeyEvent = null;
            }
        });
    }
    
    private void calculateRealAnimationViewWidth()
    {
        double needWidth = (model.getPreferredDuration() / 1000.0) * widthPerSecond.get();
        //System.out.println("Current : " + realAnimationViewWidth.doubleValue() + " New : " + needWidth);
        if (needWidth > timelineArea.widthProperty().doubleValue())
        {
            realAnimationViewWidth.set(needWidth);
            timeRuler.drawSlot = (int) Math.ceil(model.getPreferredDuration() / 1000.0);
            timeRuler.draw();
        }
    }
    
    private void expandRealAnimationViewWidth()
    {
        double needWidth = realAnimationViewWidth.doubleValue() + (PREALLOCATE_SEC * widthPerSecond.get());
        //System.out.println("Expand - Current : " + realAnimationViewWidth.doubleValue() + " New : " + needWidth);
        realAnimationViewWidth.set(needWidth);
        timeRuler.drawSlot = (int) Math.ceil(needWidth / widthPerSecond.get());
        timeRuler.draw();
    }
    
    /*
     * Inner class for each Timeline UI element 
     */
    
    /* UI constant */
    private static final int DEFAULT_BLOCK_WIDTH_PER_SEC = 50;
    private static final int BLOCK_HEIGHT = 22;          // Block Height
    private static final int SUB_BLOCK_HEIGHT = 16;      // Sub block Height
    private static final int KEYTIME_WIDTH = 8;
    private static final int SMALL_KEYTIME_WIDTH = 6;
    private static final int RULER_HEIGHT = 25;
    private static final Color[] colorList = {Color.rgb(137, 161, 199), Color.rgb(176, 142, 192), Color.rgb(200, 200, 150)};
    
    private static final long RESERVED_END_TIME_MS = 1000;
    private static final int PREALLOCATE_SEC = 5;   // Number of second to reallocate when the block reach the reserve time
    
    private static final RectangleBuilder<?> keyTimeShape = RectangleBuilder.create()
                                                            .y((BLOCK_HEIGHT / 2) - (KEYTIME_WIDTH / 2))
                                                            .width(KEYTIME_WIDTH)
                                                            .height(KEYTIME_WIDTH)
                                                            .rotate(45)
                                                            .fill(Color.WHITE);
    private static final ArcBuilder<?> halfKeyTimeShape = ArcBuilder.create()
                                                            .centerY(BLOCK_HEIGHT / 2)
                                                            .radiusX(KEYTIME_WIDTH / 2)
                                                            .radiusY(KEYTIME_WIDTH / 2)
                                                            .startAngle(-90)
                                                            .length(180)
                                                            .type(ArcType.ROUND)
                                                            .fill(Color.WHITE);
    
    private static final RectangleBuilder<?> smallKeyTimeShape = RectangleBuilder.create()
                                                            .y((SUB_BLOCK_HEIGHT / 2) - (SMALL_KEYTIME_WIDTH / 2))
                                                            .width(SMALL_KEYTIME_WIDTH)
                                                            .height(SMALL_KEYTIME_WIDTH)
                                                            .rotate(45)
                                                            .fill(Color.WHITE);
    private static final ArcBuilder<?> smallHalfKeyTimeShape = ArcBuilder.create()
                                                            .centerY(SUB_BLOCK_HEIGHT / 2)
                                                            .radiusX(SMALL_KEYTIME_WIDTH / 2)
                                                            .radiusY(SMALL_KEYTIME_WIDTH / 2)
                                                            .startAngle(-90)
                                                            .length(180)
                                                            .type(ArcType.ROUND)
                                                            .fill(Color.WHITE);
    
    private static final RectangleBuilder<?> collapseBlockButton = RectangleBuilder.create()
                                                            .width(8)
                                                            .height(8)
                                                            .fill(Color.RED);
    
    private class TimeRuler extends Canvas
    {
        private int drawSlot = 0;
        
        private double translateX = 0;
        private double sumDeltaTranslateX = 0, deltaTranslateX = 0;
        
        private GraphicsContext gc;
     
        public ChangeListener<Number> scrollHorizonChanged = new ChangeListener<Number>() 
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
            {
                // Current scroll position (t1 is as percentage)
                translateX = t1.doubleValue() * (realAnimationViewWidth.get() - animationView.getViewportBounds().getWidth());
                
                // Find the translate value for the canvas
                deltaTranslateX = - translateX - sumDeltaTranslateX;
                sumDeltaTranslateX += deltaTranslateX;
                
                // Translate the canvas and redraw
                gc.translate(deltaTranslateX, 0);
                draw();
            }
        };
        
        private TimeRuler()
        {
            timelineArea.widthProperty().addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> ov, Number oldWidth, Number newWidth)
                {
                    // We need to have separate variable to keep number of drawing time slot
                    // since we need to draw over the bound and translate
                    double needWidth = (model.getPreferredDuration()/ 1000.0) * widthPerSecond.get();
                    if (needWidth > newWidth.doubleValue())
                    {
                        realAnimationViewWidth.set(needWidth);
                        drawSlot = (int) Math.ceil(model.getPreferredDuration() / 1000.0);
                    }
                    else
                    {
                        realAnimationViewWidth.set(newWidth.doubleValue());
                        drawSlot = (int) Math.ceil(newWidth.doubleValue() / widthPerSecond.get());
                    }
                      
                    // The size of this control should be equal to the listview otherwise
                    // the scrollbar of the list view will be invisible
                    setWidth(newWidth.doubleValue());
                    draw();
                }
            });
            setHeight(RULER_HEIGHT);
            
            widthPerSecond.addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
                {
                    //System.out.println("Real Width : " + realAnimationViewWidth.get());
                    //System.out.println("Zoom at : " + widthPerSecond.get());
                    
                    // We might need to update the real width in case that the width need is
                    // more than scrollpane viewport
                    double needWidth = (model.getPreferredDuration()/ 1000.0) * widthPerSecond.get();
                    if (needWidth > realAnimationViewWidth.get())
                    {
                        //System.out.println("Set Real Width To : " + needWidth);
                        realAnimationViewWidth.set(needWidth);
                    }
                    else
                    {
                        //System.out.println("Real width not change");
                        drawSlot = (int) Math.ceil(realAnimationViewWidth.get() / widthPerSecond.get());
                    }
                    
                    draw();
                }   
            });
            
            gc = getGraphicsContext2D();
            draw();
        }
        
        private void draw()
        {
            gc.setFill(LinearGradientBuilder.create()
             .startX(0)
             .startY(0)
             .endX(0)
             .endY(RULER_HEIGHT)
             .proportional(false)
             .cycleMethod(CycleMethod.NO_CYCLE)
             .stops(
                 new Stop(0.1f, Color.rgb(86, 86, 86)),
                 new Stop(1.0f, Color.rgb(79, 79, 79)))
             .build());
            gc.fillRect(translateX, 0, getWidth(), getHeight());
            
            gc.setTextBaseline(VPos.BOTTOM);
            gc.setFill(Color.WHITESMOKE);
            gc.setStroke(Color.WHITESMOKE);
            // Find how many slot we have
            for (int i = 0; i < drawSlot; i++)
            {
                double x = i * widthPerSecond.get();
                gc.strokeLine(x, RULER_HEIGHT / 2, x, RULER_HEIGHT);
                gc.fillText(String.valueOf(i), x + 3, getHeight());
            }
        }
     }
    
    private class PlayHead extends Region
    {
        private static final int WIDTH = 12;
        private static final int OFFSET = 0;
        
        public static final int LEFT_LIMIT = -WIDTH / 2 + OFFSET;
        
        // Use for playhead drag capability
        private double initialX = 0;
        private double initialDragX = 0;
        private double deltaX = 0;
        
        public ChangeListener<Number> scrollHorizonChanged = new ChangeListener<Number>() 
        {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
            {
                double translateX = t1.doubleValue() * (realAnimationViewWidth.get() - animationView.getViewportBounds().getWidth());
                setTranslateX(-translateX);
            }
        };
        
        public PlayHead(ObservableValue<Number> height)
        {   
            setWidth(WIDTH);
            height.addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
                {
                    setHeight(t1.doubleValue());
                }
            });
            
            // Create a playhead grip use for draggings
            final Rectangle r = new Rectangle(LEFT_LIMIT , 0, WIDTH, RULER_HEIGHT);
            r.setFill(Color.LIGHTPINK.deriveColor(0, 1, 1, 0.5));
            // Detect a mouse click and record as initial position
            r.setOnMousePressed(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent t)
                {
                    initialX = r.xProperty().doubleValue();
                    initialDragX = t.getX();
                }   
            });
            // Detect a mouse drag and find drag delta
            r.setOnMouseDragged(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent t)
                {
                    deltaX = t.getX() - initialDragX;
                    if (initialX + deltaX >= LEFT_LIMIT)
                        r.xProperty().set(initialX + deltaX);
                }   
            });
            
            final Line l = new Line(0, RULER_HEIGHT, 0, getHeight());
            l.setStroke(Color.LIGHTPINK);
            l.startXProperty().bind(r.xProperty().add(WIDTH / 2));
            l.endXProperty().bind(r.xProperty().add(WIDTH / 2));
            l.endYProperty().bind(height);
            
            r.xProperty().addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
                {
                    //System.out.println("        X position changed");
                    double px = t1.doubleValue() - LEFT_LIMIT;
                    if (px != playHeadPosition.doubleValue())
                    {
                        playHeadPosition.set(px);
                        //System.out.println("aaa : " + px / AnimationBlock.BLOCK_WIDTH_PER_SEC);
                        controller.jumpToTime(Duration.seconds(px / widthPerSecond.get()));
                    }
                }              
            });
            playHeadPosition.addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
                {
                    //System.out.println("        Play head Position Changed");
                    r.setX(t1.doubleValue() + LEFT_LIMIT);
                }
            });
            
            getChildren().addAll(r, l);
            
            setPickOnBounds(false);
        }  
    }
    
    private class AnimationView extends EditablePreviewWindow
    {
//        public final Group group = new Group();
        private final Map<LayerModel, LayerBlock> layerMap = new HashMap<>();
        private final Map<AnimationModel, AnimationBlock> blockMap = new HashMap<>();
        
        private AnimationView()
        {          
            super(0, 0, Color.rgb(120, 120, 120));
            
            previewWidthProperty().bind(realAnimationViewWidth);
            previewHeightProperty().bind(heightProperty());

            // Normally the preview window has a grey border (around the real preview area)
            // which we will remove it so the timeline won't shift and lock at top left
            setBorder(false);
            
            // We don't want to move playhead when select layer
//            addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>()
//            {
//                @Override
//                public void handle(MouseEvent t)
//                {
//                    playHeadPosition.set(t.getX());
//                }
//            });
            
            // Deselect all and move PlayHead to the position clicked
            setOnMousePressed(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent event)
                {
                    // Move playhead when pressed on the timeline
                    // Since getTranslateX() return negative value -(-) = +
                    playHeadPosition.set(event.getX() - playHead.getTranslateX());
                    
                    clearShapeSelection();
                    //selectedLayerModel.set(Collections.<LayerModel>emptyList());
                    //selectedAnimationModel.set(Collections.<AbstractAnimationModel>emptyList());
                }
            });
            
            // Detect widthPerSecond change and zoom
            widthPerSecond.addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
                {
                    for (Entry<AnimationModel, AnimationBlock> entry : blockMap.entrySet())
                    {
                        entry.getValue().setWidth(newValue.doubleValue() * entry.getKey().getDuration() / 1000.0);
                    }
                }             
            });
            
            selectedShape = selectedShapeProperty();
            selectedShapeProperty().addListener(new ChangeListener<List<AnimatorShapeBase>>()
            {
                @Override
                public void changed(ObservableValue<? extends List<AnimatorShapeBase>> ov, List<AnimatorShapeBase> oldList, List<AnimatorShapeBase> newList)
                {
                    List<LayerModel> layerList = new ArrayList<>();
                    List<AbstractAnimationModel> animationList = new ArrayList<>();
                    
                    for (AnimatorShapeBase asb : oldList)
                    {
                        if (asb instanceof LayerBlock)
                        {
                            asb.setFill(LinearGradient.valueOf("linear-gradient(#626262 0%, #3e3e3e 5%, #343434 100%)"));
                        }
                    }
                    
                    if (!newList.isEmpty())
                    {
                        for (AnimatorShapeBase asb : newList)
                        {
                            if (asb instanceof LayerBlock)
                            {
                                LayerBlock lb = (LayerBlock) asb;
                                lb.setFill(LinearGradient.valueOf("linear-gradient(#929292 0%, #6e6e6e 5%, #646464 100%)"));
                                layerList.add(lb.getModel());
                            }
                            else if (asb instanceof AnimationBlock)
                            {
                                AnimationBlock ab = (AnimationBlock) asb;
                                animationList.add(ab.getAnimationModel());
                            }
                            else
                            {
                                throw new RuntimeException("Unknown kind of shape in timeline view");
                            }
                        }

                        selectedLayerModel.set(Collections.unmodifiableList(layerList));
                        selectedAnimationModel.set(Collections.unmodifiableList(animationList));
                    }
                    else
                    {
                        selectedLayerModel.set(Collections.<LayerModel>emptyList());
                        selectedAnimationModel.set(Collections.<AbstractAnimationModel>emptyList());
                    }
                }               
            });
        }
        
        public void addLayer(List<LayerModel> model)
        {
            if (model.size() != 1)
                throw new AssertionError("Shouldn't able to add multiple layer at once now");
            
            final LayerModel layer = model.get(0);
            LayerBlock lb = new LayerBlock(layer);
            // When layer is select, select the animation block at that time too
            lb.selectProperty().addListener(new ChangeListener<Boolean>()
            {
                @Override
                public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1)
                {
                    if (t1.booleanValue() && layer.isAnimationExist(getCurrentTime()))
                    {
                        AnimationModel am = layer.getAnimation(getCurrentTime());
                        AnimationBlock ab = blockMap.get(am);
                        addSelectedShape(ab);
                    }
                }
            });
            
            addShape(lb);
            layerMap.put(layer, lb);
            
            // Recalculate every block and layer y position
            invalidateAllYPosition();
        }
        
        public void removeLayer(List<LayerModel> model)
        {
            if (model.size() != 1)
                throw new AssertionError("Shouldn't able to remove multiple layer at once now");
            
            LayerModel lm = model.get(0);
            LayerBlock r = layerMap.remove(lm);
            if (r != null)
            {
                boolean remove = removeShape(r);
                if (!remove)
                    throw new AssertionError("Layer not on screen");
                for (AnimationModel am : lm.getDataUnmodifiable().values())
                {
                    removeAnimationBlock(am);
                }
            }
            else
            {
                throw new AssertionError("Layer not in sync");
            }
        }
        
        public void addAnimationBlock(AnimationModel am)
        {
            final AnimationBlock ab = new AnimationBlock(am);

            blockMap.put(am, ab);
            addShape(ab);
            
            // If the block expand or collapse recalculate every block and layer y position
            ab.expandProperty().addListener(blockExpandChanged);
            
            ab.addShapeChangedListener(blockChangedListener);
            
            // Recalculate every block and layer y position
            invalidateAllYPosition();
        }
        
        public void removeAnimationBlock(AnimationModel am)
        {
            AnimationBlock block = blockMap.remove(am);
            if (block != null)
            {
                boolean remove = removeShape(block);
                if (!remove)
                    throw new AssertionError("Animation block not on screen");
                block.expandProperty().removeListener(blockExpandChanged);
                block.removeShapeChangedListener(blockChangedListener);
                
                // Recalculate every block and layer y position
                invalidateAllYPosition();
            }
            else
            {
                throw new AssertionError("Animation block not in sync");
            }
        }
        
        public void addKeyTime(AbstractAnimationModel am, KeyTime kt)
        {
            if (am instanceof AnimationModel)
            {
                AnimationBlock ab = blockMap.get(am);
                ab.drawKeyTime(kt);
            }
            else if (am instanceof SubAnimationModel)
            {
                SubAnimationModel sam = (SubAnimationModel) am;
                AnimationBlock ab = blockMap.get(sam.getParentAnimationModel());
                SubAnimationBlock sab = ab.getChildBlockUnmodifiable().get(sam);
                sab.drawKeyTime(kt);
            }
        }
        
        // Set the Y position of all block and layer everytime any block expand or collapse
        private ChangeListener<Boolean> blockExpandChanged = new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1)
            {
                invalidateAllYPosition();
            }
        };
        
        public void invalidateAllYPosition()
        {
            List<Integer> numOfChild = new ArrayList<>();
                
            ObservableList<LayerModel> layerList = model.getDataUnmodifiable();

            // Find maximum number of expanded child block of animation in each layer
            // Ignore the block that is currently collapse
            for (LayerModel layerModel : layerList)
            {
                int maxChild = 0;
                for (AnimationModel animationModel : layerModel.getDataUnmodifiable().values())
                {
                    AnimationBlock ab = blockMap.get(animationModel);
                    if (ab.isExpand())
                    {
                        int size = animationModel.getChildModelUnmodifiable().size();
                        if (size > maxChild)
                            maxChild = size;
                    }
                }
                numOfChild.add(maxChild);
            }

            // Set Y of all block and layer
            int numOfSubBlockAboveCurrentLayer = 0;
            for (int i=0; i<layerList.size(); i++)
            {
                if (i != 0)
                    numOfSubBlockAboveCurrentLayer += numOfChild.get(i-1);

                double yPosition = (BLOCK_HEIGHT * i) + (SUB_BLOCK_HEIGHT * numOfSubBlockAboveCurrentLayer);

                LayerModel layerModel = layerList.get(i);
                
                // Set Y of the property of that layer
                Property.PropertyBlock pb = property.propertyMap.get(layerModel);
                if (pb != null)
                    pb.setLayoutY(yPosition + RULER_HEIGHT);
                
                // Set Y of the layer itself
                layerMap.get(layerModel).setY(yPosition);

                // Set Y of all block in this layer
                for (AnimationModel animationModel : layerModel.getDataUnmodifiable().values())
                {
                    blockMap.get(animationModel).setY(yPosition);
                }
            }
        }
        
        // Select layer also when select the block
        private ShapeChangedListener blockChangedListener = new ShapeChangedListener() 
        {
            @Override
            public void onShapeChanged(ShapeChangedEvent e)
            {
                if (e.getChangedProperty() == ShapeProperty.SELECTION && ((AnimationBlock) e.getSource()).isSelect())
                {
                    AbstractAnimationModel am = ((AnimationBlock) e.getSource()).getAnimationModel();
                    LayerModel lm = am.getLayerModel();
                    LayerBlock lb = layerMap.get(lm);
                    addSelectedShape(lb);
                }
            }
        };
    }
    
    private class LayerBlock extends animator.core.shape.Rectangle
    {
        private LayerModel model;
        
        public LayerBlock(LayerModel layer)
        {
            model = layer;
            
            widthProperty().bind(realAnimationViewWidth);
            setHeight(BLOCK_HEIGHT);
            
            setFill(LinearGradient.valueOf("linear-gradient(#626262 0%, #3e3e3e 5%, #343434 100%)"));
            setStrokeWidth(0);
            setDrawBound(false);
            setDraggable(false);
        }
        
        public LayerModel getModel()
        {
            return model;
        }
    }
    
    private enum AnimationBlockDragMode { MOVE, EXPAND };
           
    private class AnimationBlock extends AnimationBlockBase
    {
        private BooleanProperty expand = new SimpleBooleanProperty(true);
        
        private Map<SubAnimationModel, SubAnimationBlock> childBlock = new HashMap<>();
        private Map<SubAnimationModel, SubAnimationBlock> unmodifiableChildBlock = Collections.unmodifiableMap(childBlock);
        
        public AnimationBlock(final AnimationModel am)
        {
            super(am);

            // Set the x position according to time and bind the x position to the start time using ChangeListener
            setX((am.getStartTime() / 1000.0) * widthPerSecond.doubleValue());
            am.startTimeProperty().addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
                {
                    setX((t1.longValue()/1000.0) * widthPerSecond.doubleValue());
                }             
            });
            
            // Allow toggle between expand/collapse
            final Rectangle expandButton = collapseBlockButton.build();
            expandButton.setOnMouseClicked(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent t)
                {
                    expand.set(!expand.get());
                    t.consume();
                }
            });
            mainLayout.getChildren().add(expandButton);
            
            // Bind expand/collapse state with the GroupShape
            // The group itself is selectable when the block is collapse
            // @TODO May change to bindbidirectional
            ((GroupShape) am.getShape()).mouseTransparentProperty().bind(expandProperty());
            
            // Draw all child block
            for (SubAnimationModel sam : am.getChildModelUnmodifiable())
            {
                SubAnimationBlock sab = new SubAnimationBlock(this, sam);
                sab.selectProperty().bindBidirectional(selectProperty());
                childBlock.put(sam, sab);
                getChildren().add(sab);
            }
            // Can't use getChildren().addAll(childBlock.values()); because the order
            // isn't garantee to remain consistent with the order added
            
            am.getChildModelUnmodifiable().addListener(new ListChangeListener<SubAnimationModel>()
            {
                @Override
                public void onChanged(ListChangeListener.Change<? extends SubAnimationModel> change)
                {
//                    change.next();
//                    if (change.wasAdded())
//                        System.out.println("Change by add");
//                    if (change.wasPermutated())
//                        System.out.println("Change by per");
//                    if (change.wasRemoved())
//                        System.out.println("Change by rem");
//                    if (change.wasReplaced())
//                        System.out.println("Change by rep");
//                    if (change.wasUpdated())
//                        System.out.println("Change by update");
//                    System.out.println("--------------------");
                    
                    // Now we redraw all child block every time the list change
                    // to simplified implementation however we might want to 
                    // optimize this later
                    reDrawChildBlock();
                }
            });
        }
        
        @Override
        public void onMove(long newStartTime)
        {
            double newY = initialY + deltaY;
            
            int layerIndex = (int) (newY / BLOCK_HEIGHT);
            if (layerIndex >= model.getLayerCount())
                layerIndex = model.getLayerCount() - 1;
            
            //System.out.println(layerIndex);
            AnimationModel am = (AnimationModel) getAnimationModel();
            
            // Move layer and time
            if (layerIndex != am.getLayerModel().getLayerIndex())
            {
                LayerModel lm = model.getLayer(layerIndex);
                if (am.changeToLayerModel(lm, newStartTime))
                {
                    animationView.invalidateAllYPosition();
                }
                else
                {
                    // Revert Change
                    setX(initialX);
                    setY(initialY);
                }
            }
            // Just move time
            else
            {
                if (!getAnimationModel().moveToTime(newStartTime))
                    setX(initialX);
                
                // Revert Y
                setY(initialY);
            }
        }
        
        public final BooleanProperty expandProperty()
        {
            return expand;
        }

        public final boolean isExpand()
        {
            return expand.get();
        }
        
        public final void setExpand(boolean b)
        {
            expand.set(b);
        }
        
        private void reDrawChildBlock()
        {
            getChildren().removeAll(childBlock.values());
            childBlock.clear();
            for (SubAnimationModel sam : ((AnimationModel) getAnimationModel()).getChildModelUnmodifiable())
            {
                SubAnimationBlock sab = new SubAnimationBlock(this, sam);
                sab.selectProperty().bindBidirectional(selectProperty());
                childBlock.put(sam, sab);
                getChildren().add(sab);
            }
            // Can't use getChildren().addAll(childBlock.values()); because the order
            // isn't garantee to remain consistent with the order added
            
            // Invalidate all block and layer Y position after draw new child block
            animationView.invalidateAllYPosition();
        }
        
        public Map<SubAnimationModel, SubAnimationBlock> getChildBlockUnmodifiable()
        {
            return unmodifiableChildBlock;
        }
        
        @Override
        public void drawKeyTime(KeyTime kt)
        {
            if (kt.getTime() == getAnimationModel().getStartTime())
            {
                Arc c = halfKeyTimeShape.build();
                c.centerXProperty().bind(widthPerSecond.multiply(((kt.getTime() - getAnimationModel().getStartTime()) / 1000.0)));
                keyTimeGroup.getChildren().add(c);
            }
            else
            {
                Rectangle r = keyTimeShape.build();
                r.xProperty().bind(widthPerSecond.multiply(((kt.getTime() - getAnimationModel().getStartTime()) / 1000.0)).subtract(KEYTIME_WIDTH / 2));
                keyTimeGroup.getChildren().add(r);
            }
        }
    }
    
    private class SubAnimationBlock extends AnimationBlockBase
    {
        public SubAnimationBlock(AnimationBlock parent, SubAnimationModel am)
        {
            super(am);
            
            // Set the x position according to time and bind the x position to the start time using ChangeListener
            final AnimationModel parentModel = am.getParentAnimationModel();
            setX(((am.getStartTime() - parentModel.getStartTime()) / 1000.0) * widthPerSecond.doubleValue());
            am.startTimeProperty().addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
                {
                    setX(((t1.longValue() - parentModel.getStartTime()) / 1000.0) * widthPerSecond.doubleValue());
                }             
            });
            
            // Set fill color to make it stand out from parent block
            bg.setFill(((Color) parent.bg.getFill()).deriveColor(0, 1, 1, 0.5));
            
            // Set the height of the block 
            bg.setHeight(SUB_BLOCK_HEIGHT);
            
            // Translate in the y direction, so the child stack below the parent
            mainLayout.setLayoutY(BLOCK_HEIGHT + (am.getParentAnimationModel().getChildModelUnmodifiable().indexOf(am) * SUB_BLOCK_HEIGHT));
        
            // Hide when collapse
            visibleProperty().bind(parent.expandProperty());           
        }
        
        @Override
        public void onMove(long newStartTime)
        {
//            // Move layer and time
//            if (Math.abs(deltaY) > BLOCK_HEIGHT)
//            {
//                // do nothing
//            }
//            // Just move time
//            else
//            {
                if (!getAnimationModel().moveToTime(newStartTime))
                    setX(initialX);
                
                // Revert Y
                setY(initialY);
//            }
        }
        
        @Override
        public void drawKeyTime(KeyTime kt)
        {
            if (kt.getTime() == getAnimationModel().getStartTime())
            {
                Arc c = smallHalfKeyTimeShape.build();
                c.centerXProperty().bind(widthPerSecond.multiply(((kt.getTime() - getAnimationModel().getStartTime()) / 1000.0)));
                keyTimeGroup.getChildren().add(c);
            }
            else
            {
                Rectangle r = smallKeyTimeShape.build();
                r.xProperty().bind(widthPerSecond.multiply(((kt.getTime() - getAnimationModel().getStartTime()) / 1000.0)).subtract(KEYTIME_WIDTH / 2));
                keyTimeGroup.getChildren().add(r);
            }
        }
    }
    
    private abstract class AnimationBlockBase extends AnimatorShapeBaseAdapter
    {
        protected final Group mainLayout = new Group(); 
                
        protected final Rectangle bg;
        protected final Group keyTimeGroup = new Group();
        
        private static final int EXPAND_BORDER = 5;
        
        // Use for drag capability
        protected AnimationBlockDragMode mode;
        protected double initialX = 0, initialY = 0, initialWidth = 0;
        protected double initialDragX = 0, initialDragY = 0;
        protected double deltaX = 0, deltaY = 0;
        
        public AnimationBlockBase(AbstractAnimationModel am)
        {   
            setAnimationModel(am);
            
            bg = new Rectangle();
            //bg.widthProperty().bind(widthPerSecond.multiply(am.durationProperty()).divide(1000.0));
            // @TODO Better to use bindbidirectional
            bg.setWidth(widthPerSecond.doubleValue() * am.getDuration() / 1000.0);
            am.durationProperty().addListener(new ChangeListener<Number>()
            {
                @Override
                public void changed(ObservableValue<? extends Number> ov, Number t, Number t1)
                {
                    bg.setWidth(widthPerSecond.doubleValue() * t1.doubleValue() / 1000.0);
                }              
            });
            bg.setHeight(BLOCK_HEIGHT);
            bg.setFill(colorList[am.getLayerModel().getLayerIndex() % colorList.length]);
            
            reDrawAllKeyTime();
            
            setOnMousePressed(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent t)
                {
                    if (getAnimationModel().getLayerModel().isLock())
                        return;
                    
                    initialX = getX();
                    initialY = getY();
                    initialWidth = bg.getWidth();
                    initialDragX = t.getX();
                    initialDragY = t.getY();
                    deltaX = 0;
                    deltaY = 0;
                    
                    if (initialDragX > bg.getWidth() - EXPAND_BORDER)
                        mode = AnimationBlockDragMode.EXPAND;
                    else
                        mode = AnimationBlockDragMode.MOVE;
                    
                    t.consume();
                }              
            });
            setOnMouseDragged(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent t)
                {
                    if (getAnimationModel().getLayerModel().isLock())
                        return;
                    
                    deltaX = t.getX() - initialDragX;
                    deltaY = t.getY() - initialDragY;
                    if (mode == AnimationBlockDragMode.EXPAND)
                    {
                        bg.setWidth(initialWidth + deltaX);
                    }
                    else
                    {
                        long newEndTime = (long) ((initialX + deltaX) / widthPerSecond.doubleValue() * 1000.0) + getAnimationModel().getDuration();
                        long timeAvailable = (long) (realAnimationViewWidth.doubleValue() / widthPerSecond.doubleValue() * 1000.0);
                        
                        if (newEndTime > timeAvailable - RESERVED_END_TIME_MS)
                        {
                            expandRealAnimationViewWidth();
                        }
                        
                        setX(initialX + deltaX);
                        setY(initialY + deltaY);
                    }
                    
                    // When we put SubAnimationBlock as a child of AnimationBlock the
                    // mouse drag event will be sent to both the child and the parent
                    // so we need to consume it here so the event will be consume by 
                    // the SubAnimationBlock
                    t.consume();
                }         
            });
            setOnMouseReleased(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent t)
                {
                    // If the drag happen, try to edit the model if not success revert layoutX
                    if (deltaX != 0)
                    {
                        if (getAnimationModel().getLayerModel().isLock())
                            return;

                        if (mode == AnimationBlockDragMode.EXPAND)
                        {
                            long newDuration = (long) (bg.getWidth() / widthPerSecond.doubleValue() * 1000.0);
                            if (!getAnimationModel().setDuration(newDuration))
                                bg.setWidth(initialWidth);
                        }
                        else
                        {
                            long newStartTime = (long) (getX() / widthPerSecond.doubleValue() * 1000.0);
                            onMove(newStartTime);
                        }
                    }
                    
                    t.consume();
                }             
            });
            
            keyTimeGroup.visibleProperty().bind(selectProperty());
            
            mainLayout.getChildren().addAll(bg, keyTimeGroup);
            getChildren().addAll(mainLayout);
            
            // Since we use our own drag method
            setDraggable(false);
            
            setSupportProperty(ShapeProperty.SELECTION, ShapeProperty.X_POSITION);
        }
        
        public abstract void onMove(long newStartTime);
        
        private void reDrawAllKeyTime()
        {
            keyTimeGroup.getChildren().clear();
        
            for (KeyTime kt : getAnimationModel().getKeyTimeUnmodifiable())
            {
                drawKeyTime(kt);
            }
        }
        
        public abstract void drawKeyTime(KeyTime kt);
        
        public void setWidth(double d)
        {
            bg.setWidth(d);
        }
        
        @Override
        public Paint getFill()
        {
            return bg.getFill();
        }

        @Override
        public void setFill(Paint p)
        {
            bg.setFill(p);
        }
    }

    private class Property extends VBox
    {
        private final Map<LayerModel, PropertyBlock> propertyMap = new HashMap<>();
        
        public Property()
        {
            Button playButton = new Button();
            playButton.setPrefSize(25, 25);
            Image playImage = new Image(getClass().getResourceAsStream("/animator/play.png"));
            playButton.setGraphic(new ImageView(playImage));
            playButton.setOnAction(new EventHandler<ActionEvent>() 
            {
                @Override
                public void handle(ActionEvent event)
                {
                    controller.play();
                }           
            });
            
            Button pauseButton = new Button();
            pauseButton.setPrefSize(25, 25);
            Image pauseImage = new Image(getClass().getResourceAsStream("/animator/pause.png"));
            pauseButton.setGraphic(new ImageView(pauseImage));
            pauseButton.setOnAction(new EventHandler<ActionEvent>() 
            {
                @Override
                public void handle(ActionEvent event)
                {
                    controller.pause();
                }           
            });
            
            Button stopButton = new Button();
            stopButton.setPrefSize(25, 25);
            Image stopImage = new Image(getClass().getResourceAsStream("/animator/stop.png"));
            stopButton.setGraphic(new ImageView(stopImage));
            stopButton.setOnAction(new EventHandler<ActionEvent>() 
            {
                @Override
                public void handle(ActionEvent event)
                {
                    controller.stop();
                }           
            });
            
            Button newLayerButton = new Button();
            newLayerButton.setPrefSize(25, 25);
            Image addLImage = new Image(getClass().getResourceAsStream("/animator/addlayer.png"));
            newLayerButton.setGraphic(new ImageView(addLImage));
            newLayerButton.setOnAction(new EventHandler<ActionEvent>() 
            {
                @Override
                public void handle(ActionEvent event)
                {
                    CommandCenter.INSTANCE.addCommand(new AddLayerTimelineCommand(Timeline.this.model));
                }            
            });
            
            Button removeLayerButton = new Button();
            removeLayerButton.setPrefSize(25, 25);
            Image deleteLImage = new Image(getClass().getResourceAsStream("/animator/deletelayer.png"));
            removeLayerButton.setGraphic(new ImageView(deleteLImage));
            removeLayerButton.setOnAction(new EventHandler<ActionEvent>() 
            {
                @Override
                public void handle(ActionEvent event)
                {
                    if (!selectedLayerModel.get().isEmpty())
                    {
                        CommandCenter.INSTANCE.addCommand(new RemoveLayerTimelineCommand(Timeline.this.model, selectedLayerModel.get()));
                    }
                }
            });
            
            Button zoomIn = new Button();
            zoomIn.setPrefSize(25, 25);
            Image zoomInImage = new Image(getClass().getResourceAsStream("/animator/zoomin.png"));
            zoomIn.setGraphic(new ImageView(zoomInImage));
            zoomIn.setOnAction(new EventHandler<ActionEvent>() 
            {
                @Override
                public void handle(ActionEvent event)
                {
                    zoomIn();
                }
            });
            
            Button zoomOut = new Button();
            zoomOut.setPrefSize(25, 25);
            Image zoomOutImage = new Image(getClass().getResourceAsStream("/animator/zoomout.png"));
            zoomOut.setGraphic(new ImageView(zoomOutImage));
            zoomOut.setOnAction(new EventHandler<ActionEvent>() 
            {
                @Override
                public void handle(ActionEvent event)
                {
                    zoomOut();
                }   
            });
            
            HBox menu = new HBox(2);
            menu.getChildren().addAll(playButton, pauseButton, stopButton, newLayerButton, removeLayerButton, zoomIn, zoomOut/*, group*/);
            menu.setMinHeight(RULER_HEIGHT);
            menu.setPrefHeight(RULER_HEIGHT);
            menu.setMaxHeight(RULER_HEIGHT);
            menu.setStyle("-fx-background-color:linear-gradient(\n" +
"                            to bottom,\n" +
"                            rgb(86,86,86) 0%,\n" +
"                            rgb(79,79,79) 100%\n" +
"                                        );;");

            getChildren().add(menu);
            //setStyle("-fx-background-color:yellow;");
        }
        
        public void addLayer(List<LayerModel> model)
        {
            if (model.size() != 1)
                throw new AssertionError("Shouldn't able to add multiple layer at once now");
            
            PropertyBlock propertyBlock = new PropertyBlock(model.get(0));
            // We need to +1 because index 0 is reserved for the menu
            //getChildren().add(model.get(0).getLayerIndex() + 1, propertyBlock);
            getChildren().add(propertyBlock);
            
            propertyMap.put(model.get(0), propertyBlock);
            animationView.invalidateAllYPosition();
        }
        
        public void removeLayer(List<LayerModel> model)
        {
            if (model.size() != 1)
                throw new AssertionError("Shouldn't able to add multiple layer at once now");
            
            PropertyBlock propertyBlock = propertyMap.get(model.get(0));
            if (propertyBlock != null)
            {
                if (!getChildren().remove(propertyBlock))
                    throw new AssertionError("Property not in view before");
                
                animationView.invalidateAllYPosition();
            }
            else
            {
                throw new AssertionError("Layer not in sync");
            }
        }
        
        private class PropertyBlock extends HBox
        {
            public PropertyBlock(LayerModel model)
            {
                Label nameLabel = new Label();
                nameLabel.textProperty().bind(model.nameProperty());
                
                ToggleButton lockButton = new ToggleButton();
                lockButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/animator/lock.png"))));
                lockButton.setMinHeight(BLOCK_HEIGHT);
                lockButton.setPrefHeight(BLOCK_HEIGHT);
                lockButton.setMaxHeight(BLOCK_HEIGHT);
                lockButton.selectedProperty().bindBidirectional(model.lockProperty());
                
                ToggleButton visibleButton = new ToggleButton();
                visibleButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/animator/visible.png"))));
                visibleButton.setMinHeight(BLOCK_HEIGHT);
                visibleButton.setPrefHeight(BLOCK_HEIGHT);
                visibleButton.setMaxHeight(BLOCK_HEIGHT);
                visibleButton.selectedProperty().bindBidirectional(model.visibleProperty());
                
                HBox buttonWrapper = new HBox();
                buttonWrapper.getChildren().addAll(lockButton,visibleButton);
                //buttonWrapper.setAlignment(Pos.CENTER);
                buttonWrapper.setSpacing(5);
                buttonWrapper.setPadding(new Insets(0, 5, 0, 0));
                
                getChildren().addAll(nameLabel, buttonWrapper);
                
                setSpacing(20);
                setAlignment(Pos.CENTER_RIGHT);
                setStyle("-fx-background-color:linear-gradient(\n" +
"                               to bottom,\n" +
"                               rgb(98,98,98) 0%,\n" +
"                               rgb(62,62,62) 5%,\n" +
"                               rgb(52,52,52) 100%\n" +
"                                        );");
                
                setPrefHeight(BLOCK_HEIGHT);
                setMaxHeight(BLOCK_HEIGHT);
            }
        }
    }
}
